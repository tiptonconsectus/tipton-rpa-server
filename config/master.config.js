let port = process.env.PORT || 3005;
let CALLBACKPORT = process.env.CALLBACKPORT || 443;
let database = process.env.DATABASE || 'rpa_server';
let database_user = process.env.DATABASE_USER || 'consectusapi';
let database_password = process.env.DATABASE_PASSWORD || 'cycle2work';
let database_host = process.env.DATABASE_HOST || '192.168.0.20';
let database_port = process.env.DATABASE_PORT || "3306";

module.exports = {
    'secret': 'xyz-abc-def',
    'app_port': CALLBACKPORT,
    'port': port,
    "defaultnamespace": "cf30291d-76d6-4ec1-8569-d70f5148bad4",
    'expiresIn': '7200000',
    "callbackurl": "192.168.12.10",
    "callbackpath": "/tasks/update-queue-status/",
    "callback_key": "@1fky!3#zkt-wyst@rxzz!!#$%^&*(eytKEY",
    "processInterval": 10000,
    "database": {
        "host": database_host,
        "user": database_user,
        "password": database_password,
        "database": database,
        "port": database_port,
        "create": false
    },
    // "database2": {
    //     "host": "35.177.5.180",
    //     "user": "tiptonsysadmin",
    //     "password": "cycle2workwithtipton",
    //     "database": "tiptonlivedb",
    //     "port": "3306",
    //     "create":true
    // },
    "tables": {
        setting: "rpa_agent_settings",
        rpaAgent: "rpa_agents",
        map: 'agent_action_paths_setting',
        paths: 'action_paths',
        queues: 'action_path_queues',
        bankcustomer: 'bankcustomers',
        customersaving: 'customer_accounts',
        customertranscation: 'customer_account_transactions',
        acc: 'account_types',
        morg: 'mortgage_accounts',
        morgtran: 'mortgage_account_transactions',
        sopra_accno: "sopra_accno",
        sopra_batchno: "sopra_batchno"

    },

    "clients": [],
    "bcrypt": {
    },
    "log_folder": "./logs"
}
