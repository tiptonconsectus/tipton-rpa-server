const config = require('../config/config');


const debug = require("debug")("consectus.migration");



async function initialize(executeQuery) {
    if (config.database.create) {
        try {
            debug("Creating database no function available");
            // await createDB();
        } catch (error) {
            debug("Create database failed .... please check permissions for creating database");
        }
    }

    //check to ensure that all the database tables are correctly created
    //TYPE nodejs or script 
    const createPathsSql = `CREATE TABLE IF NOT EXISTS ${config.tables.paths} (
        id int(11) NOT NULL AUTO_INCREMENT,
        version int(11) DEFAULT 0,
        name varchar(100) NOT NULL,
        type varchar(20) NOT NULL DEFAULT 'nodejs',
        queued TINYINT NOT NULL,
        isdelete int(1) NOT NULL DEFAULT 0,
        order_id  int(2) NOT NULL DEFAULT 99,
        PRIMARY KEY (id),
        created_on datetime DEFAULT CURRENT_TIMESTAMP,
        UNIQUE KEY paths_name_unique (name),
        KEY paths_name_idx (name)       
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;
    const createStepPerformedSql = ` CREATE TABLE IF NOT EXISTS stepperformed (
        id int(11) NOT NULL AUTO_INCREMENT,
        step varchar(255) NOT NULL,
        queueid int(11) NOT NULL,
        expires_on datetime,
        PRIMARY KEY (id),
        UNIQUE KEY step_queueid_unique (queueid),
        KEY step_queueid_idx (queueid)  
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;
    const createAgentSql = `CREATE TABLE IF NOT EXISTS ${config.tables.rpaAgent} (
        id int(11) NOT NULL AUTO_INCREMENT,
        agent_name varchar(100) NOT NULL,
        agent_email varchar(60) NOT NULL,
        host varchar(100) NOT NULL,
        port varchar(100) NOT NULL,
        settimeout varchar(100) NOT NULL,
        score varchar(100) NOT NULL,
        status int(11) DEFAULT 1,
        isdelete int(1) NOT NULL DEFAULT 0,
        PRIMARY KEY (id),
        UNIQUE KEY host_unique (host,port),
        UNIQUE KEY agent_name_unique (agent_name)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;
    const createSettingsSql = `CREATE TABLE IF NOT EXISTS ${config.tables.setting} (
        id int(11) NOT NULL AUTO_INCREMENT,
        name varchar(100) NOT NULL,
        value varchar(100) NOT NULL,
        agent_id int(11) NOT NULL,
        PRIMARY KEY (id),
        UNIQUE KEY settings_name_unique (name),
        CONSTRAINT agent_id_fk FOREIGN KEY (agent_id) REFERENCES ${config.tables.rpaAgent} (id) ON DELETE CASCADE
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;
    const createPathQueuesSql = `CREATE TABLE IF NOT EXISTS ${config.tables.queues} (
        id int(11) NOT NULL AUTO_INCREMENT,
        path_id int(11) NOT NULL, 
        agent_id int(11) DEFAULT NULL, 
        agent_queue_id int(11) DEFAULT NULL,
        params text,
        status int(11) NOT NULL DEFAULT 0,
        result text,
        callback_url text,
        created_on datetime DEFAULT CURRENT_TIMESTAMP,
        updated_on datetime,
        execution_delay  int(11) DEFAULT 0,
        isdelete int(1) NOT NULL DEFAULT 0,
        PRIMARY KEY (id),
        KEY paths_path_idx (path_id),        
        CONSTRAINT queues_paths_id_fk FOREIGN KEY (path_id) REFERENCES ${config.tables.paths} (id) ON DELETE CASCADE
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;


    const createMappingSql = `CREATE TABLE IF NOT EXISTS ${config.tables.map} (
        id int(11) NOT NULL AUTO_INCREMENT,
        rpa_agent_id int(11) NOT NULL,
        action_path_id int(11) NOT NULL,
         priority int(2) NOT NULL, 
        PRIMARY KEY (id),
        KEY oauth2_code_rpa_agent_id_idx (rpa_agent_id),
        KEY oauth2_code_action_path_id_idx (action_path_id),
        CONSTRAINT oauth2_code_rpa_agent_id_fk FOREIGN KEY (rpa_agent_id) REFERENCES ${config.tables.rpaAgent} (id) ON DELETE CASCADE,
        CONSTRAINT oauth2_code_action_path_id_fk FOREIGN KEY (action_path_id) REFERENCES ${config.tables.paths} (id) ON DELETE CASCADE
      ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
    `;
    const createSopra_batchnoSql = `CREATE TABLE IF NOT EXISTS ${config.tables.sopra_batchno} (
        id int(11) NOT NULL AUTO_INCREMENT,
        batchno int(11) NOT NULL,
        is_used int(1) NOT NULL DEFAULT 0,
        next_batch int(3) DEFAULT NULL,     
        created_on datetime DEFAULT CURRENT_TIMESTAMP,
        updated_on datetime,
        PRIMARY KEY (id),
        UNIQUE KEY sopra_batchno_unique (batchno),
        KEY soprabatch_is_used (is_used)       
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;
    const createSopraAccNoSql = `CREATE TABLE IF NOT EXISTS ${config.tables.sopra_accno} (
        id int(11) NOT NULL AUTO_INCREMENT,
        accno int(11) NOT NULL,
        is_used int(1) NOT NULL DEFAULT 0,    
        created_on datetime DEFAULT CURRENT_TIMESTAMP,
        updated_on datetime,
        PRIMARY KEY (id),
        UNIQUE KEY sopra_accno_unique (accno),
        KEY sopra_is_used (is_used)       
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;
    const dropProcedure = `  DROP PROCEDURE IF EXISTS createsopraAcc; DROP PROCEDURE IF EXISTS createsoprabatchAcc; `
    const createStoreProcedureForaccno = " CREATE PROCEDURE  createsopraAcc (in startingaccno INT,endingaccno INT) \n " +
        " BEGIN   \n" + " DECLARE i INT DEFAULT 0; \n" +
        " IF(endingaccno <=100000) THEN \n" + " WHILE i < endingaccno DO \n " +
        " IF EXISTS (SELECT * FROM sopra_accno WHERE accno=startingaccno+i)  THEN \n" + " SELECT 'few account already exists'; \n " +
        " ELSE \n " + " insert into sopra_accno (accno) values(startingaccno+i); \n" + " SELECT 'Sopra account number created successfully'; \n" +
        " END IF;  \n" + " SET i = i + 1; \n" + " END WHILE; \n" +
        " ELSE   SELECT 'end sequence should not be greater than 100000'; \n " + "END IF;\n " + "END  ";

    const createStoreProcedureForbatch = " CREATE PROCEDURE  createsoprabatchAcc (in startingaccno INT,endingaccno INT) \n " +
        " BEGIN   \n" + " DECLARE i INT DEFAULT 0; \n" +
        " IF(endingaccno <=100000) THEN \n" + " WHILE i < endingaccno DO \n " +
        " IF EXISTS (SELECT * FROM sopra_batchno WHERE batchno=startingaccno+i)  THEN \n" + " SELECT 'few account already exists'; \n " +
        " ELSE \n " + " insert into sopra_batchno (batchno) values(startingaccno+i); \n" + " SELECT 'Sopra account number created successfully'; \n" +
        " END IF;  \n" + " SET i = i + 1; \n" + " END WHILE; \n" +
        " ELSE   SELECT 'end sequence should not be greater than 100000'; \n " + "END IF;\n " + "END  ";

    try {
        await executeQuery(createPathsSql);
        await executeQuery(createAgentSql);
        await executeQuery(createSettingsSql);
        await executeQuery(createPathQueuesSql);

        await executeQuery(createMappingSql);
        await executeQuery(createSopraAccNoSql);
        await executeQuery(createSopra_batchnoSql);

        await executeQuery(dropProcedure);
        await executeQuery(createStoreProcedureForaccno);
        await executeQuery(createStoreProcedureForbatch);
        await executeQuery(createStepPerformedSql);
        debug("RPA Server database verified");
    } catch (error) {
        debug(error);
        debug("RPA Server database verified failed !!!!!!!!!!!!!");

        throw error;
    }

}
module.exports.initialize = initialize;
