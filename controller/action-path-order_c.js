//const OauthToken=require('../services/oauth-rpa/oauthRPA')
class ActionPathOrder{
	constructor(){
		 const rpamodel = require("../models/rpaagent_m");
         this.Rpa_m = new rpamodel();
		 const action_model = require("../models/action_path_m");
         this.action_m = new action_model();
         const httprequest =require('../services/oauth-rpa/httprequest');
		 this.http=new httprequest();
		 const createzip_c = require('./createzip');
		 this.zip = new createzip_c();
	}
	GetActionPathOrder(req,res){
		let header = req.app.jsonrequest(req);

		 Promise.all([this.getallagent(), this.getallpath()]).then(function(result) {
			if ( header) {
				res.status(200).json({
					status: 200,             
					agent: result[0],
					path:result[1]
				}).end();
			}else{
				res.render('dashboard/action-path/order',{agent:result[0],path:result[1]});
			}
		});
	}
	ActionOrderId(req,res){
		if(req.params.id==="select all agent"){
			Promise.all([this.getallpath()]).then(function(result) {
				//console.log(result[0])
				res.status(200).json({
                    status: "OK",
                    message: "executed on agent",
                    data: result[0]
                }).end();
		   });
		}else{
			Promise.all([this.getallpathorder(req.params.id)]).then(function(result) {
						
						res.status(200).json({
							status: "OK",
							message: "executed on agent",
							data: result[0]
						}).end();
		   });
		}
	}
	ActionOrder(req,res){

		let header = req.app.jsonrequest(req);

		//console.log(req.body)
		 if(req.body.idname.length===req.body.value.length){
			 if(req.body.agent==="select all agent"){
				let data={
					status:1
				}
				 this.Rpa_m.find(data).then((finded_path)=>{
					 // resolve(finded_path);
					  if(finded_path.length>0){
							this.UpdateOrderAgent(req,finded_path,req.body.idname,req.body.value).then((path)=>{
								//  resolve(finded_path);
								this.action_m.orderupdate(req.body.idname,req.body.value).then((updated)=>{

									if ( header) {
											res.status(200).json({
												status: 200,             
												data :"successfully updated"
											}).end();
									}else{
								   	res.cookie('message',"successfully updated", {maxAge:9200}).redirect('/admin/manage/order');
									}
								}).catch((update_err)=>{
									if ( header) {
													res.status(401).json({
														status: "error",
														error: "errormessage",
														error_description: update_err
													}).end();
											}else{
													 res.cookie('errormessage',update_err, {maxAge:9200}).redirect('/admin/manage/order');
											}
								})
							}).catch((err)=>{
								if ( header) {
											res.status(401).json({
												status: "error",
												error: "errormessage",
												error_description: err
											}).end();
									}else{
										res.cookie('errormessage',err, {maxAge:9200}).redirect('/admin/manage/order');
									}
							})
					  }else{
							if ( header) {
									res.status(401).json({
										status: "error",
										error: "finded_err"						
									}).end();
					    	}else{
									res.cookie('errormessage',"finded_err", {maxAge:9200}).redirect('/admin/manage/order');
							 }
					  }
				    }).catch((finded_err)=>{
							if ( header) {
									res.status(401).json({
										status: "error",
										error: "errormessage",
										error_description: finded_err
									}).end();
					   	}else{
						    	res.cookie('errormessage',finded_err, {maxAge:9200}).redirect('/admin/manage/order');
							}
				    })

			}else {
				let data={
					id:req.body.agent,
					status:1
				}
				 this.Rpa_m.find(data).then((finded_path)=>{
					//  resolve(finded_path);
					    if(finded_path.length>0){
							
								this.UpdateOrderAgent(req,finded_path,req.body.idname,req.body.value).then((path)=>{
									this.priorityagent(finded_path,req.body.idname,req.body.value).then(()=>{
												if ( header) {
													res.status(200).json({
														status: 200,             
														data :"successfully updated"
													}).end();
												}else{
														res.cookie('message',"successfully updated", {maxAge:9200}).redirect('/admin/manage/order');
												}
										}).catch((priority_err)=>{
												if ( header) {
														res.status(401).json({
															status: "error",
															error: "errormessage",
															error_description: priority_err
														}).end();
									  		}else{
										    	res.cookie('errormessage',"something went wrong", {maxAge:9200}).redirect('/admin/manage/order');
											}
										})
								}).catch((err)=>{
								 	if ( header) {
												res.status(401).json({
													status: "error",
													error: "errormessage",
													error_description: err
												}).end();
							   	}else{
										 res.cookie('errormessage',err, {maxAge:9200}).redirect('/admin/manage/order');
									 }
								})
								
						}else{
							if ( header) {
										res.status(401).json({
											status: "error",
											error: "errormessage",
											error_description: "agent is not active"
										}).end();
					  	}else{
								res.cookie('errormessage',"agent is not active", {maxAge:9200}).redirect('/admin/manage/order');
							}
						}
					}).catch((finded_path_err)=>{
					 	if ( header) {
									res.status(401).json({
										status: "error",
										error: "errormessage",
										error_description: finded_path_err
									}).end();
	    		  	}else{
								 res.cookie('errormessage',"something went wrong", {maxAge:9200}).redirect('/admin/manage/order');
							}
					})

			}
		 }else{
			if ( header) {
					res.status(401).json({
						status: "error",
						error: "errormessage",
						error_description: "something went wrong"
					}).end();
   	  	}else{
			     	res.cookie('errormessage',"something went wrong", {maxAge:9200}).redirect('/admin/manage/order');
			 }
		 }

	}
	async priorityagent(data,name,value){
		let result;
		for(let i=0;i<data.length;i++){
			for(let j=0;j<name.length;j++){
			  try{
				let action_path_id=await this.action_m.getActionPathByName(name[j])
				result =await this.action_m.priority_setting(data[i].id,action_path_id[0].id,value[j])
				
			  }catch(err){
				throw err;
			  }
			}	
		}	
		return result;
	}
	async UpdateOrderAgent(req,data,name,value){
		let result;

		if(data.length>0){
			for (let i = 0; i < data.length; i++) {
				try{
				result = await this.SendOverHttp(req, data[i],name,value)
			   }catch(err){
				
			   	  if (err.code === 'ECONNREFUSED' || err.code === 'ENOTFOUND' || err.code ==='ECONNRESET' || err.code==='ETIMEDOUT' || err.code === 'EHOSTUNREACH') {
									// return cb(err.code);
							
									let save = {
										update_id: data[i].id,
										status: 0
									   }
								//   console.log(save)
								//   try{
								  this.Rpa_m.updateAgent(save, true);

								 
                             //  throw err.code;
					} else if(err.error_description!==undefined && err.error_description==="action path not found. please upload latest folder"){
                        throw "first upload latest file";
                    } 
					 else {
				
							let err_res = JSON.parse(err);
							throw err_res;
								
					}
			   }
			}

		}else{
			throw "no agent found"
		}
		return  result;
	}
	SendOverHttp(req, data,name,value){
	
		  return   new Promise((resolve, reject) => {
        
            let oauth = req.app.getOauthToken(data.host,data.port);
       
             oauth.getAccessToken(false).then(() => {
               //  console.log(data)
                const postData = querystring.stringify({                        
                                name:JSON.stringify(name),
                                value:JSON.stringify(value)
                            });
                 const options = {
                     hostname:  data.host,
                     port: data.port,               
                     path: '/manage/update/order',
                     method: 'POST',
                     headers: {
                         'Content-Type': 'application/x-www-form-urlencoded',
                         'Authorization': 'Bearer ' + oauth.token,
                         'Content-Length': Buffer.byteLength(postData)

                     }
                 };
                 this.http.HttpPost(options,postData).then((data)=>{
                     //console.log(data)
                     resolve(data)
                 }).catch((err)=>{
                   // console.log(err)
                     reject(err)
                 })
               //  console.log(req)
             }).catch((err)=>{
                // console.log(err)
               reject(err);
             })

        });

	}
	getallagent(){
		 return new Promise((resolve, reject) => {
		    this.Rpa_m.find().then((finded_agent)=>{
		    	resolve(finded_agent);
		    }).catch((finded_err)=>{
		    	reject(finded_err)
		    })
		 });
	}
    getallpath(){
		 return new Promise((resolve, reject) => {
		    this.action_m.find().then((finded_path)=>{
		    	resolve(finded_path);
		    }).catch((finded_err)=>{
		    	reject(finded_err)
		    })
		 });
	}
	getallpathorder(id){
		return new Promise((resolve, reject) => {
		   this.action_m.actionpath_priority(id).then((finded_path)=>{
			   resolve(finded_path);
		   }).catch((finded_err)=>{
			   reject(finded_err)
		   })
		});
   }
}
module.exports = ActionPathOrder