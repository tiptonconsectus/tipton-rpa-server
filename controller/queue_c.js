class Queue {
    constructor() {
      const queue_m= require('../models/queue_m');
      this.queue=new queue_m();
      const action_model=require("../models/action_path_m");
      this.action_m=new action_model();
    }
    ActionPathQueue(req,res){

        this.action_m.getActionPathByName(req.body.agentname).then((result)=>{

            this.queue.addQueue(result[0].id,req.body.status,req.body.params).then((queue)=>{
                res.status(200).json({
                    status: "OK",
                    message: "added in queue",
                    data: "added in queue",
                }).end();

            }).catch((err)=>{
            throw err;
        })
            

        }).catch((err)=>{
            throw err;
        })
    }

}
module.exports = Queue;