const fs = require("fs");
const path = require("path");

class CreateZip {
    constructor() {
        const Upload_c = require('./uploadfile');
        this.uploadzip = new Upload_c();
        // const task_c = require('./task_c');
        // this.task= new task_c();
    }
    uploadLatest(req, result, action_path, queueid) {
        //  console.log("uploalasted function")

        return new Promise((resolve, reject) => {
            let folder = req.body.name;
            let self = this;
            if (folder.indexOf('/') !== 0) folder = path.join(__dirname, "../action-paths/", folder);
            try {
                if (fs.lstatSync(folder).isDirectory()) {
                    let zipFolder = require('zip-folder');
                    debug("Creating a zip folder");
                    let foldername = path.basename(folder);
                    debug(foldername);
                    let zipfilename = path.join(__dirname, "../action-paths/", foldername) + ".zip";

                    zipFolder(folder, zipfilename, function (err) {
                        if (err) {
                            debug('oh no!', err);
                            reject(err)
                            // process.exit(-1);
                        }
                        debug("now uploading the file ...");
                        req.file = {};
                        req.body.name = action_path.name
                        req.body.type = action_path.type
                        req.body.version = action_path.version
                        req.body.queued = (action_path.queued === 1) ? 'true' : 'false'
                        req.file.path = zipfilename

                        self.uploadzip.AgentUploadActionPath(req, result).then((upload) => {
                            resolve(upload)
                            // self.task.AssignTaskAgent(req, result,action_path, queueid).then((updated)=>{
                            //     resolve(updated);
                            // }).catch((failed)=>{
                            //     reject(failed);
                            // })
                        }).catch((upload_fail) => {
                         
                            reject(upload_fail);
                        })


                    });
                } else {
                    reject(`${folder} is not a directory`);

                }

            } catch (error) {
                reject(`Invalid directory provided`);
                debug(`Invalid directory provided: 
            ${error.message} 

            `
                );
            }
        });
    }
}
module.exports = CreateZip;