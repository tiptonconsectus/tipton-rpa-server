//const OauthToken = require('../services/oauth-rpa/oauthRPA');
const querystring = require("querystring");

const debug = require("debug")("consectus.task:");
const debugerror = require("debug")("consectus.error:");
const config = require('../config/config');
const fs = require('fs');

class TaskController {
    constructor() {
        // const Upload_c =  require('./uploadfile');       
        const Queued_m = require('../models/queue_m');
        const rpamodel = require("../models/rpaagent_m");
        const action_model = require("../models/action_path_m");
        const httprequest = require('../services/oauth-rpa/httprequest');
        const SopraNo = require("../models/sopra_accno_m");
        // this.uploadzip = new Upload_c();
        const createzip = require('./createzip');
        this.sopraNo = new SopraNo();
        this.zip = new createzip();
        this.queued_m = new Queued_m();
        this.Rpa_m = new rpamodel();
        this.action_m = new action_model();
        this.http = new httprequest();
    }

    ExecuteActionPath(req, res) {
        let callback = (req.body.callback !== undefined && req.body.callback !== '' && req.body.callback !== null) ? JSON.parse(req.body.callback) : null
        // console.log(JSON.stringify({"host":config.callbackurl,"port":config.app_port,path:"/tasks/update-queue-status/"}))
        // let test =JSON.stringify({"host":config.callbackurl,"port":config.app_port,path:"/tasks/update-queue-status/"})
        // console.log(JSON.parse(test))
        //const url_regx = /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}/
        if (req.body.name === undefined) {
            //name of folder which is stored in db
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "name not found"
            }).end();
        } else if (req.body.name.trim().length === 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "name cannot be blank"
            }).end();
        } else if (req.body.params === undefined) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "params not found"
            }).end();
        } else if (req.body.params.trim().length === 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "params cannot be blank. use {} as empty json object"
            }).end();

        } else if (callback === null) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "callback url is not provided "
            }).end();

        } else if (callback.host === undefined || callback.port === undefined || callback.path === undefined) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: 'callback url is not provided useage eg:- {"host":"127.1.1.5","port":"8888","path":"callbackpath"}'
            }).end();
        } else {
            this.AddQueueServer(req, res).then((data) => {
                debug("Queued: ")
                res.status(200).json({
                    status: "OK",
                    message: "executed on agent",
                    data: data
                }).end();
            }).catch((err) => {
                res.status(500).json({
                    status: "error",
                    error: "Server Error",
                    data: err
                }).end();
            });
        }
    }
    async AddQueueServer(req, res, callback) {
        let upload_Data;

        let serverupload_err;
        let action_path;
        let queue_status;
        let queue_id;
        let result;
        let status = {
            status: 1
        }
        let params;
        let Sopra_accno;
        let sopra_acc = null;

        try {
            params = JSON.parse(req.body.params);
        } catch (error) {
            debug(error)
            throw 'invalid object pass use eg:- {"echo":"test"}';
        }
        try {
            if (req.body.isacc || (params && params.isacc)) {
                Sopra_accno = await this.sopraNo.GetAccNO(0);
                params.account_number = "" + Sopra_accno[0].accno + "";
                sopra_acc = params.account_number;
                await this.sopraNo.UpdateAccNo(Sopra_accno[0].accno, 1);
            }

        } catch (err) {
            debugerror(err)
            throw 'no account number';
        }
        try {
            if (req.body.istransfer || (params && params.istransfer)) {
                Sopra_accno = await this.sopraNo.GetBatchNO(0);
                params.batchno = Sopra_accno;

                await this.sopraNo.UpdateBatchAccNo(Sopra_accno, 1);
            }

        } catch (err) {
            console.log("errin trans0 in send", err)
            debugerror(err)
            throw 'no batch number';
        }
        try {
            const soprapass = path.join(__dirname, "../config/soprapassword.json");
            if (fs.existsSync(soprapass)) {
                let password_sopra = require("../config/soprapassword.json").password;
                params.sopra_password = password_sopra;
            }
        } catch (err) {

        }
        try {
            req.body.params = JSON.stringify(params);
            action_path = await this.action_m.getActionPathByName(req.body.name);
            if (action_path.length > 0) {
                queue_status = (action_path[0].queued === 0) ? 1 : 0
                queue_id = await this.queued_m.addQueue(action_path[0].id, queue_status, params, req.body.callback);
                result = await this.Rpa_m.Find_Best_ByAgent();
                if (result.length > 0) {
                    for (let i = 0; i < result.length; i++) {
                        // console.log("i ",i)
                        let allow_agent = await this.action_m.priority_setting_find(result[i].id, action_path[0].id, 0);
                        if (allow_agent.length === 0) {
                            try {
                                upload_Data = await this.AssignTaskAgent(req, result[i], action_path[0], queue_id.insertId);
                                if (req.body.isacc || (params && params.isacc)) {
                                    await this.sopraNo.UpdateAccNo(Sopra_accno[0].accno, 2);
                                }
                                break;
                            } catch (upload_err) {
                                await this.queued_m.updateQueue(queue_id.insertId, null, null, 0, null);
                                debugerror(upload_err)
                                if (upload_err.code === 'ECONNREFUSED' || upload_err.code === 'ENOTFOUND' || upload_err.code === 'ECONNRESET' || upload_err.code === 'ETIMEDOUT' || upload_err.code === 'EHOSTUNREACH') {
                                    let data = {
                                        update_id: result[i].id,
                                        status: 0
                                    }
                                    await this.Rpa_m.updateAgent(data, true)
                                }

                                else {
                                    if (i === result.length - 1) {
                                        debugerror("error in upload", result[i].id);

                                        return { data: upload_err, serverQueue: queue_id.insertId, account_number: sopra_acc }

                                    }
                                }
                                continue;
                            }
                        }
                    }
                    // }
                } else {
                    await this.queued_m.updateQueue(queue_id.insertId, null, null, 0, null)
                    debugerror("no best agent found");
                    return { data: "no best agent found", serverQueue: queue_id.insertId, account_number: sopra_acc }
                }

            } else {
                throw "action-paths not found"

            }
        } catch (error) {
            throw error;
        }
        return { data: upload_Data, serverQueue: (queue_id.insertId) ? queue_id.insertId : "-1", account_number: sopra_acc };


    }
    AssignTaskAgent(req, result, action_path, queueid) {

        return new Promise((resolve, reject) => {

            let oauth = req.app.getOauthToken(result.host, result.port);
            oauth.getAccessToken(false).then(() => {


                const postData = querystring.stringify({
                    name: req.body.name,
                    params: req.body.params,
                    Serverqueueid: queueid,
                    version: action_path.version,
                    callback: JSON.stringify({ "host": config.callbackurl, "port": config.app_port, "path": config.callbackpath + result.id + "/" + queueid })
                });
                const options = {
                    hostname: result.host,
                    port: result.port,
                    path: '/tasks/execute-action-path',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': 'Bearer ' + oauth.token,
                        'Content-Length': Buffer.byteLength(postData)

                    }
                };

                this.http.HttpPost(options, postData).then((data) => {
                    // console.log(data)
                    this.queued_m.updateQueue(queueid, result.id, data.queueid, data.status_type, JSON.stringify(data.data)).then((queue_updated) => {
                        resolve({ data: data, serverQueue: queueid, updateQueue: queue_updated })
                    }).catch((queue_update_fail) => {
                        reject(queue_update_fail)
                    })

                }).catch((err) => {
                    // console.log(err)

                    reject(err)
                })

            }).catch((autherr) => {
                // console.log(autherr)

                reject(autherr)
            })
        });

    }
    createZip(req, result, action_path, queueid) {
        return new Promise((resolve, reject) => {
            this.zip.uploadLatest(req, result, action_path, queueid).then((success) => {
                this.AssignTaskAgent(req, result, action_path, queueid).then((updated) => {
                    resolve(updated);
                }).catch((failed) => {
                    reject(failed);
                });
            }).catch((error) => {
                reject(error);
            });
        });

    }


    UpdateQueueStatus(req, res) {
        //  console.log(req.body)
        //  res.status(200).json("sddsdsds")
        //  { agentid: '1', queueid: '2' }
        //   console.log(req.params)

        this.queued_m.findByQueueId(req.params.queueid).then((success) => {
            //update disabled agent 
            let data_save = {
                update_id: success[0].agent_id,
                status: 1
            }
            this.Rpa_m.updateAgent(data_save, true);

            debug(success)
            let callback;
            try {

                callback = JSON.parse(success[0].callback_url);
                this.queued_m.updateQueue(req.params.queueid, req.params.agentid, req.body.queueid, req.body.status, req.body.response);
                try {
                    let trans = JSON.parse(req.body.response);
                    if (trans.data.istransfer && typeof trans.data.batchno !== "string") {
                        this.sopraNo.UpdateBatchAccNo(trans.data.batchno, 0);
                    }
                } catch (err) {
                    console.log("errin trans0 in update in questatus controller", err)
                }
            } catch (error) {
                debugerror("error in json parse", error);
            }

            let oauth = req.app.getOauthToken(callback.host, callback.port);
            oauth.getAccessToken(false).then(() => {
                const postData = querystring.stringify({
                    result: req.body.response
                });
                const options = {
                    hostname: callback.host,
                    port: callback.port,
                    path: callback.path,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': 'Bearer ' + oauth.token,
                        'Content-Length': Buffer.byteLength(postData)

                    }
                };
                this.http.HttpPost(options, postData).then((data) => {

                    let ret = {
                        status: "OK",
                        data: "success fully queue update at rpa-server"
                    }
                    res.status(200).json(ret).end();
                }).catch((error) => {

                    if (req.app.attempt === 5) {
                        req.app.attempt = 1;
                        debug("error in 5 attempt")
                        res.status(400).json({
                            status: "error",
                            error: "invalid_request",
                            error_description: "something went wrong in server queue update"
                        }).end();
                    } else {
                        debug(req.app.attempt)
                        req.app.attempt += 1;
                        this.UpdateQueueStatus(req, res)
                    }

                })
            }).catch((error) => {
                if (req.app.attempt === 5) {
                    req.app.attempt = 1;
                    debug("error in 5 attempt")
                    res.status(400).json({
                        status: "error",
                        error: "invalid_request",
                        error_description: "something went wrong in server oauth token"
                    }).end();
                } else {
                    debug(req.app.attempt)
                    req.app.attempt += 1;
                    this.UpdateQueueStatus(req, res)
                }
            })
        }).catch((error) => {
            if (req.app.attempt === 5) {
                req.app.attempt = 1;
                debug("error in 5 attempt")
                res.status(400).json({
                    status: "error",
                    error: "invalid_request",
                    error_description: "something went wrong in server queue find by id "
                }).end();
            } else {
                debug(req.app.attempt)
                req.app.attempt += 1;
                this.UpdateQueueStatus(req, res)
            }
        })
    }
    GetActionPathQueue(req, res) {
        if (req.body.name === undefined) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "name not found"
            }).end();
        } else if (req.body.name.trim().length === 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "name cannot be blank"
            }).end();
        } else {
            const name = req.body.name;
            this.action_m.getActionPathByName(name).then((results) => {
                // debug(results);
                if (results.length === 0) {
                    res.status(500).json({
                        status: "error",
                        error: "not_found",
                        error_description: "action path not found"
                    }).end();
                } else {
                    // debug(results[0]);
                    this.queued_m.getQueue(results[0]).then((results) => {
                        let ret = {
                            status: "OK",
                            data: results
                        }
                        res.status(200).json(ret).end();
                    }).catch((error) => {
                        // debug(error);
                        res.status(500).json({
                            status: "error",
                            error: error,
                            error_description: "System Error occurrred"
                        }).end();
                    })

                }
            }).catch((error) => {
                // debug(error);
                res.status(500).json({
                    status: "error",
                    error: error,
                    error_description: "System Error occurrred"
                }).end();
            });
        }

    }
    GetQueueStatus(req, res) {
        if (req.body.queue_id === undefined) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "queue_id not found"
            }).end();
        } else if (req.body.queue_id.trim().length === 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "queue_id cannot be blank"
            }).end();
        } else if (req.body.name === undefined) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "name not found"
            }).end();
        } else if (req.body.name.trim().length === 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "name cannot be blank"
            }).end();
        } else {
            const name = req.body.name;
            const queue_id = req.body.queue_id;
            this.queued_m.getActionPathQueueById(name, queue_id).then((results) => {
                debug(results);
                if (results.length === 0) {
                    res.status(500).json({
                        status: "error",
                        error: "not_found",
                        error_description: `queue_id '${queue_id}' not found for action path '${name}'`
                    }).end();
                } else {
                    let ret = {
                        status: "OK",
                        queue: results[0]
                    }
                    res.status(200).json(ret).end();
                }
            }).catch((error) => {
                debug(error);
                res.status(500).json({
                    status: "error",
                    error: error,
                    error_description: "System Error occurrred"
                }).end();
            });
        }

    }
    GetPendingQueue(req, res) {
        const name = req.body.name;
        this.queued_m.getPendingQueue().then((results) => {
            // debug(results);
            let ret = {
                status: "OK",
                data: results
            }
            res.status(200).json(ret).end();
        }).catch((error) => {
            // debug(error);
            res.status(500).json({
                status: "error",
                error: error,
                error_description: "System Error occurrred"
            }).end();
        });

    }


}
module.exports = TaskController;