const fs = require("fs-extra");
const path = require("path");
const unzip = require("unzip-stream");
const actionPath = path.join(__dirname, "../action-paths");
const querystring = require("querystring");

class ActionPath {
    constructor() {
        const Queued_m = require('../models/queue_m');
        this.queued_m = new Queued_m();
        const rpamodel = require("../models/rpaagent_m");
        this.Rpa_m = new rpamodel();
        const action_model = require("../models/action_path_m");
        this.action_m = new action_model();
        const Upload_c = require('./uploadfile');
        this.uploadzip = new Upload_c();
        const httprequest =require('../services/oauth-rpa/httprequest');
        this.http=new httprequest();
    }
    GetActionPath(req, res) {
        this.action_m.find().then((result) => {
            let header = req.app.jsonrequest(req)

            if ( header) {
				res.status(200).json({
					status: 200,             
					path:result
				}).end();
            }else{
                res.render('dashboard/action-path/actionpath', { path: result });
            }

        }).catch((err) => {

        })

    }
 
    UploadActionPath(req, res) {
      //  console.log(req.body)
        if (req.body.name === undefined) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "name not found"
            }).end();

        }
        else if (req.body.name.trim().length === 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "name cannot be blank"
            }).end();
        }
        else if (req.body.type === undefined) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "type not found"
            }).end();
        } else if (req.body.type.trim().length === 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "type cannot be blank"
            }).end();
        } else if (['nodejs', 'script'].indexOf(req.body.type.trim()) < 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "wrong type - use 'nodejs' or 'script'"
            }).end();
        } else if (req.body.queued === undefined) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "queued not found"
            }).end();
        } else if (req.body.queued.trim().length === 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "queued not found"
            }).end();
        } else if (['true', 'false'].indexOf(req.body.queued.trim()) < 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "wrong type - use 'true' or 'false'"
            }).end();
        } else {
            try {
                this.updateServerPath(req).then((result)=>{
                    res.status(200).json({
                        status: "OK",
                        message: (result.insertId !==undefined && result.insertId > 0) ? "action path created" : "action path updated",
                        data: result
                    }).end();
                }).catch((err)=>{
                      res.status(500).json({
                        status: "error",
                        error: (err.code!==undefined)?err.code:err,
                        error_description: (err.code!==undefined)?err.code:err,
                    }).end();
                })

              
            } catch (error) {
                res.status(500).json({
                    status: "error",
                    error: error,
                    error_description: "Error updating the action path",
                    //  name: name
                }).end();
            }

        }
    }
    async  updateServerPath(req) {
        let save = 0;
        let result;
        // let filename= await  this.UploadFile(req)
        let status = {
            status: 1
        }
        try {
             let version= await this.action_m.getActionPathByName(req.body.name,null,true);
            // console.log(version);
             if(version.length>0){
                 req.body.version=version[0].version+1;
             }
             else{
                req.body.version=null
             }
             result = await this.Rpa_m.find(status);
        } catch (err_find) {
          // console.log(err_find)
            throw "error in try catch in updateserver"
        
        }
           
            if (result.length > 0) {
                for (let i = 0; i < result.length; i++) {
                    try{
                        let res = await this.uploadzip.AgentUploadActionPath(req, result[i]);
                        
                            if (res.length > 0) {
                                save = 1
                            }
                    }catch(err){
                        if (err.code === 'ECONNREFUSED' || err.code === 'ENOTFOUND' || err.code==='ECONNRESET' || err.code==='ETIMEDOUT' || err.code === 'EHOSTUNREACH') {
                                    // return cb(err.code);
                                    let data_save = {
                                        update_id: result[i].id,
                                        status: 0
                                       }
                                await  this.Rpa_m.updateAgent(data_save, true);
                                  if(result.length===1){
                                    throw err;
                                  }
                              // throw err.code;
                        } else {
                                let err_res = JSON.parse(err);
                                throw err_res;
                                
                                  
                        }
                        continue;
                    }
             
                }

            } else {
                save = 0;
                throw "no agent found to upload"
            }
            if (save === 1) {
                // console.log("save inside")
                try{
                   let results= await  this.UploadFile(req);
                   return results
                }catch(error){
                 //   console.log(error)
                    throw error
                }
            
            }
       


    }
   
    UploadFile(req) {
        return new Promise((resolve, reject) => {


            const name = req.body.name;
            const type = req.body.type;
            const queued = req.body.queued;


            const foldername = req.file.path.substr(0, req.file.path.lastIndexOf("."));
            // debug("Upload action name: ", name);
            // debug("Zip file: ", req.file.path);
            // debug("folder name", foldername);
            try {
               
                //    debug(req.file.path, "unzip successful")
            } catch (error) {
                //    debuggerror(req.file.path, "unzip failed")
            }
            // debug("moving");
            // debug("source: ", foldername)
            let target = path.join(actionPath, path.basename(name));
           // console.log(target)
            // debug("target: ", target);
            try {
                if (fs.existsSync(target)) fs.removeSync(target);
                // fs.moveSync(foldername, path.join(actionPath, path.basename(name)));
                fs.createReadStream(req.file.path).pipe(unzip.Extract({ path: target }));
                //  debug(path.join(actionPath, path.basename(name)), 'folder deleted');
           
            let data = {
                name: name,
                type: type,

                queued: queued === 'true' ? 1 : 0
            }
            this.updateActionPath(data).then((results) => {

                resolve(results)
               
            }).catch((error) => {
                reject(error)
            })
        } catch (error) {
            throw "upload failed in server please try again"
            //  debuggerror(path.join(actionPath, path.basename(name)), "folder does not exist")
        }

        })
    }
    updateActionPath(path) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.action_m.updateActionPath(path).then((results) => {
                resolve(results);
            }).catch((error) => {
               // console.log(error)
                reject(error);
            })
        })
    }
    DeleteActionPath(req,res){
         this.Rpa_m.find().then((findrpa)=>{
             if(findrpa.length>0){
              //  this.action_m.find(req.params.id).then((find_result)=>{
                   this.DeleteMultiAgentPath(findrpa,req.params.name, req).then((result)=>{
                     if (fs.existsSync(actionPath+"/"+req.params.name)) fs.removeSync(actionPath+"/"+req.params.name);
                     this.action_m.deletePath(req.params.id).then((deleted)=>{
                         res.status(200).json({
                            status: "OK",
                            message: "action path deleted",
                            data: result
                        }).end();
                     }).catch((err_delete)=>{
                          res.status(400).json({
                            status: "error",
                            error: err,
                            error_description: err_delete,
                        }).end();
                     })
                     

                   }).catch((err)=>{
                        res.status(400).json({
                            status: "error",
                            error: err,
                            error_description: err,
                        }).end();

                   })
                // }).catch((find_err)=>{
                //      res.status(400).json({
                //         status: "error",
                //         error: "invalid error",
                //         error_description: find_err,
                //     }).end();
                // })
             }else{
              //  console.log(req.params.id)
                 if (fs.existsSync(actionPath+"/"+req.params.name)) fs.removeSync(actionPath+"/"+req.params.name);
                  this.action_m.deletePath(req.params.id).then((deleted)=>{
                         res.status(200).json({
                            status: "OK",
                            message: "no agent found",
                            data: "successfully deleted from server"
                        }).end();
                     }).catch((err_delete)=>{
                      //  console.log(err_delete)
                          res.status(400).json({
                            status: "error",
                            error: err,
                            error_description: err_delete,
                        }).end();
                     })
                 

            }

        }).catch((rpafind_err)=>{
           // console.log(rpafind_err)
            res.status(400).json({
                        status: "error",
                        error: rpafind_err,
                        error_description: rpafind_err,
                    }).end();
        })
             

    }
  async  DeleteMultiAgentPath(findrpa,name, req){

        let success
       // let err;
            for (let i = 0; i < findrpa.length; i++) {
               try{
                  success= await  this.DeleteAgentPath(findrpa[0],name, req)
               }catch(err){
                     return "true";
                     // if (err.code !== 'ECONNREFUSED' || err.code !== 'ENOTFOUND' || upload_err.code !=='ECONNRESET') {
                     //    throw err
                     // }
               }    
            }
            return success
    }
    DeleteAgentPath(result,name, req){
       return   new Promise((resolve, reject) => {
        
            let oauth = req.app.getOauthToken(result.host,result.port);
       
             oauth.getAccessToken(false).then(() => {
               //  console.log(data)
                const postData = querystring.stringify({
                                name:name  
                            });
                 const options = {
                     hostname:  result.host,
                     port: result.port,               
                     path: '/tasks/delete-action-path',
                     method: 'POST',
                     headers: {
                         'Content-Type': 'application/x-www-form-urlencoded',
                         'Authorization': 'Bearer ' + oauth.token,
                         'Content-Length': Buffer.byteLength(postData)

                     }
                 };
                 this.http.HttpPost(options,postData).then((data)=>{
                    // console.log(data)
                     resolve(data)
                 }).catch((err)=>{
                  
                     reject(err)
                 })
               //  console.log(req)
             }).catch((err)=>{
              //   console.log(err)
               reject(err);
             })

        });
   }
}
module.exports = ActionPath;