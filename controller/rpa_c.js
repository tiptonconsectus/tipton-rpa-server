const querystring = require("querystring");
class RPAManagements {
    constructor(management=null) {
        this.config =  require("../config/config");       
        this.manage=management;
        const rpamodel=require("../models/rpaagent_m")
        this.Rpa_m=new rpamodel(); 
        const Queued_m = require('../models/queue_m');
        this.queued_m = new Queued_m();
        const httprequest =require('../services/oauth-rpa/httprequest');
        this.http=new httprequest();
         
        
    }
    Getrpa(req,res){
        let header = req.app.jsonrequest(req)
        try{
            this.Rpa_m.find().then((results)=>{
                if (  header) {
                    res.status(200).json({
                        status: 200,             
                        agent: results
                    }).end();
                }
                else{
                    res.render('dashboard/RPA/rpa',{agent:results});
                }
               
            }).catch((err)=>{
            })
        }catch(err){
        }
       
    }
    GetAgentDetail(req,res){
        try{
                this.Rpa_m.findAgent(req.params.id).then((results)=>{
                    res.status(200).json({
                        status: 200,             
                        data: results
                    }).end();

                }).catch((err)=>{
                    res.status(401).json({
                        status: "error",
                        error: "agent error",
                        error_description: err
                    }).end();
                })
        }catch(err){
        }
    }
    GetConnect(req,res){
       // console.log(req)

      try{
        let data={
            host:req.body.ipaddress,
            port:req.body.port
        }
        this.Rpa_m.find(data).then((results)=>{

            if(results.length===0){
                 // let ipaddress=req.body.ipaddress;
            // let port=req.body.port
                this.CheckConnection(req, req.body.ipaddress,req.body.port).then((result)=>{

                    res.status(200).json({
                        status: 200,             
                        data: this.htmlform()
                    }).end();
                 }).catch((err)=>{
                    res.status(401).json({
                        status: "error",
                        error: "agent error",
                        error_description: err
                    }).end();
                 })
            }else{
                res.status(401).json({
                    status: "error",
                    error: "agent error",
                    error_description: "Ip Address Already In Used"
                }).end();

            }
                
               
            }).catch((err)=>{
                res.status(401).json({
                    status: "error",
                    error: "agent error",
                    error_description: err
                }).end();
            })
        }catch(err){
        }
     
//naem and value
    }
    SaveConnect(req,res){
        let data={};
    
       let more_name=(req.body.agentnameadd!==undefined)?req.body.agentnameadd:[];
       let more_value=(req.body.agentvalueadd!==undefined)?req.body.agentvalueadd:[];

 
       
        if(more_name.length!==more_value.length){
            if(more_name.length>more_value.length){
                more_value.push("null");
            }else{
                more_name.push("null");
            }

        }

        data.agentname=req.body.agentname;   
        data.adminemail=req.body.adminemail;

        data.host=req.body.host;
        data.port=req.body.port;
        data.settimeout=req.body.timeout;
        data.score=req.body.score;

    

        let find_data={
            host:req.body.ipaddress,
            port:req.body.port
        }
        this.Rpa_m.find(find_data).then((results)=>{
         if(results.length===0){
            this.Rpa_m.CreateAgent(data).then((agent)=>{
                this.UpdateRpaSetting(req, data.host,data.port,agent.insertId,data,more_name,more_value).then((success)=>{

                    if(more_name.length>0 && more_value.length>0){
                        this.Rpa_m.CreateDynamic(agent.insertId,more_name,more_value).then((dynamic)=>{
                            res.status(200).json({
                                status: 200,             
                                data:"successfully saved"
                            }).end();
                        }).catch((err)=>{
                            res.status(401).json({
                                status: "error",
                                error: "agent extra detail save error",
                                error_description: "agent extra detail save error"
                            }).end();
                        })
                    }else{
                        res.status(200).json({
                            status: 200,             
                            data:"successfully saved"
                        }).end();
                    }
                }).catch((error)=>{
                    this.Rpa_m.deleteAgent(agent.insertId).then((deleted)=>{
                            res.status(401).json({
                                status: "error",
                                error: "agent save error",
                                error_description: "problem occur during modifing agent"
                            }).end();
                    }).catch((delete_err)=>{
                            res.status(401).json({
                                status: "error",
                                error: "agent save error",
                                error_description: delete_err
                            }).end();
                    })
                })
            }).catch((err)=>{
                // console.log(err.sqlMessage)
                let result_err=(err.code==='ER_DUP_ENTRY')?"duplicate entry":err.sqlMessage
                res.status(401).json({
                    status: "error",
                    error: "agent save error",
                    error_description: result_err
                }).end();
            })  
        } else{
            res.status(401).json({
                status: "error",
                error: "agent error",
                error_description: "Ip Address Already In Used"
            }).end();

        }
            
           
        }).catch((err)=>{
          
            res.status(401).json({
                status: "error",
                error: "agent error",
                error_description: err
            }).end();
        })     
    }
    GetAgentUpdate(req,res){
        let param_id=/^[0-9]{1,11}$/;
        let header = req.app.jsonrequest(req,"getagnt")
        if(req.params.id===undefined || param_id.test(req.params.id)===false){
            if (header) {
               
                res.status(400).json({
                    status: "error",
                    error: "agent id not found",
                    error_description: "agent id not found"
                }).end();
            }else{

                res.cookie('errormessage',"agent id not found", {maxAge:9200}).redirect('/admin/manage/rpa-agent')
            }

        }else{
            //check id exsist or not if yes then return combine result of agent_setting and agent
           // console.log("here")
            let data={
                id:req.params.id
            }
            this.Rpa_m.find(data).then((results)=>{
                if(results.length===0){
                    if (header) {
                      
                        res.status(400).json({
                            status: "error",
                            error: "agent id not found",
                            error_description: "agent id not found"
                        }).end();
                    }else{
                    res.cookie('errormessage',"agent not found", {maxAge:9200}).redirect('/admin/manage/rpa-agent');
                    }
                }else{
                    if (header) {
                        res.status(200).json({
                            status:200,
                            data :results[0],
                        }).end();
                    }else{
                            res.render('dashboard/RPA/updateRPA',{host:results[0].host,port:results[0].port,name:results[0].agent_name,timeout:results[0].settimeout,
                                email:results[0].agent_email,score:results[0].score, id:req.params.id})
                        }
                    }

             
            }).catch((err)=>{
               
                res.status(401).json({
                    status: "error",
                    error: "agent not found",
                    error_description: err
                }).end();
            })
        }
    }
    AgentUpdate(req,res){
    
            let more_name=(req.body.agentnameadd!==undefined)?req.body.agentnameadd:[];
            let more_value=(req.body.agentvalueadd!==undefined)?req.body.agentvalueadd:[];
        
            if(more_name.length!==more_value.length){
                if(more_name.length>more_value.length){
                    more_value.push("null");
                }else{
                    more_name.push("null");
                }
    
            }
            let float_regex=/^[0-9]\d{0,11}(\.\d{1,6})??$/;
            let name_regex=/^(?=^.{1,10}$)[A-Za-z]{1,10}\d*$/;
            let email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            let param_regx=/^[0-9]{1,10}$/

            if(req.body.update_id===undefined || param_regx.test(req.body.update_id)===false){
            //  res.cookie('errormessage',"agent id not found", {maxAge:9200}).redirect('/admin/manage/rpa-agent')
                res.status(401).json({
                    status: "error",
                    error: "refresh",
                    error_description: "agent id not found"
                }).end();
            }
            else if(float_regex.test(req.body.score)===false || float_regex.test(req.body.timeout)===false ){
                res.status(401).json({
                    status: "error",
                    error: "invalid_error",
                    error_description: "AutoWait TimeOut or Score Is Not Integer"
                }).end();
            }else if(email.test(req.body.adminemail)===false || name_regex.test(req.body.agentname)===false){
            //   res.cookie('errormessage',"Agent Name or Email Not In Correct Format", {maxAge:9200}).redirect('/update/agent/'+req.body.update_id);
                res.status(401).json({
                    status: "error",
                    error: "invalid_error",
                    error_description: "Agent Name or Email Not In Correct Format"
                }).end();

            }else{
                let data={
                    id:req.body.update_id
                }
               // this.CheckConnection(req.body.host,req.body.port).then((result)=>{
                    this.Rpa_m.find(data).then((results)=>{
                        if(results.length===0){
                            res.status(401).json({
                                status: "error",
                                error: "refresh",
                                error_description: "agent id not found"
                            }).end();
                        }else{
                            req.body.settimeout=req.body.timeout;
                            this.UpdateRpaSetting(req, results[0].host,results[0].port,results[0].id,req.body,more_name,more_value).then((success)=>{
                                req.body.host=results[0].host;
                                req.body.port=results[0].port;
                                
                                this.Rpa_m.updateAgent(req.body).then((updated)=>{
                                
                                        this.Rpa_m.deleteDynamic(req.body.update_id).then((deleted)=>{
                                            if(more_name.length>0){
                                            this.Rpa_m.CreateDynamic(req.body.update_id,more_name,more_value).then((dynamic)=>{
                                                res.status(200).json({
                                                    status: 200,             
                                                    data:"successfully updated"
                                                }).end();
                                            }).catch((err)=>{
                                            
                                                res.status(401).json({
                                                    status: "error",
                                                    error: "agent extra detail save error",
                                                    error_description: "err"
                                                }).end();
                                            })
                                        }else{
                                        
                                            res.status(200).json({
                                                status: 200,             
                                                data:"successfully updated"
                                            }).end();
                                        }
                                        }).catch((err_deleted)=>{
                                        })  
                                
                                }).catch((err_update)=>{
                                
                                    res.status(401).json({
                                        status: "error",
                                        error: "agent extra detail save error",
                                        error_description: err_update
                                    }).end();
                                
                                });
                            }).catch((err)=>{ 
                                let data_save = {
                                    update_id: results[0].id,
                                    status: 0
                                   }
                                this.Rpa_m.updateAgent(data_save, true);  
                                   if (err.code === 'ECONNREFUSED' || err.code === 'ENOTFOUND' || err.code==='ECONNRESET' || err.code==='ETIMEDOUT' || err.code === 'EHOSTUNREACH') {
                                        res.status(401).json({
                                            status: "error",
                                            error: "connection refused",
                                            error_description: "connection refused"
                                        }).end();
                                   }else{
                                        res.status(401).json({
                                            status: "error",
                                            error: "Provided Host Not  Working",
                                            error_description: (err.code!==undefined)?"Provided Host Not  Working":err
                                        }).end();
                                   }

                                   
                
                                })




                        }
                    }).catch((err)=>{  
                        res.status(401).json({
                            status: "error",
                            error: "Provided Host Not  Working",
                            error_description: (err.code!==undefined)?"Provided Host Not  Working":err
                        }).end();
        
                    });

                // }).catch((err)=>{   
                //    console.log(err)
                //     res.status(401).json({
                //         status: "error",
                //         error: "Provided Host Not  Working",
                //         error_description: (err.code!==undefined)?"Provided Host Not  Working":err,
                //     }).end();

                // })
            }
        
    }

    CheckConnection(req, ipaddress,port){
        return   new Promise((resolve, reject) => {
           
            let oauth = req.app.getOauthToken(ipaddress,port);
             oauth.getAccessToken(false).then(() => {
             //   console.log(oauth);
                 const options = {
                     hostname:  ipaddress,
                     port: port,
                     path: '/manage/ping',
                     method: 'POST',
                     headers: {
                         'Content-Type': 'application/x-www-form-urlencoded',
                         'Authorization': 'Bearer ' + oauth.token,
                     }
                 };
                 this.http.HttpGet(options).then((data)=>{
                    resolve(data)
                }).catch((err)=>{
                    reject(err)
                })
                 
             }).catch((err)=>{
               reject(err);
             })

        });
    }
    UpdateRpaSetting(req, ipaddress,port,agentid,data,ex_name=null,ex_value=null){
        return   new Promise((resolve, reject) => {
        
            let oauth = req.app.getOauthToken(ipaddress,port);
       
             oauth.getAccessToken(false).then(() => {
               //  console.log(data)
                const postData = querystring.stringify({
                                name:data.agentname,
                                id:agentid,
                                email:data.adminemail,
                                timeout:data.settimeout,
                                score:data.score,
                                morename:JSON.stringify(ex_name),
                                morevalue:JSON.stringify(ex_value)
                            });
                 const options = {
                     hostname:  ipaddress,
                     port: port,               
                     path: '/manage/update/setting',
                     method: 'POST',
                     headers: {
                         'Content-Type': 'application/x-www-form-urlencoded',
                         'Authorization': 'Bearer ' + oauth.token,
                         'Content-Length': Buffer.byteLength(postData)

                     }
                 };
                 this.http.HttpPost(options,postData).then((data)=>{
                   
                     resolve(data)
                 }).catch((err)=>{
               
                     reject(err)
                 })
               //  console.log(req)
             }).catch((err)=>{
               reject(err);
             })

        });
    }
    DeleteAgent_c(req,res){
        
        this.Rpa_m.deleteAgent(req.params.id).then((deleted)=>{
            this.queued_m.DeleteQueue(req.params.id).then((delete_queue)=>{
                  res.status(200).json({
                                            status: 200,             
                                            data:"successfully delete"
                                        }).end();

            }).catch((delete_queue_err)=>{
                 res.status(401).json({
                        status: "error",
                        error: "delete queue err",
                        error_description: delete_queue_err
                    }).end();
            })

        }).catch((err)=>{
             res.status(401).json({
                        status: "error",
                        error: "delete agent err",
                        error_description: err
                    }).end();
        })
    }
    htmlform(){
        var html='';
				html +='<div class="clearix"></div><div class="col-md-12 removeaddform"><div class="tile"><h3 class="tile-title">Save Agent Setting</h3>'
				html +='<div class="tile-body"><form class="row"><div class="form-group col-md-5"><label class="control-label">Agent Name</label>'
				html +='<input class="form-control" id="name" type="text" name="name" placeholder="Enter Agent Name"></div>'
				html +='<div class="form-group col-md-2"><label class="control-label">Set Sikuli TimeOut</label>'
				html +='<input class="form-control" id="timeout" type="number" placeholder="Enter TimeOut" name="timeout" value="1"></div>'
				html +='</form><form class="row"><div class="form-group col-md-5"><label class="control-label">Admin Email</label>'
				html +='<input class="form-control" id="adminemail" type="email" name="adminemail" aria-describedby="emailHelp" placeholder="Enter Admin Email">'
				html +='</div><div class="form-group col-md-2">'
				html +='<label class="control-label">Set Sikuli MatchScore</label>'
				html +='<input class="form-control" id="score" type="number" placeholder="Enter TimeOut" name="score" value="0.90"></div>'
				html +='</form><div class="addmorefield"></div><div class="tile-footer"><button class="btn btn-primary" id="saveform" type="button" style="cursor: not-allowed" disabled>'
                html +='<i class="fa fa-fw fa-lg fa-check-circle"></i>save</button>&nbsp;&nbsp;&nbsp;'
                html +='<button type="button" class="btn btn-primary addmorevalue"  style="cursor: not-allowed" hidden><i class="fa fa-plus-circle" aria-hidden="true"></i>Add More</button></div></div></div></div>'
         return html;
    }


}
module.exports = RPAManagements;

