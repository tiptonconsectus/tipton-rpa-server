const querystring = require("querystring");
const HttpRequest = require('../services/oauth-rpa/httprequest');
const http = new HttpRequest();
const action_model = require("../models/action_path_m");
const action_m = new action_model();
const rpa_m = require('../models/rpaagent_m')
const Rpa_m = new rpa_m();
const Queued_m = require('../models/queue_m');
const queue_m = new Queued_m();
const config = require('../config/config');
const FormData = require("form-data");

const path = require('path');
const fs = require("fs");
// const OauthToken = require('../services/oauth-rpa/oauthRPA');
const fs_file = require("fs");
const debug = require('debug')("consectus.proccess: ");
const debugerr = require('debug')("consectus.proccess error: ");

//let authToken = null;
// function authToken(host, port) {
//     if (host !== undefined && port !== undefined) {
//         const key = host + ":" + port;
//         if (oauthTokens[key] !== undefined) {
//             return oauthTokens[key];
//         } else {
//             oauthTokens[key] = new OauthToken(host, port);
//             return oauthTokens[key];
//         }
//     }
// }
class MonitorProcess {

    constructor(OauthToken = null) {
        this.OauthToken = OauthToken;
        this.isBusy = false;
    }
    start() {
        try {
            debug("Starting Monitoring Process");
            setInterval(this.Monitor.bind(this), config.processInterval)

        } catch (e) {
            debugerr("error in set interval", e)
        }
    }
    async  CheckPathVersion() {
        try {
            // let status = {
            //     status: 1
            // }
            let agent = await Rpa_m.find();
            let actionpath = await action_m.find();
            for (let i = 0; i < agent.length; i++) {
                try {
                    await this.verifyActionPathVerion(agent[i], actionpath)
                    let savedata = {
                        update_id: agent[i].id,
                        status: 1
                    }
                    Rpa_m.updateAgent(savedata, true);
                } catch (error) {
                    debug("error in verifyActionPathVersion ", agent[i].host, agent[i].port, error);
                    if (error.code === 'ECONNREFUSED' || error.code === 'ENOTFOUND' || error.code === 'ECONNRESET' || error.code === 'ETIMEDOUT' || error.code === 'EHOSTUNREACH') {
                        let savedata = {
                            update_id: agent[i].id,
                            status: 0
                        }
                        await Rpa_m.updateAgent(savedata, true);
                    }


                    continue;
                }

            }
            return;
        } catch (err) {
            throw err;
        }
    }

    async  verifyActionPathVerion(agent, path) {
        //  /get-action-paths
        let oauth = this.OauthToken(agent.host, agent.port);

        try {
            await oauth.getAccessToken(false);
            debug(oauth.token);
            const options = {
                hostname: agent.host,
                port: agent.port,
                path: '/tasks/get-action-paths',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer ' + oauth.token,
                }
            };
            //first add missing action path
            let results = await http.HttpGet(options);
            if (results.status === "OK") {
                let savedata = {
                    update_id: agent.id,
                    status: 1
                }
                await Rpa_m.updateAgent(savedata, true);
            }
            debug("results from agent ", results);
            for (let p of path) {
                let found = false;
                for (let agent_p of results.data) {
                    if (p.name === agent_p.name) {
                        found = true;
                        if (p.version !== agent_p.version) {
                            await this.CreateZip(agent, p);
                        }
                        break;
                    }
                }
                if (!found) {
                    await this.CreateZip(agent, p);
                }
            }
            //now find the agent action path that does not exist in RPA Server list
            // acc - trans
            // trans - acc
            for (let agent_p of results.data) {
                let found = false;
                for (let p of path) {
                    if (p.name === agent_p.name) {
                        found = true;
                        break;
                    }
                }
                if (found === false) {
                    //now delete from agent
                    await this.deleteAgentPath(agent, agent_p.name);
                    debug("Deleted " + agent_p.name + " from " + agent.host + "/" + agent.port);
                }
            }

        } catch (get_err) {

            throw get_err;
        }
    }

    async  Monitor() {
        // if (!authToken && appAuthToken !== undefined) authToken = appAuthToken
        if (!this.isBusy) {
            debug("Starting processing.")
            this.isBusy = true;
            this.CheckPathVersion();
            let limit = 1;
            let exec;
            try {
                exec = await action_m.AssignAgentToNull(limit);
            } catch (err) {
                // console.log(err)
                debugerr(err)
            }
            //  console.log(exec)
            if (exec.length > 0) {
                let best_agent = await Rpa_m.Find_Best_ByAgent();
                for (let j = 0; j < exec.length; j++) {
                    if (best_agent.length > 0) {
                        /**not require to wait for upload */
                        /* add check for zero priority*/
                        for (let i = 0; i < best_agent.length; i++) {
                            let allow_agent = await action_m.priority_setting_find(best_agent[i].id, exec[j].path_id, 0);
                            if (allow_agent.length === 0) {
                                try {
                                    await this.AssignTaskToAgent(best_agent[i], exec[j])
                                    break;
                                } catch (error_bestagent) {
                                    debugerr("error_bestagent");
                                    debugerr(error_bestagent);
                                    continue;
                                }
                            } else {
                                debug("allow_agent.length !== 0")
                                continue;
                            }
                        }
                    } else {
                        debug("No Agent found to allocate");
                    }
                }
            } else {
                debug("nothing toto assign");
            }
            debug("Processing finished.")
            this.isBusy = false;
        } else {
            debug("Skipping as previous process is busy.");
            this.isBusy = false;
        }

    }
    async  AssignTaskToAgent(agent, queue) {
        let response;
        try {
            await queue_m.updateQueue(queue.id, -2, null, 0, null);
            response = await this.SendOverHttp(agent, queue)
            return response;
        } catch (err) {
            debugerr(err)
            await queue_m.updateQueue(queue.id, null, null, 0, null);

            if (err.code === 'ECONNREFUSED' || err.code === 'ENOTFOUND' || err.code === 'ECONNRESET' || err.code === 'ETIMEDOUT' || err.code === 'EHOSTUNREACH') {
                let savedata = {
                    update_id: agent.id,
                    status: 0
                }
                await Rpa_m.updateAgent(savedata, true);
                throw err;
            } else {
                debugerr("AssignTaskToAgent error at line 212 in process", agent.host, agent.port)
                debugerr(err);
                throw err;
                // throw err;
            }
        }
    }
    deleteAgentPath(agent, name) {
        return new Promise((resolve, reject) => {
            let oauth = this.OauthToken(agent.host, agent.port);
            oauth.getAccessToken(false).then(() => {
                const postData = querystring.stringify({
                    name: name
                });
                const options = {
                    hostname: agent.host,
                    port: agent.port,
                    path: '/tasks/delete-action-path',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': 'Bearer ' + oauth.token,
                        'Content-Length': Buffer.byteLength(postData)

                    }
                };
                http.HttpPost(options, postData).then((data) => {
                    // console.log(data)
                    resolve(data)
                }).catch((err) => {
                    reject(err)
                })
            }).catch((error) => {
                if (error.code === 'ECONNREFUSED' || error.code === 'ENOTFOUND' || error.code === 'ECONNRESET' || error.code === 'ETIMEDOUT' || error.code === 'EHOSTUNREACH') {
                    let savedata = {
                        update_id: agent.id,
                        status: 0
                    }
                    Rpa_m.updateAgent(savedata, true);
                }
                debugerr("error deleting action path for agent " + agent.host + "/" + agent, error)
            })
        });
    }

    SendOverHttp(agent, queue) {
        // const url=JSON.parse(exec.callback_url)
        return new Promise((resolve, reject) => {
            let oauth = this.OauthToken(agent.host, agent.port);
            oauth.getAccessToken(false).then(() => {
                const postData = querystring.stringify({
                    name: queue.name,
                    params: queue.params,
                    Serverqueueid: queue.id,
                    version: queue.version,
                    callback: JSON.stringify({ "host": config.callbackurl, "port": config.app_port, "path": config.callbackpath + agent.id + "/" + queue.id })
                });
                const options = {
                    hostname: agent.host,
                    port: agent.port,
                    path: '/tasks/execute-action-path',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': 'Bearer ' + oauth.token,
                        'Content-Length': Buffer.byteLength(postData)

                    }
                };
                http.HttpPost(options, postData).then((data) => {
                    //resolve(data)
                    // queue_m.updateQueue
                    queue_m.updateQueue(queue.id, agent.id, data.queueid, data.status_type, JSON.stringify(data.data)).then((queue_updated) => {
                        resolve(data)
                    }).catch((queue_update_fail) => {
                        reject(queue_update_fail)
                    })
                }).catch((err) => {
                    reject(err)
                });
            }).catch((error) => {
                reject(error)
                // let savedata = {
                //     update_id: agent.id,
                //     status: 0
                // }
                // Rpa_m.updateAgent(savedata, true);
                debugerr("error in ininininininini", error)
            })
        });

    }
    CreateZip(agent, queue) {
        return new Promise((resolve, reject) => {
            let folder = queue.name;
            let self = this;
            if (folder.indexOf('/') !== 0) folder = path.join(__dirname, "./", folder);
            try {
                if (fs.lstatSync(folder).isDirectory()) {
                    let zipFolder = require('zip-folder');
                    let foldername = path.basename(folder);
                    debug("Creating a zip folder", foldername);
                    let zipfilename = path.join(__dirname, "./", foldername) + ".zip";

                    zipFolder(folder, zipfilename, function (err) {
                        if (err) {
                            debug('oh no!', err);
                            reject(err)
                        }
                        debug("now uploading the file ...");
                        let file = {};
                        file.name = queue.name
                        file.type = queue.type
                        file.version = queue.version
                        file.queued = (queue.queued === 1) ? 'true' : 'false'
                        file.path = zipfilename

                        self.AgentUploadActionPath(file, agent).then((upload) => {
                            resolve(upload);
                        }).catch((upload_fail) => {
                            debugerr(upload_fail)
                            debugerr("upload_fail")
                            reject(upload_fail);
                        })


                    });
                } else {
                    reject(`${folder} is not a directory`);
                }

            } catch (error) {
                reject(`Invalid directory provided`);
                debugerr(`Invalid directory provided: ${error.message}`
                );
            }
        });
    }
    AgentUploadActionPath(file, agent) {
        //console.log(result.id)
        return new Promise((resolve, reject) => {
            this.HttpPostForm(file, agent).then((success) => {
                let data = {
                    update_id: agent.id,
                    status: 1
                }
                Rpa_m.updateAgent(data, true);
                resolve(success);
            }).catch((err) => {
                //console.log("inside resolvesssssss")
                debugerr("inside agentuploadactionpath in catch method", err)
                if (err.code === 'ECONNREFUSED' || err.code === 'ENOTFOUND' || err.code === 'ECONNRESET' || err.code === 'ETIMEDOUT' || err.code === 'EHOSTUNREACH') {
                    let data = {
                        update_id: agent.id,
                        status: 0
                    }
                    Rpa_m.updateAgent(data, true);
                    reject(err);

                } else {
                    reject(err);
                }
            })

        })

    }
    HttpPostForm(file, agent) {
        return new Promise((resolve, reject) => {
            let oauth = this.OauthToken(agent.host, agent.port);
            oauth.getAccessToken(false).then(() => {
                let formData = new FormData();
                formData.append("name", file.name);
                formData.append("type", file.type);
                formData.append("queued", file.queued);
                formData.append("version", file.version);

                formData.append("action-path-zip", fs_file.createReadStream(file.path));

                formData.submit({
                    host: agent.host,
                    port: agent.port,
                    method: "POST",
                    path: "/tasks/upload-action-path/process",
                    headers: {
                        'Authorization': 'Bearer ' + oauth.token,
                    }
                }, function (err, res) {
                    if (err) {
                        // debug(err);
                        debugerr(err)
                        debugerr("err in http line Node.240")

                        reject(err);
                    } else {
                        let errorcode = (res.statusCode !== 200);
                        var body = "";
                        res.setEncoding('utf8');
                        res.on('data', function (chunk) {
                            body += chunk;
                        });
                        res.on('error', function (err) {
                            //    debug(`Error: statusCode ${res.statusCode}`);
                            //    debug(err);

                            debugerr("res on error in httppost form method")
                            debugerr(err)
                            reject(err);
                        });
                        res.on('end', function () {
                            if (errorcode == true) {
                                reject(body);
                            } else {
                                resolve(body);
                            }
                        });
                    }
                });
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

//new MonitorProcess()
module.exports = MonitorProcess;