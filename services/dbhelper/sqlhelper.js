const mysql = require("mysql");
const logger = require('../logger/log');
const debugpool = require("debug")("pool");
const debugdb = require("debug")("consectus.dbhelper");
const debugerror = require("debug")("consectus.dberror");
const config = require("../../config/config");
//object which holds the connection to the db


global.pool = null;
// let pool2 = null;
initPool();
function initPool() {
    if (global.pool === null) {
        debug(config)
        debugdb("initalizing global pool connection");
        global.pool = mysql.createPool({
            connectionLimit: 15,
            waitForConnections: true,
            connectTimeout: 5000,
            acquireTimeout: 5000,
            multipleStatements: true,
            host: config.database.host,
            user: config.database.user,
            password: config.database.password,
            database: config.database.database,
            port: config.database.port
        });
    } else {
        let msg = `Conn Total=${global.pool._allConnections.length}, Conn Queue=${global.pool._connectionQueue.length}`
        debugpool("already initalizing global pool connection set", msg);
    }
}


async function executeQuery(queryString) {
 
        return new Promise(function (resolve, reject) {
            debugdb("execute Query:", queryString);
            let msg = `Conn Total=${global.pool._allConnections.length}, Conn Queue=${global.pool._connectionQueue.length}`
            debugpool(msg);
            global.pool.getConnection(function (err, connection) {
                if (err) {
                    debugerror(err);
                    reject(err);
                } else {
                    
                    connection.query(queryString, function (error, results) {
    
                         connection.destroy();
                        if (error) {
                            // debugerror(error);
                            reject(error);
                        } else {
                            resolve(results);
                        }
                    })
                }
    
            });
        });
 
}

module.exports.executeQuery = executeQuery;
module.exports.initPool = initPool;
