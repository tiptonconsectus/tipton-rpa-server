const http = require("http");
const https = require("https");

const FormData = require("form-data");
const fs_file = require("fs");

const debug = require("debug")("http:");
const debugerror = require("debug")("http.error:");

class HTTPRequest {
    constructor() {

    }
    HttpGet(options, https = false) {
        return new Promise((resolve, reject) => {
            let httpSend = (https) ? https : http
            const req = httpSend.request(options, (res) => {
                res.setEncoding('utf8');
                //  const { statusCode } = res;

                let data = "";
                let error = (res.statusCode !== 200);
                // if (error) {
                //     // consume response data to free up memory
                //     console.log(error)
                //     reject("error on agent response")
                //     res.resume();
                //     return;
                //     }
                res.on('data', (chunk) => {
                    data += chunk;

                    // resolve(data);
                });
                res.on('end', () => {
                    // debug('No more data in response.');

                    try {
                        debug(data)
                        data = JSON.parse(data);
                        if (error) {
                            reject(data)
                        } else {
                            resolve(data)
                        }
                    } catch (e) {
                        // data = chunk
                        debugerror("e.message in http get json parse 33");
                        debugerror(e);
                        reject("error in try method")
                    }

                });
            })
            req.on('error', (e) => {
                debugerror(e)
                debugerror(`Got error: ${e.message}`);
                /** connect issue handle here */
                //     debugerror(`problem with request: ${e.message}`);
                reject(e);
            });
            // req.setTimeout(1000, function () {
            //     reject("timeout");
            //     req.abort();
            //     });
            // write data to request body
            // req.write(postData);
            req.end();

        })

    }
    HttpPost(options, postData, https = false) {
        return new Promise((resolve, reject) => {
            let httpSend = (https) ? https : http
            const req = httpSend.request(options, (res) => {
                //  const { statusCode } = res;
                let data = "";
                let error = (res.statusCode !== 200);
                // if (statusCode !== 200) {
                //    error = new Error('Request Failed.\n' +
                //                      `Status Code: ${statusCode}`);
                //  } 
                //   else if (!/^application\/json/.test(contentType)) {
                //     error = new Error('Invalid content-type.\n' +
                //                       `Expected application/json but received ${contentType}`);
                //   }
                //  if (error) {
                //    console.error(error.message);
                //    console.error("error.message");
                //    // consume response data to free up memory
                //    reject("error on agent response");
                //    res.resume();
                //    return;
                //  }

                res.setEncoding('utf8');

                res.on('data', (chunk) => {

                    data += chunk;
                    //use here resolve

                });
                res.on('end', () => {

                    // const parsedData = data;
                    // const parsedData = data;

                    try {
                        debug(data)
                        data = JSON.parse(data);
                        if (error) {
                            reject(data)
                        } else {
                            resolve(data)
                        }
                    } catch (e) {
                        debugerror("e.message in http post json parse 33");
                        debugerror(e);
                        reject("error in try method")
                    }
                    // debug('No more data in response.');
                });


            })
            req.on('error', (e) => {
                debug(e)
                debugerror(`Got error: ${e.message}`);
                // debugerror(`problem with request: ${e.message}`);
                reject(e);
            });
            // req.setTimeout(1000, function () {
            // reject("timeout");
            // req.abort();
            // });
            // write data to request body

            req.write(postData);
            req.end();

        })
    }
    HttpPostForm(req, result, oauth, zip = null, https = false) {
        //console.log(req.body)
        return new Promise((resolve, reject) => {
            let formData = new FormData();
            formData.append("name", req.body.name);
            formData.append("type", req.body.type);
            formData.append("queued", req.body.queued);
            if (req.body.version !== undefined && req.body.version !== null) formData.append("version", req.body.version);
            if (req.body.id) formData.append("id", req.body.id);
            // if(zip===null){
            formData.append("action-path-zip", fs_file.createReadStream(req.file.path));
            //  }
            //  else{
            //     formData.append("action-path-zip", fs_file.createReadStream(zip)); 
            //  }
            formData.submit({
                host: (https) ? "https://" + result.host : result.host,
                port: result.port,
                method: "POST",
                path: "/tasks/upload-action-path",
                headers: {
                    'Authorization': 'Bearer ' + oauth,
                }
            }, function (err, res) {
                if (err) {
                    // debug(err);
                    debugerror("err in http")
                    debugerror(err)
                    reject(err);
                } else {
                    let errorcode = (res.statusCode !== 200);
                    var body = "";
                    res.setEncoding('utf8');
                    res.on('data', function (chunk) {
                        body += chunk;
                    });
                    res.on('error', function (err) {
                        //    debug(`Error: statusCode ${res.statusCode}`);
                        //    debug(err);
                        debug("res err")
                        debug(err)
                        reject(err);
                    });
                    res.on('end', function () {
                        if (errorcode == true) {
                            reject(body);
                        } else {
                            resolve(body);
                        }
                    });
                }
            });

        })
    }
}
module.exports = HTTPRequest;