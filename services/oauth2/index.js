/**
 * exports middleware module.exports.authorize = function(req, res, next);
 * requires config section to be setup with database, clients 
 * const oauth2 = require("./x/y/oauth2");
 * simply add the middleware to the paths that you want to secure.
 * app.use("/verify", oauth2.authorize, oauth2Router.verify);
 */



const config = require('./oauth-config/config');
//const utils=require("../utils/utils");
const debug = require("debug")("consectus.oauth2");
const debugcontroller = require("debug")("consectus.oauth2controller");
const debugerror = require("debug")("consectus.oauth2error");
const debugmodel = require("debug")("consectus.oauth2model");
const uuid_g = require('uuid/v4')
var crypto = require('crypto');

const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')
// const http = require('http');

var express = require('express');
const mysql = require("mysql");

const bcrypt = require('bcryptjs');
const SALT_WORK_FACTOR = 10;
const migration = require('./migration/migration_db');
let executeQuery;






// module.exports.router = routerOauth2;
module.exports.oauth2app = oauth2app;
module.exports.authorize = Authorize;
module.exports.verify = VerifyPingHandler;
module.exports.config = config;
module.exports.login = LoginHanlder;
module.exports.createuserlogin = CreateUserHanlder

module.exports.userlist = UserListHanlder
module.exports.userupdate = UserUpdateHanlder
module.exports.createurl = CreateUrl
module.exports.UserPermissionList = UserPermissionList
module.exports.permissionsList = PermissionsList
module.exports.deleteuserurlList = deletePermissionurl
module.exports.urllist = UrlList
module.exports.updateurllist = UpdateUrlList
module.exports.userLogout = userLogout


module.exports.userrole = UserRole
module.exports.UpdateUserRole = UpdateUserRole
/**
 * Hanlder to deal with authorization code api call
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * req.body must have 
 * @param client_id - unique client id issued by the backend team for the App
 * @param response_type - use "code"
 * @param code_challenge - an encrypted random number generated using SHA256 
 * @param code_challenge_method - S256 or Plain 
 * @param state - optionally provided and then returned in all responses further 
 * @returns {}  on error return error and error_description
 * invalid_request - if missing parameters
 * invalid_client - if wrong client_id
 * unauthorized_client – This client is not authorized to use the requested grant type.
 * unsupported_grant_type – If a grant type is requested that the authorization server doesn’t recognize
 */

/**
 * login handler
 * create handler
 */
function LoginHanlder(req, res, cb) {
   
    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    let res_err;

    // if (req.body.client_id === undefined) {
    //     res.status(400).json({
    //         status: "error",
    //         error: "invalid_request",
    //         error_description: "client_id not found"
    //     }).end();
    // }
    // else if (req.body.client_secret === undefined) {
    //     res.status(400).json({
    //         status: "error",
    //         error: "invalid_request",
    //         error_description: "client_secret not found"
    //     }).end();
    // } else 
    if (req.body.email === undefined) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "username email not found"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "username not found"
            }).end();
        }
    }
    else if (req.body.email.length < 3) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "username email not is not valid"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "username is not valid"
            }).end();
        }

    }
    else if (req.body.password === undefined) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "password not found"
            cb(res_err)

        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "password not found"
            }).end();
        }
    }
    else if (req.body.password.length < 5) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "password length should be greater than 5"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "password length should be greater than 5"
            }).end();
        }
    }
    else {
        logincontroller.loginUser(req.body.email, req.body.password).then((result) => {

            if (url_path.indexOf("/oauth2") < 0) {
                cb(null, result)
            }
            else {
                res.status(200).cookie('authorization', "Bearer " + result.access_token, { maxAge: 7200000 }).json({
                    status: 200,
                    data: result
                }).end();
            }

        }).catch((error) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(error)
            } else {
                res.status(400).json({
                    status: "error",
                    error: "invalid_request",
                    description: error
                }).end();
            }
        })

    }
}
function CreateUserHanlder(req, res, cb) {
    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    let res_err;

    if (req.body.client_id === undefined || req.body.client_id === '') {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "client_id not found"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "client_id not found"
            }).end();
        }
    }
    else if (req.body.client_secret === undefined || req.body.client_secret === '') {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "client_secret not found"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "client_secret not found"
            }).end();
        }
    }
    else if (req.body.username === undefined || req.body.username === '') {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "username not found"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "username not found"
            }).end();
        }
    }
    else if (req.body.username.length < 3) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "username is not valid"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "username is not valid"
            }).end();
        }
    }
    else if (req.body.email === undefined || req.body.email === '') {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "email not found"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "email not found"
            }).end();
        }
    }
    else if (req.body.firstname === undefined || req.body.firstname === '') {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "firstname not found"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "firstname not found"
            }).end();
        }
    }
    else if (req.body.lastname === undefined || req.body.lastname === '') {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "lastname not found"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "lastname not found"
            }).end();
        }
    } else if (req.body.password === undefined || req.body.password === '') {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "password not found"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "password not found"
            }).end();
        }
    }
    else if (req.body.password.length < 5) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "password length should be greater than 5"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "password length should be greater than 5"
            }).end();
        }
    }
    else {
        controller.getClient(req.body.client_id, req.body.client_secret).then((client_record) => {
            logincontroller.createUser(req.body).then((result) => {
                if (url_path.indexOf("/oauth2") < 0) {
                    cb(null, result)
                } else {
                    res.status(200).json({
                        status: "success",
                        description: result[0]
                    }).end();
                }
            }).catch((error) => {
                if (url_path.indexOf("/oauth2") < 0) {
                    cb(error)
                } else {
                    res.status(400).json({
                        status: "error",
                        error: "invalid_request",
                        description: error
                    }).end();
                }
            })
        }).catch((error) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(error)
            } else {
                res.status(400).json({
                    status: "error",
                    error: "invalid_request",
                    description: error
                }).end();
            }

        })

    }
}


function UserListHanlder(req, res, cb) {
    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    let data = (req.body !== null) ? req.body : null
    logincontroller.listUser(data).then((results) => {
        if (url_path.indexOf("/oauth2") < 0) {
            cb(null, results)
        } else {
            res.status(200).json({
                status: "success",

                description: results
            }).end();
        }

    }).catch((error) => {
        if (url_path.indexOf("/oauth2") < 0) {
            cb(error)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: error
            }).end();
        }
    })

}


function UserUpdateHanlder(req, res, cb) {
    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    let res_err;

    if (req.body.username === undefined && req.body.firstname === undefined && req.body.lastname === undefined && req.body.password === undefined && req.body.status === undefined) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "Nothing To Update "
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "Nothing To Update "
            }).end();
        }

    }
    else if (req.params.id === undefined) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "parameter is not provided  "
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "parameter is not provided  "
            }).end();
        }

    }
    else if (req.body.username !== undefined && req.body.username.length > 10) {

        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "username in between 3 to 10 character"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "username in between 3 to 10  character"
            }).end();
        }
    }

    else if (req.body.password !== undefined && req.body.password !== '' && req.body.password.length < 5) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "password length should be greater than 5"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "password length should be greater than 5"
            }).end();
        }
    }

    else {

        logincontroller.updateUser(req.params.id, req.body, req.token.Accesstoken.user_id).then((results) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(null, "successfull 1 row updated")
            } else {
                res.status(200).json({
                    status: "success",
                    description: "successfull 1 row updated"
                }).end();
            }

        }).catch((error) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(error)
            } else {
                res.status(400).json({
                    status: "error",
                    error: "invalid_request",
                    description: error
                }).end();
            }
        })
    }

}
function CreateUrl(req, res, cb) {
    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    let res_err;

    if (req.body.urlpath === undefined) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "url not provided"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: "url path not provided"
            }).end();
        }
    } else if (req.body.urlpath === '') {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "url not provided"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: "url path not provided"
            }).end();
        }
    }
    else {
        logincontroller.createPermissions(req.body.urlpath).then((result) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(null, result)
            } else {
                res.status(200).json({
                    status: "successfully create permission",
                    description: result[0]
                }).end();
            }
        }).catch((error) => {

            if (url_path.indexOf("/oauth2") < 0) {
                cb(error)
            } else {
                res.status(400).json({
                    status: "error",
                    error: "invalid_request",
                    description: error
                }).end();
            }
        });
    }
}
function UrlList(req, res, cb) {
    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    logincontroller.urlLists(req.body).then((lists) => {
        if (url_path.indexOf("/oauth2") < 0) {
            cb(null, lists)
        } else {
            res.status(200).json({
                status: "permission list",
                data: lists
            }).end();
        }

    }).catch((error) => {
        if (url_path.indexOf("/oauth2") < 0) {
            cb(error)
        } else {
            res.status(200).json({
                status: "error",
                error: "invalid_request",
                description: error
            }).end();
        }

    })
}

function UpdateUrlList(req, res, cb) {
    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    let res_err
    if (req.params.id === undefined) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "id not provided in parameter"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: "id not provided in parameter"
            }).end();
        }
    } else if (req.body.urlpath === undefined || req.body.urlpath === '') {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = " urlpath not provided"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: "urlpath not provided"
            }).end();
        }
    } else {
        logincontroller.updateUrlList_c(req.params.id, req.body.urlpath).then((results) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(null, results)
            } else {
                res.status(200).json({
                    status: "permission list",
                    data: results
                }).end();
            }
        }).catch((error) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(error)
            } else {
                res.status(200).json({
                    status: "error",
                    error: "invalid_request",
                    description: error
                }).end();
            }

        })

    }
}
function PermissionsList(req, res, cb) {
    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    let res_err
    if (req.body.userid === undefined) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "User_id not provided"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: "User_id path not provided"
            }).end();
        }
    } else {
        logincontroller.permissionsList_c(req.body.userid).then((results) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(null, results)
            } else {
                res.status(200).json({
                    status: "success",

                    description: results
                }).end();
            }

        }).catch((error) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(error)
            } else {
                res.status(400).json({
                    status: "error",
                    error: "invalid_request",
                    description: error
                }).end();
            }
        })
    }
}
function deletePermissionurl(req, res, cb) {
    logincontroller.deleteurl(req.params.id).then((deleted_url) => {
        if (url_path.indexOf("/oauth2") < 0) {
            cb(null, deleted_url)
        } else {
            res.status(200).json({
                status: "permission list",
                data: deleted_url
            }).end();
        }

    }).catch((deleted_url_err) => {

        if (url_path.indexOf("/oauth2") < 0) {
            cb(deleted_url_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: deleted_url_err
            }).end();
        }
    })
}
function UserPermissionList(req, res, cb) {
    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    let data = (req.body !== null) ? req.body : null
    logincontroller.userPermissionList_c(data).then((lists) => {
        if (url_path.indexOf("/oauth2") < 0) {
            cb(null, lists)
        } else {
            res.status(200).json({
                status: "permission list",
                data: lists
            }).end();
        }

    }).catch((error) => {
        if (url_path.indexOf("/oauth2") < 0) {
            cb(error)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: error
            }).end();
        }

    })
}
function UserRole(req, res, cb) {
    let res_err;
    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    if (req.body.userid === undefined || req.body.role === undefined) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "nothing provided for permission"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: "nothing provided for permission"
            }).end();
        }
    }
    else if (req.body.userid === '' || req.body.role === '' || JSON.parse(req.body.role).length === 0) {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "nothing to add in permission"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: "nothing to add in permission"
            }).end();
        }
    } else {
        logincontroller.userRole_c(req.body).then((lists) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(null, lists)
            } else {
                res.status(200).json({
                    status: "permission list",
                    data: lists
                }).end();
            }

        }).catch((error) => {

            if (url_path.indexOf("/oauth2") < 0) {
                cb(error)
            } else {
                res.status(200).json({
                    status: "error",
                    error: "invalid_request",
                    description: error
                }).end();
            }

        })
    }
}
function UpdateUserRole(req, res, cb) {

    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    let res_err
    if (req.body.userid === undefined || req.body.role === undefined || req.body.userid === '') {
        if (url_path.indexOf("/oauth2") < 0) {
            res_err = "nothing provided for permission"
            cb(res_err)
        } else {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                description: "nothing provided for permission"
            }).end();
        }
    }
    else {

        logincontroller.permissionsList_c(req.body.userid).then((results) => {

            logincontroller.updateUserRole_c(req.body, results).then((lists) => {
                if (url_path.indexOf("/oauth2") < 0) {
                    cb(null, lists)
                } else {
                    res.status(200).json({
                        status: "permission list",
                        data: lists
                    }).end();
                }

            }).catch((error) => {
                if (url_path.indexOf("/oauth2") < 0) {
                    cb(error)
                } else {
                    res.status(200).json({
                        status: "error",
                        error: "invalid_request",
                        description: error
                    }).end();
                }

            })
        }).catch((error) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(error)
            } else {
                res.status(200).json({
                    status: "error",
                    error: "invalid_request",
                    description: error
                }).end();
            }

        })
    }
}
function userLogout(req, res, cb) {
    //  console.log(req.token)

    let url_path = (req.baseUrl === '') ? req.path : req.originalUrl
    if (req.token.Accesstoken !== undefined) {
        logincontroller.Logout(req.token.Accesstoken.user_id).then((success) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(null, "successfully logout")
            } else {
                res.status(200).json({
                    status: "logout",
                    data: "successfully logout"
                }).end();
            }

        }).catch((err) => {
            if (url_path.indexOf("/oauth2") < 0) {
                cb(error)
            } else {
                res.status(200).json({
                    status: "error",
                    error: "invalid_request",
                    description: error
                }).end();
            }

        })
    } else {
        if (url_path.indexOf("/oauth2") < 0) {
            cb("already logout")
        } else {
            res.status(200).json({
                status: "error",
                error: "invalid_request",
                description: "already logout"
            }).end();
        }
    }
}

function AuthorizationCodeHandler(req, res, next) {

    //HTTP headers to ensure clients do not cache this request.
    debug("AuthorizationCodeHandler called")
    res.set({
        "Cache-Control": "no-store",
        "Pragma": "no-cache"
    });
    debug(JSON.stringify(req.body, false, 8));
    //verify if the request has parameters
    if (req.body.client_id === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "client_id not found"
        }).end();
    } else if (req.body.response_type === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "response_type not found"
        }).end();
    } else if (req.body.code_challenge === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "code_challenge not found"
        }).end();
    } else if (req.body.response_type !== "code") {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "invalid response_type"
        }).end();
        code_challenge_method
    } else if (req.body.code_challenge_method === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "code_challenge_method not found"
        }).end();
    } else if (req.body.code_challenge_method.toUpperCase() !== "S256" && req.body.code_challenge_method.toUpperCase() !== "PLAIN") {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "invalid code_challenge_method. uses S256 or Plain"
        }).end();
    } else {
        //all good, now try to validate the client id
        let self = this;
        controller.getClient(req.body.client_id).then((client_record) => {
            const redirect_uri = client_record.redirect_uri ? client_record.redirect_uri : null;
            const code = generateUUID4();
            debugcontroller(code);
            controller.saveCode(code, req.body.client_id, req.body.code_challenge, req.body.code_challenge_method, req.body.state === undefined ? null : req.body.state)
                .then(() => {
                    // now return redirect uri
                    const resp = {
                        state: req.body.state,
                        code: code
                    }
                    if (redirect_uri) resp.redirect_uri = redirect_uri;
                    res.json(resp).end();
                }).catch((error) => {
                    debugcontroller(error);
                    res.status(500).json({
                        status: "error",
                        error: error,
                        error_description: "System Error occurrred"
                    }).end();
                })
        }).catch((error) => {
            if (error) {
                debug("client_id not found", error);
                res.status(400).send({
                    status: "error",
                    error: "invalid_client",
                    error_description: "client_id not found"
                }).end();
            } else {
            }
        });
    }

}
/**
 * 
 * @param {*} req 
 * client_id 
 * grant_type
 * @param {*} res 
 * @param {*} next 
 */
function TokenHanlder(req, res, next) {
    //HTTP headers to ensure clients do not cache this request.
    debug("CodeHandler called")
    res.set({
        "Cache-Control": "no-store",
        "Pragma": "no-cache"
    });
    //verify if the request has parameters
    if (req.body.client_id === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "client_id not found"
        }).end();
    } else if (req.body.grant_type === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "grant_type not provided"
        }).end();
    } else if (req.body.grant_type !== "authorization_code") {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "invalid grant_type only authorization_code allowed"
        }).end();
    } else if (req.body.code === undefined) {
        let x = req.body.code;
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "code not provided"
        }).end();
    } else if (req.body.verifier_code === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "verifier_code not provided"
        }).end();
    } else if (req.body.code_challenge_method === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "code_challenge_method not found"
        }).end();
    } else if (req.body.code_challenge_method.toUpperCase() !== "S256" && req.body.code_challenge_method.toUpperCase() !== "PLAIN") {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "invalid code_challenge_method. uses S256 or Plain"
        }).end();
    } else {
        //all good, now try to validate the code id
        let self = this;
        // debug(JSON.stringify(req.body));
        controller.verifyCode(req.body.state, req.body.client_id, req.body.code, req.body.verifier_code, req.body.code_challenge_method).then((results) => {
            res.json(results).end();
        }).catch((error) => {
            debug("code not found", error);
            res.status(400).send({
                status: "error",
                error: "invalid_code",
                error_description: "code verification failed"
            }).end();
        });
    }
}
/**
 * This function can be used to check for Token validity.
 * If the calling req.path starts with "/oauth2/" then this is direct API call to this function via Postman or end user Application
 * If the calling req.path does not start with /oauth2 then this function is used to authorize access as a middleware
 * in this case the following will try to validate the token:
 *     if valid: it will add req.token = {token, user, client, roles} object
 *     else : req.token = null
 *     all the middleware using routes will have to deal with req.token being null by showing appropriate error page or message
 *     e.g. app.use("/list_users", app.authorize, mylistuserfunction);
 *      mylistuserfunction (req, res, next){
 *      if (!req.token)  {
 *          render error message
 *      } 
 *  Roles:
 *  Each user will have a role_id which will map to a role records in oauth2_roles  table (user_id, permission)
 *  4, "/Users/List"
 *  4, 'create_user'
 * }
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function Authorize(req, res, next) {


    //check for valid bearer token
    const bearer = req.headers['authorization'] || req.headers['Authorization'] || req.cookies['authorization'];

    if (bearer === undefined) {
        if (req.path.indexOf("/oauth2") < 0) {
            req.token = null;
            next();
        } else {
            res.status(401).json({
                status: "error",
                error: "invalid_request",
                error_description: "Authorization Required"
            }).end();
        }
    } else {
        //check the bearer token validity
        if (bearer.indexOf("Bearer") !== 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "Authorization header not correct. Bearer XXXXXX format required"
            }).end();
        } else {
            const token = bearer.substr(7).trim();
            if (token.length === 0) {
                res.status(400).json({
                    status: "error",
                    error: "invalid_request",
                    error_description: "Authorization header not correct. Bearer XXXXXX token missing"
                }).end();
            } else {
                //all good now let us find it in database
                controller.getAccessToken(token).then((token) => {
                    debug("Authorized:", token);
                    req.token = token;
                    next();
                }).catch((error) => {

                    debugerror(error);
                    if (req.path.indexOf("/oauth2") < 0) {
                        req.token = null;
                     
                            res.status(401).json({
                                status: "error",
                                error: "invalid_token",
                                error_description: error
                            }).end();
                      

                    } else {
                        res.status(401).json({
                            status: "error",
                            error: "invalid_token",
                            error_description: "Invalid token"
                        }).end();

                    }
                })
            }
        }
    }
}
/**
 * Allows sample code to verify if authorisation is working.
 * The response checks Authorization: Bearer XXXXXXX header to contain valid token
 * If valid it returns "Authorized" as response.
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function VerifyPingHandler(req, res, next) {
    res.status(200).json({
        token: req.token
    }).end();
}
class AccessTokenController {
    constructor(accessTokenModel) {
        this.accessToken_M = accessTokenModel;
        this.login_M = new LoginModel()
    }

    token(req, res) {

    }

    getClient(clientId, clientSecret = null) {
        let self = this;
        return new Promise((resolve, reject) => {
            // debugcontroller("getClient: clientId: %s", clientId);
            self.accessToken_M.getClient(clientId, clientSecret).then((client_record) => {
                resolve(client_record);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    grantTypeAllowed(grantType, callback) {
        if (grantType == "authorization_code") {
            callback(null);
            debug("grantTypeAllowed: clientId: %s, grantType:%s", clientId, grantType);
        } else {
            callback("Grant type not allowed");
            debug("grantTypeAllowed: clientId: %s, grantType:%s", clientId, grantType);
        }
    }
    verifyCode(state, client_id, code, verifier_code, code_challenge_method) {
        let self = this;
        if (state === undefined) state = null;
        return new Promise((resolve, reject) => {
            const code_challenge = (code_challenge_method.toUpperCase() === 'S256') ? getCodeChallenge(verifier_code) : verifier_code;
            self.accessToken_M.verifyCode(state, client_id, code, code_challenge).then((results) => {
                // debug(JSON.stringify(results));
                if (results.length === 1) {
                    //generate access token and return
                    const accessToken = {
                        code: code,
                        userid: null,
                        client_id: results[0].client_id,
                        access_token: generateUUID4(),
                        expires_in: config.expiresIn
                    }
                    self.accessToken_M.saveAccessToken(accessToken.code, accessToken.userid, accessToken.client_id, accessToken.access_token, accessToken.expires_in).then((res) => {
                        debug(JSON.stringify(res));
                        if (res.affectedRows === 1) {
                            resolve(accessToken);
                        } else {
                            reject("access token save failed");
                        }
                    }).catch((error) => {
                        reject(error);
                    })
                } else {
                    reject("code verification failed")
                }
            }).catch((error) => {
                debug(error);
                reject("technical verification failed")
            })
        });
    }
    saveCode(code, clientId, code_challenge, code_challenge_method, state, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            let codeJson = {
                "code": code,
                "clientId": clientId,
                "codeChallenge": code_challenge,
                "codeChallengeMethod": code_challenge_method,
                "state": state
            }
            debugcontroller("saveCode: code: %s clientId: %s code_challenge: %s state: %s", code, clientId, code_challenge, state);
            self.accessToken_M.saveCode(codeJson).then((result) => {
                resolve(result);
            }).catch((error) => {
                reject(error);
            });
        })
    }
    getAccessToken(bearerToken, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            this.accessToken_M.getAccessToken(bearerToken).then((results) => {

                if (results.length > 0) {
                    let token_result = {
                        Accesstoken: results[0]
                    }
                    if (results[0].user_id !== null) {
                        let Json = {
                            "id": results[0].user_id
                        }

                        this.getClient(results[0].client_id).then((client) => {
                            token_result.client = client

                            this.login_M.find(Json).then((user) => {
                                token_result.user = user[0]
                                this.login_M.permissions(user[0].id).then((perm) => {
                                    //  token_result.permission = JSON.stringify(perm)
                                    token_result.permissions = perm
                                    resolve(token_result)
                                }).catch((error) => {
                                    reject("permissions failed - " + error);
                                })
                            }).catch((error) => {
                                reject("client id: " + results[0].client_id + " find failed - " + error);
                            })
                        }).catch((error) => {
                            reject("token not valid");

                        })

                    } else {
                        resolve(results[0])
                    }
                }
                else {
                    reject("Bearer token not valid");
                }
            }).catch((error) => {
                debugerror(error);
                reject(error);
            });
        });
    }
}



class AccessTokenModel {
    constructor() {
    }
    saveCode(codeJson, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            let _newcodeJson = codeJson;
            let query = "INSERT INTO ?? (??,??,??,??,??) VALUES (?,?,?,?,?)"
            let inserts = [config.tables.codes, 'code', 'code_challenge', 'code_challenge_method', 'client_id', 'state', _newcodeJson.code, _newcodeJson.codeChallenge, _newcodeJson.codeChallengeMethod, _newcodeJson.clientId, _newcodeJson.state]
            query = mysql.format(query, inserts)
            debug(query);
            //execute the query to get the user
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    deleteCode(code, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = "DELETE FROM ?? WHERE ??=?";
            let inserts = [config.tables.codes, 'code', code];
            query = mysql.format(query, inserts)
            //execute the query to get the user
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    deleteAccess(code, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = "DELETE FROM ?? WHERE ??=?";
            let inserts = [config.tables.tokens, 'user_id', code];
            query = mysql.format(query, inserts)
            //execute the query to get the user
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    verifyCode(state, client_id, code, code_challenge, callback) {
        let self = this;
        return new Promise((resolve, reject) => {

            let query = "SELECT * FROM ?? WHERE ??=? AND ??=? AND ??=? AND used = false";
            // add state to query to compare if passed
            if (state) query += " AND ??=?"
            if (config.codeLifetimeSeconds > 0) {
                //only allow code from last 10 seconds to be used for token request
                query += ` AND created_date > date_add(NOW(), INTERVAL -${config.codeLifetimeSeconds} SECOND)`;
            }
            let inserts = [config.tables.codes, 'client_id', client_id, 'code', code, 'code_challenge', code_challenge];
            //add state to query to compare if passed
            if (state) inserts = [config.tables.codes, 'client_id', client_id, 'code', code, 'code_challenge', code_challenge, 'state', state];
            query = mysql.format(query, inserts)
            //execute the query to get the user
            executeQuery(query).then((results) => {
                if (results.length === 1) {
                    //mark as used
                    query = "UPDATE ?? SET used = true WHERE ??=? AND ??=?";
                    inserts = [config.tables.codes, 'code', code, 'code_challenge', code_challenge];
                    query = mysql.format(query, inserts);
                    executeQuery(query).then((res) => {
                        if (res.affectedRows === 1) debug("marked code as used");
                        resolve(results);
                    }).catch((error) => {
                        reject(error);
                    })
                } else {
                    self.deleteCode(code).then((results) => {
                        if (results.affectedRows === 1) {
                            reject("security: cleared previous code if it exists");
                        } else {
                            reject("code verification failed");
                        }

                    }).catch((error) => {
                        reject(error)
                    });
                }
            }).catch((error) => {
                reject(error);
            });
        });
    }
    saveAccessToken(code, userid, client_id, access_token, expires, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = `INSERT INTO ?? (??,??,??,??,??) VALUES (?,?,?,?,DATE_ADD(NOW(), INTERVAL ${config.expiresIn} SECOND))`;

            let inserts = [config.tables.tokens, 'code', 'user_id', 'client_id', 'token', 'expires', code, userid, client_id, access_token,]

            query = mysql.format(query, inserts)
            //execute the query to get the user
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {

                reject(error);
            });
        });
    }
    getAccessToken(bearerToken, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            var doesTokenExistQuery = "SELECT * FROM ?? WHERE ??=? AND ??>NOW()" // join client if not null, user if not null, permissions
            var inserts = [config.tables.tokens, 'token', bearerToken, 'expires']
            doesTokenExistQuery = mysql.format(doesTokenExistQuery, inserts)
            //execute the query to get the user
            executeQuery(doesTokenExistQuery).then((results) => {
                //for each result, get user record, client record, and permissions list add it to result object
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getClient(clientId, clientSecret) {
        let self = this;
        debugmodel("get client called")
        return new Promise((resolve, reject) => {
            debugmodel("get client promise called")
            var getClientQuery = "SELECT * FROM ?? "
            getClientQuery += (clientSecret !== null) ? " WHERE ??=? AND ??=?" : " WHERE ??=?"
            var inserts = [config.tables.clients, 'client_id', clientId, 'client_secret', clientSecret];


            getClientQuery = mysql.format(getClientQuery, inserts)
            //execute the query to get the user
            executeQuery(getClientQuery).then((results) => {
                if (results.length > 0) {
                    resolve(results[0]);
                } else {
                    reject("Incorrect client_id");
                }
            }).catch((error) => {
                reject("Incorrect client_id");
            })
        })
    }

}
function generateUUID4() {
    let d = Math.random() * 100000000000000 + "";
    return uuid_g(d, config.defaultnamespace)
}




/*
*user controller

*/
class LoginController {
    constructor(LoginModel) {
        this.LoginModel_M = LoginModel;
        this.accessToken_M = new AccessTokenModel();
    }
    createUser(data) {
        return new Promise((resolve, reject) => {
            data.password = this.hashPassword(data.password)
            let Json = {
                "email_id": data.email
            }
            this.LoginModel_M.create(data).then((result) => {
                resolve(result)
            }).catch((error) => {
                if (error.code === "ER_DUP_ENTRY") {

                    reject("User Already Exists")
                } else {
                    //some other error

                    reject(error)
                }
            })
        })
    }
    loginUser(email, password) {
        return new Promise((resolve, reject) => {
            let Json = {
                "email_id": email
            }
            let isLogin = true
            this.LoginModel_M.find(Json, isLogin).then((result) => {

                if (result.length === 0) {
                    reject("User Not Found")
                }
                else {

                    this.hasPassword(password, result[0].password).then((isMatch) => {

                        if (isMatch) {
                            const accessToken = {
                                code: null,
                                userid: result[0].id,
                                client_id: result[0].client_id,
                                access_token: generateUUID4(),
                                expires_in: config.expiresIn
                            }
                            this.accessToken_M.saveAccessToken(accessToken.code, accessToken.userid, accessToken.client_id, accessToken.access_token, accessToken.expires_in).then((res) => {
                                debug(JSON.stringify(res));
                                if (res.affectedRows === 1) {
                                    resolve(accessToken);
                                } else {
                                    reject("access token save failed");
                                }
                            }).catch((error) => {
                                reject(error);
                            })


                        }
                        else {
                            reject("password is not valid")
                        }
                    }).catch((error) => {
                        reject(error)
                    })

                }

            }).catch((error) => {
                reject(error)
            })

        })
    }
    listUser(data) {
        return new Promise((resolve, reject) => {
            this.LoginModel_M.find(data).then((result) => {
                //   result.forEach(function (v) { delete v.password });
                resolve(result)
            }).catch((error) => {
                reject(error)

            })

        });
    }
    updateUser(param, body, token) {
        let deleteToken = null;
        return new Promise((resolve, reject) => {
            let Json = {
                id: param
            }
            this.LoginModel_M.find(Json).then((result) => {
                if (result.length === 0) {
                    reject(result);
                } else if (result.length === 1) {
                    // resolve(result);
                    let Jsoncode = {};
                    if (body.username !== undefined && body.username !== '') {
                        Jsoncode.username = body.username
                    }
                    if (body.firstname !== undefined && body.firstname !== '') {
                        Jsoncode.firstname = body.firstname
                    }
                    if (body.lastname !== undefined && body.lastname !== '') {
                        Jsoncode.lastname = body.lastname
                    }
                    if (body.password !== undefined && body.password !== '') {
                        //  Jsoncode.password = body.password  deleteAccess

                        if (token == param) {
                            deleteToken = true;
                        }
                        Jsoncode.password = this.hashPassword(body.password)

                    }
                    if (body.status !== undefined) {
                        Jsoncode.status = parseInt(body.status)
                    }

                    this.LoginModel_M.update(param, Jsoncode).then((update) => {
                        resolve(update)
                        if (deleteToken !== null) {
                            this.accessToken_M.deleteAccess(param).then((tokendeleted) => {

                            }).catch((error) => {
                                reject(error)
                            })
                        }

                    }).catch((error) => {
                        reject(error)
                    })

                } else {

                    reject(result);

                }

            }).catch((error) => {
                reject(error);

            });
        });

    }
    createPermissions(data) {

        return new Promise((resolve, reject) => {
            this.LoginModel_M.createurl(data).then((result) => {
                resolve(result)
            }).catch((error) => {

                reject(error)
            })

        });

    }
    urlLists(data) {
        return new Promise((resolve, reject) => {
            this.LoginModel_M.urllist(data).then((result) => {
                resolve(result)
            }).catch((error) => {

                reject(error)
            })

        });

    }
    updateUrlList_c(id, urlpath) {
        return new Promise((resolve, reject) => {
            this.LoginModel_M.updateUrlList_m(id, urlpath).then((result) => {
                resolve(result)
            }).catch((error) => {

                reject(error)
            })

        });
    }
    permissionsList_c(data) {
        return new Promise((resolve, reject) => {
            this.LoginModel_M.permissionsList_m(data).then((result) => {
                resolve(result)
            }).catch((error) => {

                reject(error)
            })

        });
    }
    userPermissionList_c(data) {
        return new Promise((resolve, reject) => {
            this.LoginModel_M.userPermissionList_m(data).then((result) => {
                resolve(result)
            }).catch((error) => {

                reject(error)
            })

        });

    }
    userRole_c(body) {
        let role_array = JSON.parse(body.role).map(num => parseInt(num, 10))
        return new Promise((resolve, reject) => {
            this.LoginModel_M.userRole_m(body.userid, role_array).then((result) => {
                resolve(result)
            }).catch((error) => {
                reject(error)
            })

        });

    }
    deleteurl(data) {
        return new Promise((resolve, reject) => {
            this.LoginModel_M.deleteurluser(data).then((result) => {
                resolve(result)
            }).catch((error) => {
                reject(error)
            })

        });
    }
    updateUserRole_c(body, result) {
        let datareq = JSON.parse(body.role)

        let result_arr = result.map(function (val) { return val.url_id })
        let role_array = datareq.map(function (num) { return parseInt(num, 10) })
        Array.prototype.difference = function (arr) {
            return this.filter(function (i) { return arr.indexOf(i) === -1; });
        };

        if (role_array.length > result_arr.length) {
            /**insert perform */
            let diffArray = role_array.difference(result_arr);
            return new Promise((resolve, reject) => {
                this.LoginModel_M.updateUserRole_m(body.userid, diffArray).then((result) => {
                    resolve(result)
                }).catch((error) => {

                    reject(error)
                })
            });

        } else if (role_array.length < result_arr.length) {
            /**delete perform */
            let diffArray = result_arr.difference(role_array);
            let isDelete = true;
            return new Promise((resolve, reject) => {
                this.LoginModel_M.updateUserRole_m(body.userid, diffArray, isDelete).then((result) => {
                    resolve(result);
                }).catch((error) => {

                    reject(error);
                })
            });

        } else {
            return new Promise((resolve, reject) => {
                resolve("role are up to date");
            });
        }
    }
    /**logout from all browser */
    Logout(userid) {
        return new Promise((resolve, reject) => {
            this.accessToken_M.deleteAccess(userid).then((tokendeleted) => {
                resolve(tokendeleted);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    hashPassword(plain) {
        try {
            //  var salt= SALT_WORK_FACTOR
            var salt = bcrypt.genSaltSync(SALT_WORK_FACTOR);

            return bcrypt.hashSync(plain, salt);
        }
        catch (error) {

            return error;
        }
    }
    hasPassword(plain, password) {
        return new Promise((resolve, reject) => {
            if (plain && password) {
                bcrypt.compare(plain, password, function (err, isMatch) {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(isMatch);
                    }
                });
            }
            else {
                reject(false)
            }
        });

    }
}
class LoginModel {
    constructor() {
    }
    find(data, isLogin = null) {
        let query = (isLogin === null) ? "SELECT id,username,email_id,firstname,lastname,client_id,status,created_on  FROM ?? " : "SELECT * FROM ?? "
        let inserts = [config.tables.users]
        if (data !== null) {
            let index = 0;
            for (let key in data) {
                if (index == 0) {
                    query += " WHERE ??=?"
                    inserts.push(key)
                    inserts.push(data[key])
                } else {
                    query += " AND ??=?"
                    inserts.push(key)
                    inserts.push(data[key])
                }
                index++
            }
        }

        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {

                reject(error)
            })
        })

    }
    create(data) {

        let query = "INSERT INTO ?? (??,??,??,??,??,??,??)"
        query += "VALUES(?,?,?,?,?,?,?)"

        let inserts = [config.tables.users, 'username', 'email_id', 'firstname', 'lastname', 'password', 'client_id', 'status', data.username, data.email, data.firstname, data.lastname, data.password, data.client_id, 1]
        query = mysql.format(query, inserts)

        let self = this;
        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);


            }).catch((error) => {

                reject(error)
            })
        })

    }
    update(param, data) {

        let query = "UPDATE ??  "
        let inserts = [config.tables.users]
        let index = 0;
        for (let key in data) {

            if (index == 0) {
                query += " SET ??=?"
                inserts.push(key)
                inserts.push(data[key])
            }
            else {
                query += "  ,??=? "
                inserts.push(key)
                inserts.push(data[key])
            }

            index++
        }
        query += "  WHERE id=" + param + "";
        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                reject(error)
            });
        });

    }
    deleteurluser(data) {
        let num = /^[0-9]{1,11}$/
        let query = "DELETE FROM ?? WHERE ??=? ";
        let inserts = [config.tables.permission, 'user_id', data]

        query = mysql.format(query, inserts)
        return new Promise((resolve, reject) => {
            if (num.test(data)) {

                executeQuery(query).then((result) => {
                    resolve(result);

                }).catch((error) => {

                    reject(error)
                })
            }
            else {
                reject("something went wrong")
            }
        })


    }
    updateUrlList_m(id, urlpath) {
        let query = "UPDATE ?? SET ??=?  WHERE ??=?"
        let inserts = [config.tables.url, 'url', urlpath, 'id', id]

        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {

                reject(error)
            })
        })

    }
    permissions(param) {


        let query = "SELECT url.url  FROM ?? role INNER JOIN ?? url ON role.url_id=url.id WHERE ??=?"
        let inserts = [config.tables.permission, config.tables.url, 'user_id', param]

        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {

                reject(error)
            })
        })

    }
    permissionsList_m(data) {

        let query = "SELECT *  FROM ?? WHERE ??=?"
        let inserts = [config.tables.permission, 'user_id', data]

        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {

                reject(error)
            })
        })


    }
    userRole_m(userid, role) {
        let query = "INSERT INTO ?? (??,??)"
        for (let i = 0; i < role.length; i++) {
            if (i === 0) {
                query += "  VALUES(" + userid + "," + role[i] + ")"
            } else {
                query += "  ,(" + userid + "," + role[i] + ") "
            }
        }
        let inserts = [config.tables.permission, 'user_id', 'url_id']
        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);


            }).catch((error) => {
                reject(error)
            })
        })

    }
    updateUserRole_m(userid, role, isdelete = null) {

        let query = (isdelete !== null) ? `DELETE FROM ?? WHERE user_id= ${userid} AND url_id IN (${role})` : "INSERT INTO ?? (??,??)"
        if (isdelete === null) {
            for (let i = 0; i < role.length; i++) {
                if (i === 0) {
                    query += "  VALUES(" + userid + "," + role[i] + ")"
                } else {
                    query += "  ,(" + userid + "," + role[i] + ") "
                }
            }
        }


        let inserts = [config.tables.permission, 'user_id', 'url_id', userid, role]
        query = mysql.format(query, inserts)

        // let self = this;
        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);


            }).catch((error) => {
                reject(error)
            })
        })

    }


    userPermissionList_m(data) {

        let query = "select u.id,u.username,u.email_id,u.firstname,u.lastname,u.client_id,u.status,u.created_on,Group_concat(us.url) url,GROUP_CONCAT(us.id) url_id from ?? u"
        query += " left join ??  rl on u.id=rl.user_id "
        query += " left join ??  us on rl.url_id=us.id "
        query += " group by u.id,u.username,u.email_id,u.firstname,u.lastname,u.client_id,u.status,u.created_on order by u.id "
        let inserts = [config.tables.users, config.tables.permission, config.tables.url]

        if (data !== null) {
            let index = 0;
            for (let key in data) {
                if (index == 0) {
                    query += " WHERE ??=?"
                    inserts.push(key)
                    inserts.push(data[key])
                } else {
                    query += " AND ??=?"
                    inserts.push(key)
                    inserts.push(data[key])
                }
                index++
            }
        }
        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                reject(error)
            })
        })

    }
    createurl(data) {

        let query = "INSERT INTO ?? (??)"
        query += "VALUES(?)"

        let inserts = [config.tables.url, 'url', data]
        query = mysql.format(query, inserts)

        let self = this;
        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);


            }).catch((error) => {
                reject(error)
            })
        })

    }
    urllist(data = null) {
        let query = "SELECT *  FROM ??  "

        let inserts = [config.tables.url]
        if (data !== null) {
            let index = 0;
            for (let key in data) {
                if (index == 0) {
                    query += " WHERE ??=?"
                    inserts.push(key)
                    inserts.push(data[key])
                } else {
                    query += " AND ??=?"
                    inserts.push(key)
                    inserts.push(data[key])
                }
                index++
            }
        }

        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {

                reject(error)
            })
        })

    }
}


function base64URLEncode(code) {
    return code.toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=/g, '');
}
function getVerifierCode() {
    const code = crypto.randomBytes(32);
    const verifier_code = base64URLEncode(code);
    return verifier_code;
}

function getCodeChallenge(code) {
    debug(code);
    return base64URLEncode(crypto.createHash('sha256').update(code).digest());
}

const controller = new AccessTokenController(new AccessTokenModel());
const logincontroller = new LoginController(new LoginModel());

/**
 * Oauth2Server
 */
/**
 * Get port from environment and store in Express.
 */

// var port = normalizePort(process.env.RPA_AGENT_PORT || config.oauthPort || '9090');
// oauth2app.set('port', port);
// app.use('/oauth2', router);
// app.use('/oauth2/verify', module.exports.authorize, module.exports.verify);
// app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocument));

 function oauth2app(options) {
    if (options.app.executeQuery === undefined) {
        const db = require("./sqlhelper/sqlhelper")
        db.initPool();
    }
    executeQuery =  (options.app.executeQuery !== undefined) ? options.app.executeQuery : db.executeQuery;
    try {
         migration.initialize(executeQuery)
    } catch (error) {
        debugerror(error)
        throw error;
    }
    if (options.app !== undefined) {
       
        // options.app.use('/oauth2', routeExpress);
        options.app.post('/oauth2/authorization_code', AuthorizationCodeHandler);
        options.app.post('/oauth2/user_login', LoginHanlder);
        options.app.post('/oauth2/token', TokenHanlder);
        options.app.post('/oauth2/verify', module.exports.authorize, module.exports.verify);

         options.app.post('/oauth2/create_user', module.exports.authorize, module.exports.createuserlogin);


         options.app.post('/oauth2/list_users', module.exports.authorize, module.exports.userlist);
         options.app.post('/oauth2/update_user/:id', module.exports.authorize, module.exports.userupdate);

         options.app.post('/oauth2/users_permissions', module.exports.UserPermissionList);

         options.app.post('/oauth2/update_user_permissions/:id', module.exports.authorize, module.exports.userupdate);
         options.app.post('/oauth2/create_url', module.exports.createurl);

         options.app.post('/oauth2/create_role', module.exports.userrole);
        //    await options.app.use('/oauth2/verify', module.exports.authorize, module.exports.verify);

         options.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
        debug("Oauth2 Routes added ------------");
        
    } else {
        if (options.port !== undefined) {

            const compression = require('compression');
            const cors = require('cors');
            const http = require('http');
            const app = express();
            app.use(express.json());
            app.use(express.urlencoded({ extended: false }));
            const cookieParser = require('cookie-parser');
            app.use(cookieParser());

            const bodyParser = require('body-parser')
            const jsonParser = bodyParser.json({ limit: '10mb' }) //{
            const urlEncoded = bodyParser.urlencoded({ limit: '10mb', extended: true }) //

            app.set('superSecret', config.secret)
            app.disable("x-powered-by")
            app.use(cors())
            app.use(jsonParser)
            app.use(urlEncoded)
            app.use(compression())


            var port = normalizePort(process.env.RPA_AGENT_PORT || options.port);

            app.set('port', port);
            app.post('/oauth2/authorization_code', AuthorizationCodeHandler);
            app.post('/oauth2/user_login', LoginHanlder);
            app.post("/oauth2/token", TokenHanlder);
            app.post('/oauth2/verify', module.exports.authorize, module.exports.verify);


            app.post('/oauth2/create_user', module.exports.authorize, module.exports.createuserlogin);

            app.post('/oauth2/list_users', module.exports.authorize, module.exports.userlist);
            app.post('/oauth2/update_user/:id', module.exports.authorize, module.exports.userupdate);

            app.post('/oauth2/users_permissions', module.exports.UserPermissionList);

            app.post('/oauth2/update_user_permissions/:id', module.exports.authorize, module.exports.userupdate);


            app.post('/oauth2/create_url', module.exports.authorize, module.exports.createurl);


            app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
            var server = http.createServer(app);

            server.listen(options.port);
            server.on('error', onError);
            server.on('listening', onListening);

            /**
             * Normalize a port into a number, string, or false.
             */

            function normalizePort(val) {
                var port = parseInt(val, 10);

                if (isNaN(port)) {
                    // named pipe
                    return val;
                }

                if (port >= 0) {
                    // port number
                    return port;
                }

                return false;
            }

            /**
             * Event listener for HTTP server "error" event.
             */

            function onError(error) {
                debug(error);
                if (error.syscall !== 'listen') {
                    throw error;
                }

                var bind = typeof port === 'string'
                    ? 'Pipe ' + port
                    : 'Port ' + port;

                // handle specific listen errors with friendly messages
                switch (error.code) {
                    case 'EACCES':
                        console.error(bind + ' requires elevated privileges');
                        break;
                    case 'EADDRINUSE':
                        console.error(bind + ' is already in use');
                        break;
                    default:
                        throw error;
                }
            }

            /**
             * Event listener for HTTP server "listening" event.
             */

            function onListening() {
                var addr = server.address();
                var bind = typeof addr === 'string'
                    ? 'pipe ' + addr
                    : 'port ' + addr.port;
                debug('Oauth2 Server Listening on ' + bind);
            }


        } else {
            throw Error("Oauth2 Requires Port in options");
        }
        
    }

};

