const config = require('../oauth-config/config');
const mysql = require("mysql");

const bcrypt = require('bcryptjs');
const SALT_WORK_FACTOR = 10;


async function ensureClient(client,executeQuery) {


    let query = "SELECT * FROM ?? WHERE client_id = ?";
    let inserts = [config.tables.clients, client.client_id];
    query = mysql.format(query, inserts);
    try {
        let results = await executeQuery(query);

        if (results.length === 0) {
            //create this client
            query = "INSERT INTO ?? (client_id, name,client_secret, redirect_uri) VALUES (?,?,?,?)";
            inserts = [config.database.database + '.' + config.tables.clients, client.client_id, client.name, client.client_sec, client.redirect_uri];
            query = mysql.format(query, inserts);
            try {
                results = await executeQuery(query);

                let res = "db seeder completed" + results;
                if (results.affectedRows !== 1) {
                    const msg = `client creation failed : ${client.client_id} - ${client.name}`;                    
                    return msg;
                }
                return res;

            } catch (error) {
                return error;

            }

        } else {
            return "nothing to seed"
        }
    } catch (error) {
        return error;

    }

}
async function ensureUser(client,executeQuery) {

    let query = "SELECT * FROM ?? WHERE email_id = ?";
    let inserts = [config.tables.users, client.admin_email];
    query = mysql.format(query, inserts);
    try {
        let results = await executeQuery(query);


        if (results.length === 0) {
            //create this client
            let password = hashPassword(client.admin_password);
            query = "INSERT INTO ?? (username, email_id, firstname, lastname, password, client_id, status) VALUES (?,?,?,?,?,?,?)";
            inserts = [config.tables.users, client.admin_username, client.admin_email, client.admin_firstname, client.admin_lastname, password, client.admin_client, 1];
            query = mysql.format(query, inserts);
            try {
                results = await executeQuery(query);

                let res = "db seeder completed" + results;
                if (results.affectedRows !== 1) {
                    const msg = `client creation failed : ${client.admin_email} - ${client.admin_username}`;
                    return msg;
                }
                return res;

            } catch (error) {
                return error;

            }

        } else {
            return "nothing to seed"
        }
    } catch (error) {
        return error;

    }

}

function hashPassword(plain) {
    try {

        var salt = bcrypt.genSaltSync(SALT_WORK_FACTOR);

        return bcrypt.hashSync(plain, salt);
    }
    catch (error) {

        return error;
    }
}


async function Createpermission(id,executeQuery) {
   

    let query_url = "SELECT * FROM ?? ";
    let inserts_url = [config.tables.url];
    query_url = mysql.format(query_url, inserts_url);

    try {
      
        let results_url = await executeQuery(query_url);
        if(results_url.length===0){
           
            query_url= "INSERT INTO ?? (??) VALUES (?)";
            inserts_url = [config.tables.url,'url', '/*'];
            query_url = mysql.format(query_url, inserts_url);
            results_url = await executeQuery(query_url);
          /**find record in url table */
          
            await permission(id,results_url.insertId,executeQuery);
        }else{
            await permission(id,results_url[0].id,executeQuery);
        }
    }
    catch (error) {
        throw error;
    }
}

async function permission(id,result_url,executeQuery){

    let query = "SELECT * FROM ?? WHERE email_id = ?";
    let inserts = [config.tables.users, id.admin_email];
    query = mysql.format(query, inserts);
    try{
            let results = await executeQuery(query);
  
            if(results[0].id!==undefined){
                let perm_query = "SELECT * FROM ?? WHERE user_id = ?";
                let perm_inserts = [config.tables.permission, results[0].id];
                perm_query = mysql.format(perm_query, perm_inserts);
                try{
                    let results_perm = await executeQuery(perm_query);

                    if(results_perm.length===0){
                 
                        perm_query = "INSERT INTO ?? (user_id, url_id) VALUES (?,?)";
                        perm_inserts = [config.tables.permission, results[0].id,result_url];
                        perm_query = mysql.format(perm_query, perm_inserts);
                        results_perm = await executeQuery(perm_query);

                        let res_perm = "db seeder completed" + results_perm;
                        if (results_perm.affectedRows !== 1) {
                            const msg = `permission creation failed `;
                            return msg;
                        }
                        return res_perm;

                    }else{
                        return "nothing to seed";
                    }
                } catch (error) {
  
                    return error;
                }
            

            }else{
                return "user not found";
            }
     } catch (error) {
           return error;
     }


}

module.exports = {
    ensureClient: ensureClient,
    ensureUser: ensureUser,
    Createpermission: Createpermission
}