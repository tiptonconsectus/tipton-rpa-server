const debug = require("debug")("consectus.oauth2");
const debugcontroller = require("debug")("consectus.oauth2controller");
const debugerror = require("debug")("consectus.oauth2error");
const debugmodel = require("debug")("consectus.oauth2model");
const config = require('./oauth-config/config');
const mysql = require("mysql");

let connection = null
let pool = null;

initPool();

const sql = `
INSERT INTO oauth2_access_tokens
    (
        token,
        code,
        client_id,
        expires
    )
    VALUES
    (
        '52ab2b47-af45-4679-9a0a-1ffe3b4adc5a',
        '1234',
        'consectus-rpa',
        NOW()
    );
`;

executeQuery(sql).then((data)=> console.log(data)).catch((error)=>{
    if (error.code === "ER_DUP_ENTRY"){
        // this insert fails unique key constraint ... so the record exists.
    }
    console.log(error.code)
});
/**
 * Create the connection to the db
 */
function initConnection() {
    //set the global connection object
    console.log(config);
    connection = mysql.createConnection({
        host: config.database.host,
        user: config.database.user,
        password: config.database.password,
        database: config.database.database,
        port: config.database.port
    })
}

function initPool() {
    pool = mysql.createPool({
        connectionLimit: 10,
        host: config.database.host,
        user: config.database.user,
        password: config.database.password,
        database: config.database.database,
        port: config.database.port
    });
}

async function executeQuery(queryString) {
    return new Promise(function (resolve, reject) {
        // debugdb(queryString);
        // initConnection();
        // connection.connect((err) => {
        pool.getConnection(function (err, connection) {
            if (err) {
                debugerror(err);
                reject(err);
            } else {
                connection.query(queryString, function (error, results, fields) {
                    // console.log('mySql: query: error is: ', error, ' and results are: ', results)
                    //disconnect from the method
                    // connection.end()
                    connection.destroy();
                    // debugdb("disconnecting from database");
                    if (error) {
                        // debugerror(error);
                        reject(error);
                    } else {
                        resolve(results);
                    }
                })
            }
        })

    });
}