const config = require("../config/config");
const executeQuery = require("../services/dbhelper/sqlhelper").executeQuery;
const mysql = require("mysql");
class ActionPath_M {
    constructor() {

    }
    find(id = null) {
        return new Promise((resolve, reject) => {
            let query = (id === null) ? "SELECT * FROM ?? WHERE isdelete=0 " : "SELECT * FROM ?? WHERE ??=? AND isdelete=0 LIMIT 1";
            let inserts = (id === null) ? [config.tables.paths] : [config.tables.paths, 'id', id]
            query = mysql.format(query, inserts)
            // debug(query);
            //execute the query to get the user
            executeQuery(query).then((results) => {
                // debug(results);
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    getActionPathByName(name, version = null, isdelete = false) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM ?? ";
            if (isdelete === true) {
                query += " WHERE ??=?  LIMIT 1 ";
            }
            else {
                query += (version === null) ? " WHERE ??=? AND isdelete=0 LIMIT 1 " : " WHERE ??=? AND version=" + version + " AND isdelete=0 LIMIT 1 ";
            }
            let inserts = [config.tables.paths, 'name', name]
            query = mysql.format(query, inserts)
            // debug(query);
            //execute the query to get the user
            executeQuery(query).then((results) => {
                // debug(results);
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    updateActionPath(path) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = null;
            this.getActionPathByName(path.name, null, true).then((results) => {
                query = null;
                if (results.length === 0) {
                    //                 insert into action_paths (version,name,type,queued)
                    // SELECT   ifnull(MAX(version) + 1,1), "john@example.com","nodejs",1
                    // FROM action_paths
                    query = `INSERT INTO ?? (version, ??, ??,??) values(1,?,?,?)`
                    let update = [config.tables.paths,
                        'name', 'type', 'queued', path.name, path.type, path.queued
                    ];
                    query = mysql.format(query, update)
                    // console.log(query)
                } else {
                    query = (results[0].isdelete === 0) ? `UPDATE ?? SET ??=?,version=version+1, ??=?, ??=? WHERE ??=?` : `UPDATE ?? SET ??=?, ??=?, ??=?,version=version+1, isdelete=0 WHERE ??=?`;
                    let update = [
                        config.tables.paths,
                        'name', path.name,
                        'type', path.type,
                        'queued', path.queued,
                        'id', results[0].id];
                    query = mysql.format(query, update)
                    // console.log(query)
                }
                // debug(query);
                //execute the query to get the user
                executeQuery(query).then((results) => {
                    resolve(results);
                }).catch((error) => {
                    reject(error);
                });
            }).catch((error) => {
                reject(error);
            })
        });
    }
    deletePath(data) {
        let num = /^[0-9]{1,11}$/
        let query = "UPDATE ?? SET isdelete=1 WHERE ??=?";
        let inserts = [config.tables.paths, "id", data];
        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {
            if (num.test(data)) {
                executeQuery(query).then((result) => {
                    resolve(result);

                }).catch((error) => {
                    reject(error)
                });
            } else {
                reject("something went wrong")
            }
        });

    }
    orderupdate(name, value) {
        // console.log(name)
        //  console.log(value)
        return new Promise((resolve, reject) => {
            let query;
            for (var i = 0; i < name.length; i++) {
                query = " UPDATE ?? SET order_id=" + value[i] + " WHERE name='" + name[i] + "'"

                let inserts = [config.tables.paths]
                query = mysql.format(query, inserts)
                // console.log(query)

                executeQuery(query).then((result) => {
                    resolve(result);

                }).catch((error) => {
                    reject(error)
                });
            }

        })
    }
    priority_setting_find(agentid, actionpathid, priority = null) {
        let query = " select * from ?? "
        query += (priority === null) ? "where ??=? and ??=?" : " where ??=? and ??=? and priority=" + priority;
        let inserts = [config.tables.map, 'rpa_agent_id', agentid, 'action_path_id', actionpathid];
        query = mysql.format(query, inserts)
        //console.log(query)
        return new Promise((resolve, reject) => {


            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                reject(error)
            });
        })
    }
    actionpath_priority(id) {
        let query = " select a.id,a.version,a.name, a.type,a.queued,a.created_on,a.isdelete,IFNULL(p.priority,0) order_id from ?? a left join ";
        query += " (select action_path_id,priority from ?? where rpa_agent_id=?) p on p.action_path_id=a.id  where a.isdelete = 0 GROUP BY a.id"
        let inserts = [config.tables.paths, config.tables.map, id];
        query = mysql.format(query, inserts)
        // console.log(query)
        return new Promise((resolve, reject) => {


            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                reject(error)
            });
        })
    }
    //priority_setting
    priority_setting(agentid, actionpathid, value) {
        // console.log("value ",value)
        return new Promise((resolve, reject) => {
            this.priority_setting_find(agentid, actionpathid).then((result) => {
                // console.log(result.length)
                if (result.length === 0) {
                    let query = `INSERT INTO ?? (??, ??,??) values(?,?,?)`

                    let inserts = [config.tables.map, 'rpa_agent_id', 'action_path_id', 'priority', agentid, actionpathid, value];
                    query = mysql.format(query, inserts);
                    executeQuery(query).then((result) => {
                        resolve(result);

                    }).catch((error) => {
                        reject(error)
                    });
                } else {
                    let query = "UPDATE ?? SET priority=? where ??=? and ??=?"

                    let inserts = [config.tables.map, value, 'rpa_agent_id', agentid, 'action_path_id', actionpathid];
                    query = mysql.format(query, inserts);
                    executeQuery(query).then((result) => {
                        resolve(result);

                    }).catch((error) => {
                        reject(error)
                    });
                }
            }).catch((error) => {
                reject(error)
            })
        })
    }
    AssignAgentToNull(limit = null) {
        let query = " select queue.*,path.version,path.name,path.type,path.queued from ?? path "
        query += " inner join ?? queue on queue.path_id=path.id "
        query += " where queue.result IS NULL and queue.agent_queue_id IS NULL and  queue.agent_id IS NULL order by order_id "
        query += " LIMIT " + limit
        let inserts = [config.tables.paths, config.tables.queues];
        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {


            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                reject(error)
            });
        })
    }

}
module.exports = ActionPath_M;