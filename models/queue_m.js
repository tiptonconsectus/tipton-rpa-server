const config = require("../config/config");
const executeQuery = require("../services/dbhelper/sqlhelper").executeQuery;
const mysql = require("mysql");
class Queue_M {
    constructor() {

    }
    find(path_id = null) {
        return new Promise((resolve, reject) => {
            let query = (path_id === null) ? "SELECT * FROM ?? WHERE isdelete = 0" : "SELECT * FROM ?? WHERE path_id =" + path_id + " AND isdelete = 0";
            let inserts = [config.tables.queues]
            query = mysql.format(query, inserts)
            // debug(query);
            //execute the query to get the user
            executeQuery(query).then((results) => {
                // debug(results);
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    addQueue(path, status, params, cb) {

        let self = this;
        return new Promise((resolve, reject) => {
            // debug(params);
            let query = `INSERT INTO ?? (??, ??, ??,??,??) VALUES (?, ?, ?,?,?)`
            let update = [config.tables.queues,
                'path_id', 'params', 'status', 'callback_url', 'agent_id',
                path, JSON.stringify(params), status, cb, -1];
            query = mysql.format(query, update)
            // debug(query);
            //execute the query to get the user
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    updateQueue(queueId, agentid, agentqueue, status, result) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = null;
            query = `UPDATE ?? SET ??=?, ??=?, ??=?, ??=?, ??=NOW(), execution_delay= TIME_TO_SEC(TIMEDIFF(NOW(),created_on))  WHERE ??=?`
            let update = [config.tables.queues,
                'agent_id', agentid,
                'agent_queue_id', agentqueue,
                'status', status,
                'result', result,
                'updated_on',
                'id', queueId];
            query = mysql.format(query, update)

            executeQuery(query).then((results) => {
                resolve(results)
            }).catch((error) => {
                reject(error);
            });
        });
    }
    findByQueueId(queueId) {
        return new Promise((resolve, reject) => {
            let query = " select * from ?? where id=? ";
            let update = [config.tables.queues, queueId];
            query = mysql.format(query, update)
            executeQuery(query).then((results_find) => {
                resolve(results_find);
            }).catch((insert_err) => {
                reject(insert_err);
            })
        });
    }
    DeleteQueue(data) {
        return new Promise((resolve, reject) => {
            let query;
            query = `UPDATE ?? SET isdelete=1  WHERE status between 0 and 1 and ??=?`
            let update = [config.tables.queues,
                'agent_id', data];
            query = mysql.format(query, update)

            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });

    }
    getPendingQueue() {
        return new Promise((resolve, reject) => {
            let query = `SELECT 
                    ??.*, 
                    ??.name as path_name, ??.type as path_type, ??.queued as path_queued
                    FROM ??, ?? WHERE ??.??=??.?? AND ??.??=? ORDER BY created_on ASC LIMIT 1000`;
            let inserts = [
                config.tables.queues,
                config.tables.paths, config.tables.paths, config.tables.paths,
                config.tables.queues,
                config.tables.paths,
                config.tables.queues, 'path_id',
                config.tables.paths, 'id',
                config.tables.queues, 'status', 0];
            query = mysql.format(query, inserts)
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getActionPathQueueById(name, queue_id) {
        return new Promise((resolve, reject) => {
            let query = `SELECT 
                ??.*, 
                ??.name as path_name, ??.type as path_type, ??.queued as path_queued
                FROM ??, ?? WHERE ??.??=??.?? AND ??.??=? AND ??.??=? LIMIT 1`;
            let inserts = [
                config.tables.queues,
                config.tables.paths, config.tables.paths, config.tables.paths,
                config.tables.queues,
                config.tables.paths,
                config.tables.queues, 'path_id',
                config.tables.paths, 'id',
                config.tables.paths, 'name', name,
                config.tables.queues, 'id', queue_id];
            query = mysql.format(query, inserts)
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        })
    }
    getQueue(path = null, limit = 1000, offset = null) {

        let self = this;
        return new Promise((resolve, reject) => {
            let query;
            let inserts;
            if (path !== null) {
                query = "SELECT * FROM ?? WHERE ??=? ORDER BY created_on DESC LIMIT 100";
                inserts = [config.tables.queues, 'path_id', path.id]
            } else {
                query = "SELECT * FROM ?? WHERE (status BETWEEN 0 AND 1)  LIMIT " + limit + " OFFSET " + offset;
                inserts = [config.tables.queues]
            }
            query = mysql.format(query, inserts)
            // debug(query);
            //execute the query to get the user
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }

}
module.exports = Queue_M;