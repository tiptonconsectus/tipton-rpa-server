const config = require('../config/config');
const executeQuery = require("../services/dbhelper/sqlhelper").executeQuery;
const mysql = require("mysql");
class Sopra_AccNo_m {
    constructor() {
    }
    GetAccNO(isUsed, accno = null) {
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM ?? ";
            query += (isUsed !== undefined && isUsed !== null) ? " WHERE is_used=" + isUsed + " ORDER BY id LIMIT 1 " : " WHERE accno=" + accno
            let inserts = [config.tables.sopra_accno];
            query = mysql.format(query, inserts);
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    CreateAccNo(accno, endsequence) {
        return new Promise((resolve, reject) => {
            let query = "CALL createsopraAcc(?,?) "
            let inserts = [accno, endsequence];
            query = mysql.format(query, inserts)
            executeQuery(query).then((results) => {
                resolve("successfull account number added");
            }).catch((error) => {
                reject(error);
            });

        });
    }
    CreateBatchAccNo(accno, endsequence) {
        return new Promise((resolve, reject) => {
            let query = "CALL createsoprabatchAcc(?,?) "
            let inserts = [accno, endsequence];
            query = mysql.format(query, inserts)
            executeQuery(query).then((results) => {
                resolve("successfull account number added");
            }).catch((error) => {
                reject(error);
            });

        });
    }
    GetBatchNO(isUsed, accno = null, alldata = false) {
        return new Promise((resolve, reject) => {

            let query = "SELECT * FROM ?? ";
            query += (isUsed !== undefined && isUsed !== null) ? " WHERE is_used=" + isUsed + " ORDER BY id LIMIT 5 " : " WHERE accno=" + accno
            let inserts = [config.tables.sopra_batchno];
            query = mysql.format(query, inserts);

            query = mysql.format(query, inserts);
            executeQuery(query).then((results) => {
                // console.log("get batchno success", results);
                resolve(results);
            }).catch((error) => {
                // console.log("get batchno err", error);
                reject(error);
            });
        });
    }
    UpdateAccNo(accno, isUsed) {
        return new Promise((resolve, reject) => {

            let query = "UPDATE ?? SET is_used=? ,updated_on=NOW() WHERE ??=? "
            let inserts = [config.tables.sopra_accno, isUsed, 'accno', accno];
            query = mysql.format(query, inserts)
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    UpdateBatchAccNo(accno, isUsed, update = null) {
        return new Promise((resolve, reject) => {
            if (accno.length > 0) {
                let tbname = config.tables.sopra_batchno;
                let query;
                for (let i = 0; i < accno.length; i++) {
                    let usedvalue = isUsed ? isUsed : (accno[i] && accno[i].is_used !== 2) ? 0 : 2;
                    if (i === 0) query = "UPDATE " + tbname + " SET is_used=" + usedvalue + " ,updated_on=NOW() WHERE batchno=" + accno[i].batchno + "; ";
                    else query += " UPDATE " + tbname + " SET is_used=" + usedvalue + " ,updated_on=NOW() WHERE batchno=" + accno[i].batchno + "; ";
                }
                // let inserts = [config.tables.sopra_batchno, isUsed, 'batchno', accno];
                query = mysql.format(query)
                executeQuery(query).then((results) => {
                    // console.log("update batchno results", results);
                    resolve(results);
                }).catch((error) => {
                    // console.log("update batchno", error);
                    reject(error);
                });
            } else {
                reject("account no provided");
            }

        });
    }
    async getAllaccno(dataBody) {

        let found = false;
        let index = 0;
        let sql = " SELECT * from ?? "
        let total = " SELECT count(*) count from ?? "
        if (dataBody.tableobj && dataBody.tableobj.where) {
            let whereParams = dataBody.tableobj.where;

            for (let keys in whereParams) {
                if (found) {
                    sql += ` and ${keys} =  '${whereParams[keys]}' `;
                    total += ` and ${keys} =  '${whereParams[keys]}' `;
                } else {
                    sql += ` WHERE ${keys} =  '${whereParams[keys]}' `;
                    total += ` WHERE ${keys} =  '${whereParams[keys]}' `;
                    found = true;
                }
            }
        }
        if (dataBody.tableobj && dataBody.tableobj.searchDetail) {
            let dataSearch = dataBody.tableobj.searchDetail;
            let SearchLength = Object.keys(dataSearch).length;
            for (let key in dataSearch) {
                if (found) {
                    if (index === 0) sql += ` and ( ${key} like ${"'%" + dataSearch[key] + "%'"} `, total += ` and ( ${key} like ${"'%" + dataSearch[key] + "%'"} `, index = index + 1;
                    else sql += ` or ${key} like ${"'%" + dataSearch[key] + "%'"} `, total += ` or ${key} like ${"'%" + dataSearch[key] + "%'"} `, index = index + 1;
                } else {
                    if (index === 0) sql += ` WHERE ${key} like ${"'%" + dataSearch[key] + "%'"} `, total += ` WHERE ${key} like ${"'%" + dataSearch[key] + "%'"} `, found = true, index = index + 1;
                }
                if (SearchLength === index) sql += ` ) `, total += ` ) `

            }
        }
        if (dataBody.tableobj && dataBody.tableobj.sortDetail) sql += ` ORDER BY ${dataBody.tableobj.sortDetail.field}  ${dataBody.tableobj.sortDetail.type} `

        if (dataBody.tableobj && dataBody.tableobj.pageDetail) sql += ` LIMIT ${dataBody.tableobj.pageDetail.pageSize} OFFSET ${dataBody.tableobj.pageDetail.pageSize * dataBody.tableobj.pageDetail.page} `

        let inserts = [config.tables.sopra_accno];
        sql = mysql.format(sql, inserts);
        total = mysql.format(total, inserts);

        let totalcount = await executeQuery(total);

        let results = await executeQuery(sql)
        return { data: results, totalcount: totalcount };

    }
    async getAllBatchno(dataBody) {

        let found = false;
        let index = 0;
        let sql = " SELECT * from ?? "
        let total = " SELECT count(*) count from ?? "
        if (dataBody.tableobj && dataBody.tableobj.where) {
            let whereParams = dataBody.tableobj.where;

            for (let keys in whereParams) {
                if (found) {
                    sql += ` and ${keys} =  '${whereParams[keys]}' `;
                    total += ` and ${keys} =  '${whereParams[keys]}' `;
                } else {
                    sql += ` WHERE ${keys} =  '${whereParams[keys]}' `;
                    total += ` WHERE ${keys} =  '${whereParams[keys]}' `;
                    found = true;
                }
            }
        }
        if (dataBody.tableobj && dataBody.tableobj.searchDetail) {
            let dataSearch = dataBody.tableobj.searchDetail;
            let SearchLength = Object.keys(dataSearch).length;
            for (let key in dataSearch) {
                if (found) {
                    if (index === 0) sql += ` and ( ${key} like ${"'%" + dataSearch[key] + "%'"} `, total += ` and ( ${key} like ${"'%" + dataSearch[key] + "%'"} `, index = index + 1;
                    else sql += ` or ${key} like ${"'%" + dataSearch[key] + "%'"} `, total += ` or ${key} like ${"'%" + dataSearch[key] + "%'"} `, index = index + 1;
                } else {
                    if (index === 0) sql += ` WHERE ${key} like ${"'%" + dataSearch[key] + "%'"} `, total += ` WHERE ${key} like ${"'%" + dataSearch[key] + "%'"} `, found = true, index = index + 1;
                }
                if (SearchLength === index) sql += ` ) `, total += ` ) `

            }
        }
        if (dataBody.tableobj && dataBody.tableobj.sortDetail) sql += ` ORDER BY ${dataBody.tableobj.sortDetail.field}  ${dataBody.tableobj.sortDetail.type} `

        if (dataBody.tableobj && dataBody.tableobj.pageDetail) sql += ` LIMIT ${dataBody.tableobj.pageDetail.pageSize} OFFSET ${dataBody.tableobj.pageDetail.pageSize * dataBody.tableobj.pageDetail.page} `

        let inserts = [config.tables.sopra_batchno];
        sql = mysql.format(sql, inserts);
        total = mysql.format(total, inserts);

        let totalcount = await executeQuery(total);

        let results = await executeQuery(sql)
        return { data: results, totalcount: totalcount };

    }
}
module.exports = Sopra_AccNo_m;