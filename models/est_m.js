const config = require('../config/config');
const executeQuery = require("../services/dbhelper/sqlhelper").executeQuery;
const mysql = require("mysql");
class EstimatedTime_m {
    constructor() {

    }
    getTime(data) {
        return new Promise((resolve, reject) => {

            let query = " select t.*,p.name, (select count(*)  from ?? t2 where t2.agent_id = t.agent_id and t2.id <= t.id and t2.status=0 ) "
            query += " as position from ?? t inner join ?? p on p.id =t.path_id where t.status =0 and t.id=" + data

            let update = [config.tables.queues, config.tables.queues, config.tables.paths];
            query = mysql.format(query, update)

            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    createStep(step, queueid) {
        return new Promise((resolve, reject) => {
            this.getStep(queueid).then((stepresults) => {
                if (stepresults.length === 0) {
                    let query = " DELETE FROM stepperformed WHERE expires_on < NOW(); INSERT INTO stepperformed (??,??,??) VALUES (?,?,DATE_ADD(NOW(), INTERVAL 600 SECOND)) "

                    let inserts = ['step', 'queueid', 'expires_on', step, queueid]

                    query = mysql.format(query, inserts)

                    executeQuery(query).then((results) => {
                        resolve(results);
                    }).catch((error) => {
                        reject(error);
                    });
                } else {
                    let query = " UPDATE stepperformed SET  step= ?, expires_on =DATE_ADD(NOW(), INTERVAL 600 SECOND) where queueid=?"

                    let inserts = [step, queueid]

                    query = mysql.format(query, inserts)

                    executeQuery(query).then((results) => {
                        resolve(results);
                    }).catch((error) => {
                        reject(error);
                    });
                }
            }).catch((err) => {
                reject(err);
            })

        });
    }
    getStep(data) {
        return new Promise((resolve, reject) => {

            let query = " select *  from stepperformed where  queueid = ?"

            let update = [data];
            query = mysql.format(query, update)

            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
}
module.exports = EstimatedTime_m;