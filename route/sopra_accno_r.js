
let express = require('express');
let router = express.Router();


module.exports = (app) => {
    let AccNo_sopra = require("../controller/sopra_accno_c");
    let accNo_sopra = new AccNo_sopra();

    router.get('/available/accno', app.oauth2.authorize, function (req, res) {
        accNo_sopra.GetAccNo(req, res);
    });
    router.post('/create/accno', app.oauth2.authorize, function (req, res) {
        accNo_sopra.CreateAccNo(req, res);
    });

    router.post('/create/CreateBatchAccNo', app.oauth2.authorize, function (req, res) {
        accNo_sopra.CreateBatchAccNo(req, res);
    });

    router.get('/all/accno', app.oauth2.authorize, function (req, res) {
        accNo_sopra.getAllaccno(req, res);
    });

    router.get('/all/batchno', app.oauth2.authorize, function (req, res) {
        accNo_sopra.getAllBatchno(req, res);
    });

    return router;
}