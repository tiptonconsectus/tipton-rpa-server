
let express = require('express');
let router = express.Router();


module.exports = (app) => {
    let rpa_management = require("../controller/rpa_c");
    let rpa = new rpa_management();

    router.get('/rpa-agent',app.oauth2.authorize, function (req, res) {
        rpa.Getrpa(req,res);
    });
    router.post('/getconnect',app.oauth2.authorize, function (req, res) {
        rpa.GetConnect(req,res);
    });
    router.post('/save_entry',app.oauth2.authorize, function (req, res) {
        rpa.SaveConnect(req,res);
    });

    router.get('/agent_detail/:id',app.oauth2.authorize, function (req, res) {
        rpa.GetAgentDetail(req,res);
    });  
    router.get('/update/agent/:id',app.oauth2.authorize, app.permission, function (req, res) {
        rpa.GetAgentUpdate(req,res);
    }); 
    router.post('/update/agent/:id',app.oauth2.authorize, app.permission, function (req, res) {
        rpa.AgentUpdate(req,res);
    }); 
    router.get('/deleteAgent/:id',app.oauth2.authorize, function (req, res) {
        rpa.DeleteAgent_c(req,res);
    }); 

   

    return router;
}


// let express = require('express');
// let router = express.Router();


// module.exports = (app) => {
//     let rpa_management = require("../controller/rpa_c");
//     let rpa = new rpa_management();

//     router.get('/rpa-agent',app.oauth2.authorize, app.permission, function (req, res) {
//         rpa.Getrpa(req,res);
//     });
//     router.post('/getconnect',app.oauth2.authorize, app.permission, function (req, res) {
//         rpa.GetConnect(req,res);
//     });
//     router.post('/save_entry',app.oauth2.authorize, app.permission, function (req, res) {
//         rpa.SaveConnect(req,res);
//     });

//     router.get('/agent_detail/:id',app.oauth2.authorize, app.permission, function (req, res) {
//         rpa.GetAgentDetail(req,res);
//     });  
//     router.get('/update/agent/:id',app.oauth2.authorize, app.permission, function (req, res) {
//         rpa.GetAgentUpdate(req,res);
//     }); 
//     router.post('/update/agent/:id',app.oauth2.authorize, app.permission, function (req, res) {
//         rpa.AgentUpdate(req,res);
//     }); 
//     router.get('/deleteAgent/:id',app.oauth2.authorize, app.permission, function (req, res) {
//         rpa.DeleteAgent_c(req,res);
//     }); 

   

//     return router;
// }