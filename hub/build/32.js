webpackJsonp([32],{

/***/ 565:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComplaintDetailsSubjectPageModule", function() { return ComplaintDetailsSubjectPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__complaint_details_subject__ = __webpack_require__(602);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ComplaintDetailsSubjectPageModule = (function () {
    function ComplaintDetailsSubjectPageModule() {
    }
    return ComplaintDetailsSubjectPageModule;
}());
ComplaintDetailsSubjectPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__complaint_details_subject__["a" /* ComplaintDetailsSubjectPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__complaint_details_subject__["a" /* ComplaintDetailsSubjectPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], ComplaintDetailsSubjectPageModule);

//# sourceMappingURL=complaint-details-subject.module.js.map

/***/ }),

/***/ 602:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComplaintDetailsSubjectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ComplaintDetailsSubjectPage = (function () {
    function ComplaintDetailsSubjectPage(navCtrl, navParams, oauthProvider, languageProvider, settingsProvider, toastCtrl, viewCtrl, alertCtrl, loadauditProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.settingsProvider = settingsProvider;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.loadauditProvider = loadauditProvider;
        this.new = false;
        this.canUpdate = false;
        this.complaintData = {
            subject: "",
            emailid: ""
        };
        this.resultdata = null;
        this.languageString = "";
        this.complaintList = navParams.get("complaintData");
        var self = this;
        if (!navParams.get('complaintData')) {
            //add adata
            self.complaintData = {
                "subject": null,
                "emailid": null
            };
            self.title = settingsProvider.getLabel("Update Complaint Details");
            self.new = true;
        }
        else {
            //View Data
            self.complaintData = JSON.parse(JSON.stringify(navParams.get('complaintData')));
            self.complaintDetails = self.complaintData;
            console.log("complaintData", this.complaintData);
            self.canUpdate = true;
            self.title = settingsProvider.getLabel("View Complaint Details");
        }
    }
    ComplaintDetailsSubjectPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ComplaintDetailsSubjectPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        var self = this;
        var message = "Viewed " + this.complaintData.emailid + " in  Complaint Details";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "view",
            "screen": "complaints-details-subject",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
        });
    };
    ComplaintDetailsSubjectPage.prototype.UpdateComplaintDetails = function () {
        var _this = this;
        var self = this;
        var updateContactSubject = {
            "contact_subject_id": self.complaintData.contact_subject_id,
            "subject": self.complaintData.subject,
            "emailid": self.complaintData.emailid.toLowerCase(),
            "modifiedby": 1,
            "isactive": 1
        };
        this.settingsProvider.updateContactSubject(updateContactSubject)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: 'Updated Successfully.',
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.pop();
        });
        var message = "Updated " + this.complaintData.emailid + " in  Complaint Details";
        var auditUser = {
            "users_id": 2,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "update",
            "screen": "complaints-details-subject",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Updated Audit Done"),
                duration: 10000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    ComplaintDetailsSubjectPage.prototype.saveComplaintDetails = function () {
        var _this = this;
        var self = this;
        console.log(self.complaintData.subject, "Tans");
        if (self.complaintData.subject == "" || self.complaintData.subject == null) {
            this.errorAlert("Please enter subject");
        }
        else if (self.complaintData.emailid == "" || self.complaintData.emailid == null) {
            this.errorAlert("Please enter Email Id");
        }
        else {
            self.complaintData = {
                "subject": self.complaintData.subject,
                "emailid": self.complaintData.emailid.toLowerCase(),
                "createdby": 1
            };
            this.settingsProvider.addcontactsubject(JSON.parse(JSON.stringify(self.complaintData)))
                .then(function (result) {
                result.subject = "";
                result.emailid = "";
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel("Complaint Detail added successfully"),
                    duration: 3000,
                    position: 'right',
                    cssClass: "success"
                });
                toast.present();
                self.viewCtrl.dismiss();
            });
        }
    };
    ComplaintDetailsSubjectPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    return ComplaintDetailsSubjectPage;
}());
ComplaintDetailsSubjectPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-complaint-details-subject',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\complaint-details-subject\complaint-details-subject.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel(\'Complaint Details Subject\')}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-cell">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row>\n\n                  <ion-col col-3>\n\n                    <ion-item>\n\n                      <ion-label stacked>Subject</ion-label>\n\n                      <ion-input type="text" [(ngModel)]="complaintData.subject" placeholder="{{settingsProvider.getLabel(\'Subject\')}}" [disabled]="!new"></ion-input>\n\n                    </ion-item>\n\n                  </ion-col>\n\n                  <ion-col col-4>\n\n                    <ion-item>\n\n                      <ion-label stacked>Email Id</ion-label>\n\n                      <ion-input type="email" [(ngModel)]="complaintData.emailid" placeholder="{{settingsProvider.getLabel(\'Email Id\')}}" [disabled]="!new"></ion-input>\n\n                    </ion-item>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\complaint-details-subject\complaint-details-subject.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */]])
], ComplaintDetailsSubjectPage);

//# sourceMappingURL=complaint-details-subject.js.map

/***/ })

});
//# sourceMappingURL=32.js.map