webpackJsonp([11],{

/***/ 585:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductApplicationPageModule", function() { return ProductApplicationPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_application__ = __webpack_require__(622);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProductApplicationPageModule = (function () {
    function ProductApplicationPageModule() {
    }
    return ProductApplicationPageModule;
}());
ProductApplicationPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__product_application__["a" /* ProductApplicationPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__product_application__["a" /* ProductApplicationPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], ProductApplicationPageModule);

//# sourceMappingURL=product-application.module.js.map

/***/ }),

/***/ 622:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductApplicationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_product_app_product_app__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_new_account_new_account__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ProductApplicationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProductApplicationPage = (function () {
    function ProductApplicationPage(navCtrl, navParams, viewCtrl, settingsProvider, productAppProvider, cdr, newAccountProvider, alertCtrl, oauthProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.settingsProvider = settingsProvider;
        this.productAppProvider = productAppProvider;
        this.cdr = cdr;
        this.newAccountProvider = newAccountProvider;
        this.alertCtrl = alertCtrl;
        this.oauthProvider = oauthProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.currentTab = "newCustomer";
        this.newCustomerOptions = {
            data: [],
            filteredData: [],
            columns: {
                "firstname": { title: "First Name", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "lastname": { title: "Last Name", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "newmobileno": { title: "Mobile Number", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "emailid": { title: "Email ID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "modified_date": { title: "Date", search: true, col: 1, format: 'function', fn: this.formatDateTime.bind(this) },
                "nationalinsuranceno": { title: "National Insurance", search: true, col: 2, format: 'function', fn: this.commonFunction.bind(this) },
                "nominatedbankno": { title: "Nominated Bank No.", search: true, col: 2, format: 'function', fn: this.commonFunction.bind(this) },
                "state_status": { title: "Status", search: true, col: 1 },
            },
            pagination: true,
            filter: null,
            search: true,
            select: true,
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editNewAccount.bind(this),
            uploadOptions: {},
        };
        this.existingCustomerOptions = {
            data: [],
            filteredData: [],
            columns: {
                "firstname": { title: "First Name", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "lastname": { title: "Last Name", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                // "moveindate": { title: "Move In Date", search: true, col: 1, format: 'function', fn: this.formatDateTime.bind(this) },
                // "moveoutdate": { title: "Move Out Date", search: true, col: 1, format: 'function', fn: this.formatDateTime.bind(this) },
                "mobileno": { title: "Mobile Number", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                // "postalcode": { title: "Postal Code", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "emailid": { title: "Email ID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "modified_date": { title: "Date", search: true, col: 1, format: 'function', fn: this.formatDateTime.bind(this) },
                "nationalinsuranceno": { title: "National Insurance", search: true, col: 2, format: 'function', fn: this.commonFunction.bind(this) },
                "nominatedbankno": { title: "Nominated Bank No.", search: true, col: 2, format: 'function', fn: this.commonFunction.bind(this) },
                "state_status": { title: "State Status", search: true, col: 1 },
            },
            pagination: true,
            filter: null,
            search: true,
            select: true,
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editExistingAccount.bind(this),
            uploadOptions: {},
        };
        this.accountMailerOptions = {
            data: [],
            filteredData: [],
            columns: {
                "memberid": { title: "Customer ID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "accountno": { title: "Account No.", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "firstname": { title: "First Name", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "lastname": { title: "Last Name", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "mobileno": { title: "Mobile No.", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "devicetype": { title: "Device Type", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "dob": { title: "DOB", format: 'function', fn: this.formatDateTime.bind(this), search: true, col: 1 },
                "shortuuid": { title: "UUID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "lastaccess": { title: "Date", format: 'function', fn: this.formatDateTime.bind(this), search: true, col: 1 },
                "state_status": { title: "OS", search: true, col: 1, },
                "mailer_status": { title: "Mailer Status", format: 'function', fn: this.commonFunctionSystemDfined.bind(this), search: true, col: 1 }
            },
            pagination: true,
            filter: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Generate Letter",
                    action: this.letter.bind(this),
                    color: 'success'
                },
                {
                    title: "Send Letter",
                    action: this.letterDelivered.bind(this),
                    color: 'success'
                }
            ],
            allActions: [
                {
                    title: "Download All Generated Letters",
                    action: this.allLetters.bind(this),
                    color: 'success'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.mailerAccount.bind(this),
            uploadOptions: {},
        };
    }
    ProductApplicationPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("products")) {
            return true;
        }
        else {
            return false;
        }
    };
    ProductApplicationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductApplicationPage');
        this.getnewcustomerlist();
    };
    ProductApplicationPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
    };
    ProductApplicationPage.prototype.commonFunction = function (nullData) {
        var str = "";
        if (nullData == 0) {
            str = '--';
        }
        else if (nullData == null) {
            str = '--';
        }
        else if (nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    // commonStatus(nullData) {
    //   let str = "";
    //   if (nullData == "Existing Mailer Drafted" || nullData == "New Customer Mailer Drafted") {
    //     str = '<img type="file" class="imgActiveCust" src ="assets/member_images/DownloadPdf.jpg" alt="">';
    //   } else {
    //     str = nullData
    //   }
    //   return (str);
    // }
    ProductApplicationPage.prototype.editNewAccount = function (productAppData) {
        productAppData.moveindate = this.formatDateTime(productAppData.moveindate);
        productAppData.moveoutdate = this.formatDateTime(productAppData.moveoutdate);
        productAppData.dob = this.formatDateTime(productAppData.dob);
        this.navCtrl.push("ProductAppDetailsPage", { productAppData: productAppData });
    };
    ProductApplicationPage.prototype.editExistingAccount = function (productAppData) {
        productAppData.moveindate = this.formatDateTime(productAppData.moveindate);
        productAppData.moveoutdate = this.formatDateTime(productAppData.moveoutdate);
        productAppData.dob = this.formatDateTime(productAppData.dob);
        this.navCtrl.push("ProductAppDetailsPage", { productAppData: productAppData });
    };
    ProductApplicationPage.prototype.segmentChanged = function (event) {
        console.log("event", event._value);
        switch (event._value) {
            case 'newCustomer':
                this.getnewcustomerlist();
                break;
            case 'existingCustomer':
                this.getproductapplicationdetails();
                break;
            case 'accountMailer':
                this.getnewaccountmailerlist();
                break;
            default:
                break;
        }
    };
    ProductApplicationPage.prototype.getnewcustomerlist = function () {
        var self = this;
        self.productAppProvider.getnewcustomerlist().then(function (result) {
            self.newCustomerOptions.data = result.data;
            self.newCustomerOptions = Object.assign({}, self.newCustomerOptions);
            self.cdr.detectChanges();
            self.nullData = result.data;
        });
    };
    ProductApplicationPage.prototype.getproductapplicationdetails = function () {
        var self = this;
        self.productAppProvider.getproductapplicationdetails().then(function (result) {
            self.existingCustomerOptions.data = result.data;
            self.existingCustomerOptions = Object.assign({}, self.existingCustomerOptions);
            self.cdr.detectChanges();
            self.nullData = result.data;
        });
    };
    ProductApplicationPage.prototype.getnewaccountmailerlist = function () {
        var self = this;
        self.newAccountProvider.getnewaccountmailerlist("product").then(function (result) {
            self.accountMailerOptions.data = result.data;
            self.accountMailerOptions = Object.assign({}, self.accountMailerOptions);
            self.cdr.detectChanges();
            self.nullData = result.data;
        });
    };
    ProductApplicationPage.prototype.letter = function (new_accounts_id) {
        var _this = this;
        var d = [];
        var self = this;
        for (var _i = 0, new_accounts_id_1 = new_accounts_id; _i < new_accounts_id_1.length; _i++) {
            var item = new_accounts_id_1[_i];
            d.push(item.new_accounts_id);
        }
        var newaccntid = d.join(',');
        self.newAccountProvider.generateLetter(newaccntid)
            .then(function (result) {
            console.log("Letter Response" + JSON.stringify(result));
            var alert = _this.alertCtrl.create({
                title: 'Letter Generation Alert',
                subTitle: result.status == true ? "Letter generated only for the user's with status 'New Customer Mailer Input' and 'New Existing Mailer'" : "Letter generation failed!",
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    ProductApplicationPage.prototype.mailerAccount = function (newAccountData) {
        if (newAccountData.state_status == "New Existing Mailer Drafted" || newAccountData.state_status == "New Customer Mailer Drafted") {
            if (newAccountData.generated_letter == undefined || newAccountData.generated_letter == null) {
            }
            else {
                var binary = atob(newAccountData.generated_letter.replace(/\s/g, ''));
                var len = binary.length;
                var buffer = new ArrayBuffer(len);
                var view = new Uint8Array(buffer);
                var j = void 0;
                for (j = 0; j < len; j++) {
                    view[j] = binary.charCodeAt(j);
                }
                // create the blob object with content-type "application/pdf"          
                var url = URL.createObjectURL(new Blob([view], { type: "application/pdf" }));
                window.open(url);
            }
        }
        else {
            this.navCtrl.push("NewAccountViewPage", { newAccountData: newAccountData });
        }
    };
    ProductApplicationPage.prototype.generateDownload = function (base64Str) {
        var url = URL.createObjectURL(base64Str);
        window.addEventListener('focus', function (e) { return URL.revokeObjectURL(url); });
    };
    ProductApplicationPage.prototype.letterDelivered = function (data) {
        var _this = this;
        console.log(data);
        var newaccntid = [];
        for (var i = 0; i < data.length; i++) {
            var obj = {
                "new_accounts_id": data[i].new_accounts_id,
                "state_status": data[i].state_status.substring(0, data[i].state_status.length - 8) + " Send"
            };
            newaccntid.push(obj);
        }
        console.log(newaccntid);
        var self = this;
        this.newAccountProvider.sendLetterMailer("/hub/changeletterstatus", newaccntid).
            then(function (data) {
            var alert = _this.alertCtrl.create({
                title: 'Status has been updated successfully',
                buttons: ['Dismiss']
            });
            alert.present();
        }).catch(function (err) {
        });
    };
    ProductApplicationPage.prototype.allLetters = function (action) {
        console.log(action);
        // let self = this;
        this.newAccountProvider.generateLetterForAll().then(function (result) {
            console.log(result.data);
            var data = result.data;
            var binary = atob(data.replace(/\s/g, ''));
            var len = binary.length;
            var buffer = new ArrayBuffer(len);
            var view = new Uint8Array(buffer);
            var j;
            for (j = 0; j < len; j++) {
                view[j] = binary.charCodeAt(j);
            }
            // create the blob object with content-type "application/pdf"               
            var url = URL.createObjectURL(new Blob([view], { type: "application/pdf" }));
            window.open(url);
        });
    };
    ProductApplicationPage.prototype.formatDateTime = function (date) {
        var str = "";
        if (date !== null && date !== "" && date !== undefined) {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            // newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
        else {
            str = '--';
            return (str);
        }
    };
    ProductApplicationPage.prototype.commonFunctionSystemDfined = function (data) {
        var str = "";
        if (data == "Existing Mailer" || data == "New Customer Mailer") {
            str = '<img class="imgUpload"  style="width:20%; height:20%" src ="assets/member_images/close.jpg" alt="">';
        }
        else if (data == "New Customer Mailer Send" || data == "Existing Mailer Send") {
            str = '<img class="imgUpload"  style="width:20%; height:20%" src ="assets/member_images/1398911.png" alt="">';
        }
        else {
            str = '<img class="imgUpload"  style="width:20%; height:20%" src ="assets/member_images/close.jpg" alt="">';
        }
        return (str);
    };
    return ProductApplicationPage;
}());
ProductApplicationPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-product-application',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\product-application\product-application.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n    <ion-title padding-vertical>Product Application | Consectus</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n    <ion-segment-button value="newCustomer">\n      New Customer Product Application\n    </ion-segment-button>\n    <ion-segment-button value="existingCustomer">\n      Existing Customer Product Application\n    </ion-segment-button>\n    <ion-segment-button value="accountMailer">\n      {{settingsProvider.getLabel(\'Account Mailer\')}}\n    </ion-segment-button>\n  </ion-segment>\n\n  <ion-grid class="table-cell" *ngIf="currentTab==\'newCustomer\'">\n    <ion-row>\n      <ion-col>\n        <table-component [options]="newCustomerOptions"></table-component>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid class="table-cell" *ngIf="currentTab==\'existingCustomer\'">\n    <ion-row>\n      <ion-col>\n        <table-component [options]="existingCustomerOptions"></table-component>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'accountMailer\'">\n    <ion-row>\n      <ion-col>\n        <ion-grid class="table-simple">\n          <ion-row>\n            <ion-col>\n              <table-component [options]="accountMailerOptions"></table-component>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\product-application\product-application.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_product_app_product_app__["a" /* ProductAppProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_4__providers_new_account_new_account__["a" /* NewAccountProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], ProductApplicationPage);

//# sourceMappingURL=product-application.js.map

/***/ })

});
//# sourceMappingURL=11.js.map