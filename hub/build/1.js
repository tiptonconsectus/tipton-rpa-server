webpackJsonp([1],{

/***/ 559:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPageModule", function() { return UserPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user__ = __webpack_require__(596);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var UserPageModule = (function () {
    function UserPageModule() {
    }
    return UserPageModule;
}());
UserPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__user__["a" /* UserPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__user__["a" /* UserPage */]),
            __WEBPACK_IMPORTED_MODULE_0__components_table_table_module__["a" /* TableComponentModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_3__user__["a" /* UserPage */]
        ]
    })
], UserPageModule);

//# sourceMappingURL=user.module.js.map

/***/ }),

/***/ 596:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_roles_user_roles__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_users_users__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var UserPage = (function () {
    function UserPage(languageProvider, cdr, alertCtrl, modalCtrl, viewCtrl, oauthProvider, navCtrl, navParams, userRolesProvider, usersProvider, toastCtrl, loadauditProvider, global, settingsProvider, masterpermissionProvider) {
        this.languageProvider = languageProvider;
        this.cdr = cdr;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.oauthProvider = oauthProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userRolesProvider = userRolesProvider;
        this.usersProvider = usersProvider;
        this.toastCtrl = toastCtrl;
        this.loadauditProvider = loadauditProvider;
        this.global = global;
        this.settingsProvider = settingsProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.new = false;
        this.canUpdate = false;
        // canAdd: boolean = false;
        this.languageString = "en";
        this.resultdata = null;
        this.userRoles = {
            super_admin: "Super Administrator",
            marketing: "Marketing Administrator",
            push: "Push Messaging",
            user_admin: "User Administrator",
            reports: "Reports",
            customer: "Customer Administrator"
        };
        this.user = {};
        this.columns = {
            "date": { title: "Date", format: "date" },
            "summary": { title: "Summary", search: true },
        };
        this.userOptions = {
            data: [],
            filteredData: [],
            columns: {
                "trail_date": { title: "Date", format: "date" },
                "trail_details": { title: "Trail Details", search: true },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            add: {
                addActionTitle: "",
                addAction: null,
            },
            rowActions: [],
            columnClass: "table-column",
            tapRow: this.showUser.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
        this.dirtyFlag = false;
        this.logs = [];
        this.email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        this.number_regex = /((07)|(7)|((\+|00)447)){1}[0-9]{9}\b/;
        this.pwd_regex = /(?=^.{6,}$)((?=.*\w)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[|!"$%&\/\(\)\?\^\'\\\+\-\*]))^.*/;
        var self = this;
        //changes by bipin
        this.callback = navParams.get('user');
        if (!navParams.get('user')) {
            this.user = {
                "id": new Date().getTime().toString(),
                "roleid": "selected",
                "name": "",
                "client_id": "consectus-api",
                "client_secret": "#xyz-@abc-$e3a-$#be-&@4e",
                // "passwd": "",
                "mobileno": "",
                "emailid": "",
                "password": "",
                "confirmpassword": ""
            };
            this.title = "Add Staff User";
            console.log(this.title);
        }
        else {
            this.title = "Update Staff User";
            self.user = JSON.parse(JSON.stringify(navParams.get('user')));
            self.usereditdata = JSON.parse(JSON.stringify(navParams.get('user')));
            self.canUpdate = true;
        }
    }
    UserPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("staffusers")) {
            return true;
        }
        else {
            return false;
        }
    };
    UserPage.prototype.ionViewDidLoad = function () {
        // if (!this.oauthProvider.user) {
        //   this.navCtrl.setRoot("LoginPage");
        // }else{
        var self = this;
        var message = "Viewed " + this.user.name + " in staff users ";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "view",
            "screen": "staff users",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
        });
        //  }
    };
    // ///// bipin ///////
    UserPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        var self = this;
        this.userRolesProvider.getUserRoles()
            .then(function (result) {
            console.log(result, "result");
            _this.rolesData = result.data;
            console.log(_this.rolesData, "rolesData");
        }).catch(function (error) {
        });
        if (self.user.users_id !== undefined) {
            this.loadauditProvider.getaudittrailbyuserid(self.user.users_id)
                .then(function (result) {
                console.log(result, "result");
                self.userOptions.data = result.data;
                self.userOptions = Object.assign({}, self.userOptions);
            });
        }
    };
    UserPage.prototype.RoleNameData = function () {
        this.user.roleid = this.selectedRoleData.user_roles_id;
        console.log(this.selectedRoleData, "selectedRoleData", this.user.roleid);
    };
    UserPage.prototype.selectRole = function (data) {
        this.selectedRoleData = data;
    };
    UserPage.prototype.dirty = function () {
        this.dirtyFlag = true;
    };
    UserPage.prototype.showUser = function () {
        console.log("showuser");
    };
    UserPage.prototype.getRoles = function () {
        if (this.user.roles.trim().length > 0)
            return this.user.roles.split(' ');
        else
            return [];
    };
    UserPage.prototype.deleteRole = function (role) {
        var roles = [];
        for (var _i = 0, _a = this.user.roles.split(' '); _i < _a.length; _i++) {
            var r = _a[_i];
            if (r != role)
                roles.push(r);
        }
        this.logs.push("removed role: " + role);
        this.user.roles = roles.join(' ');
        this.dirty();
    };
    UserPage.prototype.addRole = function () {
        var arr = {};
        for (var _i = 0, _a = Object.keys(this.userRoles); _i < _a.length; _i++) {
            var key = _a[_i];
            if (this.user.roles.indexOf(key) < 0)
                arr[key] = this.userRoles[key];
        }
        var self = this;
        var alert = this.alertCtrl.create();
        alert.setTitle('Add Role');
        for (var _b = 0, _c = Object.keys(arr); _b < _c.length; _b++) {
            var key = _c[_b];
            alert.addInput({
                type: 'checkbox',
                label: arr[key],
                value: key,
                checked: false
            });
        }
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                if (data.length > 0) {
                    self.logs.push("Roles added: " + data.join(' '));
                    if (self.user.roles.length > 0)
                        self.user.roles = self.user.roles.split(' ').concat(data).join(' ');
                    else
                        self.user.roles = data.join(' ');
                    self.dirty();
                    self.cdr.detectChanges();
                }
                else {
                    return false;
                }
            }
        });
        alert.present();
    };
    UserPage.prototype.resetPassword = function () {
        var self = this;
        var prompt = this.alertCtrl.create({
            title: self.settingsProvider.getLabel('Reset User Password'),
            message: "Please enter password of atleast 8 characters.",
            inputs: [
                {
                    name: 'Password',
                    placeholder: 'Enter Password',
                    type: 'password'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Reset Password',
                    handler: function (data) {
                        console.log('');
                        self.logs.push("Password Reset");
                        self.dirty();
                    }
                }
            ]
        });
        prompt.present();
    };
    UserPage.prototype.save = function () {
        var _this = this;
        var msg = [];
        //added by bipin ///
        if (this.user.password !== this.user.confirmpassword)
            msg.push(this.settingsProvider.getLabel("confirm password does not match"));
        //// end //
        var validemail = this.email_regex.test(this.user.emailid.trim());
        var validnumber = this.number_regex.test(this.user.mobileno);
        var validPwd = this.pwd_regex.test(this.user.passwd);
        if (this.user.roleid === "selected" || this.user.roleid === undefined)
            msg.push(this.settingsProvider.getLabel("At least one role must be specified"));
        if (this.user.rolename <= 0)
            msg.push(this.settingsProvider.getLabel("At least one role must be specified"));
        if (this.user.name.trim().length <= 0)
            msg.push(this.settingsProvider.getLabel("User Name must be specified"));
        if (this.user.passwd.length <= 0) {
            msg.push(this.settingsProvider.getLabel("Password must be specified"));
        }
        // else {
        //   if (validPwd === true) {
        //     console.log("valid")
        //   } else {
        //     msg.push(this.settingsProvider.getLabel("Minimum length of 6, at least one uppercase letter, at least one lowercase letter, at least one number, at least one special character"));
        //   }
        // }
        if (this.user.mobileno.length <= 0) {
            msg.push(this.settingsProvider.getLabel("Mobile Number must be specified"));
        }
        else {
            if (validnumber === true) {
                console.log("valid");
            }
            else {
                msg.push(this.settingsProvider.getLabel("Invalid Mobile Format, Enter a valid Mobile Number"));
            }
        }
        if (this.user.emailid.trim().length <= 0) {
            msg.push(this.settingsProvider.getLabel("Email address must be specified"));
        }
        else {
            if (validemail === true) {
                console.log("valid");
            }
            else {
                msg.push(this.settingsProvider.getLabel("Invalid Email Id Format, Enter a valid Email Id"));
            }
        }
        if (msg.length > 0) {
            var alert_1 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel('Error'),
                message: msg.join("<br />"),
                buttons: [
                    {
                        text: this.settingsProvider.getLabel('OK'),
                        role: 'cancel',
                        handler: function (data) {
                        }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            var self_1 = this;
            var message = "added " + this.user.name + " in staff users ";
            var auditUser = {
                "users_id": this.oauthProvider.user[0].users_id,
                "customer_devices_id": 0,
                "usertype": "staff",
                "uuid": "",
                "appname": "hub",
                "event_type": "add",
                "screen": "staff users",
                "trail_details": message
            };
            this.loadauditProvider.loadaudit(auditUser)
                .then(function (data) {
                var toast = self_1.toastCtrl.create({
                    message: _this.settingsProvider.getLabel("Add Audit Done"),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
            });
            this.usersProvider.adduser(this.user)
                .then(function (data) {
                var toast = self_1.toastCtrl.create({
                    message: 'Updated Successfully.',
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                self_1.navCtrl.pop();
            });
        }
    };
    UserPage.prototype.changePwd = function () {
        this.navCtrl.push("ChangePasswordPage", { userId: this.user.users_id });
    };
    UserPage.prototype.updateUser = function () {
        var _this = this;
        var self = this;
        this.usersProvider.updateuser(this.user)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel("User Access Updated Successfully"),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.pop();
        });
        var message = "Updated " + this.user.name + " details in staff users ";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "update",
            "screen": "staff users",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Updated Audit Done"),
                duration: 10000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    UserPage.prototype.block = function () {
        var _this = this;
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel('Block the User'),
            message: 'Are you sure you want to Block this user Access.',
            buttons: [
                {
                    text: self.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                    handler: function (data) {
                    }
                },
                {
                    text: self.settingsProvider.getLabel('Block'),
                    handler: function (data) {
                        self.user.status = "BLOCKED";
                        self.callback(self.user, self.settingsProvider.getLabel('Blocked User') + " [" + _this.user.name + "]")
                            .then(function () {
                            self.navCtrl.pop();
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    UserPage.prototype.activate = function () {
        var _this = this;
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel('Activate the User'),
            message: self.settingsProvider.getLabel('Are you sure you want to Activate this user\' Access.'),
            buttons: [
                {
                    text: self.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                    handler: function (data) {
                    }
                },
                {
                    text: self.settingsProvider.getLabel('Activate'),
                    handler: function (data) {
                        self.user.status = "ACTIVE";
                        self.callback(self.user, self.settingsProvider.getLabel('Activated User') + " [" + _this.user.name + "]")
                            .then(function () {
                            self.navCtrl.pop();
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    UserPage.prototype.delete = function () {
        var self = this;
        var log = self.settingsProvider.getLabel("Deleted User") + " [" + this.user.name + "]";
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel('Confirm Delete'),
            message: self.settingsProvider.getLabel('Are you sure you want to Delete this user?'),
            buttons: [
                {
                    text: self.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                    handler: function (data) {
                    }
                },
                {
                    text: self.settingsProvider.getLabel('Delete'),
                    handler: function (data) {
                        self.callback(self.user, log, true)
                            .then(function () {
                            self.navCtrl.pop();
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    UserPage.prototype.checkemailid = function () {
        if (!this.canUpdate || this.usereditdata.emailid !== this.user.emailid) {
            var self_2 = this;
            this.usersProvider.checkEmailid("/accounts/checkemailid", { "emailid": this.user.emailid })
                .then(function (result) {
                if (result.status == false) {
                    var prompt_1 = self_2.alertCtrl.create({
                        title: "Emaild d already in use",
                        message: "Please enter different email id.",
                        buttons: [
                            {
                                text: 'Ok',
                                handler: function (data) {
                                    console.log('Cancel clicked');
                                }
                            }
                        ]
                    });
                    prompt_1.present();
                }
                console.log(result);
            }).catch(function (err) {
            });
        }
    };
    UserPage.prototype.checkmobileno = function () {
        if (!this.canUpdate || this.usereditdata.mobileno !== this.user.mobileno) {
            var self_3 = this;
            this.usersProvider.checkEmailid("/accounts/checkmobileno", { "mobileno": this.user.mobileno })
                .then(function (result) {
                if (result.status == false) {
                    var prompt_2 = self_3.alertCtrl.create({
                        title: "Mobile no already in use",
                        message: "Please enter different Mobile no.",
                        buttons: [
                            {
                                text: 'Ok',
                                handler: function (data) {
                                    console.log('Cancel clicked');
                                }
                            }
                        ]
                    });
                    prompt_2.present();
                }
                console.log(result);
            }).catch(function (err) {
            });
        }
    };
    return UserPage;
}());
UserPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* IonicPage */])({ name: "UserPage" }),
    Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["n" /* Component */])({
        selector: 'page-user',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\user\user.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row *ngIf="!canUpdate">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="save()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row *ngIf="canUpdate">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="updateUser()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col col-5>\n\n        <ion-card padding>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Role Name\')}}</ion-label>\n\n              <ion-select [(ngModel)]="user.roleid" name="rolename" multiple="false" (ionChange)="RoleNameData()">\n\n                <ion-option value="selected">Select Role</ion-option>\n\n                <ion-option *ngFor="let obj of rolesData" value="{{ obj.user_roles_id }}" (ionSelect)="selectRole(obj)">\n\n                  {{ obj.rolename }}</ion-option>\n\n              </ion-select>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <!-- <ion-row *ngIf="user.name !== null && user.name !== \'\'"> -->\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Full Name\')}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="user.name" (input)="dirty()"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <!-- \n\n          <ion-row *ngIf="user.mobileno !== null && user.mobileno !== \'\'"> -->\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Mobile Number\')}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="user.mobileno" (focusout)="checkmobileno()" (input)="dirty()">\n\n              </ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row >\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Email\')}}</ion-label>\n\n              <ion-input type="email" [(ngModel)]="user.emailid" (focusout)="checkemailid()" (input)="dirty()">\n\n              </ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n  \n\n          <ion-row *ngIf="canUpdate===false">\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Password\')}}</ion-label>\n\n              <ion-input type="password" [(ngModel)]="user.password" (input)="dirty()"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row *ngIf="canUpdate===false">\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Confirm Password\')}}</ion-label>\n\n              <ion-input type="password" [(ngModel)]="user.confirmpassword" (input)="dirty()"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row *ngIf="canUpdate" >\n\n            <ion-col>\n\n              <button ion-button small *ngIf="canUpdate" (click)="changePwd()">\n\n                <span class="button-text">{{settingsProvider.getLabel(\'Change Password\')}}</span>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col *ngIf="!new" col-7>\n\n        <ion-card padding>\n\n          <ion-card-header>User Activity</ion-card-header>\n\n          <table-component [options]="userOptions"></table-component>\n\n        </ion-card>\n\n\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\user\user.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_3__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__providers_user_roles_user_roles__["a" /* UserRolesProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_users_users__["a" /* UsersProvider */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_1__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], UserPage);

//# sourceMappingURL=user.js.map

/***/ })

});
//# sourceMappingURL=1.js.map