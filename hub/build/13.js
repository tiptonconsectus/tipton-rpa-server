webpackJsonp([13],{

/***/ 583:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnboardingSetupPageModule", function() { return OnboardingSetupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__onboarding_setup__ = __webpack_require__(620);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var OnboardingSetupPageModule = (function () {
    function OnboardingSetupPageModule() {
    }
    return OnboardingSetupPageModule;
}());
OnboardingSetupPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__onboarding_setup__["a" /* OnboardingSetupPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__onboarding_setup__["a" /* OnboardingSetupPage */]),
        ],
    })
], OnboardingSetupPageModule);

//# sourceMappingURL=onboarding-setup.module.js.map

/***/ }),

/***/ 620:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnboardingSetupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_onboarding_setup_onboarding_setup__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_settings_settings__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var OnboardingSetupPage = (function () {
    function OnboardingSetupPage(navCtrl, navParams, oauthProvider, languageProvider, onboardingSetupProvider, toastCtrl, settingsProvider, alertCtrl, cdr) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.onboardingSetupProvider = onboardingSetupProvider;
        this.toastCtrl = toastCtrl;
        this.settingsProvider = settingsProvider;
        this.alertCtrl = alertCtrl;
        this.cdr = cdr;
        this.currentTab = "registrationfield";
        this.onboard = {
            ismailerforexisting: "",
            ismailerfornewexisting: "",
            isregisterforexisting: "",
            isregisterfornewexisting: ""
        };
        this.register = {
            registrationfields_id: "",
            firstname: "",
            lastname: "",
            dob: ""
        };
        this.login = {
            istouchid: "",
            ismaxtouchid: ""
        };
        this.touchID = false;
    }
    OnboardingSetupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OnboardingSetupPage');
        this.getregistrationfieldsv1();
    };
    OnboardingSetupPage.prototype.ionViewDidEnter = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    OnboardingSetupPage.prototype.getonboardingprocess = function () {
        var self = this;
        // this.globalProvider.showLoader();
        this.onboardingSetupProvider.getonboardingprocess()
            .then(function (result) {
            if (result.data) {
                // this.globalProvider.hideLoader();
                self.onboard = result.data;
            }
        }).catch(function (error) {
            // this.globalProvider.hideLoader();
        });
    };
    OnboardingSetupPage.prototype.getregistrationfieldsv1 = function () {
        var self = this;
        // this.globalProvider.showLoader();
        this.onboardingSetupProvider.getregistrationfieldsv1()
            .then(function (result) {
            if (result.data) {
                // this.globalProvider.hideLoader();
                self.register = result.data;
            }
        }).catch(function (error) {
            // this.globalProvider.hideLoader();
        });
    };
    OnboardingSetupPage.prototype.getloginprocess = function () {
        var self = this;
        // this.globalProvider.showLoader();
        this.onboardingSetupProvider.getloginprocess()
            .then(function (result) {
            if (result.data) {
                // this.globalProvider.hideLoader();
                if (result.data.ismaxtouchid == 999999) {
                    result.data.ismaxtouchid = 0;
                }
                result.data.istouchid ? self.touchID = true : self.touchID = false;
                self.login = result.data;
            }
        }).catch(function (error) {
            // this.globalProvider.hideLoader();
        });
    };
    OnboardingSetupPage.prototype.updateloginprocess = function () {
        var _this = this;
        var self = this;
        // if (self.login.ismaxtouchid == "" || self.login.ismaxtouchid == null) {
        //   this.errorAlert(this.settingsProvider.getLabel("Please enter value of MaxTouchID"));
        // } else {
        console.log(self.touchID, "TouchiD");
        var loginData = {
            istouchid: this.login.istouchid,
            ismaxtouchid: this.login.ismaxtouchid
        };
        self.onboardingSetupProvider.updateloginprocess(loginData).then(function (result) {
            console.log("result accounttype", result);
            if (result.status) {
                var toast = self.toastCtrl.create({
                    message: result.msg,
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                _this.getloginprocess();
            }
        });
    };
    OnboardingSetupPage.prototype.TouchEnable = function () {
        this.touchID ? this.login.istouchid = 1 : this.login.istouchid = 0;
    };
    // getnewaccountmailerlist() {
    //   let self = this;
    //   self.newAccountProvider.getnewaccountmailerlist().then((result: any) => {
    //     self.accountMailerOptions.data = result.data;
    //     self.accountMailerOptions = Object.assign({}, self.accountMailerOptions);
    //     self.cdr.detectChanges();
    //   });
    // }
    OnboardingSetupPage.prototype.segmentChanged = function (event) {
        console.log("eventValue", event._value);
        switch (event._value) {
            case 'registrationfield':
                this.getregistrationfieldsv1();
                break;
            case 'onboarding':
                this.getonboardingprocess();
                break;
            case 'loginProcess':
                this.getloginprocess();
                break;
            default:
                break;
        }
    };
    OnboardingSetupPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: this.settingsProvider.getLabel('Ok'),
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    return OnboardingSetupPage;
}());
OnboardingSetupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-onboarding-setup',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\onboarding-setup\onboarding-setup.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <!-- <ion-title>{{languageProvider.get(title)}}</ion-title> -->\n\n    <ion-title>Onboarding Setup</ion-title>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n    <ion-segment-button value="registrationfield">\n\n      {{languageProvider.get(\'Registration Fields\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="onboarding">\n\n      {{languageProvider.get(\'Onboarding Process\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="loginProcess">\n\n      {{languageProvider.get(\'Login Process\')}}\n\n    </ion-segment-button>\n\n  </ion-segment>\n\n\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'registrationfield\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{languageProvider.get(\'Registration Id\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="register.registrationfields_id" placeholder="{{languageProvider.get(\'Registration Id\')}}"\n\n                  (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{languageProvider.get(\'First Name\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="register.firstname" placeholder="{{languageProvider.get(\'First Name\')}}" (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{languageProvider.get(\'Last Name\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="register.lastname" placeholder="{{languageProvider.get(\'Last Name\')}}"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{languageProvider.get(\'DOB\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="register.dob" placeholder="{{languageProvider.get(\'DOB\')}}"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'onboarding\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{languageProvider.get(\'Mailer for Existing Customer\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="onboard.ismailerforexisting" placeholder="{{languageProvider.get(\'Mailer for Existing Customer\')}}"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{languageProvider.get(\'Mailer for New Existing Customer\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="onboard.ismailerfornewexisting" placeholder="{{languageProvider.get(\'Mailer for New Existing Customer\')}}"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{languageProvider.get(\'Register for existing Customer\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="onboard.isregisterforexisting" placeholder="{{languageProvider.get(\'Register for existing Customer\')}}"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{languageProvider.get(\'Register for new Existing\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="onboard.isregisterfornewexisting" placeholder="{{languageProvider.get(\'Register for new Existing\')}}"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'loginProcess\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="updateloginprocess()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label>{{settingsProvider.getLabel(\'Touch ID\')}}</ion-label>\n\n                <ion-checkbox [(ngModel)]="touchID" (ionChange)="TouchEnable()"></ion-checkbox>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{languageProvider.get(\'Maximum Touch ID\')}}</ion-label>\n\n                <ion-input type="text" [(ngModel)]="login.ismaxtouchid" placeholder="{{languageProvider.get(\'Max Touch ID\')}}"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\onboarding-setup\onboarding-setup.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_onboarding_setup_onboarding_setup__["a" /* OnboardingSetupProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */]])
], OnboardingSetupPage);

//# sourceMappingURL=onboarding-setup.js.map

/***/ })

});
//# sourceMappingURL=13.js.map