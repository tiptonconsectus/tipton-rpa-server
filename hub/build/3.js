webpackJsonp([3],{

/***/ 593:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateagentPageModule", function() { return UpdateagentPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__updateagent__ = __webpack_require__(630);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UpdateagentPageModule = (function () {
    function UpdateagentPageModule() {
    }
    return UpdateagentPageModule;
}());
UpdateagentPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__updateagent__["a" /* UpdateagentPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__updateagent__["a" /* UpdateagentPage */]),
        ],
    })
], UpdateagentPageModule);

//# sourceMappingURL=updateagent.module.js.map

/***/ }),

/***/ 630:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateagentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the UpdateagentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UpdateagentPage = (function () {
    function UpdateagentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    UpdateagentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UpdateagentPage');
    };
    return UpdateagentPage;
}());
UpdateagentPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-updateagent',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\updateagent\updateagent.html"*/'<!--\n\n  Generated template for the UpdateagentPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>updateagent</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-grid *ngIf="currentTab == \'RPAAgentManagement\'">\n\n        <ion-card>\n\n          <ion-row style="padding : 10px;">\n\n            <ion-col col-4>\n\n              <ion-label stacked >IP Address</ion-label>\n\n              <ion-input style="font-size:15px !important;" type="text" placeholder="Enter IP Address" [(ngModel)]="ipaddress"></ion-input>\n\n            </ion-col>\n\n    \n\n            <ion-col col-2>\n\n              <ion-label stacked >Port</ion-label>\n\n              <ion-input style="font-size:15px !important;" type="text" placeholder="Enter Port" [(ngModel)]="port">\n\n              </ion-input>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n              <br>\n\n              <br>\n\n              <!-- <button ion-button Default round icon-left color="primary" (click)="pingAgent()">connect</button> -->\n\n              <!-- <button ion-button color="blue" shape="round" fill="outline" size="small">Connect</button> -->\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n        <ion-card *ngIf="enableAgentForm==true">\n\n              <ion-card-header>\n\n                <h1 style="color : black; margin : 10px;">Save Agent Setting</h1>\n\n                <hr>\n\n              </ion-card-header>\n\n            <ion-row style="padding : 10px;">\n\n              <ion-col col-4>\n\n                  <ion-label stacked>Agent Name</ion-label>\n\n                  <ion-input  type="text" placeholder="Enter Agent Name" [(ngModel)]="name">\n\n                  </ion-input>\n\n              </ion-col>\n\n              <ion-col col-2>\n\n                  <ion-label stacked>Set Sikuli TimeOut</ion-label>\n\n                  <ion-input type="text" placeholder="Enter Set Sikuli TimeOut" [(ngModel)]="timeout">\n\n                  </ion-input>\n\n              </ion-col>\n\n            </ion-row>\n\n            <ion-row style="padding : 10px;">\n\n                <ion-col col-4>\n\n                    <ion-label stacked >Admin Email</ion-label>\n\n                    <ion-input  type="text" placeholder="Enter Admin Email" [(ngModel)]="adminemail">\n\n                    </ion-input>\n\n                </ion-col>\n\n                <ion-col col-2>\n\n                    <ion-label stacked>Set Sikuli MatchScore</ion-label>\n\n                    <ion-input type="text" placeholder="Enter Set Sikuli MatchScore" [(ngModel)]="score">\n\n                    </ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col col-sm-12 col-md-12 col-lg-12>\n\n                    <!-- <button ion-button Default round icon-left color="primary" (click)="saveAgent()">Save</button> -->\n\n                </ion-col>\n\n              </ion-row>\n\n        </ion-card>\n\n        <ion-card>\n\n          <div style="padding : 10px;">\n\n            <table-component [options]="rpaList"></table-component>\n\n            <!-- <table-component></table-component> -->\n\n          </div>\n\n        </ion-card>\n\n      </ion-grid>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\updateagent\updateagent.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], UpdateagentPage);

//# sourceMappingURL=updateagent.js.map

/***/ })

});
//# sourceMappingURL=3.js.map