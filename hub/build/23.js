webpackJsonp([23],{

/***/ 573:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageModule", function() { return ForgotPasswordPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forgot_password__ = __webpack_require__(610);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ForgotPasswordPageModule = (function () {
    function ForgotPasswordPageModule() {
    }
    return ForgotPasswordPageModule;
}());
ForgotPasswordPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__forgot_password__["a" /* ForgotPasswordPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__forgot_password__["a" /* ForgotPasswordPage */]),
        ],
    })
], ForgotPasswordPageModule);

//# sourceMappingURL=forgot-password.module.js.map

/***/ }),

/***/ 610:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ForgotPasswordPage = (function () {
    function ForgotPasswordPage(navCtrl, navParams, alertCtrl, oauthProvider, languageProvider, toastCtrl, global, loadauditProvider, settingsProvider, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.toastCtrl = toastCtrl;
        this.global = global;
        this.loadauditProvider = loadauditProvider;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.emailId = "";
        this.ispassword = false;
        this.resultdata = null;
        this.languageString = "en";
    }
    ForgotPasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgotPasswordPage');
    };
    ForgotPasswordPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        this.ispassword = this.navParams.get("ispassword");
        if (!this.ispassword) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    ForgotPasswordPage.prototype.send = function () {
        var _this = this;
        var self = this;
        console.log("thisemailId", this.emailId);
        self.oauthProvider.resetpassword(this.emailId)
            .then(function (result) {
            if (result.status) {
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel('Password reset link has been sent to your registered email id'),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                _this.emailId = "";
            }
            else {
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel('User not found, Please enter your registered email-id'),
                    duration: 3000,
                    cssClass: 'success',
                    position: 'right'
                });
                toast.present();
            }
        }).catch(function (error) {
            console.log("error", error);
        });
    };
    return ForgotPasswordPage;
}());
ForgotPasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-forgot-password',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\forgot-password\forgot-password.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel(\'Forgot Password | Consectus\')}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div class="valign-center">\n\n    <!-- <div class="login-logo" text-center>\n\n      <img src="assets/images/logo-blue(2).png" height="58">\n\n    </div> -->\n\n    <ion-card class="login">\n\n      <ion-card-content padding>\n\n        <ion-grid>\n\n          <ion-row>\n\n            <ion-col col-12 margin-bottom>\n\n              <b style="font-weight: 400;">{{settingsProvider.getLabel("Please enter your email address. You will recieve a link to create a new password\n\n                via email.")}}</b>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked style="font-size:20px !important;">{{settingsProvider.getLabel("Email")}}</ion-label>\n\n              <ion-input type="email" [(ngModel)]="emailId" placeholder="{{settingsProvider.getLabel(\'Email\')}}"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <button ion-button float-left round (click)="send()">\n\n                <span class="button-text">{{settingsProvider.getLabel("Reset")}}</span>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  </div>\n\n</ion-content>\n\n\n\n<!-- <ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-cell">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row>\n\n                  <ion-col col-12 margin-bottom margin-left>\n\n                    <b style="font-weight: 400;">{{settingsProvider.getLabel("Enter Your Email address and we will send you a link to reset your password")}}</b>\n\n                  </ion-col>\n\n                  <ion-col col-4>\n\n                    <ion-item>\n\n                      <ion-label stacked>{{settingsProvider.getLabel("Email")}}</ion-label>\n\n                      <ion-input type="email" [(ngModel)]="emailId" placeholder="{{settingsProvider.getLabel(\'Email\')}}"></ion-input>\n\n                    </ion-item>\n\n                  </ion-col>\n\n                  <ion-col col-12>\n\n                    <ion-item>\n\n                      <button ion-button small default (click)="send()">{{settingsProvider.getLabel("Send")}}</button>\n\n                    </ion-item>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content> -->'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\forgot-password\forgot-password.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
], ForgotPasswordPage);

//# sourceMappingURL=forgot-password.js.map

/***/ })

});
//# sourceMappingURL=23.js.map