webpackJsonp([14],{

/***/ 581:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsPageModule", function() { return NewsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__news__ = __webpack_require__(618);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NewsPageModule = (function () {
    function NewsPageModule() {
    }
    return NewsPageModule;
}());
NewsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__news__["a" /* NewsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__news__["a" /* NewsPage */]),
        ],
    })
], NewsPageModule);

//# sourceMappingURL=news.module.js.map

/***/ }),

/***/ 618:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_news_news__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var NewsPage = (function () {
    function NewsPage(navCtrl, navParams, languageProvider, oauthProvider, toastCtrl, alertCtrl, newsProvider, viewCtrl, settingProvider, cdr, domSanitizer, settingsProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.newsProvider = newsProvider;
        this.viewCtrl = viewCtrl;
        this.settingProvider = settingProvider;
        this.cdr = cdr;
        this.domSanitizer = domSanitizer;
        this.settingsProvider = settingsProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.title = "Marketing News Management";
        this.deviceType = 'ios';
        this.languageString = "en";
        if (!this.navParams.get('news')) {
            this.news = {
                title: "",
                body: "",
                image: null,
                action: ""
            };
            this.title = "Add Marketing News Management";
            this.new = true;
        }
        else {
            this.news = JSON.parse(JSON.stringify(navParams.get('news')));
            this.new = false;
            this.title = "Update Marketing News Management";
        }
    }
    NewsPage.prototype.ionViewCanEnter = function () {
        if (this.masterpermissionProvider.permission("default")) {
            return true;
        }
        else {
            return false;
        }
    };
    NewsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FaqsPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        else {
            this.updatePreview();
        }
    };
    NewsPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        this.updatePreview();
    };
    NewsPage.prototype.removeImage = function () {
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel('Confirm Delete'),
            message: self.settingsProvider.getLabel('Are you sure you want to delete Image?'),
            buttons: [
                {
                    text: self.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function (data) {
                        self.news.image = null;
                        self.dirty();
                    }
                }
            ]
        });
        alert.present();
    };
    NewsPage.prototype.dirty = function () {
        this.dirtyFlag = true;
        this.updatePreview();
    };
    NewsPage.prototype.addUpdateNews = function (news) {
        var self = this;
        if (this.news.title == "") {
            this.errorAlert(this.settingsProvider.getLabel("Please enter a news title"));
        }
        else if (this.news.body == "") {
            this.errorAlert(this.settingsProvider.getLabel("please enter a News description"));
        }
        else if (this.news.action == "") {
            this.errorAlert(this.settingsProvider.getLabel("News action should not be blank"));
        }
        else {
            if (this.new) {
                return new Promise(function (resolve, reject) {
                    self.newsProvider.createNews(JSON.parse(JSON.stringify(self.news)))
                        .then(function () {
                        self.news.title = "";
                        self.news.body = "";
                        self.viewCtrl.dismiss();
                        resolve();
                    });
                });
            }
            else {
                return new Promise(function (resolve, reject) {
                    self.newsProvider.updateNews(JSON.parse(JSON.stringify(self.news)))
                        .then(function () {
                        var toast = self.toastCtrl.create({
                            message: self.settingsProvider.getLabel('News updated successfully'),
                            duration: 3000,
                            position: 'right',
                            cssClass: "success"
                        });
                        self.viewCtrl.dismiss();
                        toast.present();
                        resolve();
                    });
                });
            }
        }
    };
    NewsPage.prototype.updatePreview = function () {
        var _this = this;
        this.settingProvider.getBankLookAndFeel().then(function (lookandfeel) {
            var color = lookandfeel.primary_color;
            var image = "";
            if (_this.news.image) {
                image = "<img src=\"" + _this.news.image + "\" width=\"278px\">";
            }
            var title = "";
            if (_this.news.title) {
                title = "<h1>" + _this.news.title + "</h1>";
            }
            var body = "";
            if (_this.news.body) {
                body = "<p>" + _this.news.body + "</p>";
            }
            switch (_this.deviceType) {
                case 'ios':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n          <html>\n            <head>\n              <style>\n                @font-face {\n                  font-family: 'Ionicons';\n                  src: url(assets/fonts/ionicons.woff2);\n                }\n                body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n                .nav-bar{position:fixed; height: 60px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n                .logo{height: 50%; width: auto;}\n                .content{position:fixed;height: 438px; overflow: scroll; margin-top: 60px; min-width: 300px;}\n                .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n                .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n                .tab:not(:last-child){border-right: 1px solid #fff}\n                .icon {\n                  margin-top: 5px;\n                  width: inherit;\n                  font-size: 1.4em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                  height: 30px;\n                }\n                .tab-title {\n                  position: absolute;\n                  bottom: 5px;\n                  width: inherit;\n                  font-size: 0.7em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                }\n                div.icon-cash:before{ content: \"\\f143\"; }\n                div.icon-pin:before{ content: \"\\f1e4\";}\n                div.icon-pricetags:before{ content: \"\\f48e\"; }\n                div.icon-text:before{ content: \"\\f24f\"; }\n                div.icon-settings::before{ content: \"\\f20d\"; }\n                .padding{ padding: 10px; }\n                .card {border: 1px solid #dedede; overflow: hidden; }\n                h1, h2, p{color: " + color + " }\n                h1{font-size: 1.5em; font-weight: normal;}\n                h2{font-size: 1.2em; font-weight: normal;}\n                button{\n                  padding: 5px;\n                  border-radius: 10px;\n                  background-color: " + color + ";\n                  color: white;\n                  display: block;\n                }\n              </style>\n            </head>\n            <body>\n              <div class=\"nav-bar\">\n                <img class=\"statusbar-img \" src=\"assets/images/ios-statusbar.png\" width=\"320\">\n                <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n              </div>\n              <div class=\"content padding\">\n                <div class=\"card padding\">\n                    " + image + "\n                    " + title + "              \n                    " + body + "\n                </div>\n              </div>\n            </body>\n            </html>\n          ");
                    break;
                case 'android':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n          <html>\n            <head>\n\n              <style>\n                @font-face {\n                  font-family: 'Ionicons';\n                  src: url(assets/fonts/ionicons.woff2);\n                }\n                body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n                .nav-bar{position:fixed; height: 70px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n                .logo{height: 50%; width: auto;}\n                .content{position:fixed;height: 438px; overflow: scroll; margin-top: 70px; min-width: 300px;}\n                .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n                .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n                .tab:not(:last-child){border-right: 1px solid #fff}\n                .icon {\n                  margin-top: 5px;\n                  width: inherit;\n                  font-size: 1.4em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                  height: 30px;\n                }\n                .tab-title {\n                  position: absolute;\n                  bottom: 5px;\n                  width: inherit;\n                  font-size: 0.7em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                }\n                div.icon-cash:before{ content: \"\\f143\"; }\n                div.icon-pin:before{ content: \"\\f1e4\";}\n                div.icon-pricetags:before{ content: \"\\f48e\"; }\n                div.icon-text:before{ content: \"\\f24f\"; }\n                div.icon-settings::before{ content: \"\\f20d\"; }\n                .padding{ padding: 10px; }\n                .card {border: 1px solid #dedede; overflow: hidden; }\n                h1, h2, p{color: " + color + " }\n                h1{font-size: 1.5em; font-weight: normal;}\n                h2{font-size: 1.2em; font-weight: normal;}\n                button{\n                  padding: 5px;\n                  border-radius: 10px;\n                  background-color: " + color + ";\n                  color: white;\n                  display: block;\n                }\n              </style>\n            </head>\n            <body>\n              <div class=\"nav-bar\">\n                <img class=\"statusbar-img \" src=\"assets/images/android-statusbar.png\" width=\"320\">\n                <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n              </div>\n              <div class=\"content padding\">\n                <div class=\"card padding\">\n                  " + image + "\n                  " + title + "\n                  " + body + "\n                </div>\n              </div>\n              <div class=\"tab-bar\">\n                <div class=\"tab\">\n                  <div class=\"icon icon-cash\"></div><div class=\"tab-title\">Accounts</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pin\"></div><div class=\"tab-title\">Near Me</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pricetags\"></div><div class=\"tab-title\">Products</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-text\"></div><div class=\"tab-title\">Messages</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-settings\"></div><div class=\"tab-title\">Settings</div>\n                </div>\n              </div>\n            </body>\n            </html>\n          ");
                    break;
                case 'windows':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n          <html>\n            <head>\n\n              <style>\n                @font-face {\n                  font-family: 'Ionicons';\n                  src: url(assets/fonts/ionicons.woff2);\n                }\n                body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n                .nav-bar{position:fixed; height: 60px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n                .logo{height: 50%; width: auto;}\n                .content{position:fixed;height: 438px; overflow: scroll; margin-top: 60px; min-width: 300px;}\n                .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n                .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n                .tab:not(:last-child){border-right: 1px solid #fff}\n                .icon {\n                  margin-top: 5px;\n                  width: inherit;\n                  font-size: 1.4em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                  height: 30px;\n                }\n                .tab-title {\n                  position: absolute;\n                  bottom: 5px;\n                  width: inherit;\n                  font-size: 0.7em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                }\n                div.icon-cash:before{ content: \"\\f143\"; }\n                div.icon-pin:before{ content: \"\\f1e4\";}\n                div.icon-pricetags:before{ content: \"\\f48e\"; }\n                div.icon-text:before{ content: \"\\f24f\"; }\n                div.icon-settings::before{ content: \"\\f20d\"; }\n                .padding{ padding: 10px; }\n                .card {border: 1px solid #dedede; overflow: hidden; }\n                h1, h2, p{color: " + color + " }\n                h1{font-size: 1.5em; font-weight: normal;}\n                h2{font-size: 1.2em; font-weight: normal;}\n                button{\n                  padding: 5px;\n                  border-radius: 10px;\n                  background-color: " + color + ";\n                  color: white;\n                  display: block;\n                }\n              </style>\n            </head>\n            <body>\n              <div class=\"nav-bar\">\n                <img class=\"statusbar-img \" src=\"assets/images/wp-statusbar.png\" width=\"320\">\n                <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n              </div>\n              <div class=\"content padding\">\n                <div class=\"card padding\">\n                  " + image + "\n                  " + title + "\n                  " + body + "\n                </div>\n              </div>\n              <div class=\"tab-bar\">\n                <div class=\"tab\">\n                  <div class=\"icon icon-cash\"></div><div class=\"tab-title\">Accounts</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pin\"></div><div class=\"tab-title\">Near Me</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pricetags\"></div><div class=\"tab-title\">Products</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-text\"></div><div class=\"tab-title\">Messages</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-settings\"></div><div class=\"tab-title\">Settings</div>\n                </div>\n              </div>\n            </body>\n            </html>\n            ");
                    break;
                default:
                    break;
            }
            _this.cdr.detectChanges();
        });
    };
    NewsPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: this.settingsProvider.getLabel('Ok'),
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    NewsPage.prototype.changeListener = function ($event) {
        this.image = $event.target.files[0];
        this.renderImage(this.image);
    };
    NewsPage.prototype.renderImage = function (file) {
        var self = this;
        // generate a new FileReader object
        var reader = new window.FileReader();
        // inject an image with the src url
        reader.onload = function (event) {
            var the_url = event.target.result;
            console.log("url", the_url);
            self.news.image = the_url;
            // self.viewCtrl.dismiss({image: the_url});
        };
        // when the file is read it triggers the onload event above.
        reader.readAsDataURL(file);
    };
    return NewsPage;
}());
NewsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-news',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\news\news.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons start>\n\n    </ion-buttons>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="save()" *ngIf="dirtyFlag">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-6 padding>\n\n        <ion-card class="ion-card-news">\n\n          <ion-row>\n\n            <ion-col>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Title\')}}</ion-label>\n\n              <ion-input [(ngModel)]="news.title" (input)="dirty()"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item no-padding>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Description\')}}</ion-label>\n\n                <ion-textarea [(ngModel)]="news.body" (input)="dirty()"></ion-textarea>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Image\')}}</ion-label>\n\n              <img margin class="imgUpload" *ngIf="news.image" style="width:50%; height:50%" [src]="news.image" alt="Click here to upload Image">\n\n              <ion-input type="file" accept="image/*" [(ngModel)]="imageFilePath" (change)="changeListener($event)"></ion-input>\n\n              <button float-right ion-button round small color="danger" (click)="removeImage()" *ngIf="news.image">{{settingsProvider.getLabel(\'Remove Image\')}}</button>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row align-items-center>\n\n            <ion-col>\n\n              <div>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Action Url\')}}</ion-label>\n\n              <ion-input [(ngModel)]="news.action" (input)="dirty()"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <button ion-button medium (click)="addUpdateNews()">\n\n                <span *ngIf="!new">{{settingsProvider.getLabel("Update News")}}</span>\n\n                <span *ngIf="new">{{settingsProvider.getLabel("Add News")}}</span>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col col-6 padding>\n\n        <ion-grid class="table-cell">\n\n          <ion-row justify-content-center>\n\n            <ion-col text-center text-nowrap padding>\n\n              <div class="header">{{settingsProvider.getLabel(\'Live News Preview\')}}</div>\n\n              <ion-segment [(ngModel)]="deviceType" padding-top padding-bottom>\n\n                <ion-segment-button value="ios">iOS</ion-segment-button>\n\n                <ion-segment-button value="android">Android</ion-segment-button>\n\n                <ion-segment-button value="windows">Windows</ion-segment-button>\n\n              </ion-segment>\n\n              <div class="preview" id="demo-device-ios" *ngIf="deviceType==\'ios\'">\n\n                <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n              </div>\n\n              <div class="preview" id="demo-device-android" *ngIf="deviceType==\'android\'">\n\n                <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n              </div>\n\n              <div class="preview" id="demo-device-windows" *ngIf="deviceType==\'windows\'">\n\n                <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\news\news.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_news_news__["a" /* NewsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], NewsPage);

//# sourceMappingURL=news.js.map

/***/ })

});
//# sourceMappingURL=14.js.map