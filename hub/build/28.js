webpackJsonp([28],{

/***/ 568:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsMessagesPageModule", function() { return ContactsMessagesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contacts_messages__ = __webpack_require__(605);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ContactsMessagesPageModule = (function () {
    function ContactsMessagesPageModule() {
    }
    return ContactsMessagesPageModule;
}());
ContactsMessagesPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__contacts_messages__["a" /* ContactsMessagesPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contacts_messages__["a" /* ContactsMessagesPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], ContactsMessagesPageModule);

//# sourceMappingURL=contacts-messages.module.js.map

/***/ }),

/***/ 605:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactsMessagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_push_messaging_push_messaging__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ContactsMessagesPage = (function () {
    function ContactsMessagesPage(navCtrl, languageProvider, navParams, messagesProvider, cdr, toastCtrl, oauthProvider, settingsProvider, viewCtrl) {
        this.navCtrl = navCtrl;
        this.languageProvider = languageProvider;
        this.navParams = navParams;
        this.messagesProvider = messagesProvider;
        this.cdr = cdr;
        this.toastCtrl = toastCtrl;
        this.oauthProvider = oauthProvider;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.contactOptions = {
            data: [],
            filteredData: [],
            columns: {
                "customerId": { title: "Consectus ID", search: true },
                "customer_email": { title: "Email", search: true },
                "message": { title: "Message", search: true },
                "delivered": { title: "Delivered", format: 'date' },
            },
            pagination: true,
            filter: null,
            search: true,
            select: false,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.deleteContactUs.bind(this),
                    color: 'danger'
                },
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editMessage.bind(this),
            uploadOptions: {},
        };
        this.issend = navParams.get("issend");
    }
    ContactsMessagesPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        var self = this;
        if (this.issend !== undefined) {
            this.messagesProvider.getFailedMessage(this.issend).then(function (result) {
                self.contactOptions.data = result;
                self.contactOptions = Object.assign({}, self.contactOptions);
                self.cdr.detectChanges();
            });
        }
        else {
            this.loadContactusMessages();
        }
    };
    ContactsMessagesPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    ContactsMessagesPage.prototype.loadContactusMessages = function () {
        var self = this;
        this.messagesProvider.getContactUsMessages().then(function (result) {
            self.contactOptions.data = result;
            self.contactOptions = Object.assign({}, self.contactOptions);
            self.cdr.detectChanges();
        });
    };
    ContactsMessagesPage.prototype.deleteContactUs = function (contactusMessages) {
        var _this = this;
        console.log(contactusMessages);
        var d = [];
        var self = this;
        for (var _i = 0, contactusMessages_1 = contactusMessages; _i < contactusMessages_1.length; _i++) {
            var contact = contactusMessages_1[_i];
            d.push(this.messagesProvider.deleteContactUsMessage(contact));
        }
        Promise.all(d)
            .then(function () {
            var toast = self.toastCtrl.create({
                message: contactusMessages.length == 1 ? _this.settingsProvider.getLabel(' Message was Deleted successfully') : contactusMessages.length + _this.settingsProvider.getLabel(' Messages were Deleted successfully'),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
            self.loadContactusMessages();
        });
    };
    ContactsMessagesPage.prototype.editMessage = function () {
        console.log("editMessage");
    };
    return ContactsMessagesPage;
}());
ContactsMessagesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contacts-messages',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\contacts-messages\contacts-messages.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2)" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(\'Contact Us messages\')}} | Consectus</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <table-component [options]="contactOptions"></table-component>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\contacts-messages\contacts-messages.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__providers_push_messaging_push_messaging__["a" /* PushMessagingProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
], ContactsMessagesPage);

//# sourceMappingURL=contacts-messages.js.map

/***/ })

});
//# sourceMappingURL=28.js.map