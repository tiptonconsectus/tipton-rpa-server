webpackJsonp([8],{

/***/ 587:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SavingAccountRulesPageModule", function() { return SavingAccountRulesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__saving_account_rules__ = __webpack_require__(624);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SavingAccountRulesPageModule = (function () {
    function SavingAccountRulesPageModule() {
    }
    return SavingAccountRulesPageModule;
}());
SavingAccountRulesPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__saving_account_rules__["a" /* SavingAccountRulesPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__saving_account_rules__["a" /* SavingAccountRulesPage */]),
        ],
    })
], SavingAccountRulesPageModule);

//# sourceMappingURL=saving-account-rules.module.js.map

/***/ }),

/***/ 624:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavingAccountRulesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_saving_account_rules_list_saving_account_rules_list__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SavingAccountRulesPage = (function () {
    function SavingAccountRulesPage(navCtrl, navParams, languageProvider, oauthProvider, toastCtrl, alertCtrl, viewCtrl, settingProvider, savingAccountRulesListProvider, cdr, settingsProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.settingProvider = settingProvider;
        this.savingAccountRulesListProvider = savingAccountRulesListProvider;
        this.cdr = cdr;
        this.settingsProvider = settingsProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.inoutmoney = "";
        this.currentTab = "inmoney";
        this.accountType = "selectAccount";
        this.transferInYes = false;
        this.transferInNo = false;
        this.transferOutYes = false;
        this.transferOutNo = false;
        this.addrule = false;
        this.addRuleBtn = true;
        this.maxInAmountYes = false;
        this.maxInAmountNo = false;
        this.maxOutAmountYes = false;
        this.maxOutAmountNo = false;
        this.allowInRequest = "year";
        this.allowOutRequest = "year";
        this.maxBalAmountYes = false;
        this.maxBalAmountNo = false;
        this.minBalAmountYes = false;
        this.minBalAmountNo = false;
        this.maxInAmt = 0;
        this.maxOutAmt = 0;
        this.maxInAmountYesText = "";
        this.maxInAmountNoText = "";
        this.maxOutAmountYesText = "";
        this.maxOutAmountNoText = "";
        this.minBalAmt = 0;
        this.maxBalAmt = 0;
        this.minBalAmountYesText = "";
        this.minBalAmountNoText = "";
        this.maxBalAmountYesText = "";
        this.maxBalAmountNoText = "";
        this.acceptMinBalAmtTxt = "";
        this.isEdit = false;
        this.isPenalty = false;
        this.resultdata = null;
        this.languageString = "en";
        this.accountRuleType = "selected";
        this.accountTypeId = false;
        this.channelIn = "day";
        this.channelOut = "day";
        this.channelWithin = "day";
        this.accountTypeid = "";
        this.hideMaxAmt = true;
        this.hideMinAmt = true;
        this.getAccountTypes();
        this.savingAccount = this.navParams.get('accountType');
        if (this.savingAccount) {
        }
        else {
        }
        var self = this;
        if (navParams.get('accountType') == undefined) {
            //add adata
            self.accountObj = {
                istransferin: 0,
                istransferout: 0,
                isPenaltyTransferIn: true,
                istransferwithin: true,
                isPenaltyTransferOut: true,
                transferIn: false,
                transferOut: false,
                account_types_id: null,
                openingbalance: 0,
                minbalance: 0,
                maxbalance: 0,
                maxtransferinamount: 0,
                maxtrasferinmsg: null,
                mintransferinamount: 0,
                mintrasferinmsg: null,
                daytransferinamount: 0,
                daytransferinamountmsg: null,
                daytransferinmsg: null,
                daymaxtransferinamount: 0,
                daymaxtransferinamountmsg: null,
                monthtransferinamount: 0,
                monthtransferinmsg: null,
                monthmaxtransferinamount: 0,
                monthmaxtransferinamountmsg: null,
                yearmaxtransferinamount: 0,
                yearmaxtransferinamountmsg: null,
                yeartransferinmsg: null,
                maxtransferoutamount: 0,
                maxtrasferoutmsg: null,
                mintransferoutamount: 0,
                mintrasferoutmsg: null,
                daytransferoutamount: 0,
                daytransferoutamountmsg: null,
                daymaxtransferoutamount: 0,
                daymaxtransferoutamountmsg: null,
                monthmaxtransferoutamount: 0,
                monthmaxtransferoutamountmsg: null,
                yearmaxtransferoutamount: 0,
                yearmaxtransferoutamountmsg: null,
                daytransferoutmsg: null,
                monthtransferoutamount: 0,
                monthtransferoutmsg: null,
                yeartransferoutamount: 0,
                yeartransferoutmsg: null,
                interestpenaltyamt: 0,
                interestpenaltyamtmsg: "Transferring money from this account may affect interest payable on your account",
                transferinpenaltymsg: "Transferring money from this account may affect interest payable on your account",
                transferoutpenalty: 0,
                createdby: 1,
                isactive: 1,
                maxtransferinamountmsg: null,
                maxtransferoutmsg: null,
                mintransferoutmsg: null,
                transferoutpenaltymsg: null,
                transferoutpenaltyamount: 0,
                yeartransferinamount: 0,
                accPerCustomer: 0
            };
            this.title = "Add Account Types & Rules";
        }
        else {
            //View Data
            this.title = "Update Account Types & Rules";
            self.updateData = navParams.get('accountType');
            self.accountObj = self.updateData;
            self.accountRuleType = self.updateData.account_types_id;
            self.accountObj.istransferin = self.accountObj.istransferin ? 1 : 0;
            if (!self.accountObj.istransferin) {
                self.hideMaxAmt = false;
            }
            else {
                self.hideMaxAmt = true;
            }
            self.accountObj.istransferout = self.accountObj.istransferout ? 1 : 0;
            if (!self.accountObj.istransferout) {
                self.hideMinAmt = false;
            }
            else {
                self.hideMinAmt = true;
            }
            self.accountRuleType = self.updateData.account_types_id;
            self.accountTypeId = true;
            if (self.accountObj.transferinpenalty) {
                self.accountObj.isPenaltyTransferIn = true;
            }
            else {
                self.accountObj.isPenaltyTransferIn = false;
            }
            if (self.accountObj.transferoutpenalty) {
                self.accountObj.isPenaltyTransferOut = true;
            }
            else {
                self.accountObj.isPenaltyTransferOut = false;
            }
        }
    }
    SavingAccountRulesPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("rulesengine")) {
            return true;
        }
        else {
            return false;
        }
    };
    SavingAccountRulesPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
    };
    SavingAccountRulesPage.prototype.ionViewDidLoad = function (data) {
        console.log('ionViewDidLoad SavingAccountRulesPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        console.log(this.accountObj, "accountObj");
    };
    SavingAccountRulesPage.prototype.getAccountTypes = function () {
        var _this = this;
        var self = this;
        self.savingAccountRulesListProvider.getAccountTypes().then(function (result) {
            console.log("result accounttype", result);
            if (result.data !== null) {
                _this.accountTypes = result.data;
            }
        });
    };
    SavingAccountRulesPage.prototype.addRuleEngine = function () {
        var self = this;
        self.accountData = {
            "account_types_id": this.accountObj.account_types_id,
            "openingbalance": this.accountObj.openingbalance ? self.accountObj.openingbalance : self.accountObj.openingbalance = null,
            "minbalance": this.accountObj.minbalance ? self.accountObj.minbalance : self.accountObj.minbalance = null,
            "maxbalance": this.accountObj.maxbalance ? self.accountObj.maxbalance : self.accountObj.maxbalance = null,
            "maxtransferinamount": this.accountObj.maxtransferinamount ? self.accountObj.maxtransferinamount : self.accountObj.maxtransferinamount = null,
            "maxtrasferinmsg": this.accountObj.maxtrasferinmsg,
            "mintransferinamount": this.accountObj.mintransferinamount ? self.accountObj.mintransferinamount : self.accountObj.mintransferinamount = null,
            "mintrasferinmsg": this.accountObj.mintrasferinmsg ? self.accountObj.mintrasferinmsg : self.accountObj.mintrasferinmsg = null,
            "daytransferinamount": this.accountObj.daytransferinamount ? self.accountObj.daytransferinamount : self.accountObj.daytransferinamount = null,
            "daytransferinamountmsg": this.accountObj.daytransferinamountmsg,
            "daytransferinmsg": this.accountObj.daytransferinmsg,
            "daymaxtransferinamount": this.accountObj.daymaxtransferinamount ? self.accountObj.daymaxtransferinamount : self.accountObj.daymaxtransferinamount = null,
            "maxtransferoutmsg": this.accountObj.maxtransferoutmsg,
            "daymaxtransferinamountmsg": this.accountObj.daymaxtransferinamountmsg,
            "monthtransferinamount": this.accountObj.monthtransferinamount ? self.accountObj.monthtransferinamount : self.accountObj.monthtransferinamount = null,
            "monthtransferinmsg": this.accountObj.monthtransferinmsg,
            "monthmaxtransferinamount": this.accountObj.monthmaxtransferinamount ? self.accountObj.monthmaxtransferinamount : self.accountObj.monthmaxtransferinamount = null,
            "monthmaxtransferinamountmsg": this.accountObj.monthmaxtransferinamountmsg,
            "yearmaxtransferinamount": this.accountObj.yearmaxtransferinamount ? self.accountObj.yearmaxtransferinamount : self.accountObj.yearmaxtransferinamount = null,
            "yearmaxtransferinamountmsg": this.accountObj.yearmaxtransferinamountmsg,
            "yeartransferinmsg": this.accountObj.yeartransferinmsg,
            "maxtransferoutamount": this.accountObj.maxtransferoutamount ? self.accountObj.maxtransferoutamount : self.accountObj.maxtransferoutamount = null,
            "maxtrasferoutmsg": this.accountObj.maxtrasferoutmsg,
            "mintransferoutamount": this.accountObj.mintransferoutamount ? self.accountObj.mintransferoutamount : self.accountObj.mintransferoutamount = null,
            "mintrasferoutmsg": this.accountObj.mintrasferoutmsg,
            "daytransferoutamount": this.accountObj.daytransferoutamount ? self.accountObj.daytransferoutamount : self.accountObj.daytransferoutamount = null,
            "daytransferoutamountmsg": this.accountObj.daytransferoutamountmsg,
            "daymaxtransferoutamount": this.accountObj.daymaxtransferoutamount ? self.accountObj.daymaxtransferoutamount : self.accountObj.daymaxtransferoutamount = null,
            "daymaxtransferoutamountmsg": this.accountObj.daymaxtransferoutamountmsg,
            "monthmaxtransferoutamount": this.accountObj.monthmaxtransferoutamount ? self.accountObj.monthmaxtransferoutamount : self.accountObj.monthmaxtransferoutamount = null,
            "monthmaxtransferoutamountmsg": this.accountObj.monthmaxtransferoutamountmsg,
            "yearmaxtransferoutamount": this.accountObj.yearmaxtransferoutamount ? self.accountObj.yearmaxtransferoutamount : self.accountObj.yearmaxtransferoutamount = null,
            "yearmaxtransferoutamountmsg": this.accountObj.yearmaxtransferoutamountmsg,
            "mintransferoutmsg": this.accountObj.mintransferoutmsg,
            "daytransferoutmsg": this.accountObj.daytransferoutmsg,
            "monthtransferoutamount": this.accountObj.monthtransferoutamount ? self.accountObj.monthtransferoutamount : self.accountObj.monthtransferoutamount = null,
            "monthtransferoutmsg": this.accountObj.monthtransferoutmsg,
            "transferoutpenaltyamount": this.accountObj.transferoutpenaltyamount ? self.accountObj.transferoutpenaltyamount : self.accountObj.transferoutpenaltyamount = null,
            "yeartransferoutamount": this.accountObj.yeartransferoutamount ? self.accountObj.yeartransferoutamount : self.accountObj.yeartransferoutamount = null,
            "yeartransferoutmsg": this.accountObj.yeartransferoutmsg,
            "interestpenaltyamt": this.accountObj.interestpenaltyamt ? self.accountObj.interestpenaltyamt : self.accountObj.interestpenaltyamt = null,
            "interestpenaltyamtmsg": this.accountObj.interestpenaltyamtmsg,
            "yeartransferinamount": this.accountObj.yeartransferinamount ? self.accountObj.yeartransferinamount : self.accountObj.yeartransferinamount = null,
            "transferoutpenalty": this.accountObj.transferoutpenalty ? self.accountObj.transferoutpenalty : self.accountObj.transferoutpenalty = null,
            "transferoutpenaltymsg": this.accountObj.transferoutpenaltymsg,
            "createdby": 1,
            "isactive": 1,
            "istransferin": this.accountObj.istransferin ? 1 : 0,
            "istransferout": this.accountObj.istransferout ? 1 : 0,
            "accPerCustomer": this.accountObj.accPerCustomer ? self.accountObj.accPerCustomer : self.accountObj.accPerCustomer = null
        };
        console.log("accountData");
        self.savingAccountRulesListProvider.addRulesEngine(this.accountData).then(function (result) {
            var toast = self.toastCtrl.create({
                message: result.message,
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.pop();
        });
    };
    SavingAccountRulesPage.prototype.updateRuleEngine = function () {
        var self = this;
        var msg = [];
        if (this.accountObj.account_types_id <= 0)
            msg.push(this.settingsProvider.getLabel("Account Type Id must be specified."));
        // if (this.accountObj.accPerCustomer <= 0) msg.push(this.settingsProvider.getLabel("Number of accounts per customer should be specified"));
        // if (this.accountObj.openingbalance <= 0) msg.push(this.settingsProvider.getLabel("Opening Balance must be specified"));
        // if (this.accountObj.minbalance <= 0) msg.push(this.settingsProvider.getLabel("User Name must be specified"));
        // if (this.accountObj.maxbalance <= 0) msg.push(this.settingsProvider.getLabel("Maximum Balance must be specified"));
        // if (this.accountObj.maxtransferinamount <= 0) msg.push(this.settingsProvider.getLabel("Maximum Transfer in amount  must be specified"));
        // if (this.accountObj.maxtrasferinmsg <= 0) msg.push(this.settingsProvider.getLabel("Maximum transfer in message must be specified"));
        // if (this.accountObj.mintransferinamount <= 0) msg.push(this.settingsProvider.getLabel("Minimum transfer in amount must be specified"));
        // if (this.accountObj.mintrasferinmsg <= 0) msg.push(this.settingsProvider.getLabel("Minimum transfer in message must be specified"));
        // if (this.accountObj.daytransferinamount <= 0) msg.push(this.settingsProvider.getLabel("Daliy transfer in amount must be specified"));
        // if (this.accountObj.daytransferinmsg <= 0) msg.push(this.settingsProvider.getLabel("Daliy transfer in message must be specified"));
        // if (this.accountObj.daymaxtransferinamount <= 0) msg.push(this.settingsProvider.getLabel("Daliy maximum transfer in amount must be specified"));
        // if (this.accountObj.daymaxtransferinamountmsg <= 0) msg.push(this.settingsProvider.getLabel("Daliy Maximum transfer in message must be specified"));
        // if (this.accountObj.monthtransferinamount <= 0) msg.push(this.settingsProvider.getLabel("Monthly Transfer in amount must be specified"));
        // if (this.accountObj.monthtransferinmsg <= 0) msg.push(this.settingsProvider.getLabel("Monthly Transfer in message must be specified"));
        // if (this.accountObj.monthmaxtransferinamount <= 0) msg.push(this.settingsProvider.getLabel("Monthly Maximum Transfer in amount must be specified"));
        // if (this.accountObj.monthmaxtransferinamountmsg <= 0) msg.push(this.settingsProvider.getLabel("Monthly Maximum Transfer in message must be specified"));
        // if (this.accountObj.yeartransferinamount <= 0) msg.push(this.settingsProvider.getLabel("Yearly transfer in amount must be specified"));
        // if (this.accountObj.yeartransferinmsg <= 0) msg.push(this.settingsProvider.getLabel("Yearly transfer in message must be specified"));
        // if (this.accountObj.yearmaxtransferinamount <= 0) msg.push(this.settingsProvider.getLabel("Yearly Maximum transfer in amount must be specified"));
        // if (this.accountObj.yearmaxtransferinamountmsg <= 0) msg.push(this.settingsProvider.getLabel("Yearly Maximum transfer in message must be specified"));
        // if (this.accountObj.maxtransferoutamount <= 0) msg.push(this.settingsProvider.getLabel("Maximum Transfer out amount must be specified"));
        // if (this.accountObj.maxtransferoutmsg <= 0) msg.push(this.settingsProvider.getLabel("Maximum Transfer Out message must be specified"));
        // if (this.accountObj.mintransferoutamount <= 0) msg.push(this.settingsProvider.getLabel("Minimum Transfer Out amount must be specified"));
        // if (this.accountObj.mintransferoutmsg <= 0) msg.push(this.settingsProvider.getLabel("Minimum Transfer Out message must be specified"));
        // if (this.accountObj.daytransferoutamount <= 0) msg.push(this.settingsProvider.getLabel("Daily Transfer Out amount must be specified"));
        // if (this.accountObj.daytransferoutmsg <= 0) msg.push(this.settingsProvider.getLabel("Daily Transfer Out message must be specified"));
        // if (this.accountObj.daymaxtransferoutamount <= 0) msg.push(this.settingsProvider.getLabel("Daily Maximum Transfer Out amount must be specified"));
        // if (this.accountObj.daymaxtransferoutamountmsg <= 0) msg.push(this.settingsProvider.getLabel("Daily Maximum Transfer Out message must be specified"));
        // if (this.accountObj.monthtransferoutamount <= 0) msg.push(this.settingsProvider.getLabel("Monthly Transfer Out amount must be specified"));
        // if (this.accountObj.monthtransferoutmsg <= 0) msg.push(this.settingsProvider.getLabel("Monthly Transfer Out message must be specified"));
        // if (this.accountObj.monthmaxtransferoutamount <= 0) msg.push(this.settingsProvider.getLabel("Monthly Maximum Transfer Out amount must be specified"));
        // if (this.accountObj.monthmaxtransferoutamountmsg <= 0) msg.push(this.settingsProvider.getLabel("Monthly Maximum Transfer Out message must be specified"));
        // if (this.accountObj.yeartransferoutamount <= 0) msg.push(this.settingsProvider.getLabel("Yearly Transfer Out amount must be specified"));
        // if (this.accountObj.yeartransferoutmsg <= 0) msg.push(this.settingsProvider.getLabel("Yearly Transfer Out message must be specified"));
        // if (this.accountObj.yearmaxtransferoutamount <= 0) msg.push(this.settingsProvider.getLabel("Yearly Maximum Transfer Out amount must be specified"));
        // if (this.accountObj.yearmaxtransferoutamountmsg <= 0) msg.push(this.settingsProvider.getLabel("Yearly Maximum Transfer Out message must be specified"));
        // if (this.accountObj.transferoutpenaltyamount <= 0) msg.push(this.settingsProvider.getLabel("Transfer Out penalty amount must be specified"));
        // if (this.accountObj.transferoutpenaltymsg <= 0) msg.push(this.settingsProvider.getLabel("Transfer Out penalty message must be specified"));
        if (msg.length > 0) {
            var alert_1 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel('Error'),
                message: msg.join("<br />"),
                buttons: [
                    {
                        text: this.settingsProvider.getLabel('OK'),
                        role: 'cancel',
                        handler: function (data) {
                        }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            self.accountObj.istransferin = self.accountObj.istransferin ? 1 : 0;
            // if (self.accountObj.istransferin == 0) {
            // } else {
            //   self.accountObj.transferintooltipmsg = "";
            // }
            self.accountObj.istransferout = self.accountObj.istransferout ? 1 : 0;
            // if (self.accountObj.istransferout == 0) {
            // } else {
            //   self.accountObj.transferouttooltipmsg = "";
            // }
            // self.accountObj.daymaxtransferinamount 
            console.log("accountobj", self.accountObj);
            self.accountData = {
                "rules_engine_id": this.accountObj.rules_engine_id,
                "account_types_id": this.accountObj.account_types_id,
                "openingbalance": this.accountObj.openingbalance ? self.accountObj.openingbalance : self.accountObj.openingbalance = null,
                "minbalance": this.accountObj.minbalance ? self.accountObj.minbalance : self.accountObj.minbalance = null,
                "maxbalance": this.accountObj.maxbalance ? self.accountObj.maxbalance : self.accountObj.maxbalance = null,
                "maxtransferinamount": this.accountObj.maxtransferinamount ? self.accountObj.maxtransferinamount : self.accountObj.maxtransferinamount = null,
                "maxtrasferinmsg": this.accountObj.maxtrasferinmsg,
                "mintransferinamount": this.accountObj.mintransferinamount ? self.accountObj.mintransferinamount : self.accountObj.mintransferinamount = null,
                "mintrasferinmsg": this.accountObj.mintrasferinmsg ? self.accountObj.mintrasferinmsg : self.accountObj.mintrasferinmsg = null,
                "daytransferinamount": this.accountObj.daytransferinamount ? self.accountObj.daytransferinamount : self.accountObj.daytransferinamount = null,
                "daytransferinamountmsg": this.accountObj.daytransferinamountmsg,
                "daytransferinmsg": this.accountObj.daytransferinmsg,
                "daymaxtransferinamount": this.accountObj.daymaxtransferinamount ? self.accountObj.daymaxtransferinamount : self.accountObj.daymaxtransferinamount = null,
                "maxtransferoutmsg": this.accountObj.maxtransferoutmsg,
                "daymaxtransferinamountmsg": this.accountObj.daymaxtransferinamountmsg,
                "monthtransferinamount": this.accountObj.monthtransferinamount ? self.accountObj.monthtransferinamount : self.accountObj.monthtransferinamount = null,
                "monthtransferinmsg": this.accountObj.monthtransferinmsg,
                "monthmaxtransferinamount": this.accountObj.monthmaxtransferinamount ? self.accountObj.monthmaxtransferinamount : self.accountObj.monthmaxtransferinamount = null,
                "monthmaxtransferinamountmsg": this.accountObj.monthmaxtransferinamountmsg,
                "yearmaxtransferinamount": this.accountObj.yearmaxtransferinamount ? self.accountObj.yearmaxtransferinamount : self.accountObj.yearmaxtransferinamount = null,
                "yearmaxtransferinamountmsg": this.accountObj.yearmaxtransferinamountmsg,
                "yeartransferinmsg": this.accountObj.yeartransferinmsg,
                "maxtransferoutamount": this.accountObj.maxtransferoutamount ? self.accountObj.maxtransferoutamount : self.accountObj.maxtransferoutamount = null,
                "maxtrasferoutmsg": this.accountObj.maxtrasferoutmsg,
                "mintransferoutamount": this.accountObj.mintransferoutamount ? self.accountObj.mintransferoutamount : self.accountObj.mintransferoutamount = null,
                "mintrasferoutmsg": this.accountObj.mintrasferoutmsg,
                "daytransferoutamount": this.accountObj.daytransferoutamount ? self.accountObj.daytransferoutamount : self.accountObj.daytransferoutamount = null,
                "daytransferoutamountmsg": this.accountObj.daytransferoutamountmsg,
                "daymaxtransferoutamount": this.accountObj.daymaxtransferoutamount ? self.accountObj.daymaxtransferoutamount : self.accountObj.daymaxtransferoutamount = null,
                "daymaxtransferoutamountmsg": this.accountObj.daymaxtransferoutamountmsg,
                "monthmaxtransferoutamount": this.accountObj.monthmaxtransferoutamount ? self.accountObj.monthmaxtransferoutamount : self.accountObj.monthmaxtransferoutamount = null,
                "monthmaxtransferoutamountmsg": this.accountObj.monthmaxtransferoutamountmsg,
                "yearmaxtransferoutamount": this.accountObj.yearmaxtransferoutamount ? self.accountObj.yearmaxtransferoutamount : self.accountObj.yearmaxtransferoutamount = null,
                "yearmaxtransferoutamountmsg": this.accountObj.yearmaxtransferoutamountmsg,
                "mintransferoutmsg": this.accountObj.mintransferoutmsg,
                "daytransferoutmsg": this.accountObj.daytransferoutmsg,
                "monthtransferoutamount": this.accountObj.monthtransferoutamount ? self.accountObj.monthtransferoutamount : self.accountObj.monthtransferoutamount = null,
                "monthtransferoutmsg": this.accountObj.monthtransferoutmsg,
                "transferoutpenaltyamount": this.accountObj.transferoutpenaltyamount ? self.accountObj.transferoutpenaltyamount : self.accountObj.transferoutpenaltyamount = null,
                "yeartransferoutamount": this.accountObj.yeartransferoutamount ? self.accountObj.yeartransferoutamount : self.accountObj.yeartransferoutamount = null,
                "yeartransferoutmsg": this.accountObj.yeartransferoutmsg,
                "interestpenaltyamt": this.accountObj.interestpenaltyamt ? self.accountObj.interestpenaltyamt : self.accountObj.interestpenaltyamt = null,
                "interestpenaltyamtmsg": this.accountObj.interestpenaltyamtmsg,
                "yeartransferinamount": this.accountObj.yeartransferinamount ? self.accountObj.yeartransferinamount : self.accountObj.yeartransferinamount = null,
                "transferoutpenalty": this.accountObj.transferoutpenalty ? self.accountObj.transferoutpenalty : self.accountObj.transferoutpenalty = null,
                "transferoutpenaltymsg": this.accountObj.transferoutpenaltymsg,
                "createdby": 1,
                "isactive": 1,
                "istransferin": this.accountObj.istransferin ? 1 : 0,
                "istransferout": this.accountObj.istransferout ? 1 : 0,
                "accPerCustomer": this.accountObj.accPerCustomer ? self.accountObj.accPerCustomer : self.accountObj.accPerCustomer = null
            };
            console.log("accountdata", this.accountData);
            self.savingAccountRulesListProvider.updateRulesEngine(self.accountData).then(function (result) {
                console.log("result accounttype", result);
                var toast = self.toastCtrl.create({
                    message: result.message,
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                self.navCtrl.pop();
            });
        }
    };
    SavingAccountRulesPage.prototype.segmentTransferIn = function (event) {
        switch (this.channelIn) {
        }
    };
    SavingAccountRulesPage.prototype.onAccTypeSelect = function () {
        console.log("log", this.accountRuleType);
        if (this.accountRuleType !== "selected") {
            this.accountTypeId = true;
        }
        else {
            this.accountTypeId = false;
        }
        this.accountObj.account_types_id = this.accountRuleType;
    };
    SavingAccountRulesPage.prototype.segmentTransferOut = function (event) {
        switch (this.channelOut) {
        }
    };
    SavingAccountRulesPage.prototype.segmentTransferWithin = function (event) {
        switch (this.channelWithin) {
        }
    };
    SavingAccountRulesPage.prototype.changeInMoney = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Transfer In"),
            message: this.settingsProvider.getLabel("message"),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Yes"),
                    role: this.settingsProvider.getLabel("Ok"),
                    handler: function (data) {
                        var alert1 = _this.alertCtrl.create({
                            title: _this.settingsProvider.getLabel("Transfer In - allowed"),
                            message: _this.settingsProvider.getLabel("message"),
                            buttons: [
                                {
                                    text: _this.settingsProvider.getLabel("Proceed"),
                                    role: 'Ok',
                                    handler: function (data) {
                                    }
                                },
                                {
                                    text: _this.settingsProvider.getLabel("Cancel"),
                                    role: 'Cancel',
                                    handler: function (data) {
                                    }
                                }
                            ]
                        });
                        alert1.present();
                    }
                },
                {
                    text: this.settingsProvider.getLabel("No"),
                    role: 'Cancel',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    SavingAccountRulesPage.prototype.noChangeInMoney = function () {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Transfer In - not allowed"),
            message: this.settingsProvider.getLabel("message"),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Continue"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    SavingAccountRulesPage.prototype.toggleTransferInPenalty = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.accountObj.isPenaltyTransferIn = true;
                this.accountObj.transferinpenalty = 1;
            }
            if (type == "false") {
                this.accountObj.isPenaltyTransferIn = false;
                this.accountObj.transferinpenalty = 0;
            }
        }
        else {
            if (type == "true") {
                this.accountObj.isPenaltyTransferIn = false;
                this.accountObj.transferinpenalty = 0;
            }
            if (type == "false") {
                this.accountObj.isPenaltyTransferIn = true;
                this.accountObj.transferinpenalty = 1;
            }
        }
        this.cdr.detectChanges();
    };
    SavingAccountRulesPage.prototype.toggleTransferOutPenalty = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.accountObj.isPenaltyTransferOut = true;
                this.accountObj.transferoutpenalty = 1;
            }
            if (type == "false") {
                this.accountObj.isPenaltyTransferOut = false;
                this.accountObj.transferoutpenalty = 0;
            }
        }
        else {
            if (type == "true") {
                this.accountObj.isPenaltyTransferOut = false;
                this.accountObj.transferoutpenalty = 0;
            }
            if (type == "false") {
                this.accountObj.isPenaltyTransferOut = true;
                this.accountObj.transferoutpenalty = 1;
            }
        }
        this.cdr.detectChanges();
    };
    SavingAccountRulesPage.prototype.transferInYesFn = function () {
        if (this.transferInYes) {
            this.updateData.transferIn = this.transferInYes;
            this.transferInNo = false;
        }
    };
    SavingAccountRulesPage.prototype.transferInNoFn = function () {
        if (this.transferInNo) {
            this.updateData.transferIn = false;
            this.transferInYes = false;
        }
    };
    SavingAccountRulesPage.prototype.transferOutYesFn = function () {
        if (this.transferOutYes) {
            this.updateData.transferOut = true;
            this.transferOutNo = false;
        }
    };
    SavingAccountRulesPage.prototype.transferOutNoFn = function () {
        if (this.transferOutNo) {
            this.accountObj.transferOut = false;
            this.transferOutYes = false;
        }
    };
    SavingAccountRulesPage.prototype.addRule = function () {
        var self = this;
        if (self.accountType !== "selectAccount") {
            self.addrule = true;
            self.addRuleBtn = false;
        }
        else {
            var alert_2 = self.alertCtrl.create({
                title: this.settingsProvider.getLabel("Error"),
                message: this.settingsProvider.getLabel("Please select atleast one account Type"),
                buttons: [
                    {
                        text: self.settingsProvider.getLabel("Ok"),
                        role: 'Ok',
                        handler: function (data) {
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    SavingAccountRulesPage.prototype.saveAccRuleList = function () {
        var self = this;
        console.log("fdf", this.accountObj);
        if (this.accountRuleType == "selected") {
            this.errorAlert("Please select account type");
        }
        else {
            self.addRuleEngine();
        }
    };
    SavingAccountRulesPage.prototype.save = function () {
        var _this = this;
        console.log("accountobj", this.accountObj);
        var self = this;
        var toast = self.toastCtrl.create({
            message: self.settingsProvider.getLabel('Rule added successfully'),
            duration: 1000,
            position: 'right',
            cssClass: "success"
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
            self.settingProvider.saveSavingAccountRule(_this.accountObj).then(function (data) {
                console.log("data", data);
            });
            self.emptyAllField();
        });
        toast.present();
    };
    SavingAccountRulesPage.prototype.editSavingAccountRule = function (savingAccountRule) {
        console.log(savingAccountRule);
    };
    SavingAccountRulesPage.prototype.getAccountRules = function () {
        var self = this;
    };
    SavingAccountRulesPage.prototype.selectAccoutType = function () {
        var self = this;
        self.savingAccount = [];
        console.log(self.accountType);
        self.emptyAllField();
        self.settingProvider.getSavingAccountRules().then(function (data) {
            if (data) {
                self.savingAccount = data.filter(function (item) {
                    return item.type == self.accountType;
                });
                console.log("savingAccountLength", self.savingAccount.length);
                if (self.savingAccount.length > 0) {
                    self.addrule = true;
                    self.addRuleBtn = false;
                    self.isEdit = true;
                    // self.emptyAllField();
                    self.savingAcountRuleChanges(self.savingAccount[0]);
                }
                else {
                    self.savingAccount = [];
                    self.emptyAllField();
                    self.addrule = false;
                    self.isEdit = false;
                    self.addRuleBtn = true;
                }
            }
        });
    };
    SavingAccountRulesPage.prototype.savingAcountRuleChanges = function (savinngAcc) {
        var self = this;
        self.allowInRequest = savinngAcc.allowInRequest;
        self.allowOutRequest = savinngAcc.allowInRequest;
        self.maxInAmt = savinngAcc.maxInAmt > 0 ? savinngAcc.maxInAmt : 0;
        self.maxOutAmt = savinngAcc.maxOutAmt > 0 ? savinngAcc.maxOutAmt : 0;
        if (savinngAcc.transferIn) {
            self.transferInYes = true;
        }
        else {
            self.transferInNo = true;
        }
        if (savinngAcc.transferOut) {
            self.transferOutYes = true;
        }
        else {
            self.transferOutNo = true;
        }
        if (savinngAcc.exceedInAmt) {
            self.maxInAmountYes = true;
        }
        else {
            self.maxInAmountNo = true;
        }
        if (savinngAcc.exceedOutAmt) {
            self.maxOutAmountYes = true;
        }
        else {
            self.maxOutAmountNo = true;
        }
        if (self.maxInAmountYes) {
            self.maxInAmountYesText = savinngAcc.acceptInAmtTxt;
        }
        else {
            self.maxInAmountNoText = savinngAcc.acceptInAmtTxt;
        }
        if (self.maxOutAmountYes) {
            self.maxOutAmountYesText = savinngAcc.acceptOutAmtTxt;
        }
        else {
            self.maxOutAmountNoText = savinngAcc.acceptOutAmtTxt;
        }
        if (savinngAcc.minBalAmt > 0) {
            self.minBalAmt = savinngAcc.minBalAmt;
        }
        else {
            self.minBalAmt = 0;
        }
        if (savinngAcc.exceedMinBalAmt) {
            self.minBalAmountYes = true;
        }
        else {
            self.minBalAmountNo = true;
        }
        if (self.minBalAmountYes) {
            self.minBalAmountYesText = savinngAcc.acceptMinBalAmtTxt;
        }
        else {
            self.minBalAmountNoText = savinngAcc.acceptMinBalAmtTxt;
        }
        if (savinngAcc.maxBalAmt > 0) {
            self.maxBalAmt = savinngAcc.maxBalAmt;
        }
        else {
            self.maxBalAmt = 0;
        }
        if (savinngAcc.exceedMaxBalAmt) {
            self.maxBalAmountYes = true;
        }
        else {
            self.maxBalAmountNo = true;
        }
        if (self.maxBalAmountYes) {
            self.maxBalAmountYesText = savinngAcc.acceptMaxBalAmtTxt;
        }
        else {
            self.maxBalAmountNoText = savinngAcc.acceptMaxBalAmtTxt;
        }
    };
    SavingAccountRulesPage.prototype.emptyAllField = function () {
        var self = this;
        self.inoutmoney = "";
        self.transferInYes = false;
        self.transferInNo = false;
        self.transferOutYes = false;
        self.transferOutNo = false;
        self.addrule = false;
        self.addRuleBtn = true;
        self.maxInAmountYes = false;
        self.maxInAmountNo = false;
        self.maxOutAmountYes = false;
        self.maxOutAmountNo = false;
        self.allowInRequest = "year";
        self.allowOutRequest = "year";
        self.maxBalAmountYes = false;
        self.maxBalAmountNo = false;
        self.minBalAmountYes = false;
        self.minBalAmountNo = false;
        self.maxInAmt = 0;
        self.maxOutAmt = 0;
        self.maxInAmountYesText = "";
        self.maxInAmountNoText = "";
        self.maxOutAmountYesText = "";
        self.maxOutAmountNoText = "";
        self.minBalAmt = 0;
        self.maxBalAmt = 0;
        self.minBalAmountYesText = "";
        self.minBalAmountNoText = "";
        self.maxBalAmountYesText = "";
        self.maxBalAmountNoText = "";
    };
    SavingAccountRulesPage.prototype.update = function () {
        var _this = this;
        this.transferInYesFn();
        this.transferInNoFn();
        this.transferOutYesFn();
        this.transferOutNoFn();
        if (this.accountname == null || this.accountname == undefined) {
            console.log(this.accountname, "accountname");
            this.errorAlert("Please select account type");
        }
        else {
            this.settingProvider.updateSavingAccountRules(this.accountObj).then(function (data) {
                console.log("data", data);
                console.log("accountobj", _this.accountObj);
                var self = _this;
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel('Updated successfully'),
                    duration: 1000,
                    position: 'right',
                    cssClass: "success"
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    self.emptyAllField();
                });
                toast.present();
            });
        }
    };
    SavingAccountRulesPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    SavingAccountRulesPage.prototype.toggleTransferin = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.hideMaxAmt = true;
                this.accountObj.istransferin = 1;
            }
            if (type == "false") {
                this.hideMaxAmt = false;
                this.accountObj.istransferin = 0;
            }
        }
        else {
            if (type == "true") {
                this.accountObj.istransferin = 0;
            }
            if (type == "false") {
                this.accountObj.istransferin = 1;
            }
        }
        this.cdr.detectChanges();
    };
    SavingAccountRulesPage.prototype.toggleTransferOut = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.hideMinAmt = true;
                this.accountObj.istransferout = 1;
            }
            if (type == "false") {
                this.hideMinAmt = false;
                this.accountObj.istransferout = 0;
            }
        }
        else {
            if (type == "true") {
                this.accountObj.istransferout = 0;
            }
            if (type == "false") {
                this.accountObj.istransferout = 1;
            }
        }
        this.cdr.detectChanges();
    };
    SavingAccountRulesPage.prototype.toggleTransferWithin = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.accountObj.istransferwithin = true;
            }
            if (type == "false") {
                this.accountObj.istransferwithin = false;
            }
        }
        else {
            if (type == "true") {
                this.accountObj.istransferwithin = false;
            }
            if (type == "false") {
                this.accountObj.istransferwithin = true;
            }
        }
        this.cdr.detectChanges();
    };
    SavingAccountRulesPage.prototype._keyUp = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    };
    return SavingAccountRulesPage;
}());
SavingAccountRulesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-saving-account-rules',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\saving-account-rules\saving-account-rules.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(title)}} </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row *ngIf="updateData == undefined">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="saveAccRuleList()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row *ngIf="updateData !== undefined">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="updateRuleEngine()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-card>\n\n      <ion-row margin>\n\n        <ion-col col-4>\n\n          <ion-item>\n\n            <ion-label stacked>{{settingsProvider.getLabel("Account Type")}}</ion-label>\n\n            <ion-select [(ngModel)]="accountRuleType" (ionChange)="onAccTypeSelect()">\n\n              <ion-option>Please Select Account Type</ion-option>\n\n              <ion-option multiple="false" selected="true" *ngFor="let accountType of accountTypes" [value]="accountType.account_types_id">{{accountType.accountname}}</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n        </ion-col>\n\n        <ion-col col-4 *ngIf="accountTypeId">\n\n          <ion-item>\n\n            <ion-label stacked>{{settingsProvider.getLabel("Account Type Id")}}</ion-label>\n\n            <ion-input type="text" [(ngModel)]="accountObj.account_types_id" name="account_types_id" style="height: 34px!important;font-size:20px !important;"></ion-input>\n\n          </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row margin>\n\n        <ion-col col-4>\n\n          <ion-item>\n\n            <ion-label stacked>{{settingsProvider.getLabel("Opening Balance")}}</ion-label>\n\n            <ion-input type="number" [(ngModel)]="accountObj.openingbalance" (keyup)="_keyUp($event)"></ion-input>\n\n          </ion-item>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <ion-item>\n\n            <ion-label stacked>{{settingsProvider.getLabel("Minimum Balance")}}</ion-label>\n\n            <ion-input type="number" [(ngModel)]="accountObj.minbalance" (keyup)="_keyUp($event)"></ion-input>\n\n          </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row margin>\n\n        <ion-col col-4>\n\n          <ion-item>\n\n            <ion-label stacked>{{settingsProvider.getLabel("Maximum Balance")}}</ion-label>\n\n            <ion-input type="number" [(ngModel)]="accountObj.maxbalance" (keyup)="_keyUp($event)"></ion-input>\n\n          </ion-item>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <ion-item>\n\n            <ion-label stacked>{{settingsProvider.getLabel("Number of accounts per customer ")}}</ion-label>\n\n            <ion-input type="number" [(ngModel)]="accountObj.accPerCustomer" (keyup)="_keyUp($event)"></ion-input>\n\n          </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n\n\n      <ion-row *ngIf="accountTypeId" margin>\n\n\n\n        <!--Transfer In-->\n\n        <ion-col col-4>\n\n          <ion-card-content style="padding:0px 0px 0px 16px">\n\n            <div class="col-card" float-left text-center>\n\n              <h3 style="padding-top: 15px;">{{settingsProvider.getLabel("TRANSFER IN")}}</h3>\n\n              <table>\n\n                <tr>\n\n                  <td>\n\n                    <ion-item>\n\n                      <ion-label>{{settingsProvider.getLabel("Yes")}}</ion-label>\n\n                      <ion-checkbox [checked]="accountObj.istransferin" (ionChange)="toggleTransferin(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                  </td>\n\n                  <td>\n\n                    <ion-item>\n\n                      <ion-label> {{settingsProvider.getLabel("No")}}</ion-label>\n\n                      <ion-checkbox [checked]="!accountObj.istransferin" (ionChange)="toggleTransferin(\'false\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                  </td>\n\n                </tr>\n\n              </table>\n\n\n\n              <div *ngIf="accountObj.istransferin">\n\n\n\n                <div style="padding: 10px;">\n\n                  <fieldset style="width: 100%; margin-top: 5px; margin-bottom: 5px; text-align: left;">\n\n                    <legend>Per Transaction:</legend>\n\n                    <ion-label for="kraken" style="font-size: 13px;">{{settingsProvider.getLabel("Maximum Amount")}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.maxtransferinamount" (keyup)="_keyUp($event)" name="maxtransferinamount"></ion-input>\n\n                    <ion-label for="kraken" style="font-size: 13px;">{{settingsProvider.getLabel("Maximum Warning")}}</ion-label>\n\n                    <ion-input type="text" [(ngModel)]="accountObj.maxtrasferinmsg" name="maxtrasferinmsg"></ion-input>\n\n                    <ion-label for="kraken" style="font-size: 13px;">{{settingsProvider.getLabel("Minimum Amount")}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.mintransferinamount" (keyup)="_keyUp($event)" name="mintransferinamount"></ion-input>\n\n                    <ion-label for="kraken" style="font-size: 13px;">{{settingsProvider.getLabel("Minimum Warning")}}</ion-label>\n\n                    <ion-input type="text" [(ngModel)]="accountObj.mintrasferinmsg" name="mintrasferinmsg"></ion-input>\n\n                    <br/>\n\n                  </fieldset>\n\n                </div>\n\n\n\n\n\n                <ion-segment [(ngModel)]="channelIn" color="primary" (ionChange)="segmentTransferIn($event)">\n\n                  <ion-segment-button value="day">\n\n                    {{settingsProvider.getLabel("Day")}}\n\n                  </ion-segment-button>\n\n                  <ion-segment-button value="month">\n\n                    {{settingsProvider.getLabel("Month")}}\n\n                  </ion-segment-button>\n\n                  <ion-segment-button value="year">\n\n                    {{settingsProvider.getLabel("Year")}}\n\n                  </ion-segment-button>\n\n                </ion-segment>\n\n\n\n\n\n                <div *ngIf="channelIn == \'day\'" style="border-style: ridge;border-color: #488aff;padding: 6px;">\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.daytransferinamount" (keyup)="_keyUp($event)" name="daytransferinamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.daytransferinmsg" name="daytransferinmsg"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.daymaxtransferinamount" (keyup)="_keyUp($event)" name="daymaxtransferinamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.daymaxtransferinamountmsg" name="daymaxtransferinamountmsg"></ion-input>\n\n                  </ion-item>\n\n\n\n                </div>\n\n                <div *ngIf="channelIn == \'month\'" style="border-style: ridge;border-color: #488aff;padding: 6px;">\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.monthtransferinamount" (keyup)="_keyUp($event)" name="monthtransferinamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.monthtransferinmsg" name="monthtransferinmsg"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.monthmaxtransferinamount" (keyup)="_keyUp($event)" name="monthmaxtransferinamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.monthmaxtransferinamountmsg" name="monthmaxtransferinamountmsg"></ion-input>\n\n                  </ion-item>\n\n\n\n                </div>\n\n                <div *ngIf="channelIn == \'year\'" style="border-style: ridge;border-color: #488aff;padding: 6px;">\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.yeartransferinamount" (keyup)="_keyUp($event)" name="yeartransferinamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.yeartransferinmsg" name="yeartransferinmsg"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.yearmaxtransferinamount" (keyup)="_keyUp($event)" name="yeartransferinmaxamt"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.yearmaxtransferinamountmsg" name="yearmaxtransferinamountmsg"></ion-input>\n\n                  </ion-item>\n\n                </div>\n\n                <!-- <table>\n\n                  <tr>\n\n                    <td>\n\n                      <ion-label>{{settingsProvider.getLabel("Interest Penalties")}}</ion-label>\n\n                    </td>\n\n                  </tr>\n\n                  <tr>\n\n                    <td>\n\n                      <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Yes")}}</ion-label>\n\n                        <ion-checkbox [checked]="accountObj.isPenaltyTransferOut" (ionChange)="toggleTransferOutPenalty(\'true\',$event)"></ion-checkbox>\n\n                      </ion-item>\n\n                    </td>\n\n                    <td>\n\n                      <ion-item>\n\n                        <ion-label> {{settingsProvider.getLabel("No")}}</ion-label>\n\n                        <ion-checkbox [checked]="!accountObj.isPenaltyTransferOut" (ionChange)="toggleTransferOutPenalty(\'false\',$event)"></ion-checkbox>\n\n                      </ion-item>\n\n                    </td>\n\n                  </tr>\n\n                </table> -->\n\n                <!-- <div *ngIf="accountObj.isPenaltyTransferOut">\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Interest Penalties Message\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.transferoutpenaltymsg" name="transferinwarning" style="padding-left: 14px;"></ion-input>\n\n                  </ion-item>\n\n                </div> -->\n\n              </div>\n\n            </div>\n\n            <div style="clear:both"></div>\n\n          </ion-card-content>\n\n        </ion-col>\n\n\n\n\n\n        <!--Transfer Out-->\n\n        <ion-col col-4>\n\n          <ion-card-content style="padding:0px 0px 0px 16px">\n\n            <div class="col-card" float-left text-center>\n\n              <h3 style="padding-top: 15px;">{{settingsProvider.getLabel("TRANSFER OUT")}}</h3>\n\n              <table>\n\n                <tr>\n\n                  <td>\n\n                    <ion-item>\n\n                      <ion-label>{{settingsProvider.getLabel("Yes")}}</ion-label>\n\n                      <ion-checkbox [checked]="accountObj.istransferout" (ionChange)="toggleTransferOut(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                  </td>\n\n                  <td>\n\n                    <ion-item>\n\n                      <ion-label> {{settingsProvider.getLabel("No")}}</ion-label>\n\n                      <ion-checkbox [checked]="!accountObj.istransferout" (ionChange)="toggleTransferOut(\'false\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                  </td>\n\n                </tr>\n\n              </table>\n\n\n\n              <div *ngIf="accountObj.istransferout">\n\n                <div style="padding:10px;">\n\n                  <fieldset style="width: 100%; margin-top: 5px; margin-bottom: 5px; text-align: left;">\n\n                    <legend>Per Transaction:</legend>\n\n                    <ion-label for="kraken" style="font-size: 13px;">{{settingsProvider.getLabel("Maximum Amount")}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.maxtransferoutamount" (keyup)="_keyUp($event)" name="maxtransferoutamount"></ion-input>\n\n                    <ion-label for="kraken" style="font-size: 13px;">{{settingsProvider.getLabel("Maximum Warning")}}</ion-label>\n\n                    <ion-input type="text" [(ngModel)]="accountObj.maxtransferoutmsg" name="maxtransferoutmsg"></ion-input>\n\n                    <ion-label for="kraken" style="font-size: 13px;">{{settingsProvider.getLabel("Minimum Amount")}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.mintransferoutamount" (keyup)="_keyUp($event)" name="mintransferoutamount"></ion-input>\n\n                    <ion-label for="kraken" style="font-size: 13px;">{{settingsProvider.getLabel("Minimum Warning")}}</ion-label>\n\n                    <ion-input type="text" [(ngModel)]="accountObj.mintransferoutmsg" name="mintransferoutmsg"></ion-input>\n\n                    <br/>\n\n                  </fieldset>\n\n                </div>\n\n\n\n\n\n\n\n                <ion-segment [(ngModel)]="channelOut" color="primary" (ionChange)="segmentTransferOut($event)">\n\n                  <ion-segment-button value="day">\n\n                    {{settingsProvider.getLabel("Day")}}\n\n                  </ion-segment-button>\n\n                  <ion-segment-button value="month">\n\n                    {{settingsProvider.getLabel("Month")}}\n\n                  </ion-segment-button>\n\n                  <ion-segment-button value="year">\n\n                    {{settingsProvider.getLabel("Year")}}\n\n                  </ion-segment-button>\n\n                </ion-segment>\n\n\n\n\n\n                <div *ngIf="channelOut == \'day\'" style="border-style: ridge;border-color: #488aff;padding: 6px;">\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.daytransferoutamount" (keyup)="_keyUp($event)" name="daytransferoutamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.daytransferoutmsg" name="daytransferoutmsg"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.daymaxtransferoutamount" (keyup)="_keyUp($event)" name="daymaxtransferoutamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.daymaxtransferoutamountmsg" name="daymaxtransferoutamountmsg"></ion-input>\n\n                  </ion-item>\n\n\n\n                </div>\n\n                <div *ngIf="channelOut == \'month\'" style="border-style: ridge;border-color: #488aff;padding: 6px;">\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.monthtransferoutamount" (keyup)="_keyUp($event)" (keyup)="_keyUp($event)"\n\n                      name="monthtransferoutamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.monthtransferoutmsg" name="monthtransferoutmsg"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.monthmaxtransferoutamount" (keyup)="_keyUp($event)" name="monthmaxtransferoutamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.monthmaxtransferoutamountmsg" name="monthmaxtransferoutamountmsg" style="padding-left: 14px;"></ion-input>\n\n                  </ion-item>\n\n                </div>\n\n                <div *ngIf="channelOut == \'year\'" style="border-style: ridge;border-color: #488aff;padding: 6px;">\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.yeartransferoutamount" (keyup)="_keyUp($event)" name="yeartransferoutamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Number of Transactions Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.yeartransferoutmsg" name="yeartransferoutmsg"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.yearmaxtransferoutamount" (keyup)="_keyUp($event)" name="yearmaxtransferoutamount"></ion-input>\n\n                  </ion-item>\n\n\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Amount Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.yearmaxtransferoutamountmsg" name="yearmaxtransferoutamountmsg"></ion-input>\n\n                  </ion-item>\n\n\n\n                </div>\n\n\n\n                <!--Interest Penalties-->\n\n                <div style="padding: 10px;">\n\n                  <table>\n\n                    <tr>\n\n                      <td>\n\n                        <ion-label>{{settingsProvider.getLabel("Interest Penalties")}}</ion-label>\n\n                      </td>\n\n                    </tr>\n\n                    <tr>\n\n                      <td>\n\n                        <ion-item>\n\n                          <ion-label>{{settingsProvider.getLabel("Yes")}}</ion-label>\n\n                          <ion-checkbox [checked]="accountObj.isPenaltyTransferOut" (ionChange)="toggleTransferOutPenalty(\'true\',$event)"></ion-checkbox>\n\n                        </ion-item>\n\n                      </td>\n\n                      <td>\n\n                        <ion-item>\n\n                          <ion-label> {{settingsProvider.getLabel("No")}}</ion-label>\n\n                          <ion-checkbox [checked]="!accountObj.isPenaltyTransferOut" (ionChange)="toggleTransferOutPenalty(\'false\',$event)"></ion-checkbox>\n\n                        </ion-item>\n\n                      </td>\n\n                    </tr>\n\n                  </table>\n\n                </div>\n\n\n\n                <div *ngIf="accountObj.isPenaltyTransferOut" style="padding: 10px;">\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Interest Penalty Amount\')}}</ion-label>\n\n                    <ion-input type="number" [(ngModel)]="accountObj.transferoutpenaltyamount" (keyup)="_keyUp($event)" name="transferoutpenaltyamount"></ion-input>\n\n                  </ion-item>\n\n                  <ion-item no-padding>\n\n                    <ion-label stacked>{{settingsProvider.getLabel(\'Interest Penalty Warning\')}}</ion-label>\n\n                    <ion-input type="tel" [(ngModel)]="accountObj.transferoutpenaltymsg" name="transferoutpenaltymsg"></ion-input>\n\n                  </ion-item>\n\n                </div>\n\n              </div>\n\n            </div>\n\n            <div style="clear:both"></div>\n\n          </ion-card-content>\n\n        </ion-col>\n\n\n\n      </ion-row>\n\n\n\n\n\n\n\n\n\n    </ion-card>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\saving-account-rules\saving-account-rules.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_saving_account_rules_list_saving_account_rules_list__["a" /* SavingAccountRulesListProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], SavingAccountRulesPage);

//# sourceMappingURL=saving-account-rules.js.map

/***/ })

});
//# sourceMappingURL=8.js.map