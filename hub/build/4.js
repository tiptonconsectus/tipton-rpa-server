webpackJsonp([4],{

/***/ 591:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionPageModule", function() { return TransactionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__transaction__ = __webpack_require__(628);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var TransactionPageModule = (function () {
    function TransactionPageModule() {
    }
    return TransactionPageModule;
}());
TransactionPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__transaction__["a" /* TransactionPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__transaction__["a" /* TransactionPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], TransactionPageModule);

//# sourceMappingURL=transaction.module.js.map

/***/ }),

/***/ 628:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_dashboard_dashboard__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_transaction_transaction__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var TransactionPage = (function () {
    function TransactionPage(navCtrl, dashboardProvider, navParams, oauthProvider, languageProvider, transactionProvider, toastCtrl, globalProvider, settingsProvider, viewCtrl, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.dashboardProvider = dashboardProvider;
        this.navParams = navParams;
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.transactionProvider = transactionProvider;
        this.toastCtrl = toastCtrl;
        this.globalProvider = globalProvider;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.transactionstatus = "";
        this.period = "";
        this.channel = "";
        this.transactionOptions = {
            data: [],
            filteredData: [],
            columns: {
                "customer_account_transactions_id": { title: "Transaction ID", search: true },
                "create_date": { title: "Date", search: true, format: 'date' },
                "amount": { title: "Amount", format: 'currency', search: true },
                "type": { title: "Transfer Type", search: true },
                "transferaccount": { title: "From Account", search: true },
                "status": { title: "Status", search: true }
            },
            pagination: true,
            filter: null,
            search: true,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "",
                addAction: this.addTransaction.bind(this),
            },
            tapRow: this.editTransaction.bind(this),
            uploadOptions: {},
            resetFilter: null,
        };
        this.resultdata = null;
        this.languageString = "en";
        this.transactionstatus = navParams.get("customerstatus");
        this.period = navParams.get("type");
        this.channel = navParams.get("channel");
    }
    TransactionPage.prototype.addTransaction = function () {
        this.navCtrl.push("TransactionDetailsPage");
    };
    TransactionPage.prototype.editTransaction = function (transactionOptions) {
        this.navCtrl.push("TransactionDetailsPage", { transactionData: transactionOptions });
    };
    TransactionPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("transactions")) {
            return true;
        }
        else {
            return false;
        }
    };
    TransactionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TransactionPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    TransactionPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        var self = this;
        if (self.oauthProvider.user_role.transactions) {
            if (this.period != undefined && this.channel != undefined) {
                this.dashboardProvider.getDashboardFilterTransaction(this.transactionstatus, this.period, this.channel)
                    .then(function (data) {
                    self.transactionOptions.data = data;
                    self.transactionOptions = Object.assign({}, self.transactionOptions);
                }).catch(function (error) {
                });
            }
            else {
                this.transactionProvider.getyearlytransactions()
                    .then(function (result) {
                    self.transactionOptions.data = result.data;
                    self.transactionOptions = Object.assign({}, self.transactionOptions);
                });
            }
        }
        else {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.setRoot("DashboardPage");
        }
    };
    return TransactionPage;
}());
TransactionPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-transaction',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\transaction\transaction.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel("Transactions")}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <table-component [options]="transactionOptions"></table-component>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\transaction\transaction.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_dashboard_dashboard__["a" /* DashboardProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_transaction_transaction__["a" /* TransactionProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], TransactionPage);

//# sourceMappingURL=transaction.js.map

/***/ })

});
//# sourceMappingURL=4.js.map