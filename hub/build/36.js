webpackJsonp([36],{

/***/ 560:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppSettingPageModule", function() { return AppSettingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_setting__ = __webpack_require__(597);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AppSettingPageModule = (function () {
    function AppSettingPageModule() {
    }
    return AppSettingPageModule;
}());
AppSettingPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_setting__["a" /* AppSettingPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__app_setting__["a" /* AppSettingPage */]),
            __WEBPACK_IMPORTED_MODULE_1__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], AppSettingPageModule);

//# sourceMappingURL=app-setting.module.js.map

/***/ }),

/***/ 597:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_setting_app_setting__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_onboarding_setup_onboarding_setup__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










// declare var window: any;
// declare var LocalFileSystem: any;
// declare var navigator: any;
var AppSettingPage = (function () {
    function AppSettingPage(navCtrl, navParams, appSettingProvider, languageProvider, oauthProvider, toastCtrl, alertCtrl, viewCtrl, settingsProvider, cdr, globalProvider, onboardingSetupProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appSettingProvider = appSettingProvider;
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.settingsProvider = settingsProvider;
        this.cdr = cdr;
        this.globalProvider = globalProvider;
        this.onboardingSetupProvider = onboardingSetupProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.title = "App Setting";
        this.resultdata = null;
        this.languageString = "en";
        this.touchID = false;
        this.currentTab = "bankdetails";
        this.currentTab2 = "registrationfield";
        this.onboard = {
            ismailerforexisting: "",
            ismailerfornewexisting: "",
            isregisterforexisting: "",
            isregisterfornewexisting: "",
            passcodetimeout: ""
        };
        this.register = {
            registrationfields_id: "",
            firstname: "",
            lastname: "",
            dob: ""
        };
        this.login = {
            istouchid: "",
            ismaxtouchid: ""
        };
        this.deviceData = {
            data: [],
            filteredData: [],
            columns: {
                "devicetype": { title: "Device Type", search: true },
                "version": { title: "App Version", search: true },
                "appname": { title: "App Name", search: true },
                "appurl": { title: "App URL", search: true },
            },
            pagination: true,
            filter: null,
            search: true,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add",
                addAction: this.createVersion.bind(this),
            },
            tapRow: this.editCustomer.bind(this),
            // resetFilter: null,
            uploadOptions: {},
            resetFilter: null,
        };
        this.dirtyBankDetailsFlag = false;
        this.dirtyLookAndFeelFlag = false;
        {
            this.bank = {
                savingsaccount: false,
                mortgageaccount: false,
                currentaccount: false,
                externalaccount: false
            };
        }
    }
    AppSettingPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("admin")) {
            return true;
        }
        else {
            return false;
        }
    };
    AppSettingPage.prototype.saveBankLookAndFeel = function () {
        var self = this;
        this.settingsProvider.updateBankLookAndFeel(this.lookandfeel)
            .then(function (data) {
            self.dirtyLookAndFeelFlag = false;
            var toast = self.toastCtrl.create({
                message: 'Bank Look and Feel updated.',
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    AppSettingPage.prototype.saveBankDetails = function () {
        var self = this;
        console.log("this.bank", this.bank);
        this.bankData = {
            "users_id": 2,
            "banksettings_id": 1,
            "savingsaccount": 1,
            "mortgageaccount": 1,
            "currentaccount": 1,
            "externalaccount": 1,
            "menu_new_account": 1,
            "menu_existing_account": 1,
            "menu_prodcuts": 1,
            "menu_savinganalysis": 1,
            "menu_goals": 1,
            "menu_calculators": 1,
            "menu_cheque": 1,
            "menu_contactus": 1,
            "menu_thirdparty": 1,
            "menu_settings": 1,
            "menu_termsconditions": 1,
            "menu_branches": 1,
            "menu_paycontacts": 1,
            "menu_contactusMessages": 1,
            "menu_productApplication": 1,
            "menu_messages": 1,
            "menu_members": 1,
            "menu_saver": 1,
            "menu_saver_regular": 1,
            "menu_saver_smart": 1,
            "menu_saver_sweeper": 1,
            "menu_regularsavings": 1
        };
        this.bank.savingsaccount = this.bank.savingsaccount ? 1 : 0;
        this.bank.externalaccount = this.bank.externalaccount ? 1 : 0;
        this.bank.menu_branches = this.bank.menu_branches ? 1 : 0;
        this.bank.menu_calculators = this.bank.menu_calculators ? 1 : 0;
        this.bank.menu_calculators = this.bank.menu_calculators ? 1 : 0;
        this.bank.menu_contactus = this.bank.menu_contactus ? 1 : 0;
        this.bank.menu_goals = this.bank.menu_goals ? 1 : 0;
        this.bank.menu_new_account = this.bank.menu_new_account ? 1 : 0;
        this.bank.menu_paycontacts = this.bank.menu_paycontacts ? 1 : 0;
        this.bank.menu_contactusMessages = this.bank.menu_contactusMessages ? 1 : 0;
        this.bank.menu_productApplication = this.bank.menu_productApplication ? 1 : 0;
        this.bank.menu_prodcuts = this.bank.menu_prodcuts ? 1 : 0;
        this.bank.menu_saver = this.bank.menu_saver ? 1 : 0;
        this.bank.menu_saver_regular = this.bank.menu_saver_regular ? 1 : 0;
        this.bank.menu_saver_smart = this.bank.menu_saver_smart ? 1 : 0;
        this.bank.menu_saver_sweeper = this.bank.menu_saver_sweeper ? 1 : 0;
        this.bank.menu_settings = this.bank.menu_settings ? 1 : 0;
        this.bank.menu_cheque = this.bank.menu_cheque ? 1 : 0;
        this.bank.menu_termsconditions = this.bank.menu_termsconditions ? 1 : 0;
        this.bank.mortgageaccount = this.bank.mortgageaccount ? 1 : 0;
        this.bank.menu_thirdparty = this.bank.menu_thirdparty ? 1 : 0;
        this.bank.menu_existing_account = this.bank.menu_existing_account ? 1 : 0;
        this.bank.menu_messages = this.bank.menu_messages ? 1 : 0;
        this.bank.currentaccount = this.bank.currentaccount ? 1 : 0;
        this.bank.menu_members = this.bank.menu_members ? 1 : 0;
        this.bank.menu_messages = this.bank.menu_messages ? 1 : 0;
        this.bank.menu_savinganalysis = this.bank.menu_savinganalysis ? 1 : 0;
        this.bankData.banksettings_id = 1;
        this.bankData.menu_regularsavings = 1;
        this.bankData.savingsaccount = this.bank.savingsaccount;
        this.bankData.mortgageaccount = this.bank.mortgageaccount;
        this.bankData.externalaccount = this.bank.externalaccount;
        this.bankData.currentaccount = this.bank.currentaccount;
        this.bankData.menu_branches = this.bank.menu_branches;
        this.bankData.menu_calculators = this.bank.menu_calculators;
        this.bankData.menu_calculators = this.bank.menu_calculators;
        this.bankData.menu_contactus = this.bank.menu_contactus;
        this.bankData.menu_goals = this.bank.menu_goals;
        this.bankData.menu_new_account = this.bank.menu_new_account;
        this.bankData.menu_paycontacts = this.bank.menu_paycontacts;
        this.bankData.menu_contactusMessages = this.bank.menu_contactusMessages;
        this.bankData.menu_productApplication = this.bank.menu_productApplication;
        this.bankData.menu_prodcuts = this.bank.menu_prodcuts;
        this.bankData.menu_saver = this.bank.menu_saver;
        this.bankData.menu_saver_regular = this.bank.menu_saver_regular;
        this.bankData.menu_saver_smart = this.bank.menu_saver_smart;
        this.bankData.menu_saver_sweeper = this.bank.menu_saver_sweeper;
        this.bankData.menu_settings = this.bank.menu_settings;
        this.bankData.menu_cheque = this.bank.menu_cheque;
        this.bankData.menu_termsconditions = this.bank.menu_termsconditions;
        this.bankData.menu_thirdparty = this.bank.menu_thirdparty;
        this.bankData.menu_existing_account = this.bank.menu_existing_account;
        this.bankData.menu_members = this.bank.menu_members;
        this.bankData.menu_messages = this.bank.menu_messages;
        this.bankData.menu_savinganalysis = this.bank.menu_savinganalysis;
        this.settingsProvider.updateBankDetails(this.bankData)
            .then(function (data) {
            self.dirtyBankDetailsFlag = false;
            var toast = self.toastCtrl.create({
                message: 'Bank Details updated.',
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.setRoot("DashboardPage");
        });
    };
    AppSettingPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        this.loadAppData();
        switch (this.currentTab) {
            case 'bankdetails':
                this.loadBankDetails();
                break;
            // case 'lookandfeel':
            //   this.loadLookAndFeelData();
            //   break;
            case 'contactus':
                this.appversion();
                break;
            case 'onboarding':
                this.getregistrationfieldsv1();
                break;
        }
        var self = this;
        if (self.oauthProvider.user_role.admin) {
            this.appSettingProvider.getappversion()
                .then(function (result) {
                self.deviceData.data = result.data;
                self.deviceData = Object.assign({}, self.deviceData);
            });
        }
        else {
            var toast = self.toastCtrl.create({
                message: '',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.setRoot("DashboardPage");
        }
    };
    //start of tab section 
    AppSettingPage.prototype.segmentChanged = function (event) {
        console.log("eventValue", event._value);
        switch (event._value) {
            case 'bankdetails':
                this.loadBankDetails();
                break;
            case 'lookandfeel':
                this.loadLookAndFeelData();
                break;
            case 'faqs':
                this.appversion();
                break;
            case 'onboarding':
                this.getregistrationfieldsv1();
                break;
        }
    };
    //tab section in super admin
    AppSettingPage.prototype.segmentChanged1 = function (event) {
        console.log("eventValue", event._value);
        switch (event._value) {
            case 'registrationfield':
                this.getregistrationfieldsv1();
                break;
            case 'onboardingProcess':
                this.getonboardingprocess();
                break;
            case 'loginProcess':
                this.getloginprocess();
                break;
        }
    };
    AppSettingPage.prototype.loadBankDetails = function () {
        var _this = this;
        var self = this;
        this.globalProvider.showLoader();
        this.settingsProvider.getBankDetails()
            .then(function (result) {
            if (result.status) {
                _this.globalProvider.hideLoader();
                self.bank = result.data[0];
            }
        }).catch(function (error) {
            _this.globalProvider.hideLoader();
        });
    };
    AppSettingPage.prototype.dirtyBankDetails = function () {
        this.dirtyBankDetailsFlag = true;
    };
    AppSettingPage.prototype.dirtyLookAndFeel = function () {
        this.dirtyLookAndFeelFlag = true;
    };
    AppSettingPage.prototype.loadLookAndFeelData = function () {
        var _this = this;
        var self = this;
        this.globalProvider.showLoader();
        this.settingsProvider.getBankLookAndFeel()
            .then(function (data) {
            _this.globalProvider.hideLoader();
            self.lookandfeel = data;
        }).catch(function (error) {
            _this.globalProvider.hideLoader();
        });
    };
    AppSettingPage.prototype.appversion = function () {
        var _this = this;
        var self = this;
        this.globalProvider.showLoader();
        this.appSettingProvider.getAppData()
            .then(function (result) {
            _this.globalProvider.hideLoader();
            _this.appVersionData = result;
        }).catch(function (error) {
            _this.globalProvider.hideLoader();
        });
    };
    AppSettingPage.prototype.getonboardingprocess = function () {
        var self = this;
        // this.globalProvider.showLoader();
        this.onboardingSetupProvider.getonboardingprocess()
            .then(function (result) {
            if (result.data) {
                // this.globalProvider.hideLoader();
                self.onboard = result.data;
            }
        }).catch(function (error) {
            // this.globalProvider.hideLoader();
        });
    };
    AppSettingPage.prototype.getregistrationfieldsv1 = function () {
        var self = this;
        // this.globalProvider.showLoader();
        this.onboardingSetupProvider.getregistrationfieldsv1()
            .then(function (result) {
            if (result.data) {
                // this.globalProvider.hideLoader();
                self.register = result.data;
            }
        }).catch(function (error) {
            // this.globalProvider.hideLoader();
        });
    };
    // getloginprocess() {
    //   let self = this;
    //   // this.globalProvider.showLoader();
    //   this.onboardingSetupProvider.getloginprocess()
    //     .then((result: any) => {
    //       if (result.data) {
    //         // this.globalProvider.hideLoader();
    //         self.login = result.data;
    //       }
    //     }).catch(error => {
    //       // this.globalProvider.hideLoader();
    //     });
    // }
    AppSettingPage.prototype.getloginprocess = function () {
        var self = this;
        // this.globalProvider.showLoader();
        this.onboardingSetupProvider.getloginprocess()
            .then(function (result) {
            if (result.data) {
                // this.globalProvider.hideLoader();
                if (result.data.ismaxtouchid == 999999) {
                    result.data.ismaxtouchid = 0;
                }
                result.data.istouchid ? self.touchID = true : self.touchID = false;
                self.login = result.data;
            }
        }).catch(function (error) {
            // this.globalProvider.hideLoader();
        });
    };
    AppSettingPage.prototype.updateloginprocess = function () {
        var _this = this;
        var self = this;
        // if (self.login.ismaxtouchid == "" || self.login.ismaxtouchid == null ) {
        //   this.errorAlert(this.settingsProvider.getLabel("Please enter value of MaxTouchID"));
        // } else {
        console.log(self.touchID, "TouchiD");
        var loginData = {
            istouchid: this.login.istouchid,
            ismaxtouchid: this.login.ismaxtouchid == 0 || this.login.ismaxtouchid == "0" ? 999999 : this.login.ismaxtouchid
        };
        // if (self.login.ismaxtouchid == "" || self.login.ismaxtouchid == null) {
        //   this.errorAlert(this.settingsProvider.getLabel("Please enter value of MaxTouchID"));
        // } else {
        self.onboardingSetupProvider.updateloginprocess(loginData).then(function (result) {
            console.log("result accounttype", result);
            if (result.status) {
                var toast = self.toastCtrl.create({
                    message: result.msg,
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                _this.getloginprocess();
            }
        });
    };
    AppSettingPage.prototype.UpdateOnboardingProcess = function () {
        var _this = this;
        var self = this;
        var onboardingData = {
            ismailerforexisting: this.onboard.ismailerforexisting,
            ismailerfornewexisting: this.onboard.ismailerfornewexisting,
            isregisterforexisting: this.onboard.isregisterforexisting,
            isregisterfornewexisting: this.onboard.isregisterfornewexisting,
            passcodetimeout: this.onboard.passcodetimeout
        };
        self.onboardingSetupProvider.updateonboardingprocess(onboardingData).then(function (result) {
            if (result.status) {
                var toast = self.toastCtrl.create({
                    message: result.msg,
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                _this.getloginprocess();
            }
        });
    };
    AppSettingPage.prototype.TouchEnable = function () {
        this.touchID ? this.login.istouchid = 1 : this.login.istouchid = 0;
    };
    AppSettingPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        else {
            var self_1 = this;
        }
    };
    AppSettingPage.prototype.loadAppData = function () {
        var _this = this;
        var self = this;
        this.appSettingProvider.getAppData()
            .then(function (result) {
            self.data = result.data;
            self.deviceData.data = result;
            self.deviceData = Object.assign({}, self.deviceData);
            _this.cdr.detectChanges();
        });
    };
    AppSettingPage.prototype.createVersion = function () {
        this.navCtrl.push("AppversionPage");
    };
    AppSettingPage.prototype.editCustomer = function (appVersionData) {
        this.globalProvider.showLoader();
        this.navCtrl.push("AppversionPage", { appData: appVersionData });
    };
    AppSettingPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: this.settingsProvider.getLabel('Ok'),
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    AppSettingPage.prototype.commonFunction = function (nullData) {
        var str = "";
        if (nullData == 0) {
            str = '--';
        }
        else if (nullData == null) {
            str = '--';
        }
        else if (nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    return AppSettingPage;
}());
AppSettingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-app-setting',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\app-setting\app-setting.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white.png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(\'Super Admin | Consectus\')}} </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n\n\n</ion-content>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n    <ion-segment-button value="bankdetails">\n\n      {{settingsProvider.getLabel(\'Functions\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="appversion">\n\n      {{settingsProvider.getLabel(\'App Version\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="onboarding">\n\n      {{settingsProvider.getLabel(\'Onboarding\')}}\n\n    </ion-segment-button>\n\n    <!-- <ion-segment-button value="registrationfield">\n\n      {{languageProvider.get(\'Registration Fields\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="onboardingProcess">\n\n      {{languageProvider.get(\'Onboarding Process\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="loginProcess">\n\n      {{languageProvider.get(\'Login Process\')}}\n\n    </ion-segment-button> -->\n\n  </ion-segment>\n\n\n\n  <!--App Version-->\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'appversion\'">\n\n\n\n    <table-component [options]="deviceData"></table-component>\n\n\n\n  </ion-grid>\n\n\n\n  <!--Onboarding-->\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'onboarding\'">\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n\n\n              <!--TABS-->\n\n              <ion-segment [(ngModel)]="currentTab2" (ionChange)="segmentChanged1($event)">\n\n                <ion-segment-button value="registrationfield">\n\n                  {{languageProvider.get(\'Registration Fields\')}}\n\n                </ion-segment-button>\n\n                <ion-segment-button value="onboardingProcess">\n\n                  {{languageProvider.get(\'Onboarding Process\')}}\n\n                </ion-segment-button>\n\n                <ion-segment-button value="loginProcess">\n\n                  {{languageProvider.get(\'Login Process\')}}\n\n                </ion-segment-button>\n\n              </ion-segment>\n\n\n\n              <!--registrationfield-->\n\n              <ion-grid class="table-cell" *ngIf="currentTab2 == \'registrationfield\'">\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-grid class="table-simple">\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label stacked>{{languageProvider.get(\'Registration Id\')}}</ion-label>\n\n                            <ion-input type="text" disabled [(ngModel)]="register.registrationfields_id" placeholder="{{languageProvider.get(\'Registration Id\')}}"></ion-input>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row>\n\n\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label stacked>{{languageProvider.get(\'First Name\')}}</ion-label>\n\n                            <ion-input type="text" disabled [(ngModel)]="register.firstname" placeholder="{{languageProvider.get(\'First Name\')}}"></ion-input>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row>\n\n\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label stacked>{{languageProvider.get(\'Last Name\')}}</ion-label>\n\n                            <ion-input type="text" disabled [(ngModel)]="register.lastname" placeholder="{{languageProvider.get(\'Last Name\')}}"></ion-input>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row>\n\n\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label stacked>{{languageProvider.get(\'DOB\')}}</ion-label>\n\n                            <ion-input type="text" disabled [(ngModel)]="register.dob" placeholder="{{languageProvider.get(\'DOB\')}}"></ion-input>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                    </ion-grid>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n\n\n              <!--onboarding process-->\n\n              <ion-grid class="table-cell" *ngIf="currentTab2 == \'onboardingProcess\'">\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-toolbar>\n\n                      <ion-buttons end>\n\n                        <button ion-button small round icon-left color="primary" (click)="UpdateOnboardingProcess()">\n\n                          <ion-icon name="checkmark-circle"></ion-icon>\n\n                          {{settingsProvider.getLabel(\'Save Changes\')}}\n\n                        </button>\n\n                      </ion-buttons>\n\n                    </ion-toolbar>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-grid class="table-simple">\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label stacked>{{languageProvider.get(\'Mailer for Existing Customer\')}}</ion-label>\n\n                            <ion-input type="text" [(ngModel)]="onboard.ismailerforexisting" placeholder="{{languageProvider.get(\'Mailer for Existing Customer\')}}"></ion-input>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row>\n\n\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label stacked>{{languageProvider.get(\'Mailer for New Existing Customer\')}}</ion-label>\n\n                            <ion-input type="text" [(ngModel)]="onboard.ismailerfornewexisting" placeholder="{{languageProvider.get(\'Mailer for New Existing Customer\')}}"></ion-input>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row>\n\n\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label stacked>{{languageProvider.get(\'Register for existing Customer\')}}</ion-label>\n\n                            <ion-input type="text" [(ngModel)]="onboard.isregisterforexisting" placeholder="{{languageProvider.get(\'Register for existing Customer\')}}"></ion-input>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row>\n\n\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label stacked>{{languageProvider.get(\'Register for new Existing\')}}</ion-label>\n\n                            <ion-input type="text" [(ngModel)]="onboard.isregisterfornewexisting" placeholder="{{languageProvider.get(\'Register for new Existing\')}}"></ion-input>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                      <!-- <ion-row>\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label stacked>{{languageProvider.get(\'Passcode Timeout\')}}</ion-label>\n\n                            <ion-input type="text" disabled [(ngModel)]="onboard.passcodetimeout" placeholder="{{languageProvider.get(\'Register for new Existing\')}}"></ion-input>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row> -->\n\n\n\n                      <ion-row style="margin-left: 17px;">\n\n                        <ion-col>\n\n                          <ion-label stacked>{{settingsProvider.getLabel(\'Passcode Timeout\')}}</ion-label>\n\n                          <ion-range min="0" max="10" pin="true" snaps="true" step="1" [(ngModel)]="onboard.passcodetimeout">\n\n                            <ion-label range-left>0</ion-label>\n\n                            <ion-label range-right>10</ion-label>\n\n                          </ion-range>\n\n                        </ion-col>\n\n                      </ion-row>\n\n\n\n                    </ion-grid>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n\n\n              <!--login Process-->\n\n              <ion-grid class="table-cell" *ngIf="currentTab2 == \'loginProcess\'">\n\n                <!-- <ion-row>\n\n        <ion-col>\n\n          <ion-grid class="table-simple">\n\n            <ion-row>\n\n              <ion-col>\n\n                <ion-item>\n\n                  <ion-label stacked>{{languageProvider.get(\'Touch ID\')}}</ion-label>\n\n                  <ion-input type="text" disabled [(ngModel)]="login.istouchid" placeholder="{{languageProvider.get(\'Touch ID\')}}"></ion-input>\n\n                </ion-item>\n\n              </ion-col>\n\n            </ion-row>\n\n  \n\n            <ion-row>\n\n              <ion-col>\n\n                <ion-item>ismaxtouchid\n\n                  <ion-label stacked>{{languageProvider.get(\'Max Touch ID\')}}</ion-label>\n\n                  <ion-input type="text" disabled [(ngModel)]="login.ismaxtouchid" placeholder="{{languageProvider.get(\'Max Touch ID\')}}"></ion-input>\n\n                </ion-item>\n\n              </ion-col>\n\n            </ion-row>\n\n  \n\n          </ion-grid>\n\n        </ion-col>\n\n      </ion-row> -->\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-toolbar>\n\n                      <ion-buttons end>\n\n                        <button ion-button small round icon-left color="primary" (click)="updateloginprocess()">\n\n                          <ion-icon name="checkmark-circle"></ion-icon>\n\n                          {{settingsProvider.getLabel(\'Save Changes\')}}\n\n                        </button>\n\n                      </ion-buttons>\n\n                    </ion-toolbar>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-grid class="table-simple">\n\n                      <ion-row style="margin-left: 17px;">\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label>{{settingsProvider.getLabel(\'Touch ID\')}}</ion-label>\n\n                            <ion-checkbox [(ngModel)]="touchID" (ionChange)="TouchEnable()"></ion-checkbox>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                      <!-- <ion-row>\n\n                        <ion-col>\n\n                          <ion-item>\n\n                            <ion-label stacked>{{languageProvider.get(\'Maximum Touch ID\')}}</ion-label>\n\n                            <ion-input type="text" [(ngModel)]="login.ismaxtouchid" placeholder="{{languageProvider.get(\'Max Touch ID\')}}"></ion-input>\n\n                          </ion-item>\n\n                        </ion-col>\n\n                      </ion-row> -->\n\n\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <ion-label stacked>{{settingsProvider.getLabel(\'Maximum Touch ID\')}}</ion-label>\n\n                          <ion-range min="0" max="20" pin="true" snaps="true" step="1" [(ngModel)]="login.ismaxtouchid">\n\n                            <ion-label range-left>0</ion-label>\n\n                            <ion-label range-right>20</ion-label>\n\n                          </ion-range>\n\n                        </ion-col>\n\n                      </ion-row>\n\n\n\n                    </ion-grid>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n\n\n\n\n  <!--Bank Details-->\n\n  <ion-grid *ngIf="currentTab == \'bankdetails\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="saveBankDetails()" *ngIf="dirtyBankDetailsFlag">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row class="header">\n\n            <ion-col>\n\n              {{settingsProvider.getLabel(\'Functions\')}}\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              {{settingsProvider.getLabel(\'Type of Accounts:\')}}\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label>{{settingsProvider.getLabel(\'Current Accounts\')}}</ion-label>\n\n                <ion-checkbox [(ngModel)]="bank.currentaccount" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label>{{settingsProvider.getLabel(\'Savings Accounts\')}}</ion-label>\n\n                <ion-checkbox [(ngModel)]="bank.savingsaccount" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label>{{settingsProvider.getLabel(\'Mortgage Accounts\')}} </ion-label>\n\n                <ion-checkbox [(ngModel)]="bank.mortgageaccount" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label>{{settingsProvider.getLabel(\'External Accounts\')}}</ion-label>\n\n                <ion-checkbox [(ngModel)]="bank.externalaccount" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              {{settingsProvider.getLabel(\'Option of Menus:\')}}\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col col-6>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label> {{settingsProvider.getLabel(\'Accounts\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_existing_account" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Products\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_prodcuts" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Saver\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_saver" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Regular Savings\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_saver_regular" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Smart Savings\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_saver_smart" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Sweeper\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_saver_sweeper" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Member\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_members" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Messages\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_messages" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Find a Branch near me\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_branches" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Pay Contacts\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_paycontacts" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-col>\n\n            <ion-col col-6>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Deposit a Cheque\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_cheque" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'3rd Party Notifications\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_thirdparty" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Settings\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_settings" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Terms & Conditions\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_termsconditions" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Savings Analysis\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_savinganalysis" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Savings Goals\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_goals" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Mortgage Calculator\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_calculators" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'Contact Us Messages\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_contactusMessages" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-item>\n\n                    <ion-label>{{settingsProvider.getLabel(\'New Product Application\')}}</ion-label>\n\n                    <ion-checkbox [(ngModel)]="bank.menu_productApplication" (ionChange)="dirtyBankDetails()"></ion-checkbox>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n\n\n  <!--Look And Feel-->\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'lookandfeel\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="saveBankLookAndFeel()" *ngIf="dirtyLookAndFeelFlag">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row class="header">\n\n            <ion-col>\n\n              {{settingsProvider.getLabel(\'Bank Look & Feel\')}}\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Primary Color\')}}</ion-label>\n\n                <ion-input [style.color]="lookandfeel.primary_color" type="text" [(ngModel)]="lookandfeel.primary_color" placeholder="{{settingsProvider.getLabel(\'Primary Color\')}}"\n\n                  (input)="dirtyLookAndFeel()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Text Color\')}}</ion-label>\n\n                <ion-input [style.color]="lookandfeel.text_color" type="text" [(ngModel)]="lookandfeel.text_color" placeholder="{{settingsProvider.getLabel(\'Text Color\')}}"\n\n                  (input)="dirtyLookAndFeel()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Sub Color\')}}</ion-label>\n\n                <ion-input [style.background-color]="lookandfeel.sub_color" type="text" [(ngModel)]="lookandfeel.sub_color" placeholder="{{settingsProvider.getLabel(\'Sub Color\')}}"\n\n                  (input)="dirtyLookAndFeel()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Page Background Color\')}}</ion-label>\n\n                <ion-input [style.background-color]="lookandfeel.page_background_color" type="text" [(ngModel)]="lookandfeel.page_background_color"\n\n                  placeholder="{{settingsProvider.getLabel(\'Page Background Color\')}}" (input)="dirtyLookAndFeel()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Global CSS\')}}</ion-label>\n\n                <ion-textarea [(ngModel)]="lookandfeel.css" placeholder="{{settingsProvider.getLabel(\'Global CSS\')}}" (input)="dirtyLookAndFeel()"></ion-textarea>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\app-setting\app-setting.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_app_setting_app_setting__["a" /* AppSettingProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_6__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_onboarding_setup_onboarding_setup__["a" /* OnboardingSetupProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], AppSettingPage);

//# sourceMappingURL=app-setting.js.map

/***/ })

});
//# sourceMappingURL=36.js.map