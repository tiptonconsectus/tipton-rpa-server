webpackJsonp([12],{

/***/ 584:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductAppDetailsPageModule", function() { return ProductAppDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_app_details__ = __webpack_require__(621);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProductAppDetailsPageModule = (function () {
    function ProductAppDetailsPageModule() {
    }
    return ProductAppDetailsPageModule;
}());
ProductAppDetailsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__product_app_details__["a" /* ProductAppDetailsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__product_app_details__["a" /* ProductAppDetailsPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], ProductAppDetailsPageModule);

//# sourceMappingURL=product-app-details.module.js.map

/***/ }),

/***/ 621:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductAppDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_product_app_product_app__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ProductAppDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProductAppDetailsPage = (function () {
    function ProductAppDetailsPage(navCtrl, navParams, settingsProvider, productAppProvider, oauthProvider, loadauditProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.settingsProvider = settingsProvider;
        this.productAppProvider = productAppProvider;
        this.oauthProvider = oauthProvider;
        this.loadauditProvider = loadauditProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.productAppData = null;
        this.resultdata = null;
        this.newAccountActivityOptions = {
            data: [],
            filteredData: [],
            columns: {
                "create_date": { title: "Date", format: 'function', fn: this.formatDateTime.bind(this), col: 5 },
                "trail_details": { title: "Trail Details", search: true },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            add: {
                addActionTitle: "",
                addAction: null,
            },
            rowActions: [],
            columnClass: "table-column",
            // tapRow: this.showUser.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
        var self = this;
        this.productAppData = navParams.get('productAppData');
        console.log(this.productAppData, " this.productAppData");
        this.productAppData = {
            firstname: this.productAppData.firstname,
            lastname: this.productAppData.lastname,
            postalcode: this.productAppData.postalcode,
            nationalinsuranceno: this.productAppData.nationalinsuranceno,
            nominatedbankno: this.productAppData.nominatedbankno,
            devicetype: this.productAppData.devicetype,
            productcode: this.productAppData.productcode,
            state_status: this.productAppData.state_status,
            moveindate: this.productAppData.moveindate,
            moveoutdate: this.productAppData.moveoutdate,
            accountholdername: this.productAppData.accountholdername,
            emailid: this.productAppData.emailid,
            gender: this.productAppData.gender,
            dob: this.productAppData.dob
        };
        this.naccountid = this.productAppData.naccountid;
    }
    ProductAppDetailsPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("products")) {
            return true;
        }
        else {
            return false;
        }
    };
    ProductAppDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductAppDetailsPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    ProductAppDetailsPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var message = "Viewed " + this.productAppData.accountholdername + "in New Account Page";
        console.log(this.oauthProvider.user[0].users_id, "user_id_value");
        var auditUser = {
            // "users_id": this.oauthProvider.user[0].users_id,
            "users_id": this.naccountid,
            "customer_devices_id": 0,
            "usertype": "newaccount",
            "uuid": "",
            "appname": "hub",
            "event_type": "add",
            "screen": "new-account-view",
            "trail_details": message
        };
        this.productAppProvider.getnewaccountaudits(auditUser)
            .then(function (result) {
            console.log(result, "result");
            if (result.message !== undefined) {
                var self_1 = _this;
                self_1.newAccountActivityOptions.data = result.message;
                self_1.newAccountActivityOptions = Object.assign({}, self_1.newAccountActivityOptions);
            }
        });
    };
    ProductAppDetailsPage.prototype.formatDateTime = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            var hh = newdate.getHours();
            var min = newdate.getMinutes().toString().length == 1 ? "0" + newdate.getMinutes().toString() : newdate.getMinutes();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            return (newdate);
        }
    };
    return ProductAppDetailsPage;
}());
ProductAppDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-product-app-details',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\product-app-details\product-app-details.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n    <ion-title padding-vertical>Product Application Details</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-grid>\n\n    <ion-row align-items-start>\n      <ion-col col-6>\n        <ion-card padding>\n          <ion-row>\n            <ion-col col-6>\n              <ion-row\n                *ngIf="productAppData.firstname &&  productAppData.lastname != null || productAppData.firstname &&  productAppData.lastname != \'\' || productAppData.firstname &&  productAppData.lastname != undefined">\n                <ion-col>\n                  <ion-label fixed>Full Name</ion-label>\n                  <ion-input [disabled]="true" [(ngModel)]="productAppData.firstname +\' \'+ productAppData.lastname">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row *ngIf="productAppData.postalcode !== null && productAppData.postalcode !== \'\'">\n                <ion-col>\n                  <ion-label fixed>Postal Code</ion-label>\n                  <ion-input [disabled]="true" [(ngModel)]="productAppData.postalcode"></ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row *ngIf="productAppData.nationalinsuranceno !== null && productAppData.nationalinsuranceno !== \'\'">\n                <ion-col>\n                  <ion-label fixed>National Insurance Number</ion-label>\n                  <ion-input [disabled]="true" [(ngModel)]="productAppData.nationalinsuranceno"></ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row *ngIf="productAppData.nominatedbankno !== null && productAppData.nominatedbankno !== \'\'">\n                <ion-col>\n                  <ion-label fixed>Nominated Bank Number</ion-label>\n                  <ion-input [disabled]="true" [(ngModel)]="productAppData.nominatedbankno"></ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row\n                *ngIf="productAppData.devicetype !== null && productAppData.devicetype !== \'\' && productAppData.deviceType !== undefined">\n                <ion-col>\n                  <ion-label fixed>Device Type</ion-label>\n                  <ion-input [disabled]="true" name="gender" [(ngModel)]="productAppData.devicetype">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row *ngIf="productAppData.state_status !== null && productAppData.state_status !== \'\'">\n                <ion-col>\n                  <ion-label fixed>Status</ion-label>\n                  <ion-input [disabled]="true" name="gender" [(ngModel)]="productAppData.state_status">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n            <ion-col col-6>\n              <ion-row *ngIf="productAppData.moveindate !== null && productAppData.moveindate !== \'\'">\n                <ion-col>\n                  <ion-label fixed>Move In Date</ion-label>\n                  <ion-input [disabled]="true" [(ngModel)]="productAppData.moveindate"></ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row *ngIf="productAppData.moveoutdate !== null && productAppData.moveoutdate !== \'\'">\n                <ion-col>\n                  <ion-label fixed>Move Out Date</ion-label>\n                  <ion-input [disabled]="true" [(ngModel)]="productAppData.moveoutdate"></ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row *ngIf="productAppData.accountholdername !== null && productAppData.accountholdername !== \'\'">\n                <ion-col>\n                  <ion-label fixed>Account Holder Name</ion-label>\n                  <ion-input [disabled]="true" [(ngModel)]="productAppData.accountholdername"></ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row *ngIf="productAppData.emailid !== null && productAppData.emailid !== \'\'">\n                <ion-col>\n                  <ion-label fixed>Email Id</ion-label>\n                  <ion-input [disabled]="true" [(ngModel)]="productAppData.emailid"></ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row *ngIf="productAppData.gender !== null && productAppData.gender !== \'\'">\n                <ion-col>\n                  <ion-label fixed>Gender</ion-label>\n                  <ion-input [disabled]="true" name="gender" [(ngModel)]="productAppData.gender">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row\n                *ngIf="productAppData.dob !== null && productAppData.dob !== \'\' && productAppData.dob !== undefined">\n                <ion-col>\n                  <ion-label fixed>DOB</ion-label>\n                  <ion-input [disabled]="true" name="gender" [(ngModel)]="productAppData.dob">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n\n              <ion-row>\n                <ion-col>\n                  <button ion-button round small color="primary"\n                    *ngIf="productAppData.state_status==\'Existing Customer ID Blocked\' || productAppData.state_status==\'New Existing Customer Blocked\' ||  productAppData.state_status==\'New Existing Registration Blocked\' || productAppData.state_status==\'Existing Blocked\' || productAppData.state_status==\'Existing Registration Blocked\'|| productAppData.state_status==\'New Active PIN Verification Blocked\' || productAppData.state_status==\'Active PIN Verification Blocked\' || productAppData.state_status==\'New Existing Mailer Blocked\' ||  productAppData.state_status==\'New Existing Blocked\'"\n                    (click)="approveblocked(productAppData.state_status)">Approve Blocked</button>\n                  <!-- <button ion-button round small color="primary" *ngIf=" productAppData.state_status==\'Existing Mailer\' || productAppData.state_status==\'New Existing Mailer\' || productAppData.state_status==\'New Existing Mailer Blocked\' || productAppData.state_status==\'Existing Mailer Blocked\'"\n                        (click)="unblockedMailer(productAppData.state_status)">{{mailerButton}}</button> -->\n                </ion-col>\n              </ion-row>\n\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n\n      <ion-col col-6>\n        <ion-card padding>\n          <ion-col col-12>\n            <ion-card-header>User Activity</ion-card-header>\n            <table-component [options]="newAccountActivityOptions"></table-component>\n          </ion-col>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\product-app-details\product-app-details.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_product_app_product_app__["a" /* ProductAppProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], ProductAppDetailsPage);

//# sourceMappingURL=product-app-details.js.map

/***/ })

});
//# sourceMappingURL=12.js.map