webpackJsonp([33],{

/***/ 563:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordPageModule", function() { return ChangePasswordPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__change_password__ = __webpack_require__(600);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ChangePasswordPageModule = (function () {
    function ChangePasswordPageModule() {
    }
    return ChangePasswordPageModule;
}());
ChangePasswordPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__change_password__["a" /* ChangePasswordPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__change_password__["a" /* ChangePasswordPage */]),
        ],
    })
], ChangePasswordPageModule);

//# sourceMappingURL=change-password.module.js.map

/***/ }),

/***/ 600:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_change_password_change_password__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_settings_settings__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ChangePasswordPage = (function () {
    function ChangePasswordPage(languageProvider, navCtrl, navParams, changePasswordProvider, toastCtrl, alertCtrl, oauthProvider, loadauditProvider, settingsProvider, viewCtrl) {
        this.languageProvider = languageProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.changePasswordProvider = changePasswordProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.oauthProvider = oauthProvider;
        this.loadauditProvider = loadauditProvider;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.resultdata = null;
        this.languageString = "en";
        this.pwd = {
            users_id: "",
            oldpassword: "",
            newpassword: ""
        };
        this.confirmpassword = "";
        this.userId = this.navParams.get("userId");
        console.log("usersId", this.userId);
    }
    ChangePasswordPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        console.log('ionViewDidLoad ChangePasswordPage');
    };
    ChangePasswordPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
    };
    ChangePasswordPage.prototype.ChangePassword = function () {
        var _this = this;
        var self = this;
        console.log(self.pwd.newpassword.toLowerCase(), "", this.confirmpassword.toLowerCase());
        if (self.pwd.oldpassword == "" || self.pwd.oldpassword == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter old password"));
        }
        else if (self.pwd.newpassword == "" || self.pwd.newpassword == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter new password"));
        }
        else if (self.pwd.newpassword !== this.confirmpassword) {
            this.errorAlert(this.settingsProvider.getLabel("Confirm password does not match"));
        }
        else {
            self.pwd = {
                "users_id": this.userId,
                "oldpassword": "123456",
                "newpassword": self.pwd.newpassword
            };
            this.changePasswordProvider.changepassword(this.pwd)
                .then(function (result) {
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel(result.message),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                self.navCtrl.pop();
            }).catch(function (response) {
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel("update password fail"),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'danger'
                });
                toast.present();
            });
        }
        var message = "Password changed successfully";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "add",
            "screen": "change-password",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
        });
    };
    ChangePasswordPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: this.settingsProvider.getLabel('Ok'),
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    return ChangePasswordPage;
}());
ChangePasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-change-password',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\change-password\change-password.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel(\'Change Password\')}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n\n\n      <ion-card padding>\n\n\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked>{{settingsProvider.getLabel(\'Old Password\')}}</ion-label>\n\n            <ion-input type="text" [(ngModel)]="pwd.oldpassword "></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        \n\n          <hr style="height:1px;background-color:#000; border:none; width:100%; margin-top: 10px;">\n\n        \n\n        \n\n\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked>{{settingsProvider.getLabel(\'New Password\')}}</ion-label>\n\n            <ion-input type="text" [(ngModel)]="pwd.newpassword"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked>{{settingsProvider.getLabel(\'Confirm Password\')}}</ion-label>\n\n            <ion-input type="text" [(ngModel)]="confirmpassword"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col>\n\n            <button ion-button default (click)="ChangePassword()" color="secondary">{{settingsProvider.getLabel("Save")}}</button>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        \n\n\n\n\n\n      </ion-card>\n\n\n\n\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\change-password\change-password.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_change_password_change_password__["a" /* ChangePasswordProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */]])
], ChangePasswordPage);

//# sourceMappingURL=change-password.js.map

/***/ })

});
//# sourceMappingURL=33.js.map