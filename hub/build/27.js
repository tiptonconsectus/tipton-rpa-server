webpackJsonp([27],{

/***/ 569:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerCallPageModule", function() { return CustomerCallPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__customer_call__ = __webpack_require__(606);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CustomerCallPageModule = (function () {
    function CustomerCallPageModule() {
    }
    return CustomerCallPageModule;
}());
CustomerCallPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__customer_call__["a" /* CustomerCallPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__customer_call__["a" /* CustomerCallPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], CustomerCallPageModule);

//# sourceMappingURL=customer-call.module.js.map

/***/ }),

/***/ 606:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomerCallPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_table_table__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_dashboard_dashboard__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_customers_customers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var CustomerCallPage = (function () {
    function CustomerCallPage(languageProvider, oauthProvider, customersProvider, modalCtrl, dashboardProvider, toastCtrl, navCtrl, navParams, globalProvider, settingsProvider, viewCtrl, masterpermissionProvider) {
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.customersProvider = customersProvider;
        this.modalCtrl = modalCtrl;
        this.dashboardProvider = dashboardProvider;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.globalProvider = globalProvider;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.period = "";
        this.callOptions = {
            data: [],
            filteredData: [],
            columns: {
                "create_date": { title: "Date", col: 2, format: "function", fn: this.formatDate.bind(this), search: true },
                "memberid": { title: "Member ID" },
                "customername": { title: "Customer Name" },
                "mobileno": { title: "Mobile no." },
                "query": { title: "Subject" },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {},
            tapRow: this.edit.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
        this.languageString = "en";
        this.resultdata = null;
        this.data = [];
        this.period = navParams.get("period");
    }
    CustomerCallPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("customers")) {
            return true;
        }
        else {
            return false;
        }
    };
    CustomerCallPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    CustomerCallPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        var self = this;
        if (self.oauthProvider.user_role.calls) {
            if (this.period != "") {
                console.log("period", this.period);
                this.dashboardProvider.getDashboardFilterCalls(this.period)
                    .then(function (data) {
                    self.callOptions.data = data;
                    self.callOptions = Object.assign({}, self.callOptions);
                });
            }
            else {
                self.getCalls();
            }
        }
        else {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.setRoot("DashboardPage");
        }
    };
    CustomerCallPage.prototype.getCalls = function () {
        var self = this;
        this.customersProvider.getCalls()
            .then(function (result) {
            self.callOptions.data = result.data;
            self.callOptions = Object.assign({}, self.callOptions);
        });
    };
    CustomerCallPage.prototype.edit = function (callData) {
        this.navCtrl.push("CallDetailPage", { call: callData, callback: this.complaintResolved.bind(this) });
    };
    CustomerCallPage.prototype.push = function (item) {
    };
    CustomerCallPage.prototype.complaintResolved = function (product) { };
    CustomerCallPage.prototype.getStatus = function (status) {
        console.log(status, "status value");
        if (status == 0) {
            return "pending";
        }
        else {
            return "Resolved";
        }
    };
    CustomerCallPage.prototype.formatDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    return CustomerCallPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1__components_table_table__["a" /* TableComponent */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__components_table_table__["a" /* TableComponent */])
], CustomerCallPage.prototype, "table", void 0);
CustomerCallPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-customer-call',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\customer-call\customer-call.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(\'Customer Calls | Consectus\')}} </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <table-component [options]="callOptions"></table-component>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\customer-call\customer-call.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_customers_customers__["a" /* CustomersProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_dashboard_dashboard__["a" /* DashboardProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_7__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], CustomerCallPage);

//# sourceMappingURL=customer-call.js.map

/***/ })

});
//# sourceMappingURL=27.js.map