webpackJsonp([30],{

/***/ 567:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsMessagePageModule", function() { return ContactUsMessagePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_us_message__ = __webpack_require__(604);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ContactUsMessagePageModule = (function () {
    function ContactUsMessagePageModule() {
    }
    return ContactUsMessagePageModule;
}());
ContactUsMessagePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__contact_us_message__["a" /* ContactUsMessagePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contact_us_message__["a" /* ContactUsMessagePage */]),
        ],
    })
], ContactUsMessagePageModule);

//# sourceMappingURL=contact-us-message.module.js.map

/***/ }),

/***/ 604:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUsMessagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_messages_messages__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_settings_settings__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ContactUsMessagePage = (function () {
    function ContactUsMessagePage(navCtrl, navParams, languageProvider, oauthProvider, globalProvider, messagesProvider, toastCtrl, settingsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.globalProvider = globalProvider;
        this.messagesProvider = messagesProvider;
        this.toastCtrl = toastCtrl;
        this.settingsProvider = settingsProvider;
        this.resultdata = null;
        this.languageString = "en";
        this.new = false;
        this.canUpdate = false;
        this.contactUsMessage = {
            customersid: "",
            memberid: "",
            subject: "",
            description: "",
            create_date: "",
            isresolved: "",
            emailId: ""
        };
        this.userMessage = "";
        this.resolveMessage = "";
        this.resolveCustomerMessage = "";
        this.contactUsMessageList = navParams.get("contactUsMessage");
        var self = this;
        if (!navParams.get('contactUsMessage')) {
            //add adata
            self.contactUsMessage = {
                "customersid": null,
                "memberid": null,
                "subject": null,
                "description": null,
                "create_date": null,
                "isresolved": null,
                "emailId": null,
            };
        }
        else {
            //View Data
            this.title = "Contact Us Message ";
            self.contactUsMessage = JSON.parse(JSON.stringify(navParams.get('contactUsMessage')));
            self.conatctMessage = { "customer_complaint_id": self.contactUsMessage.customer_complaint_id };
            self.contactMessageData = self.contactUsMessage;
            console.log("contactUsMessage", this.contactUsMessage);
            self.canUpdate = true;
        }
    }
    ContactUsMessagePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactUsMessagePage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    ContactUsMessagePage.prototype.ionViewDidEnter = function () {
        var self = this;
        if (self.oauthProvider.user_role) {
            // self.getcontactusmessagebyid();
        }
        else {
            var toast = self.toastCtrl.create({
                message: '',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.setRoot("DashboardPage");
        }
    };
    // getcontactusmessagebyid() {
    //   let self = this;
    //   self.messagesProvider.getcontactusmessagebyid(self.conatctMessage)
    //     .then((result: any) => {
    //       self.messageData = result.data;
    //       let toast = self.toastCtrl.create({
    //         message: this.settingsProvider.getLabel("Message recieved"),
    //         duration: 3000,
    //         position: 'right',
    //         cssClass: 'success',
    //       })
    //       toast.present();
    //     }).catch((err) => {
    //       console.log(err, "err")
    //     })
    // }
    // SendMessage() {
    //   let self = this;
    //   self.sendmessagetocustomer();
    //   self.getcontactusmessagebyid();
    // }
    // sendmessagetocustomer() {
    //   let self = this;
    //   let staffUsermessage = {
    //     message: self.userMessage,
    //     messageby: this.oauthProvider.user[0].users_id,
    //     customer_complaint_id: self.contactUsMessageList.customer_complaint_id
    //   }
    //   console.log(staffUsermessage, "staffUsermessage")
    //   console.log(staffUsermessage.message == "", "staffUsermessage.message")
    //   if (staffUsermessage.message !== "") {
    //     self.messagesProvider.sendmessagetocustomer(staffUsermessage)
    //       .then((result: any) => {
    //         self.getcontactusmessagebyid();
    //         self.userMessage = "";
    //         let toast = self.toastCtrl.create({
    //           message: this.settingsProvider.getLabel("Conact Us Message Sent"),
    //           duration: 3000,
    //           position: 'right',
    //           cssClass: 'success',
    //         })
    //         toast.present();
    //       }).catch((err) => {
    //         console.log(err, "err")
    //       })
    //   }
    // }
    ContactUsMessagePage.prototype.SubmitContactUsMessages = function () {
        var _this = this;
        var self = this;
        // self.globalProvider.showLoader();
        var customerResolved = {
            resolvedfeedback: self.resolveMessage,
            complaint_status: self.resolveCustomerMessage,
            resolvedby: this.oauthProvider.user[0].users_id,
            customer_complaint_id: self.contactUsMessage.customer_complaint_id
        };
        console.log('hihfias', customerResolved);
        self.messagesProvider.complaintresolved(customerResolved)
            .then(function (result) {
            // self.globalProvider.hideLoader();
            // self.resolveMessage = "";
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Issue Resolved"),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.pop();
        }).catch(function (err) {
            console.log(err, "err");
        });
    };
    return ContactUsMessagePage;
}());
ContactUsMessagePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contact-us-message',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\contact-us-message\contact-us-message.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel("Contact Us Message | Consectus")}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="SubmitContactUsMessages()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <ion-grid class="table-cell">\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row class="header">\n\n                  <ion-col>\n\n                    {{settingsProvider.getLabel(\'Contact Us Message Details\')}}\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col col-6>\n\n                    <ion-label fixed>{{settingsProvider.getLabel("Consectus ID")}}</ion-label>\n\n                    <ion-input [(ngModel)]="contactUsMessage.customersid" disabled></ion-input>\n\n                  </ion-col>\n\n                  <ion-col col-6>\n\n                    <ion-label fixed>{{settingsProvider.getLabel("Member ID")}}</ion-label>\n\n                    <ion-input [(ngModel)]="contactUsMessage.memberid" disabled></ion-input>\n\n                  </ion-col>\n\n\n\n\n\n                  <ion-col col-6>\n\n                    <ion-label fixed>{{settingsProvider.getLabel("Subject")}}</ion-label>\n\n                    <ion-input [(ngModel)]="contactUsMessage.subject" disabled></ion-input>\n\n                  </ion-col>\n\n                  <ion-col col-6>\n\n                    <ion-label fixed>{{settingsProvider.getLabel("Message")}}</ion-label>\n\n                    <ion-input [(ngModel)]="contactUsMessage.description" disabled></ion-input>\n\n                  </ion-col>\n\n                  <ion-col col-6>\n\n                    <ion-label fixed>{{settingsProvider.getLabel("Date")}}</ion-label>\n\n                    <ion-input [(ngModel)]="contactUsMessage.create_date" disabled></ion-input>\n\n                  </ion-col>\n\n\n\n                  <ion-col col-6>\n\n                    <ion-label fixed>{{settingsProvider.getLabel("Email ID")}}</ion-label>\n\n                    <ion-input [(ngModel)]="contactUsMessage.emailid" disabled></ion-input>\n\n                  </ion-col>\n\n                  <!-- complaint_status -->\n\n\n\n                  <ion-col col-6>\n\n                    <ion-item style="padding-left:0 !important;">\n\n                      <ion-label stacked>{{settingsProvider.getLabel("Status")}}</ion-label>\n\n                      <ion-select [(ngModel)]="resolveCustomerMessage" name="resolveCustomerMessage " multiple="false">\n\n                        <ion-option [selected]="resolveCustomerMessage  == \'\'" value="">Select Type</ion-option>\n\n                        <ion-option [selected]="resolveCustomerMessage  == \'In Process\'">In Process</ion-option>\n\n                        <ion-option [selected]="resolveCustomerMessage  == \'Resolved by Bank\'">Resolved by Bank\n\n                        </ion-option>\n\n                        <ion-option [selected]="resolveCustomerMessage  == \'Resolved by Customer\'">Resolved by Customer\n\n                        </ion-option>\n\n                        <ion-option [selected]="resolveCustomerMessage  == \'Not Valid\'">Not Valid</ion-option>\n\n                      </ion-select>\n\n                    </ion-item>\n\n                  </ion-col>\n\n\n\n                  <ion-col col-6>\n\n                    <ion-label fixed>{{settingsProvider.getLabel("Feedback")}}</ion-label>\n\n                    <ion-textarea [(ngModel)]="resolveMessage" style="height: 82px;padding: 5px;"></ion-textarea>\n\n                  </ion-col>\n\n\n\n\n\n                </ion-row>\n\n                <!-- <ion-row>\n\n                  <ion-col>\n\n                    <ion-label fixed>{{settingsProvider.getLabel("Feedback")}}</ion-label>\n\n                    <ion-textarea [(ngModel)]="resolveMessage" style="height: 82px;padding: 5px;"></ion-textarea>\n\n                  </ion-col>\n\n                </ion-row> -->\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n      <!-- <ion-col col-4>\n\n        <ion-grid class="table-cell">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row class="header">\n\n                  <ion-col>\n\n                    {{settingsProvider.getLabel(\'Messages\')}}\n\n                  </ion-col>\n\n                </ion-row>\n\n\n\n                <ion-row style="height: 471px;overflow-y:scroll;">\n\n                  <div *ngFor="let customerMessage of messageData">\n\n                    <div class="staffUserContact1" *ngIf="customerMessage.messageusertype == \'customer\'">\n\n                      Customer : {{ customerMessage.message }}\n\n                    </div>\n\n                    <br>\n\n                    <div class="staffUserContact2" *ngIf="customerMessage.messageusertype == \'user\'">\n\n                      Me: {{ customerMessage.message }}\n\n                    </div>\n\n                  </div>\n\n                  <div style="height: 125px;"></div>\n\n                  <ion-footer>\n\n                    <ion-toolbar>\n\n                      <ion-buttons end>\n\n                        <button large class="send-button" (click)="SendMessage()" style="padding: 2px !important;">\n\n                          <ion-icon name="send" class="send-icon" class="sendbn"></ion-icon>\n\n                        </button>\n\n                      </ion-buttons>\n\n                      <ion-textarea [(ngModel)]="userMessage" class="msgtext" rows="2" cols="20"></ion-textarea>\n\n                    </ion-toolbar>\n\n                  </ion-footer>\n\n\n\n                </ion-row>\n\n\n\n              </ion-grid>\n\n            </ion-col>\n\n\n\n          </ion-row>\n\n\n\n        </ion-grid>\n\n\n\n      </ion-col> -->\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\contact-us-message\contact-us-message.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_messages_messages__["a" /* MessagesProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_settings_settings__["a" /* SettingsProvider */]])
], ContactUsMessagePage);

//# sourceMappingURL=contact-us-message.js.map

/***/ })

});
//# sourceMappingURL=30.js.map