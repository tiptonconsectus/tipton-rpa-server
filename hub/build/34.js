webpackJsonp([34],{

/***/ 562:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallDetailPageModule", function() { return CallDetailPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__call_detail__ = __webpack_require__(599);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CallDetailPageModule = (function () {
    function CallDetailPageModule() {
    }
    return CallDetailPageModule;
}());
CallDetailPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__call_detail__["a" /* CallDetailPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__call_detail__["a" /* CallDetailPage */]),
        ],
    })
], CallDetailPageModule);

//# sourceMappingURL=call-detail.module.js.map

/***/ }),

/***/ 599:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CallDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_customers_customers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CallDetailPage = (function () {
    function CallDetailPage(navCtrl, navParams, customersProvider, toastCtrl, languageProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.customersProvider = customersProvider;
        this.toastCtrl = toastCtrl;
        this.languageProvider = languageProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.callData = {
            memberid: "",
            customername: "",
            create_date: "",
            mobileno: "",
            query: "",
        };
        this.languageString = "en";
        this.new = false;
        this.canUpdate = false;
        var self = this;
        if (!navParams.get('call')) {
            //add adata
            self.callData = {
                "memberid": null,
                "customername": null,
                "create_date": null,
                "mobileno": null,
                "query": null,
            };
            self.new = true;
        }
        else {
            //View Data
            self.callData = JSON.parse(JSON.stringify(navParams.get('call')));
            self.callData = self.callData;
            console.log("callData", this.callData);
            self.canUpdate = true;
        }
    }
    CallDetailPage.prototype.ionViewCanEnter = function () {
        if (this.masterpermissionProvider.permission("default")) {
            return true;
        }
        else {
            return false;
        }
    };
    CallDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ComplaintDetailsPage');
    };
    return CallDetailPage;
}());
CallDetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-call-detail',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\call-detail\call-detail.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>Call Details</ion-title>\n\n\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-card padding>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>Member Id</ion-label>\n\n              <ion-input type="text" [(ngModel)]="callData.memberid" [disabled]="!new"></ion-input>\n\n              <!--  -->\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>Customer Name</ion-label>\n\n              <ion-input type="text" [(ngModel)]="callData.customername" [disabled]="!new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>Date</ion-label>\n\n              <ion-input type="text" [(ngModel)]="callData.create_date" [disabled]="!new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>Mobile no.</ion-label>\n\n              <ion-input type="text" [(ngModel)]="callData.mobileno" [disabled]="!new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>Query</ion-label>\n\n              <ion-input type="text" [(ngModel)]="callData.query" [disabled]="!new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\call-detail\call-detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_customers_customers__["a" /* CustomersProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], CallDetailPage);

//# sourceMappingURL=call-detail.js.map

/***/ })

});
//# sourceMappingURL=34.js.map