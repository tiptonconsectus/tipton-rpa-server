webpackJsonp([37],{

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GatewayClientProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_crypto_js__ = __webpack_require__(529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_crypto_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_crypto_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var customers = [];
var users = [];
var transactions = [];
var auditlogs = [];
var products = [];
var messages = [];
var branches = [];
var contacts = [];
var accountRules = [];
var faqs = [];
var news = [];
var appversions = [];
var regularSavings = [];
var cheques = [];
var appSettings = [];
var members = [];
var savigGoals = [];
var contactUsMessages = [];
var labels;
var bankDetails = {
    name: "Yorkshire Building Society",
    logo: '/assets/images/logo.png',
    address: 'Yorkshire House, Yorkshire Drive, Bradford BD5 8LJ',
    savings: true,
    current: true,
    mortgage: true,
    account: true,
    product: true,
    saver: true,
    member: true,
    messages: true,
    nearBranch: true,
    payContacts: true,
    depositCheque: true,
    thirdParty: true,
    settingsOption: true,
    termsConditions: true,
    savingAnalysis: true,
    savingsGoals: true,
    mortgageCalculator: true
};
var bankLookAndFeel = {
    primary_color: "#660066",
    page_background_color: '#ffffff',
    sub_color: '#feccfe',
    text_color: "#660066",
    css: ''
};
var GatewayClientProvider = (function () {
    function GatewayClientProvider(http, events, network, globalProvider, loadingCtrl) {
        this.http = http;
        this.events = events;
        this.network = network;
        this.globalProvider = globalProvider;
        this.loadingCtrl = loadingCtrl;
        // token: any = null;
        this.verifier_code = null;
        this.code_challenge = null;
        this.toauth2Host = null;
        this.oauth2Port = null;
        this.renewBeforeMilliseconds = 10;
        this.code = null;
        this.state = null;
        this.expires_in = null;
        this.expires = null;
        this.flag = 1;
        var id = new Date().getTime();
        var self = this;
        this.baseUrl = this.globalProvider.getBaseUrl();
        this.RpabaseUrl = this.globalProvider.getBaseUrlRpa();
        //watch network for a disconnect
        this.network.onDisconnect().subscribe(function () {
        });
        // watch network for a connect
        this.network.onConnect().subscribe(function () {
        });
        if (news.length == 0) {
            this.http.get("assets/data/news.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                news = result;
                console.log("news: " + news.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (savigGoals.length == 0) {
            this.http.get("assets/data/saving-goals.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                savigGoals = result;
                console.log("saving goals: " + savigGoals.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        this.http.get("assets/data/languages.json")
            .map(function (response) { return response.json(); })
            .subscribe(function (result) {
            labels = result;
            console.log("labels", labels);
            self.events.publish("labels:loaded", labels);
        }, function (error) {
            console.log(JSON.stringify(error));
        });
        if (regularSavings.length == 0) {
            this.http.get("assets/data/regular-saving.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                regularSavings = result;
                console.log("regularSavings: " + regularSavings.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (cheques.length == 0) {
            this.http.get("assets/data/customer-cheque.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                cheques = result;
                console.log("cheques: " + cheques.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (accountRules.length == 0) {
            this.http.get("assets/data/saving-account-rule.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                accountRules = result;
                console.log("accountRules: " + accountRules.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (faqs.length == 0) {
            this.http.get("assets/data/faqs.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                faqs = result;
                console.log("faqs: " + faqs.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (appSettings.length == 0) {
            this.http.get("assets/data/appsetting.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                appSettings = result;
                console.log("appsetting: " + appSettings.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (contacts.length == 0) {
            this.http.get("assets/data/contactus.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                contacts = result;
                console.log("contacts: " + contacts.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (branches.length == 0) {
            this.http.get("assets/data/branches.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                for (var _i = 0, result_1 = result; _i < result_1.length; _i++) {
                    var item = result_1[_i];
                    if (item['id'] == undefined)
                        item['id'] = id++;
                }
                branches = result;
                console.log("branches: " + branches.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (users.length == 0) {
            this.http.get("assets/data/users.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                for (var _i = 0, result_2 = result; _i < result_2.length; _i++) {
                    var item = result_2[_i];
                    if (item['id'] == undefined)
                        item['id'] = id++;
                }
                users = result;
                console.log("users: " + users.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (customers.length == 0) {
            this.http.get("assets/data/customers.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                for (var _i = 0, result_3 = result; _i < result_3.length; _i++) {
                    var item = result_3[_i];
                    if (item['id'] == undefined)
                        item['id'] = id++;
                }
                customers = result;
                console.log("customers: " + customers.length);
                self.events.publish("customers:loaded");
            }, function (error) {
                console.log(JSON.stringify(error));
            });
            ;
        }
        if (appversions.length == 0) {
            this.http.get("assets/data/appversion.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                appversions = result;
                console.log("appversions: " + appversions.length);
                self.events.publish("appversions:loaded");
            }, function (error) {
                console.log(JSON.stringify(error));
            });
            ;
        }
        if (contactUsMessages.length == 0) {
            this.http.get("assets/data/contact-us-messages.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                contactUsMessages = result;
                console.log("contactUsMessages: " + contactUsMessages.length);
                self.events.publish("contactUsMessages:loaded", contactUsMessages.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
            ;
        }
        if (transactions.length == 0) {
            this.http.get("assets/data/transactions.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                for (var _i = 0, result_4 = result; _i < result_4.length; _i++) {
                    var item = result_4[_i];
                    if (item['id'] == undefined)
                        item['id'] = id++;
                }
                transactions = result;
                console.log("transactions: " + transactions.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (auditlogs.length == 0) {
            this.http.get("assets/data/auditlogs.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                for (var _i = 0, result_5 = result; _i < result_5.length; _i++) {
                    var item = result_5[_i];
                    if (item['id'] == undefined)
                        item['id'] = id++;
                }
                auditlogs = result;
                console.log("auditlogs: " + auditlogs.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (products.length == 0) {
            this.http.get("assets/data/products.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                for (var _i = 0, result_6 = result; _i < result_6.length; _i++) {
                    var item = result_6[_i];
                    if (item['id'] == undefined)
                        item['id'] = id++;
                }
                products = result;
                console.log("products: " + products.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (messages.length == 0) {
            this.http.get("assets/data/messages.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                for (var _i = 0, result_7 = result; _i < result_7.length; _i++) {
                    var item = result_7[_i];
                    if (item['id'] == undefined)
                        item['id'] = id++;
                }
                messages = result;
                console.log("messages: " + messages.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
        if (members.length == 0) {
            this.http.get("assets/data/members.json")
                .map(function (response) { return response.json(); })
                .subscribe(function (result) {
                for (var _i = 0, result_8 = result; _i < result_8.length; _i++) {
                    var item = result_8[_i];
                    if (item['id'] == undefined)
                        item['id'] = id++;
                }
                members = result;
                console.log("members: " + members.length);
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
    }
    GatewayClientProvider.prototype.call_get = function (screen, url, event, RpaUrl) {
        if (RpaUrl === void 0) { RpaUrl = null; }
        var self = this;
        return new Promise(function (resolve, reject) {
            self.getAccessToken().then(function (tokenData) {
                var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
                headers.append('Content-Type', 'application/json');
                headers.append('Authorization', "Bearer " + self.token);
                var UrlBase = (RpaUrl == null) ? self.baseUrl : RpaUrl;
                self.http.get(UrlBase + url, { headers: headers })
                    .map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    resolve(data);
                    if (self.loader) {
                        self.loader.dismiss();
                        self.loader = null;
                    }
                }, function (error) {
                    self.adderror(screen, url, event, "", error);
                    console.log(error.status, "call_get error");
                    console.log(error, "call_get error");
                    if (error.status == 404) {
                        console.log("consectus:404.error");
                        self.events.publish("consectus:404.error");
                    }
                    else if (error.status == 500) {
                        self.events.publish("consectus:500.error");
                    }
                    else {
                        self.events.publish("consectus:network.error");
                    }
                    reject(error);
                    if (self.loader) {
                        self.loader.dismiss();
                        self.loader = null;
                    }
                });
            });
        });
    };
    GatewayClientProvider.prototype.call_post = function (screen, url, body, event, flag, RpaUrl) {
        if (RpaUrl === void 0) { RpaUrl = null; }
        var self = this;
        return new Promise(function (resolve, reject) {
            self.getAccessToken().then(function (tokenData) {
                var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
                headers.append('Content-Type', 'application/json');
                headers.append('Authorization', "Bearer " + self.token);
                // self.showLoading();
                if (flag) {
                    console.log("showloading 1");
                }
                else {
                    self.showLoading();
                }
                var UrlBase = (RpaUrl == null) ? self.baseUrl : RpaUrl;
                self.http.post(UrlBase + url, body, { headers: headers })
                    .map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    self.hideLoading();
                    resolve(data);
                }, function (error) {
                    self.hideLoading();
                    self.adderror(screen, UrlBase + url, body, event, error);
                    console.log(error.status, "call_post error");
                    if (error.status == 404) {
                        console.log("consectus:404.error");
                        self.events.publish("consectus:404.error");
                    }
                    else if (error.status == 500) {
                        self.events.publish("consectus:500.error");
                    }
                    else {
                        self.events.publish("consectus:network.error");
                    }
                    reject(error);
                });
            });
        });
    };
    GatewayClientProvider.prototype.call_put = function (screen, url, body, event) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.getAccessToken().then(function (tokenData) {
                var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
                headers.append('Content-Type', 'application/json');
                headers.append('Authorization', "Bearer " + self.token);
                self.http.put(self.baseUrl + url, body, { headers: headers })
                    .map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    resolve(data);
                    if (self.loader) {
                        self.loader.dismiss();
                        self.loader = null;
                    }
                }, function (error) {
                    self.adderror(screen, url, body, event, error);
                    console.log(error.status, "call_put error");
                    if (error.status == 404) {
                        console.log("consectus:404.error");
                        self.events.publish("consectus:404.error");
                    }
                    else if (error.status == 500) {
                        self.events.publish("consectus:500.error");
                    }
                    else {
                        self.events.publish("consectus:network.error");
                    }
                    reject(error);
                    if (self.loader) {
                        self.loader.dismiss();
                        self.loader = null;
                    }
                });
            });
        });
    };
    GatewayClientProvider.prototype.call_delete = function (url) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', "Bearer " + self.token);
            self.getAccessToken().then(function (tokenData) {
                self.http.delete(self.baseUrl + url, { headers: headers })
                    .map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    resolve(data);
                    if (self.loader) {
                        self.loader.dismiss();
                        self.loader = null;
                    }
                }, function (error) {
                    console.log(error.status, "call_delete error");
                    if (error.status == 404) {
                        console.log("consectus:404.error");
                        self.events.publish("consectus:404.error");
                    }
                    else if (error.status == 500) {
                        self.events.publish("consectus:500.error");
                    }
                    else {
                        self.events.publish("consectus:network.error");
                    }
                    reject(error);
                    if (self.loader) {
                        self.loader.dismiss();
                        self.loader = null;
                    }
                });
            });
        });
    };
    GatewayClientProvider.prototype.isTokenValid = function () {
        return (this.refreshTokenExpiry > Date.now);
    };
    // refreshToken() {
    // }
    // getToken() {
    // }
    // handleResponse() {
    // }
    GatewayClientProvider.prototype.getContactUsMessages = function () {
        return new Promise(function (resolve, reject) {
            resolve(contactUsMessages);
        });
    };
    GatewayClientProvider.prototype.getAverage = function (count) {
        var self = this;
        var url = '/hub/getdashboard';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        self.flag = 0;
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_post("DashboardPage", url, count, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    //----- rpa --------------- //
    GatewayClientProvider.prototype.getAverageRpa = function () {
        var self = this;
        var url = '/admin/average';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("RpaAverage", url, "click", self.RpabaseUrl)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getfindtotalagent = function () {
        var self = this;
        var url = '/admin/get-agent';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("RpaTotalAgent", url, "click", self.RpabaseUrl)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.pingAgent = function (rpaData) {
        var self = this;
        var url = '/admin/manage/getconnect';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        self.flag = 0;
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_post("rpaAgentManagement", url, rpaData, "click", self.flag, self.RpabaseUrl)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.SaveAgentdata = function (rpaData) {
        var self = this;
        var url = "/admin/manage/save_entry";
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        self.flag = 0;
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_post("SaverpaAgent", url, rpaData, "click", self.flag, self.RpabaseUrl)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getAgentData = function () {
        var self = this;
        var url = '/admin/manage/rpa-agent';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("RpaAgentdata", url, "click", self.RpabaseUrl)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.DeleteAgent = function (rpaData) {
        console.log(rpaData);
        var self = this;
        var url = "/admin/manage/deleteAgent/" + rpaData.id;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("RpaAgentdata", url, "click", self.RpabaseUrl)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.AllAgentList = function () {
        var self = this;
        var url = '/admin/manage/order';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("RpaAgentdata", url, "click", self.RpabaseUrl)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.GetActionPath = function () {
        var self = this;
        var url = '/admin/manage/action-path';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("RpaAgentdata", url, "click", self.RpabaseUrl)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    //----- rpa --------------- //
    GatewayClientProvider.prototype.getDashboardCount = function (count) {
        var self = this;
        var url = '/hub/getdashboard';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        self.flag = 0;
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_post("DashboardPage", url, count, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addgoalmilestone = function (count) {
        var self = this;
        var url = '/hub/addgoalmilestone';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = count;
        console.log(body, "hi body");
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("AddGoalMilestone", url, count, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.logout = function () {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        var url = '/accounts/userlogout';
        headers.append('Content-type', 'application/json');
        // headers.append('Authorization', "Bearer " + self.token)
        return new Promise(function (resolve, reject) {
            self.call_get("Logout", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addaccounttypes = function (count) {
        var self = this;
        var url = '/accountrules/addaccounttypes';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        // let body = count; // 4/11
        return new Promise(function (resolve, reject) {
            self.call_post("AddAccounts", url, count, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateaccounttypes = function (body) {
        var self = this;
        var url = '/accountrules/updateaccounttypes';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("UpdateAccount", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                self.adderror("saving-account-type", self.baseUrl + url, "click", body, err)
                    .then(function (data) {
                }).catch(function (err1) {
                });
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.changepassword = function (passwordData) {
        var self = this;
        var url = '/accounts/changepassword';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.http.put(self.baseUrl + '/accounts/changepassword', passwordData);
            self.call_put("UpdatePassword", url, passwordData, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                self.adderror("change-password", self.baseUrl + url, passwordData, "onload", err)
                    .then(function (data) {
                }).catch(function (err1) {
                });
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.resetPassword = function (passwordData) {
        var self = this;
        var url = '/accounts/resetpassword';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = passwordData;
        console.log(body, "hi body");
        return new Promise(function (resolve, reject) {
            self.call_put("ResetPassword", url, passwordData, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                self.adderror("ResetPassword", self.baseUrl + url, passwordData, "click", err)
                    .then(function (data) {
                }).catch(function (err1) {
                });
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addcontactsubject = function (contactSubject) {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        var url = '/accounts/addcontactsubject';
        headers.append('Content-Type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("AddContactSubject", url, contactSubject, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updategoalmilestone = function (body) {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        var url = '/hub/updategoalmilestone';
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("UpdateGoalMilestone", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updatecustomeraccounttransations = function (body) {
        var self = this;
        var url = '/accounts/updatecustomeraccounttransations';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("UpdateAccountTransactions", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getgoalmilestones = function () {
        var self = this;
        var url = '/hub/getgoalmilestones';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("GoalMilestone", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getcontactusmessagebyid = function (complaint_message_id) {
        var self = this;
        var url = '/accounts/getcontactusmessagebyid';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        var body = complaint_message_id;
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("ContactUsMessage", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.sendmessagetocustomer = function (complaint_message_id) {
        var self = this;
        var url = '/accounts/sendmessagetocustomer';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        var body = complaint_message_id;
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("SendMessagetoCustomer", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.complaintresolved = function (complaint_message_id) {
        var self = this;
        var url = '/accounts/complaintresolved';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        var body = complaint_message_id;
        return new Promise(function (resolve, reject) {
            self.call_put("Complaintresolved", url, body, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getyearlytransactions = function () {
        var self = this;
        var url = '/accounts/getyearlytransactions';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("transaction", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getCalls = function () {
        var self = this;
        var url = '/accounts/getcustomercall';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("customer-call", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getComplaints = function () {
        var self = this;
        var url = '/accounts/getcustomercomplaint';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("messages", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.complaintResolved = function (body) {
        var self = this;
        var url = '/accounts/complaintresolved';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        console.log(body, "update Body");
        return new Promise(function (resolve, reject) {
            self.call_put("Complaintresolved", url, body, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getaccounttypes = function () {
        var self = this;
        var url = '/accountrules/getaccounttypes';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("saving-accout-rules-list", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // getaccounttypes() {
    //   let self = this;
    //   let url = '/accountrules/getaccounttypesnullproducts';
    //   let headers = new Headers();
    //   headers.append('Content-Type', 'application/json');
    //   return new Promise(function (resolve, reject) {
    //     self.call_get("saving-accout-rules-list", url, "onload")
    //       .then((data) => {
    //         resolve(data)
    //       }).catch((err) => {
    //         reject(err)
    //       })
    //   });
    // }
    GatewayClientProvider.prototype.getaccountrules = function () {
        var self = this;
        var url = '/accounts/getrulesengine';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("GetAccountRules", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateaccountrules = function (body) {
        var self = this;
        var url = '/accountrules/updateaccountrules';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("UpdateAccountRules", url, body, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getOnBoardingDashboard = function () {
        return new Promise(function (resolve, reject) {
            resolve({
                NEW: 123,
                OTP: 200,
                BLOCKED: 112,
                SETPIN: 233,
                SETUP: 1988
            });
        });
    };
    GatewayClientProvider.prototype.getCustomers = function (filter) {
        if (filter === void 0) { filter = null; }
        var self = this;
        var url = '/accounts/getallcustomers';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("GetCustomers", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getCustomersById = function (filter) {
        if (filter === void 0) { filter = null; }
        return new Promise(function (resolve, reject) {
            console.log("getting customer data", filter);
            if (filter) {
                resolve(customers.filter(function (item) {
                    return item.customerId == filter;
                }));
            }
            else {
                resolve(customers);
            }
        });
    };
    GatewayClientProvider.prototype.getBlockedCustomers = function (item) {
        var arr = [];
        for (var _i = 0, customers_1 = customers; _i < customers_1.length; _i++) {
            var customer = customers_1[_i];
            if (customer.status == item) {
                arr.push(customer);
            }
        }
        return new Promise(function (resolve, reject) {
            resolve(arr);
        });
    };
    GatewayClientProvider.prototype.getFailedMessage = function (issend) {
        var arr = [];
        for (var _i = 0, contactUsMessages_1 = contactUsMessages; _i < contactUsMessages_1.length; _i++) {
            var contactUsMessage = contactUsMessages_1[_i];
            if (contactUsMessage.issend == issend) {
                arr.push(contactUsMessage);
            }
        }
        return new Promise(function (resolve, reject) {
            resolve(arr);
        });
    };
    GatewayClientProvider.prototype.getPasscodeCustomers = function (item) {
        var arr = [];
        for (var _i = 0, customers_2 = customers; _i < customers_2.length; _i++) {
            var customer = customers_2[_i];
            if (customer.status == item) {
                arr.push(customer);
            }
        }
        return new Promise(function (resolve, reject) {
            resolve(arr);
        });
    };
    GatewayClientProvider.prototype.getfailedPasscodeCustomers = function (item) {
        var arr = [];
        for (var _i = 0, customers_3 = customers; _i < customers_3.length; _i++) {
            var customer = customers_3[_i];
            if (customer.status == item) {
                arr.push(customer);
            }
        }
        return new Promise(function (resolve, reject) {
            resolve(arr);
        });
    };
    GatewayClientProvider.prototype.getAllCustomers = function () {
        var self = this;
        var url = '/accounts/getcustomerdetails';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("customers", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getregistrationfields = function () {
        var self = this;
        var url = '/register/getregistrationfields';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("customers", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getcustomeraccounts = function (customersid) {
        var self = this;
        var url = '/accounts/getcustomeraccounts';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("customers", url, customersid, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getAppData = function () {
        return new Promise(function (resolve, reject) {
            console.log("appversions", appversions);
            resolve(appversions);
        });
    };
    // blockCustomer(userIds) {
    //   let self = this;
    //   let url = '/accounts/updatecustomerstatus';
    //   let headers = new Headers();
    //   headers.append('Content-Type', 'application/json');
    //   let body = {
    //     customers_id: userIds,
    //     status: "BLOCKED"
    //   };
    //   return new Promise(function (resolve, reject) {
    //     self.call_put("customers", url, body, "click")
    //       .then((data) => {
    //         resolve(data)
    //       }).catch((err) => {
    //         reject(err)
    //       })
    //   });
    // }
    GatewayClientProvider.prototype.blockCustomer = function (block_customer) {
        var self = this;
        var url = '/accounts/blockcustomerv2';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = block_customer;
        return new Promise(function (resolve, reject) {
            self.call_put("customers", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // unBlockCustomer(userIds) {
    //   let self = this;
    //   let url = '/accounts/updatecustomerstatus';
    //   let headers = new Headers();
    //   headers.append('Content-Type', 'application/json');
    //   let body = {
    //     customers_id: userIds,
    //     status: "ACTIVE"
    //   };
    //   return new Promise(function (resolve, reject) {
    //     self.call_put("customers", url, body, "click")
    //       .then((data) => {
    //         resolve(data)
    //       }).catch((err) => {
    //         reject(err)
    //       })
    //   });
    // }
    GatewayClientProvider.prototype.unBlockCustomer = function (userIds) {
        var self = this;
        var url = '/accounts/activecustomer';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            customers_id: userIds
        };
        return new Promise(function (resolve, reject) {
            self.call_put("customers", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.registerCustomer = function (item) {
        var arr = [];
        for (var _i = 0, customers_4 = customers; _i < customers_4.length; _i++) {
            var customer = customers_4[_i];
            if (customer.customerId == item.customerId) {
                customer.status = "OTP";
                item.status = "OTP";
                arr.push(customer);
            }
            else {
                arr.push(customer);
            }
        }
        return new Promise(function (resolve, reject) {
            resolve(arr);
        });
    };
    GatewayClientProvider.prototype.resendPasscode = function (userIds) {
        var self = this;
        var url = '/accounts/resendotp';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            customers_id: userIds,
        };
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("forgot-password", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getUsers = function (filter) {
        if (filter === void 0) { filter = null; }
        return new Promise(function (resolve, reject) {
            resolve(users);
        });
    };
    // modified by bipin
    GatewayClientProvider.prototype.adminLogin = function (username, password) {
        var self = this;
        var url = '/accounts/loginuser';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            // email: username,
            emailid: username,
            password: password
        };
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("AdminLogin", url, body, "click", self.flag)
                .then(function (data) {
                if (data.accessToken !== undefined && data.accessToken !== "") {
                    self.token = data.accessToken;
                    self.userid = data.data.users_id;
                }
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // end
    GatewayClientProvider.prototype.addUser = function (user) {
        return new Promise(function (resolve, reject) {
            user.id = new Date().getTime().toString();
            users.push(user);
            resolve();
        });
    };
    GatewayClientProvider.prototype.getSavingGoals = function () {
    };
    GatewayClientProvider.prototype.deleteUser = function (user) {
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, users_1 = users; _i < users_1.length; _i++) {
                var u = users_1[_i];
                if (u.id != user.id) {
                    arr.push(u);
                }
            }
            users = arr;
            resolve();
        });
    };
    GatewayClientProvider.prototype.updateUser = function (user) {
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, users_2 = users; _i < users_2.length; _i++) {
                var u = users_2[_i];
                if (u.id == user.id) {
                    arr.push(user);
                }
                else {
                    arr.push(u);
                }
            }
            users = arr;
            resolve();
        });
    };
    GatewayClientProvider.prototype.AddUserActivity = function (user, summary) {
        return new Promise(function (resolve, reject) {
            for (var _i = 0, users_3 = users; _i < users_3.length; _i++) {
                var u = users_3[_i];
                if (u.id == user.id) {
                    u.activities.push({
                        id: new Date().getTime().toString(),
                        date: new Date().toISOString(),
                        summary: summary
                    });
                    break;
                }
            }
            resolve();
        });
    };
    GatewayClientProvider.prototype.getTransactions = function () {
        return new Promise(function (resolve, reject) {
            resolve(transactions);
        });
    };
    GatewayClientProvider.prototype.getAccountTransactions = function (account) {
        return new Promise(function (resolve, reject) {
            var ret = [];
            for (var _i = 0, transactions_1 = transactions; _i < transactions_1.length; _i++) {
                var transaction = transactions_1[_i];
                if (transaction.account_number == account.account_number && transaction.sort_code == account.sort_code) {
                    ret.push(transaction);
                }
            }
            resolve(ret);
        });
    };
    GatewayClientProvider.prototype.getAuditLogs = function (customer) {
        return new Promise(function (resolve, reject) {
            var ret = [];
            for (var _i = 0, auditlogs_1 = auditlogs; _i < auditlogs_1.length; _i++) {
                var auditlog = auditlogs_1[_i];
                if (auditlog.customerId == customer.customerId) {
                    ret.push(auditlog);
                }
            }
            resolve(ret);
        });
    };
    GatewayClientProvider.prototype.getCustomerCount = function () {
        return new Promise(function (resolve, reject) {
            resolve(customers.length);
        });
    };
    GatewayClientProvider.prototype.getProducts = function () {
        var self = this;
        var url = '/accounts/getproduct';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("GetProducts", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getallcategories = function () {
        var self = this;
        var url = '/hub/getallcategories';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("GetFaqCategories", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getUserRoles = function () {
        var self = this;
        var url = '/accounts/getroles';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("user", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addrole = function (roleData) {
        var self = this;
        var url = '/accounts/addrole';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("Addrole", url, roleData, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.roleSetting = function (body) {
        var self = this;
        var url = '/accounts/rolessetting';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("RoleSetting", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getcontactsubject = function () {
        var self = this;
        var url = '/accounts/getcontactsubject';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("settings", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getcustomercomplaint = function () {
        var self = this;
        var url = '/accounts/getcustomercomplaint';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("Getcustomercomplaint", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateContactSubject = function (body) {
        var self = this;
        var url = '/accounts/updatecontactsubject';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        console.log(body, "bodySubject");
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("contact-us", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getuser = function () {
        var self = this;
        var url = '/accounts/getuser';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("contact-us", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addcategories = function (count) {
        var self = this;
        var url = '/hub/addcategories';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = count;
        self.flag = 0;
        console.log(body, "body");
        return new Promise(function (resolve, reject) {
            self.call_post("addcategories", url, count, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.editcategory = function (body) {
        var self = this;
        var url = '/hub/editcategory';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("UpdateFaqCategory", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.adduser = function (count) {
        var self = this;
        var url = '/accounts/adduser';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = count;
        self.flag = 0;
        console.log(body, "body");
        return new Promise(function (resolve, reject) {
            self.call_post("Adduser", url, count, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateuser = function (body) {
        var self = this;
        var url = '/accounts/updateuser';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("Updateuser", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addProduct = function (product) {
        var self = this;
        var url = '/accounts/addproduct';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = product;
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("AddProduct", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateProduct = function (product) {
        var self = this;
        var url = '/accounts/updateproduct';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = product;
        console.log("product");
        return new Promise(function (resolve, reject) {
            self.call_put("Updateproduct", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.deleteProduct = function (productId) {
        var self = this;
        var url = '/accounts/updateproductstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            products_id: productId,
            isactive: 0
        };
        return new Promise(function (resolve, reject) {
            self.call_put("product", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.deleteCategory = function (categoryid) {
        var self = this;
        var url = '/hub/updatecategorystatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            category_id: categoryid,
            isactive: 0
        };
        return new Promise(function (resolve, reject) {
            self.call_put("category", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.deletepushMessage = function (pushmessageId) {
        var self = this;
        var url = '/accounts/updatemessagestatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        console.log(pushmessageId, "pushmessageId");
        headers.append('Content-Type', 'application/json');
        var body = {
            pushmessages_id: pushmessageId,
            isactive: 0
        };
        return new Promise(function (resolve, reject) {
            self.call_put("PushMessage", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.deletefaq = function (faqId) {
        var self = this;
        var url = '/accounts/updatefaqstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            faq_id: faqId,
            isactive: 0
        };
        return new Promise(function (resolve, reject) {
            self.call_put("Faq", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.activatePushMessage = function (pushmessageId) {
        var self = this;
        var url = '/accounts/updatemessagestatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            pushmessages_id: pushmessageId,
            isactive: 1
        };
        return new Promise(function (resolve, reject) {
            self.call_put("ActivatePushMessage", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.activateFaq = function (faqId) {
        var self = this;
        var url = '/accounts/updatefaqstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            faq_id: faqId,
            isactive: 1
        };
        return new Promise(function (resolve, reject) {
            self.call_put("ActivatePushMessage", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.deleteMilestone = function (milestoneId) {
        var self = this;
        var url = '/hub/updatemilestonestatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        console.log(milestoneId, "milestoneId");
        headers.append('Content-Type', 'application/json');
        var body = {
            saving_goal_milestones_id: milestoneId,
            isactive: 0
        };
        return new Promise(function (resolve, reject) {
            self.call_put("DeleteMilestone", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.activateMilestone = function (milestoneId) {
        var self = this;
        var url = '/hub/updatemilestonestatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            saving_goal_milestones_id: milestoneId,
            isactive: 1
        };
        return new Promise(function (resolve, reject) {
            self.call_put("ActivateMilestone", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.deletebranch = function (milestoneId) {
        var self = this;
        var url = '/accounts/updatebranchstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            branches_id: milestoneId,
            isactive: 0
        };
        return new Promise(function (resolve, reject) {
            self.call_put("DeleteBranch", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.activatebranch = function (milestoneId) {
        var self = this;
        var url = '/accounts/updatebranchstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            branches_id: milestoneId,
            isactive: 1
        };
        return new Promise(function (resolve, reject) {
            self.call_put("ActivateBranch", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.activateProduct = function (productId) {
        var self = this;
        var url = '/accounts/updateproductstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            products_id: productId,
            isactive: 1
        };
        return new Promise(function (resolve, reject) {
            self.call_put("ActivateProduct", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.activateFaqCategory = function (categoryid) {
        var self = this;
        var url = '/hub/updatecategorystatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            category_id: categoryid,
            isactive: 1
        };
        return new Promise(function (resolve, reject) {
            self.call_put("ActivateFaqCategory", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getMessages = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            resolve(messages);
        });
    };
    GatewayClientProvider.prototype.getmessages = function () {
        var self = this;
        var url = '/accounts/getmessages';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("GetMessages", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getcustomerbymessagesid = function (pushMessaging) {
        var self = this;
        var url = '/accounts/getcustomerbymessagesid';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("GetMessages", url, pushMessaging, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getcustomerbyproductid = function (productId) {
        var self = this;
        var url = '/accounts/getcustomerbyproductid';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("product", url, productId, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getnewaccountdetails = function (new_accounts_id) {
        var self = this;
        var url = '/register/getnewaccountdetails';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("NewAccountViewPage", url, new_accounts_id, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getcustomerlistbymemberid = function (pushMember) {
        var self = this;
        var url = '/accounts/getcustomerlistbymemberid';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("MemberbyId", url, pushMember, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getcustomerdetailsbycustomerid = function (customer) {
        var self = this;
        var url = '/accounts/getcustomerdetailsbycustomerid';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        var body = {
            customers_id: customer.customers_id
        };
        headers.append('Content-type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("CustomerDetail", url, body, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getproduct = function () {
        var self = this;
        var url = '/accounts/getproductforhub';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("products", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getmember = function () {
        var self = this;
        var url = '/accounts/getmemberforhub';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("members", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updatemessage = function (body) {
        var self = this;
        var url = '/accounts/updatemessage';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("UpdateMessage", url, body, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.resetpassword = function (emailName) {
        var self = this;
        var url = '/accounts/resetpassword';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        var body = {
            emailid: emailName
        };
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("forgot-password", url, body, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getnewaccountslist = function () {
        var self = this;
        var url = '/hub/getnewaccountslist';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("GetNewAccountsList", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getnewaccountmailerlist = function (product) {
        var self = this;
        var url = '/register/getnewaccountmailerlist/' + product;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("GetNewAccountMailerlist", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getnewcustomerlist = function () {
        var self = this;
        var url = '/accounts/getnewcustomerlist';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("Getnewcustomerlist", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getproductapplicationdetails = function () {
        var self = this;
        var url = '/accounts/getproductapplicationdetails';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("Getproductapplicationdetails", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.generateLetter = function (new_accounts_id) {
        var self = this;
        var url = '/register/generateletter';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            new_accounts_id: new_accounts_id
        };
        return new Promise(function (resolve, reject) {
            self.call_post("letter", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.openPdfLetter = function (new_accounts_id) {
        var self = this;
        var url = '/register/downloadpdfletter';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            new_accounts_id: new_accounts_id
        };
        return new Promise(function (resolve, reject) {
            self.call_post("letter", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.generateLetterForAll = function () {
        var self = this;
        var url = '/register/generateletterforall';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("letterforall", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.generateletterforonboard = function () {
        var self = this;
        var url = '/register/generateletterforonboard';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("letterforall", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updatenewaccountstatus = function (customerIds, status) {
        var self = this;
        var url = '/hub/updatenewaccountstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            newaccounts_id: customerIds,
            status: status
        };
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("UpdateNewAccouuntStatus", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.restartregistrationbyaccountsidv2 = function (naccountid, state_status) {
        var self = this;
        var url = '/register/restartregistrationbyaccountsidv2';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            new_accounts_id: naccountid,
            state_status: state_status
        };
        self.flag = 0;
        console.log(JSON.stringify(body));
        return new Promise(function (resolve, reject) {
            self.call_post("UpdateNewAccountStatus", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.blockexistingcustomer = function (mailerBlock) {
        var self = this;
        var url = '/register/blockexistingcustomer';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = mailerBlock;
        console.log(JSON.stringify(body));
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("UpdateNewAccountStatus", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addMessage = function (message) {
        var self = this;
        var url = '/accounts/addmessage';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = message;
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("AddMessage", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateMessage = function (message) {
        var self = this;
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, messages_1 = messages; _i < messages_1.length; _i++) {
                var p = messages_1[_i];
                if (p.id == message.id) {
                    arr.push(message);
                }
                else {
                    arr.push(p);
                }
            }
            messages = arr;
            resolve();
        });
    };
    GatewayClientProvider.prototype.deleteMessage = function (item) {
        var self = this;
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, messages_2 = messages; _i < messages_2.length; _i++) {
                var m = messages_2[_i];
                if (m.id != item.id) {
                    arr.push(m);
                }
            }
            messages = arr;
            resolve(messages);
        });
    };
    GatewayClientProvider.prototype.getBranches = function () {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.http.get(self.baseUrl + '/accounts/getbranch', { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (result) {
                console.log(result, "branch obj");
                resolve(result);
            }, function (error) {
                reject(error);
            });
            resolve(branches);
        });
    };
    GatewayClientProvider.prototype.getbranch = function () {
        var self = this;
        var url = '/accounts/getbranch';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("settings", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getBankDetails = function () {
        var self = this;
        var url = '/hub/getbanksettings';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            users_id: 2
            // devicetype: "browser"
        };
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("settings", url, body, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.resetcustomer = function (body) {
        var self = this;
        var url = '/hub/resetcustomer';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("customer", url, body, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateBankDetails = function (details) {
        bankDetails = details;
        var self = this;
        var url = '/hub/updatebanksettings';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = details;
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("SuperAdmin", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getBankLookAndFeel = function () {
        return new Promise(function (resolve, reject) {
            resolve(bankLookAndFeel);
        });
    };
    GatewayClientProvider.prototype.updateBankLookAndFeel = function (lookandfeel) {
        return new Promise(function (resolve, reject) {
            bankLookAndFeel = lookandfeel;
            resolve();
        });
    };
    GatewayClientProvider.prototype.addbranch = function (count) {
        var self = this;
        var url = '/accounts/addbranch';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = count;
        console.log(body, "bodyAddBranch");
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("AddBranch", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.deleteBranch = function (branch) {
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, branches_1 = branches; _i < branches_1.length; _i++) {
                var b = branches_1[_i];
                if (b.id != branch.id) {
                    arr.push(b);
                }
            }
            branches = arr;
            resolve();
        });
    };
    GatewayClientProvider.prototype.updateBranch = function (branch) {
        var self = this;
        var url = '/accounts/updatebranch';
        var body = branch;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("UpdateBranch", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateonboardingtnc = function (body) {
        var self = this;
        var url = '/hub/updateonboardingtnc';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_post("updateonboardingtnc", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updatenewaccountnc = function (body) {
        var self = this;
        var url = '/hub/updatenewaccountnc';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_post("updatenewaccountnc", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // updateBankDetails(details) {
    //   bankDetails = details;
    //   let self = this;
    //   let url = '/hub/updatebanksettings';
    //   let headers = new Headers();
    //   headers.append('Content-Type', 'application/json');
    //   let body = details;
    //   self.flag = 0;
    //   return new Promise(function (resolve, reject) {
    //     self.call_post("SuperAdmin", url, body, "click", self.flag)
    //       .then((data) => {
    //         resolve(data)
    //       }).catch((err) => {
    //         reject(err)
    //       })
    //   })
    // }
    GatewayClientProvider.prototype.getContactusSubject = function () {
        return new Promise(function (resolve, reject) {
            resolve(contacts);
        });
    };
    GatewayClientProvider.prototype.addSubject = function (subject) {
        return new Promise(function (resolve, reject) {
            contacts.push(subject);
            resolve(contacts);
        });
    };
    GatewayClientProvider.prototype.saveVersion = function (verData) {
        return new Promise(function (resolve, reject) {
            console.log("verdata", verData);
            appversions.push(verData);
            resolve(appversions);
        });
    };
    GatewayClientProvider.prototype.updateSubject = function (contact) {
        var self = this;
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, contacts_1 = contacts; _i < contacts_1.length; _i++) {
                var c = contacts_1[_i];
                if (c.subject == contact.subject) {
                    arr.push(contact);
                }
                else {
                    arr.push(c);
                }
            }
            contacts = arr;
            resolve();
        });
    };
    GatewayClientProvider.prototype.deleteContactUsMessage = function (contactus) {
        var self = this;
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, contactUsMessages_2 = contactUsMessages; _i < contactUsMessages_2.length; _i++) {
                var c = contactUsMessages_2[_i];
                if (c.id != contactus.id) {
                    arr.push(c);
                }
            }
            contactUsMessages = arr;
            resolve(contactUsMessages);
        });
    };
    GatewayClientProvider.prototype.deleteContact = function (contact) {
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, contacts_2 = contacts; _i < contacts_2.length; _i++) {
                var c = contacts_2[_i];
                if (c.subject != contact.subject) {
                    arr.push(c);
                }
            }
            contacts = arr;
            resolve(contacts);
        });
    };
    GatewayClientProvider.prototype.getFaqs = function () {
        var self = this;
        var url = '/accounts/getfaq';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("Faq", url, "onLoad")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.gettermsandcondition = function () {
        var self = this;
        var url = '/hub/gettermsandcondition';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("TermsCondition", url, "onLoad")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addFaqs = function (faq) {
        var self = this;
        var url = '/accounts/addfaq';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        self.commonLoader();
        faq.createdby = 2;
        faq.isactive = 1;
        var body = faq;
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("AddFaq", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
            // faq.createdby = 2;
            // faq.isactive = 1;
            // let body = faq;
            // self.http.post(self.baseUrl + '/accounts/addfaq', body, { headers: headers })
            //   .map(res => res.json())
            //   .subscribe(faq => {
            //     console.log(faq);
            //     resolve(faq);
            //     if (self.loader) {
            //       self.loader.dismiss();
            //       self.loader = null;
            //     }
            //   }, error => {
            //     self.adderror(self.userid,"faqs","/accounts/addfaq",JSON.stringify(headers),"post", "click",JSON.stringify(error))
            //     reject(error);
            //     if (self.loader) {
            //       self.loader.dismiss();
            //       self.loader = null;
            //     }
            //   });
        });
    };
    GatewayClientProvider.prototype.updateFaqs = function (faq) {
        var self = this;
        var url = '/accounts/updatefaq';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        // let arr = [];
        return new Promise(function (resolve, reject) {
            self.call_put("updateFaq", url, faq, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.deleteFaqs = function (faq) {
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, faqs_1 = faqs; _i < faqs_1.length; _i++) {
                var f = faqs_1[_i];
                if (f.title != faq.title) {
                    arr.push(f);
                }
            }
            faqs = arr;
            resolve();
        });
    };
    /*Get members */
    GatewayClientProvider.prototype.getMembers = function () {
        var self = this;
        var url = '/accounts/getmember';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        // this.commonLoader();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("getMembers", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    /* Add Members */
    GatewayClientProvider.prototype.createMember = function (member) {
        var self = this;
        var url = '/accounts/addmember';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        var body = member;
        headers.append('Content-Type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("Members", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    /*Update member */
    GatewayClientProvider.prototype.updateMember = function (member) {
        var self = this;
        var url = '/accounts/updatemember';
        console.log(member);
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        // let body = member; //4/11
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("UpdateMembers", url, member, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    /**
     *
     * Api Name
     Delete Members
     */
    GatewayClientProvider.prototype.deleteMembers = function (membersId) {
        var self = this;
        var url = '/accounts/updatememberstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            member_id: membersId,
            isactive: 0
        };
        return new Promise(function (resolve, reject) {
            self.call_put("UpdateMembers", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    /**
     * Api Name
     * Activate Members
     */
    GatewayClientProvider.prototype.activateMembers = function (membersId) {
        var self = this;
        var url = '/accounts/updatememberstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            member_id: membersId,
            isactive: 1
        };
        return new Promise(function (resolve, reject) {
            self.call_put("ActivateMembers", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getAppVerisonByDevice = function (device) {
        var self = this;
        var arr = [];
        console.log(appSettings);
        return new Promise(function (resolve, reject) {
            for (var _i = 0, appSettings_1 = appSettings; _i < appSettings_1.length; _i++) {
                var d = appSettings_1[_i];
                if (d.type == device) {
                    arr.push(d);
                }
            }
            resolve(arr);
        });
    };
    GatewayClientProvider.prototype.addAppVersion = function (appData) {
        return new Promise(function (resolve, reject) {
            appSettings.push(appData);
            resolve();
        });
    };
    GatewayClientProvider.prototype.getAllAppVersions = function () {
        return new Promise(function (resolve, reject) {
            resolve(appSettings);
        });
    };
    GatewayClientProvider.prototype.getNews = function () {
        return new Promise(function (resolve, reject) {
            resolve(news);
        });
    };
    GatewayClientProvider.prototype.createNews = function (newsData) {
        return new Promise(function (resolve, reject) {
            newsData.id = new Date().getTime().toString();
            news.push(newsData);
            resolve();
        });
    };
    GatewayClientProvider.prototype.updateNews = function (newsData) {
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, news_1 = news; _i < news_1.length; _i++) {
                var n = news_1[_i];
                if (n.id == newsData.id) {
                    arr.push(newsData);
                }
                else {
                    arr.push(n);
                }
            }
            news = arr;
            resolve();
        });
    };
    GatewayClientProvider.prototype.deleteNews = function (newsData) {
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, news_2 = news; _i < news_2.length; _i++) {
                var n = news_2[_i];
                if (n.id != newsData.id) {
                    arr.push(n);
                }
            }
            news = arr;
            resolve();
        });
    };
    /**
     * Get All Saving account Rules
     */
    GatewayClientProvider.prototype.getSavingAccountRules = function () {
        return new Promise(function (resolve, reject) {
            resolve(accountRules);
        });
    };
    GatewayClientProvider.prototype.getAccountRules = function () {
        var self = this;
        var url = '/accountrules/getaccountrules';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("AccountRules", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    //Saving Accounts Rule
    GatewayClientProvider.prototype.updateaccountrules2 = function (updateData) {
        return new Promise(function (resolve, reject) {
            appSettings.push(updateData);
            resolve();
        });
    };
    /**
     * Update Saving Account Rule
     */
    GatewayClientProvider.prototype.updateSavingAccountRules = function (accObj) {
        var self = this;
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, accountRules_1 = accountRules; _i < accountRules_1.length; _i++) {
                var a = accountRules_1[_i];
                if (a.type == accObj.type) {
                    arr.push(accObj);
                }
                else {
                    arr.push(a);
                }
            }
            accountRules = arr;
            resolve();
        });
    };
    /**
     * Save Account Rule
     */
    GatewayClientProvider.prototype.saveSavingAccountRule = function (accObj) {
        return new Promise(function (resolve, reject) {
            accountRules.push(accObj);
            resolve(accountRules);
        });
    };
    GatewayClientProvider.prototype.getsmtp = function () {
        var self = this;
        var url = '/hub/getsmtp';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("settings", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addsmtp = function (addData) {
        var self = this;
        var url = 'hub/addsmtp';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("Addsmtp", url, addData, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updatesmtp = function (updateData) {
        var self = this;
        var url = '/hub/updatesmtp';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post("UpdateSmtp", url, updateData, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    /**
     * Get customer saving goals
     */
    GatewayClientProvider.prototype.getCustomerSavingGoals = function (customer) {
        return new Promise(function (resolve, reject) {
            // Old code
            var ret = [];
            for (var _i = 0, savigGoals_1 = savigGoals; _i < savigGoals_1.length; _i++) {
                var savigGoal = savigGoals_1[_i];
                if (savigGoal.customerId == customer.customerId) {
                    ret.push(savigGoal);
                }
            }
            resolve(ret);
        });
    };
    /**
     * Delete Customer saving Goal
     */
    GatewayClientProvider.prototype.deleteSavingGoals = function (savingGoal) {
        var self = this;
        var arr = [];
        return new Promise(function (resolve, reject) {
            for (var _i = 0, savigGoals_2 = savigGoals; _i < savigGoals_2.length; _i++) {
                var s = savigGoals_2[_i];
                if (s.title.trim() != savingGoal.title.trim()) {
                    arr.push(s);
                }
            }
            savigGoals = arr;
            resolve();
        });
    };
    /**
     * Get Customer All Regular savings
     */
    GatewayClientProvider.prototype.getAllRegularSavings = function (customer) {
        return new Promise(function (resolve, reject) {
            var ret = [];
            for (var _i = 0, regularSavings_1 = regularSavings; _i < regularSavings_1.length; _i++) {
                var regularSaving = regularSavings_1[_i];
                if (regularSaving.customerId == customer.customerId) {
                    ret.push(regularSaving);
                }
            }
            resolve(ret);
        });
    };
    /**
     * Get Customer All cheques
     */
    GatewayClientProvider.prototype.getAllCustomerCheques = function (customer) {
        return new Promise(function (resolve, reject) {
            var ret = [];
            for (var _i = 0, cheques_1 = cheques; _i < cheques_1.length; _i++) {
                var checque = cheques_1[_i];
                if (checque.customerId == customer.customerId) {
                    ret.push(checque);
                }
            }
            resolve(ret);
        });
    };
    /*
         * Name : commonLoader
         * Description : commonLoader for whole the application
         */
    GatewayClientProvider.prototype.commonLoader = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        this.loader.present();
    };
    GatewayClientProvider.prototype.oauthToken = function () {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('Username', '12321');
        headers.append('Password', '21');
        var body = new URLSearchParams();
        body.set('username', "Test");
        body.set('password', "$2a$10$R2xYGhoHvGi6hLfyBu4p9.tqOkmKl67zBrMDcjoAg/KTqdK6S1rw2");
        body.set('name', "Test");
        body.set('client_id', "4fad6c1c-27a0-5b35-a3ba-50dba1d80fda");
        body.set('client_secret', "d9e65941-ff50-5694-8d2a-7b30a73168de");
        body.set('grant_type', "password");
        return new Promise(function (resolve, reject) {
            self.http.post(self.baseUrl + '/auth/login', body.toString(), { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
                resolve(data);
            }, function (err) {
                console.error("Could not get data: " + err);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.blockSelected = function (blockuserIds) {
        var self = this;
        var url = '/accounts/updateuserstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            users_id: blockuserIds,
            isactive: 2
        };
        return new Promise(function (resolve, reject) {
            self.call_put("settings", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.activateSelected = function (activeusersId) {
        var self = this;
        var url = '/accounts/updateuserstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            users_id: activeusersId,
            isactive: 1
        };
        return new Promise(function (resolve, reject) {
            self.call_put("UpdateUserStatus", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.deleteSelectedUser = function (deleteUserIds) {
        var self = this;
        var url = '/accounts/updateuserstatus';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = {
            users_id: deleteUserIds,
            isactive: 0
        };
        return new Promise(function (resolve, reject) {
            self.call_put("UpdateUserStatus", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getLabels = function () {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        var url = '/hub/getLabels';
        headers.append('Content-type', 'application/json');
        // headers.append('Authorization', "Bearer " + self.token)
        return new Promise(function (resolve, reject) {
            self.call_get("Labels", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.saveLabels = function (labels1, labelPage, screen) {
        var self = this;
        var url = '/hub/updatelabels';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = labelPage;
        return new Promise(function (resolve, reject) {
            self.call_put("LabelManagement", url, body, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addLabel = function () {
        var self = this;
        var url = '/hub/updatelabels';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_put("addLabel", url, labels, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addRulesEngine = function (accountData) {
        var self = this;
        var url = '/accounts/addrulesengine';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_post("Rulesengine", url, accountData, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateRulesEngine = function (accountData) {
        var self = this;
        var url = '/accounts/updaterulesengine';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = accountData;
        return new Promise(function (resolve, reject) {
            self.call_put("Rulesengine", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateloginprocess = function (loginData) {
        var self = this;
        var url = '/register/updateloginprocess';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = loginData;
        return new Promise(function (resolve, reject) {
            self.call_put("loginprocess", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.updateonboardingprocess = function (onboardingData) {
        var self = this;
        var url = '/register/updateonboardingprocess';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var body = onboardingData;
        return new Promise(function (resolve, reject) {
            self.call_put("onboardingprocess", url, body, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // loadaudit(body) {
    //   let self = this;
    //   // self.commonLoader();
    //   let headers = new Headers();
    //   headers.append('Content-Type', 'application/json');
    //   headers.append('Authorization', "Bearer " + self.token);
    //   return new Promise(function (resolve, reject) {
    //     self.http.post(self.baseUrl + '/accounts/loadaudit', body, { headers: headers })
    //       .map(res => res.json())
    //       .subscribe(data => {
    //         resolve(data);
    //         if (self.loader) {
    //           self.loader.dismiss();
    //           self.loader = null;
    //         }
    //       }, error => {
    //         reject(error);
    //         if (self.loader) {
    //           self.loader.dismiss();
    //           self.loader = null;
    //         }
    //       });
    //   });
    // }
    GatewayClientProvider.prototype.loadaudit = function (body) {
        var self = this;
        var url = '/accounts/loadaudit';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_post("loadaudit", url, body, "onload", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getnewaccountaudits = function (body) {
        var self = this;
        // self.commonLoader();
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "Bearer " + self.token);
        return new Promise(function (resolve, reject) {
            self.http.post(self.baseUrl + '/hub/getnewaccountaudits', body, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            }, function (error) {
                reject(error);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            });
        });
    };
    GatewayClientProvider.prototype.adderror = function (screenname, apiurl, data, event, err) {
        console.log(apiurl);
        var self = this;
        var url = '/accounts/adderror';
        var body = {
            "appname": "hub",
            "user_id": self.userid,
            "screen": screenname,
            "apiurl": apiurl,
            "headers": "application/json",
            "apibody": JSON.stringify(data),
            "event": "event",
            "err": JSON.stringify(err)
        };
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.getAccessToken().then(function (tokenData) {
                var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
                headers.append('Content-Type', 'application/json');
                headers.append('Authorization', "Bearer " + self.token);
                var UrlBase = self.baseUrl;
                self.http.post(UrlBase + url, body, { headers: headers })
                    .map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    resolve(data);
                });
            });
        });
    };
    GatewayClientProvider.prototype.getaudittrailbyuserid = function (userid) {
        var self = this;
        var url = '/accounts/getaudittrailbyuserid';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        var body = {
            "users_id": userid
        };
        self.flag = 0;
        headers.append('Content-Type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_post("audittrail", url, body, "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.addappversion = function (count) {
        var self = this;
        var url = '/hub/addappversion';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        // let body = count;//4/11
        self.flag = 0;
        return new Promise(function (resolve, reject) {
            self.call_post(url, count, "AddAppVersion", "click", self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getappversion = function () {
        var self = this;
        var url = '/hub/getappversion';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("AddAppVersion", url, "click")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getonboardingprocess = function () {
        var self = this;
        var url = '/register/getonboardingprocess';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("OnboardingProcess", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getloginprocess = function () {
        var self = this;
        var url = '/register/getloginprocess';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("OnboardingProcess", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getregistrationfieldsv1 = function () {
        var self = this;
        var url = '/register/getregistrationfieldsv1';
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */];
        headers.append('Content-type', 'application/json');
        return new Promise(function (resolve, reject) {
            self.call_get("OnboardingProcess", url, "onload")
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getLatLong = function (roleData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.http.get('https://maps.googleapis.com/maps/api/geocode/json?address=400022&key=AIzaSyCaZv5gKyNuDIPjYuk1WG2SARahvbxDkYE')
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data, "data");
                resolve(data);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            }, function (error) {
                reject(error);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            });
        });
    };
    GatewayClientProvider.prototype.upload_image = function (url, formData) {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('enctype', 'multipart/form-data');
        headers.append('Authorization', "Bearer " + self.token);
        return new Promise(function (resolve, reject) {
            self.http.post(self.baseUrl + url, formData, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            }, function (error) {
                console.log(error.status, "call_post error");
                if (error.status == 404) {
                    console.log("consectus:404.error");
                    self.events.publish("consectus:404.error");
                }
                else if (error.status == 500) {
                    self.events.publish("consectus:500.error");
                }
                else {
                    self.events.publish("consectus:network.error");
                }
                reject(error);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            });
        });
    };
    GatewayClientProvider.prototype.updateProduct_TargetCustomer = function (url, formData) {
        console.log(JSON.stringify(formData), "vikrantttttttttt");
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('enctype', 'multipart/form-data');
        headers.append('Authorization', "Bearer " + self.token);
        return new Promise(function (resolve, reject) {
            self.http.put(self.baseUrl + url, formData, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            }, function (error) {
                console.log(error.status, "call_post error");
                if (error.status == 404) {
                    console.log("consectus:404.error");
                    self.events.publish("consectus:404.error");
                }
                else if (error.status == 500) {
                    self.events.publish("consectus:500.error");
                }
                else {
                    self.events.publish("consectus:network.error");
                }
                reject(error);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            });
        });
    };
    GatewayClientProvider.prototype.updateMessage_TargetCustomer = function (url, formData) {
        console.log(JSON.stringify(formData), "vikrantttttttttt");
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('enctype', 'multipart/form-data');
        headers.append('Authorization', "Bearer " + self.token);
        return new Promise(function (resolve, reject) {
            self.http.put(self.baseUrl + url, formData, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            }, function (error) {
                console.log(error.status, "call_post error");
                if (error.status == 404) {
                    console.log("consectus:404.error");
                    self.events.publish("consectus:404.error");
                }
                else if (error.status == 500) {
                    self.events.publish("consectus:500.error");
                }
                else {
                    self.events.publish("consectus:network.error");
                }
                reject(error);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            });
        });
    };
    GatewayClientProvider.prototype.push_Product = function (url, product) {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('enctype', 'multipart/form-data');
        headers.append('Authorization', "Bearer " + self.token);
        return new Promise(function (resolve, reject) {
            self.http.post(self.baseUrl + url, product, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            }, function (error) {
                console.log(error.status, "call_post error");
                if (error.status == 404) {
                    console.log("consectus:404.error");
                    self.events.publish("consectus:404.error");
                }
                else if (error.status == 500) {
                    self.events.publish("consectus:500.error");
                }
                else {
                    self.events.publish("consectus:network.error");
                }
                reject(error);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            });
        });
    };
    GatewayClientProvider.prototype.sendMailerLetter = function (url, newaccounts) {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('enctype', 'multipart/form-data');
        headers.append('Authorization', "Bearer " + self.token);
        return new Promise(function (resolve, reject) {
            self.http.put(self.baseUrl + url, newaccounts, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            }, function (error) {
                console.log(error.status, "call_put error");
                if (error.status == 404) {
                    console.log("consectus:404.error");
                    self.events.publish("consectus:404.error");
                }
                else if (error.status == 500) {
                    self.events.publish("consectus:500.error");
                }
                else {
                    self.events.publish("consectus:network.error");
                }
                reject(error);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            });
        });
    };
    GatewayClientProvider.prototype.checkEmailid = function (url, emailid) {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('enctype', 'multipart/form-data');
        headers.append('Authorization', "Bearer " + self.token);
        return new Promise(function (resolve, reject) {
            self.http.post(self.baseUrl + url, emailid, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            }, function (error) {
                console.log(error.status, "call_post error");
                if (error.status == 404) {
                    console.log("consectus:404.error");
                    self.events.publish("consectus:404.error");
                }
                else if (error.status == 500) {
                    self.events.publish("consectus:500.error");
                }
                else {
                    self.events.publish("consectus:network.error");
                }
                reject(error);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            });
        });
    };
    GatewayClientProvider.prototype.checkMobileno = function (url, mobilenodata) {
        var self = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('enctype', 'multipart/form-data');
        headers.append('Authorization', "Bearer " + self.token);
        return new Promise(function (resolve, reject) {
            self.http.post(self.baseUrl + url, mobilenodata, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            }, function (error) {
                console.log(error.status, "call_post error");
                if (error.status == 404) {
                    console.log("consectus:404.error");
                    self.events.publish("consectus:404.error");
                }
                else if (error.status == 500) {
                    self.events.publish("consectus:500.error");
                }
                else {
                    self.events.publish("consectus:network.error");
                }
                reject(error);
                if (self.loader) {
                    self.loader.dismiss();
                    self.loader = null;
                }
            });
        });
    };
    /**
       * Show loader after successr
       */
    GatewayClientProvider.prototype.showLoading = function () {
        if (!this.loading) {
            this.loading = this.loadingCtrl.create();
            this.loading.present();
        }
    };
    /**
     * Hide loader after success and error
     */
    GatewayClientProvider.prototype.hideLoading = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    GatewayClientProvider.prototype.oauth_post = function (url, body, flag) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            if (flag) {
                console.log("showloading 1");
            }
            else {
                self.showLoading();
            }
            self.http.post(self.baseUrl + url, body, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                self.hideLoading();
                resolve(data);
            }, function (error) {
                if (error.status == 404) {
                    self.events.publish("consectus:404.error", error);
                }
                else if (error.status == 500) {
                    self.events.publish("consectus:500.error", error);
                }
                else if (error.status !== 0) {
                    self.events.publish("consectus:network.error", error);
                }
                self.hideLoading();
                reject(error);
            });
        });
    };
    /*   Get autorization code
    */
    GatewayClientProvider.prototype.getAuthoriztionCode = function (postData) {
        var self = this;
        var url = "/oauth2/authorization_code";
        self.flag = 1;
        return new Promise(function (resolve, reject) {
            self.oauth_post(url, postData.toString(), self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    GatewayClientProvider.prototype.getToken = function (postData) {
        var self = this;
        var url = "/oauth2/token";
        self.flag = 1;
        return new Promise(function (resolve, reject) {
            self.oauth_post(url, postData.toString(), self.flag)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    /**
    * Check access token for the each app
    * @param fresh
    */
    GatewayClientProvider.prototype.getAccessToken = function (fresh) {
        if (fresh === void 0) { fresh = false; }
        //if token exists check if the expires time is in next 2 seconds
        var currentDate = new Date();
        var self = this;
        return new Promise(function (resolve, reject) {
            console.log("self.token", self.token, fresh, self.expires - currentDate, self.expires);
            if (self.token && !fresh && (self.expires - currentDate) > self.renewBeforeMilliseconds) {
                console.log("access token", self.token);
                resolve(self.token);
            }
            else {
                self.getAuthorizationCode().then(function (oauthTokenData) {
                    if (oauthTokenData !== null || oauthTokenData !== '') {
                        resolve(oauthTokenData.token);
                    }
                }).catch(function (error) {
                    reject(error);
                });
            }
        });
    };
    /**
     * Get Authorization code for generate the passcode
     */
    GatewayClientProvider.prototype.getAuthorizationCode = function () {
        var self = this;
        var verifier_code = self.getVerifierCode(); // This method is used when the verify code with getToken
        var code_challenge = self.getCodeChallenge(verifier_code); // This method will be use when code_challenge_method =  's256'
        var state = __WEBPACK_IMPORTED_MODULE_6_crypto_js__["lib"].WordArray.random(32) + ""; // some random string useful to verify when token is requested.
        return new Promise(function (resolve, reject) {
            var postData = new URLSearchParams();
            postData.set('code_challenge', code_challenge);
            postData.set('client_id', "consectus-api");
            postData.set('state', state);
            postData.set('response_type', "code");
            postData.set('code_challenge_method', "S256");
            self.getAuthoriztionCode(postData).then(function (authdata) {
                if (authdata !== null || authdata !== '') {
                    console.log("authorization code:", authdata.code);
                    if (authdata.code !== null) {
                        var resend_data = new URLSearchParams();
                        resend_data.set('code', authdata.code);
                        resend_data.set('client_id', "consectus-api");
                        resend_data.set('verifier_code', verifier_code);
                        resend_data.set('state', state);
                        resend_data.set('code_challenge_method', "S256");
                        resend_data.set('grant_type', "authorization_code");
                        self.getToken(resend_data).then(function (authtoken) {
                            self.token = authtoken.access_token;
                            self.expires_in = authtoken.expires_in;
                            self.expires = new Date(new Date().getTime() + parseInt(self.expires_in));
                            console.log("token: ", authtoken.access_token, "expires:", self.expires);
                            resolve(authtoken);
                        }).catch(function (error) {
                            reject(error);
                        });
                    }
                }
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    /**
    * Encode base 64 url
    */
    GatewayClientProvider.prototype.base64URLEncode = function (code) {
        return code.toString('base64')
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=/g, '');
    };
    /**
     * Check verifycode for the generate the token
     *
     * */
    GatewayClientProvider.prototype.getVerifierCode = function () {
        var code = __WEBPACK_IMPORTED_MODULE_6_crypto_js__["lib"].WordArray.random(32);
        var verifier_code = __WEBPACK_IMPORTED_MODULE_6_crypto_js__["enc"].Base64.stringify(code);
        return verifier_code.replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=/g, '');
    };
    GatewayClientProvider.prototype.getCodeChallenge = function (code) {
        // console.log("verifier_code", this.base64URLEncode(CryptoJS.SHA256(code)));
        var encCode = __WEBPACK_IMPORTED_MODULE_6_crypto_js__["enc"].Base64.stringify(__WEBPACK_IMPORTED_MODULE_6_crypto_js__["SHA256"](code));
        return encCode.replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=/g, '');
    };
    return GatewayClientProvider;
}());
GatewayClientProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__["a" /* Network */],
        __WEBPACK_IMPORTED_MODULE_4__global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["h" /* LoadingController */]])
], GatewayClientProvider);

//# sourceMappingURL=gateway-client.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function() { return DashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_chart_chart_module__ = __webpack_require__(490);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_doughnut_doughnut_module__ = __webpack_require__(492);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_bar_chart_bar_chart_module__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dashboard__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_consectus_header_consectus_header_module__ = __webpack_require__(326);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var DashboardPageModule = (function () {
    function DashboardPageModule() {
    }
    return DashboardPageModule;
}());
DashboardPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__dashboard__["a" /* DashboardPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_5__dashboard__["a" /* DashboardPage */]),
            __WEBPACK_IMPORTED_MODULE_2__components_bar_chart_bar_chart_module__["a" /* BarChartComponentModule */],
            __WEBPACK_IMPORTED_MODULE_1__components_doughnut_doughnut_module__["a" /* DoughnutComponentModule */],
            __WEBPACK_IMPORTED_MODULE_0__components_chart_chart_module__["a" /* ChartComponentModule */],
            __WEBPACK_IMPORTED_MODULE_6__components_consectus_header_consectus_header_module__["a" /* ConsectusHeaderModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_5__dashboard__["a" /* DashboardPage */]
        ]
    })
], DashboardPageModule);

//# sourceMappingURL=dashboard.module.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CustomerProvider = (function () {
    function CustomerProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello CustomerProvider Provider');
    }
    CustomerProvider.prototype.getAccountTransactions = function (account) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAccountTransactions(account)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomerProvider.prototype.getAuditLogs = function (customer) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAuditLogs(customer)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomerProvider.prototype.getSavingGoals = function (customer) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getCustomerSavingGoals(customer)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomerProvider.prototype.deleteSavingGoals = function (savingGoal) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteSavingGoals(savingGoal)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomerProvider.prototype.getAllRegularSavings = function (customer) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAllRegularSavings(customer)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomerProvider.prototype.getAllCheques = function (customer) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAllCustomerCheques(customer)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomerProvider.prototype.resendPasscode = function (item) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.resendPasscode(item)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomerProvider.prototype.unBlockCustomer = function (item) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.unBlockCustomer(item)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomerProvider.prototype.blockCustomer = function (block_customer) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.blockCustomer(block_customer)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomerProvider.prototype.getcustomerdetailsbycustomerid = function (customer) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getcustomerdetailsbycustomerid(customer)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomerProvider.prototype.resetcustomer = function (resetdata) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.resetcustomer(resetdata)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return CustomerProvider;
}());
CustomerProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], CustomerProvider);

//# sourceMappingURL=customer.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UsersProvider = (function () {
    function UsersProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello UsersProvider Provider');
    }
    UsersProvider.prototype.getuser = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getuser()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    UsersProvider.prototype.adduser = function (staffuserData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.adduser(staffuserData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    UsersProvider.prototype.updateuser = function (updateStaffuserData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateuser(updateStaffuserData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    UsersProvider.prototype.checkMobileNo = function (url, mobileno) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.checkMobileno(url, mobileno)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    UsersProvider.prototype.checkEmailid = function (url, mobileno) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.checkEmailid(url, mobileno)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return UsersProvider;
}());
UsersProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], UsersProvider);

//# sourceMappingURL=users.js.map

/***/ }),

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_settings_settings__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LanguageProvider = (function () {
    function LanguageProvider(http, gateway, events, settingsProvider) {
        var _this = this;
        this.http = http;
        this.gateway = gateway;
        this.events = events;
        this.settingsProvider = settingsProvider;
        this.T = {
            "Menu": "Menu",
            "Login": "Login",
            "Logout": "Logout",
            "Dashboard": "Dashboard",
            "User Management": "User Management",
            "Customer On-boarding": "Customer On-Boarding",
            "Products Management": "Products Management",
            "Push Messaging": "Push Messaging",
            "Reports": "Reports",
            //Login Page
            "LOGIN000": "Consectus Dashboard Login",
            "LOGIN001": "Login",
            "LOGIN002": "Username",
            "LOGIN003": "Password",
            "LOGIN004": "Invalid Username and/or Password.",
            "LOGIN005": "Welcome to Consecutus Dashboard",
            //Dashboard Page
            "DASH001": "Consectus Dashboard",
            //Charts
            "OBC001": "Customer On Boarding",
            "PUC001": "Push Messages",
            "PRC001": "Product Messages",
            "TRC001": "Transactions",
            //Customer details page
            'CDP000': 'Customer Details',
            'CDP001': 'Name',
            'CDP002': 'Date of Birth',
            'CDP003': 'Status',
            'CDP004': 'Customer Id',
            'CDP005': 'Transactions',
            'CDP006': 'Audit Logs',
            'Date': 'Date',
            'Amount': 'Amount',
            'Reference': 'Reference',
            'Comments': 'Comments',
        };
        this.events.subscribe("labels:loaded", function (labels) {
            console.log("labelssda", labels);
            _this.resultdata = labels.hub.labels_en;
        });
    }
    LanguageProvider.prototype.get = function (key) {
        if (this.T[key])
            return this.T[key];
        else
            return key;
    };
    LanguageProvider.prototype.getLabels = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getLabels()
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    LanguageProvider.prototype.saveLabels = function (labels, labelPage, screen) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.saveLabels(labels, labelPage, screen)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    LanguageProvider.prototype.addLabel = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addLabel()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return LanguageProvider;
}());
LanguageProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_5__providers_settings_settings__["a" /* SettingsProvider */]])
], LanguageProvider);

//# sourceMappingURL=language.js.map

/***/ }),

/***/ 148:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 148;

/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__branch_branch_module__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_upload_button_upload_button_module__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__settings__ = __webpack_require__(553);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var SettingsPageModule = (function () {
    function SettingsPageModule() {
    }
    return SettingsPageModule;
}());
SettingsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__settings__["a" /* SettingsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_5__settings__["a" /* SettingsPage */]),
            __WEBPACK_IMPORTED_MODULE_1__components_upload_button_upload_button_module__["a" /* UploadButtonComponentModule */],
            __WEBPACK_IMPORTED_MODULE_2__components_table_table_module__["a" /* TableComponentModule */],
            __WEBPACK_IMPORTED_MODULE_0__branch_branch_module__["BranchPageModule"]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_5__settings__["a" /* SettingsPage */]
        ]
    })
], SettingsPageModule);

//# sourceMappingURL=settings.module.js.map

/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchPageModule", function() { return BranchPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__branch__ = __webpack_require__(412);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BranchPageModule = (function () {
    function BranchPageModule() {
    }
    return BranchPageModule;
}());
BranchPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_0__branch__["a" /* BranchPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_0__branch__["a" /* BranchPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_0__branch__["a" /* BranchPage */]
        ]
    })
], BranchPageModule);

//# sourceMappingURL=branch.module.js.map

/***/ }),

/***/ 192:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../components/change-page-size-popover/change-page-size-popover.module": [
		193
	],
	"../pages/app-setting/app-setting.module": [
		560,
		36
	],
	"../pages/appversion/appversion.module": [
		561,
		35
	],
	"../pages/branch/branch.module": [
		150
	],
	"../pages/call-detail/call-detail.module": [
		562,
		34
	],
	"../pages/change-password/change-password.module": [
		563,
		33
	],
	"../pages/complaint-details-subject/complaint-details-subject.module": [
		565,
		32
	],
	"../pages/complaint-details/complaint-details.module": [
		564,
		31
	],
	"../pages/contact-us-message/contact-us-message.module": [
		567,
		30
	],
	"../pages/contact-us/contact-us.module": [
		566,
		29
	],
	"../pages/contacts-messages/contacts-messages.module": [
		568,
		28
	],
	"../pages/customer-call/customer-call.module": [
		569,
		27
	],
	"../pages/customer/customer.module": [
		329
	],
	"../pages/customers/customers.module": [
		330
	],
	"../pages/dashboard/dashboard.module": [
		111
	],
	"../pages/faq-category-detail/faq-category-detail.module": [
		571,
		26
	],
	"../pages/faq-category/faq-category.module": [
		570,
		25
	],
	"../pages/faqs/faqs.module": [
		572,
		24
	],
	"../pages/forgot-password/forgot-password.module": [
		573,
		23
	],
	"../pages/labels/labels.module": [
		574,
		22
	],
	"../pages/languages/languages.module": [
		575,
		21
	],
	"../pages/login/login.module": [
		65
	],
	"../pages/marketing-prefrences/marketing-prefrences.module": [
		576,
		20
	],
	"../pages/member/member.module": [
		332
	],
	"../pages/members/members.module": [
		331
	],
	"../pages/messages/messages.module": [
		577,
		19
	],
	"../pages/new-account-view/new-account-view.module": [
		579,
		18
	],
	"../pages/new-account/new-account.module": [
		578,
		17
	],
	"../pages/new-savings-milestone/new-savings-milestone.module": [
		580,
		16
	],
	"../pages/news-list/news-list.module": [
		582,
		15
	],
	"../pages/news/news.module": [
		581,
		14
	],
	"../pages/onboarding-setup/onboarding-setup.module": [
		583,
		13
	],
	"../pages/product-app-details/product-app-details.module": [
		584,
		12
	],
	"../pages/product-application/product-application.module": [
		585,
		11
	],
	"../pages/product/product.module": [
		327
	],
	"../pages/products/products.module": [
		328
	],
	"../pages/push-message/push-message.module": [
		195
	],
	"../pages/push-messaging/push-messaging.module": [
		323
	],
	"../pages/reports/reports.module": [
		194
	],
	"../pages/rpa-dashboard/rpa-dashboard.module": [
		586,
		10
	],
	"../pages/saving-account-rules-milestone/saving-account-rules-milestone.module": [
		588,
		9
	],
	"../pages/saving-account-rules/saving-account-rules.module": [
		587,
		8
	],
	"../pages/saving-account-type/saving-account-type.module": [
		589,
		7
	],
	"../pages/saving-accout-rules-list/saving-accout-rules-list.module": [
		590,
		6
	],
	"../pages/settings/settings.module": [
		149
	],
	"../pages/test/test.module": [
		333
	],
	"../pages/transaction-details/transaction-details.module": [
		592,
		5
	],
	"../pages/transaction/transaction.module": [
		591,
		4
	],
	"../pages/updateagent/updateagent.module": [
		593,
		3
	],
	"../pages/user-access/user-access.module": [
		594,
		2
	],
	"../pages/user/user.module": [
		559,
		1
	],
	"../pages/users/users.module": [
		595,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 192;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePageSizePopoverPageModule", function() { return ChangePageSizePopoverPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__change_page_size_popover__ = __webpack_require__(435);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ChangePageSizePopoverPageModule = (function () {
    function ChangePageSizePopoverPageModule() {
    }
    return ChangePageSizePopoverPageModule;
}());
ChangePageSizePopoverPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__change_page_size_popover__["a" /* ChangePageSizePopoverPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__change_page_size_popover__["a" /* ChangePageSizePopoverPage */]),
        ],
    })
], ChangePageSizePopoverPageModule);

//# sourceMappingURL=change-page-size-popover.module.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsPageModule", function() { return ReportsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__reports__ = __webpack_require__(436);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ReportsPageModule = (function () {
    function ReportsPageModule() {
    }
    return ReportsPageModule;
}());
ReportsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__reports__["a" /* ReportsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__reports__["a" /* ReportsPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__reports__["a" /* ReportsPage */]
        ]
    })
], ReportsPageModule);

//# sourceMappingURL=reports.module.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushMessagePageModule", function() { return PushMessagePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_upload_button_upload_button_module__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_bar_chart_bar_chart_module__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__push_message__ = __webpack_require__(486);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var PushMessagePageModule = (function () {
    function PushMessagePageModule() {
    }
    return PushMessagePageModule;
}());
PushMessagePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__push_message__["c" /* PushMessagePage */],
            __WEBPACK_IMPORTED_MODULE_2__push_message__["a" /* AddActionModal */],
            __WEBPACK_IMPORTED_MODULE_2__push_message__["b" /* AddBulletModal */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__push_message__["c" /* PushMessagePage */]),
            __WEBPACK_IMPORTED_MODULE_1__components_bar_chart_bar_chart_module__["a" /* BarChartComponentModule */],
            __WEBPACK_IMPORTED_MODULE_0__components_upload_button_upload_button_module__["a" /* UploadButtonComponentModule */],
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__push_message__["a" /* AddActionModal */],
            __WEBPACK_IMPORTED_MODULE_2__push_message__["b" /* AddBulletModal */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__push_message__["c" /* PushMessagePage */],
            __WEBPACK_IMPORTED_MODULE_2__push_message__["a" /* AddActionModal */],
            __WEBPACK_IMPORTED_MODULE_2__push_message__["b" /* AddBulletModal */]
        ]
    })
], PushMessagePageModule);

//# sourceMappingURL=push-message.module.js.map

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OauthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// declare var window;
var OauthProvider = (function () {
    function OauthProvider(events, gateway, zone, http) {
        // this.user = this.getStorageVariable('profile');
        // this.user_role = this.getStorageVariable('user_role')
        // if (!this.user_role) this.user_role = {};
        this.events = events;
        this.gateway = gateway;
        this.zone = zone;
        this.http = http;
        this.user_role = {};
        console.log(this.user_role, "this.user_rolethis.user_role");
        // this.idToken = this.getStorageVariable('id_token');
    }
    // private getStorageVariable(name) {
    //   return JSON.parse(window.localStorage.getItem(name));
    // }
    // private setStorageVariable(name, data) {
    //    window.localStorage.setItem(name, JSON.stringify(data));
    // }
    // private setIdToken(token) {
    //   this.idToken = token;
    //   // this.setStorageVariable('id_token', token);
    // }
    // private setAccessToken(token) {
    //   this.accessToken = token;
    //   // this.setStorageVariable('access_token', token);
    // }
    // public isAuthenticated() {
    //   const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    //   return Date.now() < expiresAt;
    // }
    OauthProvider.prototype.blockSelected = function (blockSelected) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.blockSelected(blockSelected)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    OauthProvider.prototype.deleteSelected = function (deleteSelected) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteSelectedUser(deleteSelected)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    OauthProvider.prototype.activateSelected = function (activateSelected) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.activateSelected(activateSelected)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    OauthProvider.prototype.login = function (username, password) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getUsers()
                .then(function (data) {
                console.log("databipin", data);
                for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                    var u = data_1[_i];
                    if (u.username == username && password == "consectus") {
                        // self.setIdToken(Math.random().toString(36).substr(8, 8).toUpperCase());
                        // self.setAccessToken(Math.random().toString(36).substr(8, 8).toUpperCase());
                        //  const expiresAt = JSON.stringify((3600 * 1000) + new Date().getTime());
                        // self.setStorageVariable('expires_at', expiresAt);
                        // self.setStorageVariable('profile', u);
                        self.user = u;
                        self.events.publish("consectus:login");
                        resolve(self.user);
                    }
                }
                reject("Username / Password do not match");
            });
        });
    };
    OauthProvider.prototype.logout = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.user = null;
            self.events.publish("consectus:login");
            resolve();
            self.gateway.logout().then(function (result) {
            }).catch(function (error) {
            });
        });
    };
    OauthProvider.prototype.adminLogin = function (username, password) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.adminLogin(username, password)
                .then(function (result) {
                if (result.status) {
                    // self.setIdToken(Math.random().toString(36).substr(8, 8).toUpperCase());
                    // self.setAccessToken(Math.random().toString(36).substr(8, 8).toUpperCase());
                    // const expiresAt = JSON.stringify((3600 * 1000) + new Date().getTime());
                    // self.setStorageVariable('expires_at', expiresAt);
                    // self.setStorageVariable('profile', result.data);
                    // console.log(result.user_role, "user_role");
                    // self.setStorageVariable('user_role', result.user_role[0]);
                    // self.setStorageVariable('login_token', result.user_role[0]);
                    self.user = result.data;
                    self.user_role = result.user_role[0];
                    self.events.publish("consectus:login");
                    self.events.publish("consectus:userrole", result.user_role[0]);
                    resolve(result);
                }
                else {
                    reject("Username / Password do not match");
                }
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    OauthProvider.prototype.getUsers = function (filter) {
        if (filter === void 0) { filter = null; }
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getUsers(filter)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    OauthProvider.prototype.addUser = function (user) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addUser(user)
                .then(function () {
                resolve();
            });
        });
    };
    OauthProvider.prototype.oauthToken = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.oauthToken().then(function (data) {
                resolve(data);
            }).catch(function (error) {
                reject("Something Went wrong");
            });
        });
    };
    OauthProvider.prototype.updateUser = function (user) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateUser(user)
                .then(function () {
                resolve();
            });
        });
    };
    OauthProvider.prototype.deleteUser = function (user) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteUser(user)
                .then(function () {
                resolve();
            });
        });
    };
    OauthProvider.prototype.addUserActivity = function (user, summary) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.AddUserActivity(user, summary)
                .then(function () {
                resolve();
            });
        });
    };
    OauthProvider.prototype.resetpassword = function (emailName) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.resetpassword(emailName)
                .then(function (result) {
                resolve(result);
            });
        });
    };
    return OauthProvider;
}());
OauthProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__["a" /* GatewayClientProvider */],
        __WEBPACK_IMPORTED_MODULE_1__angular_core__["P" /* NgZone */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */]])
], OauthProvider);

//# sourceMappingURL=oauth.js.map

/***/ }),

/***/ 21:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SettingsProvider = (function () {
    function SettingsProvider(gateway, http, events) {
        this.gateway = gateway;
        this.http = http;
        this.events = events;
        this.resultData = null;
        console.log('SettingsProvider Provider');
    }
    SettingsProvider.prototype.getBankDetails = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getBankDetails()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.updateBankDetails = function (details) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateBankDetails(details)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getbranch = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getbranch()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.addbranch = function (branch) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addbranch(branch)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.deleteBranch = function (branch) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteBranch(branch)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.deletefaq = function (faq) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deletefaq(faq)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.activateFaq = function (faq) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.activateFaq(faq)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.deletebranch = function (branch) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deletebranch(branch)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.activatebranch = function (branch) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.activatebranch(branch)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.updateBranch = function (branch) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateBranch(branch)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getBankLookAndFeel = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getBankLookAndFeel()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.updateBankLookAndFeel = function (lookandfeel) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateBankLookAndFeel(lookandfeel)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.updateonboardingtnc = function (termsData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateonboardingtnc(termsData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.updatenewaccountnc = function (termsData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updatenewaccountnc(termsData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getContactusSubject = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getContactusSubject()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.gettermsandcondition = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.gettermsandcondition()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.addSubject = function (subject) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addSubject(subject)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.updateSubject = function (subject) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateSubject(subject)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.deleteContact = function (contact) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteContact(contact)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getFaqs = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getFaqs()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.addFaqs = function (faq) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addFaqs(faq)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getAllFaqCategories = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getallcategories()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.updateFaqs = function (faq) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateFaqs(faq)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.deleteFaqs = function (faq) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteFaqs(faq)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getMarketingPrefrences = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
        });
    };
    SettingsProvider.prototype.getSavingAccountRules = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getSavingAccountRules().then(function (data) {
                console.log(data);
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getAccountRules = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAccountRules().then(function (data) {
                console.log(data);
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.updateSavingAccountRules = function (accObj) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateSavingAccountRules(accObj).then(function (data) {
                console.log(data);
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.saveSavingAccountRule = function (accObj) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.saveSavingAccountRule(accObj)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getsmtp = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getsmtp()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.addsmtp = function (addData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addsmtp(addData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.updatesmtp = function (updateData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updatesmtp(updateData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getcontactsubject = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getcontactsubject()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getcustomercomplaint = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getcustomercomplaint()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.addcontactsubject = function (addData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addcontactsubject(addData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.updateContactSubject = function (contactSubjectData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateContactSubject(contactSubjectData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.getLatLong = function (latLang) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getLatLong(latLang)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SettingsProvider.prototype.labelApi = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getLabels()
                .then(function (result) {
                self.labelsData = result.data;
                self.events.publish("consectus:menu");
                resolve();
            });
        });
    };
    /**
    * Get all the page labels
    * @param pagename :Use for the page labels
    */
    SettingsProvider.prototype.getAppLabels = function (pagename) {
        var self = this;
        if (self.labelsData !== undefined) {
            if (pagename !== undefined) {
                for (var key1 in self.labelsData.hub[pagename]) {
                    if (key1 == "labels_" + self.languageString) {
                        self.resultData = self.labelsData.hub[pagename][key1];
                    }
                    else if (key1 == "labels_en") {
                        self.resultData = self.labelsData.hub[pagename][key1];
                    }
                }
            }
        }
    };
    /**
     * Get page key from page Labels
     * @param key : find key in page labels
     */
    SettingsProvider.prototype.getLabel = function (key) {
        var self = this;
        if (self.resultData !== null && self.resultData[key] !== undefined) {
            return self.resultData[key];
        }
        else {
            return key;
        }
    };
    return SettingsProvider;
}());
SettingsProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__["a" /* GatewayClientProvider */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* Events */]])
], SettingsProvider);

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MasterpermissionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__oauth_oauth__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { HttpClient } from '@angular/common/http';


// import { GatewayClientProvider } from './../gateway-client/gateway-client';
// import { Events } from 'ionic-angular';
// import { Http } from '@angular/http';
// import 'rxjs/add/operator/map';
/*
  Generated class for the MasterpermissionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var MasterpermissionProvider = (function () {
    function MasterpermissionProvider(oauthProvider) {
        this.oauthProvider = oauthProvider;
    }
    MasterpermissionProvider.prototype.permission = function (name) {
        if (this.oauthProvider.user !== undefined) {
            if (this.oauthProvider.user_role.admin === 1 && name === "admin") {
                return true;
            }
            else if (this.oauthProvider.user_role.appsettings === 1 && name === "appsettings") {
                return true;
            }
            else if (this.oauthProvider.user_role.calls === 1 && name === "calls") {
                return true;
            }
            else if (this.oauthProvider.user_role.customers === 1 && name === "customers") {
                return true;
            }
            else if (this.oauthProvider.user_role.goal_milestone === 1 && name === "goal_milestone") {
                return true;
            }
            else if (this.oauthProvider.user_role.isactive === 1 && name === "isactive") {
                return true;
            }
            else if (this.oauthProvider.user_role.labels === 1 && name === "labels") {
                return true;
            }
            else if (this.oauthProvider.user_role.members === 1 && name === "members") {
                return true;
            }
            else if (this.oauthProvider.user_role.newaccount === 1 && name === "newaccount") {
                return true;
            }
            else if (this.oauthProvider.user_role.news === 1 && name === "news") {
                return true;
            }
            else if (this.oauthProvider.user_role.products === 1 && name === "products") {
                return true;
            }
            else if (this.oauthProvider.user_role.pushmessages === 1 && name === "pushmessages") {
                return true;
            }
            else if (this.oauthProvider.user_role.rulesengine === 1 && name === "rulesengine") {
                return true;
            }
            else if (this.oauthProvider.user_role.staffusers === 1 && name === "staffusers") {
                return true;
            }
            else if (this.oauthProvider.user_role.transactions === 1 && name === "transactions") {
                return true;
            }
            else if (this.oauthProvider.user && name === "default") {
                return true;
            }
            else {
                // console.log("this.appCtr", this.app)
                // setTimeout(this.appCtr.getRootNav().setRoot('LoginPage'), 0);
                setTimeout(function () { window.app.nav.setRoot("LoginPage"); }, 0);
                return false;
            }
        }
        else {
            // console.log("this.appCtr", this.app)
            setTimeout(function () { window.app.nav.setRoot("LoginPage"); }, 0);
            // setTimeout(this.app.getRootNav().setRoot('LoginPage'), 0);
            return false;
        }
    };
    return MasterpermissionProvider;
}());
MasterpermissionProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__oauth_oauth__["a" /* OauthProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__oauth_oauth__["a" /* OauthProvider */]) === "function" && _a || Object])
], MasterpermissionProvider);

var _a;
//m:nu8!D[EY7CMf
//# sourceMappingURL=masterpermission.js.map

/***/ }),

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GlobalProvider = (function () {
    function GlobalProvider(http, platform, loadingCtrl) {
        this.http = http;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        console.log('Hello GlobalProvider Provider');
        var flag = false;
        this.setIsLive(flag);
        if (window.rpa) {
            this.setBaseUrlRpa("http://localhost:3005");
        }
        else if (window.isDev) {
            this.setBaseUrl("http://18.188.127.1:8888");
        }
        else if (window.isLive) {
            this.setBaseUrl("http://35.176.159.156:8888");
        }
        else if (window.localhost) {
            this.setBaseUrl("http://localhost:8888");
        }
        else if (window.vpc) {
            this.setBaseUrl("http://18.216.72.33:8888");
        }
        if (this.platform.is('core') || this.platform.is('mobileweb')) {
            this.isDevice = false;
        }
        else {
            this.isDevice = true;
        }
    }
    GlobalProvider.prototype.setIsLive = function (value) {
        this.isLive = value;
    };
    GlobalProvider.prototype.getIsLive = function () {
        return this.isLive;
    };
    GlobalProvider.prototype.setBaseUrl = function (value) {
        this.baseUrl = value;
    };
    GlobalProvider.prototype.getBaseUrl = function () {
        return this.baseUrl;
    };
    GlobalProvider.prototype.setBaseUrlRpa = function (value) {
        this.RpabaseUrl = value;
    };
    GlobalProvider.prototype.getBaseUrlRpa = function () {
        return this.RpabaseUrl;
    };
    GlobalProvider.prototype.setCustomerBaseUrl = function (value) {
        this.baseUrl = value;
    };
    GlobalProvider.prototype.getCustomerBaseUrl = function () {
        return this.baseUrl;
    };
    GlobalProvider.prototype.pageLoading = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            content: 'Loading Please Wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
        setTimeout(function () {
            _this.loading.dismiss();
        }, 3000);
    };
    GlobalProvider.prototype.dismissLoading = function () {
        this.loading.dismiss();
    };
    GlobalProvider.prototype.showLoader = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            dismissOnPageChange: true
        });
        this.loader.present();
    };
    GlobalProvider.prototype.hideLoader = function () {
        this.loader.dismiss();
    };
    GlobalProvider.prototype.formatDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    return GlobalProvider;
}());
GlobalProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */]])
], GlobalProvider);

//# sourceMappingURL=global.js.map

/***/ }),

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushMessagingPageModule", function() { return PushMessagingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__push_message_push_message_module__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__push_messaging__ = __webpack_require__(489);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var PushMessagingPageModule = (function () {
    function PushMessagingPageModule() {
    }
    return PushMessagingPageModule;
}());
PushMessagingPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__push_messaging__["a" /* PushMessagingPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__push_messaging__["a" /* PushMessagingPage */]),
            __WEBPACK_IMPORTED_MODULE_0__push_message_push_message_module__["PushMessagePageModule"],
            __WEBPACK_IMPORTED_MODULE_1__components_table_table_module__["a" /* TableComponentModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_4__push_messaging__["a" /* PushMessagingPage */]
        ]
    })
], PushMessagingPageModule);

//# sourceMappingURL=push-messaging.module.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_customers_customers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_dashboard_dashboard__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_products_products__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_push_messaging_push_messaging__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_member_member__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_chart_js__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_users_users__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var DashboardPage = (function () {
    // @ViewChild('')
    // @viewChild('')
    function DashboardPage(customersProvider, languageProvider, oauthProvider, dashboardProvider, productsProvider, messagesProvider, memberProvider, globalProvider, cdr, usersProvider, navCtrl, navParams, event, settingsProvider, toastCtrl, viewCtrl, masterpermissionProvider) {
        var _this = this;
        this.customersProvider = customersProvider;
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.dashboardProvider = dashboardProvider;
        this.productsProvider = productsProvider;
        this.messagesProvider = messagesProvider;
        this.memberProvider = memberProvider;
        this.globalProvider = globalProvider;
        this.cdr = cdr;
        this.usersProvider = usersProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.event = event;
        this.settingsProvider = settingsProvider;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.customersData = [];
        this.channel = "mobileApp";
        this.customersAction = this.customersClicked.bind(this);
        this.productsAction = this.productsClicked.bind(this);
        this.pushAction = this.pushClicked.bind(this);
        this.transactionsAction = this.transactionsClicked.bind(this);
        this.activityAction = this.activityClicked.bind(this);
        this.performanceAction = this.activityClicked.bind(this);
        this.filter = null;
        this.callsData = [];
        this.resultdata = [];
        this.languageString = "";
        console.log("masterpermission");
        this.event.subscribe("consectus:language", function (language) {
            _this.languageString = language;
            //console.log("subscribe language", this.languageString);
        });
        this.filter = null;
        this.graphData = {
            "active": 0,
            "inactive": 0,
            "onboard": 0,
            "telephone": 0,
            "fiveactive": 0,
            "fiveinactive": 0,
            "fiveonboard": 0,
            "monthactive": 0,
            "monthinactive": 0,
            "monthonboard": 0,
            "yearactive": 0,
            "yearinactive": 0,
            "yearonboard": 0,
            "transactiontotal": 0,
            "transactionfive": 0,
            "transactionfivetotal": 0,
            "transactionmonth": 0,
            "transactionmonthtotal": 0,
            "transactionyear": 0,
            "transactionyeartotal": 0,
            "callstotal": 0,
            "callsfive": 0,
            "callsmonth": 0,
            "callsyear": 0,
            "productcount": 0,
            "totaldelivered": 0,
            "totalinterested": 0,
            "totalviewed": 0,
            "messagecount": 0,
            "messagetotaldelivered": 0,
            "messagetotalinterested": 0,
            "messagetotalviewed": 0
        };
    }
    DashboardPage.prototype.ionViewCanEnter = function () {
        if (this.masterpermissionProvider.permission("default")) {
            return true;
        }
        else {
            return false;
        }
    };
    DashboardPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        this.getCustomCount();
    };
    DashboardPage.prototype.getPlatformGraphData = function () {
        var _this = this;
        var self = this;
        this.dashboardProvider.getPlatformGraph()
            .then(function (data) {
            self.platformData = data;
            _this.graphData.active = data.allcount.activecount.length;
            _this.graphData.monthactive = data.allcount.month.length;
            _this.graphData.fiveactive = data.allcount.five.length;
            _this.graphData.yearactive = data.allcount.year.length;
            _this.graphData.inactive = data.allcount.inactivecount.length;
            _this.graphData.fiveinactive = data.allcount.fiveinactive.length;
            _this.graphData.monthinactive = data.allcount.monthinactive.length;
            _this.graphData.yearinactive = data.allcount.yearinactive.length;
            _this.graphData.onboard = data.allcount.onboardingcount.length;
            _this.graphData.fiveonboard = data.allcount.fiveonboarding.length;
            _this.graphData.monthonboard = data.allcount.monthonboarding.length;
            _this.graphData.yearonboard = data.allcount.yearonboarding.length;
            self.cdr.detectChanges();
        }).catch(function (err) {
        });
        this.dashboardProvider.getProductCount()
            .then(function (data) {
            self.graphData.productcount = data.length;
            for (var i in data) {
                self.graphData.totaldelivered += data[i].delivered;
                self.graphData.totalinterested += data[i].interested;
                self.graphData.totalviewed += data[i].viewed;
            }
        }).catch(function (err) {
        });
        this.dashboardProvider.getMessagesCount()
            .then(function (data) {
            self.graphData.messagecount = data.length;
            for (var i in data) {
                self.graphData.messagetotaldelivered += data[i].delivered;
                self.graphData.messagetotalinterested += data[i].interested;
                self.graphData.messagetotalviewed += data[i].viewed;
            }
        });
    };
    DashboardPage.prototype.bindGraphCount = function (type) {
        if (type == "all") {
            this.graphData.active = this.platformData.allcount.activecount.length;
            this.graphData.monthactive = this.platformData.allcount.month.length;
            this.graphData.fiveactive = this.platformData.allcount.five.length;
            this.graphData.yearactive = this.platformData.allcount.year.length;
            this.graphData.inactive = this.platformData.allcount.inactivecount.length;
            this.graphData.fiveinactive = this.platformData.allcount.fiveinactive.length;
            this.graphData.monthinactive = this.platformData.allcount.monthinactive.length;
            this.graphData.yearinactive = this.platformData.allcount.yearinactive.length;
            this.graphData.onboard = this.platformData.allcount.onboardingcount.length;
            this.graphData.fiveonboard = this.platformData.allcount.fiveonboarding.length;
            this.graphData.monthonboard = this.platformData.allcount.monthonboarding.length;
            this.graphData.yearonboard = this.platformData.allcount.yearonboarding.length;
        }
        else if (type == "mobile") {
            this.graphData.active = this.platformData.mobile.activecount.length;
            this.graphData.monthactive = this.platformData.mobile.month.length;
            this.graphData.fiveactive = this.platformData.mobile.five.length;
            this.graphData.yearactive = this.platformData.mobile.year.length;
            this.graphData.inactive = this.platformData.mobile.inactivecount.length;
            this.graphData.fiveinactive = this.platformData.mobile.fiveinactive.length;
            this.graphData.monthinactive = this.platformData.mobile.monthinactive.length;
            this.graphData.yearinactive = this.platformData.mobile.yearinactive.length;
            this.graphData.onboard = this.platformData.mobile.onboardingcount.length;
            this.graphData.fiveonboard = this.platformData.mobile.fiveonboarding.length;
            this.graphData.monthonboard = this.platformData.mobile.monthonboarding.length;
            this.graphData.yearonboard = this.platformData.mobile.yearonboarding.length;
        }
        else if (type == "online") {
            this.graphData.active = this.platformData.online.activecount.length;
            this.graphData.monthactive = this.platformData.online.month.length;
            this.graphData.fiveactive = this.platformData.online.five.length;
            this.graphData.yearactive = this.platformData.online.year.length;
            this.graphData.inactive = this.platformData.online.inactivecount.length;
            this.graphData.fiveinactive = this.platformData.online.fiveinactive.length;
            this.graphData.monthinactive = this.platformData.online.monthinactive.length;
            this.graphData.yearinactive = this.platformData.online.yearinactive.length;
            this.graphData.onboard = this.platformData.online.onboardingcount.length;
            this.graphData.fiveonboard = this.platformData.online.fiveonboarding.length;
            this.graphData.monthonboard = this.platformData.online.monthonboarding.length;
            this.graphData.yearonboard = this.platformData.online.yearonboarding.length;
        }
        else if (type == "telephone") {
            this.graphData.active = this.platformData.telephone.activecount.length;
            this.graphData.monthactive = this.platformData.telephone.month.length;
            this.graphData.fiveactive = this.platformData.telephone.five.length;
            this.graphData.yearactive = this.platformData.telephone.year.length;
            this.graphData.inactive = this.platformData.telephone.inactivecount.length;
            this.graphData.fiveinactive = this.platformData.telephone.fiveinactive.length;
            this.graphData.monthinactive = this.platformData.telephone.monthinactive.length;
            this.graphData.yearinactive = this.platformData.telephone.yearinactive.length;
            this.graphData.onboard = this.platformData.telephone.onboardingcount.length;
            this.graphData.monthonboard = this.platformData.telephone.monthonboarding.length;
            this.graphData.yearonboard = this.platformData.telephone.yearonboarding.length;
        }
    };
    DashboardPage.prototype.bindPushMessageChart = function (mode) {
        if (mode == 0) {
            this.pushMessagesDoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.pushMessagesDoughnut.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Delivered', 'Viewed', 'Interested'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.messagetotaldelivered, this.graphData.messagetotalviewed, this.graphData.messagetotalinterested],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(153, 102, 255, 1)',
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
        else {
            this.pushMessagesDoughnutChart.destroy();
            this.pushMessagesDoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.pushMessagesDoughnut.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Delivered', 'Viewed', 'Interested'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.messagetotaldelivered, this.graphData.messagetotalviewed, this.graphData.messagetotalinterested],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(153, 102, 255, 1)',
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
    };
    DashboardPage.prototype.bindTransactionChart = function (mode) {
        if (mode == 0) {
            this.transactionCustomerDoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.transactiondoughnut.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Last 5 Days', 'Last Month', 'Last Year'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.transactionfive, this.graphData.transactionmonth, this.graphData.transactionyear],
                            backgroundColor: [
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255, 206, 86, 1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(153, 102, 255, 1)',
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
        else {
            this.transactionCustomerDoughnutChart.destroy();
            this.transactionCustomerDoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.transactiondoughnut.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Deposits', 'Withdrawals', 'Rejections'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.transactionfive, this.graphData.transactionmonth, this.graphData.transactionyear],
                            backgroundColor: [
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255, 206, 86, 1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(153, 102, 255, 1)',
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
    };
    // bindCallChart(mode) {
    //     if (mode == 0) {
    //         this.callsDoughnutChart = new Chart(this.callsDoughnut.nativeElement, {
    //             type: 'doughnut',
    //             data: {
    //                 labels: ['Last 5 Days', 'Last Month', 'Last Year'],
    //                 datasets: [{
    //                     label: '',
    //                     data: [this.graphData.callsfive, this.graphData.callsmonth, this.graphData.callsyear],
    //                     backgroundColor: [
    //                         'rgba(255, 206, 86, 0.2)',
    //                         'rgba(230, 245, 19, 0.2)',
    //                         'rgba(153, 102, 255, 0.2)',
    //                     ],
    //                     borderColor: [
    //                         'rgba(255, 206, 86, 1)',
    //                         'rgba(230, 245, 19, 0.2)',
    //                         'rgba(153, 102, 255, 1)',
    //                     ],
    //                     borderWidth: 1
    //                 }]
    //             }
    //         });
    //     }
    //     else {
    //         this.callsDoughnutChart.destroy();
    //         this.callsDoughnutChart = new Chart(this.callsDoughnut.nativeElement, {
    //             type: 'doughnut',
    //             data: {
    //                 labels: ['Deposits', 'Withdrawals', 'Rejections'],
    //                 datasets: [{
    //                     label: '',
    //                     data: [this.graphData.callsfive, this.graphData.callsmonth, this.graphData.callsyear],
    //                     backgroundColor: [
    //                         'rgba(255, 206, 86, 0.2)',
    //                         'rgba(230, 245, 19, 0.2)',
    //                         'rgba(153, 102, 255, 0.2)',
    //                     ],
    //                     borderColor: [
    //                         'rgba(255, 206, 86, 1)',
    //                         'rgba(230, 245, 19, 0.2)',
    //                         'rgba(153, 102, 255, 1)',
    //                     ],
    //                     borderWidth: 1
    //                 }]
    //             }
    //         });
    //     }
    // }
    DashboardPage.prototype.bindOnBoardingChart = function (mode) {
        if (mode == 0) {
            this.onboardingDoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.onboardingdoughnut.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Last 5 Days', 'Last Month', 'Last Year'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.fiveonboard, this.graphData.monthonboard, this.graphData.yearonboard],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 1)',
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
        else {
            this.onboardingDoughnutChart.destroy();
            this.onboardingDoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.onboardingdoughnut.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Last 5 Days', 'Last Month', 'Last Year'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.fiveonboard, this.graphData.monthonboard, this.graphData.yearonboard],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 1)',
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
    };
    DashboardPage.prototype.bindNonActiveCustomerChart = function (mode) {
        if (mode == 0) {
            this.nonActiveCustomerdoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.nonActiveCustomerdoughnut.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Last 3 Months', 'Last 6 Months', 'Last Year'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.fiveinactive, this.graphData.monthinactive, this.graphData.yearinactive],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 1)',
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
        else {
            this.nonActiveCustomerdoughnutChart.destroy();
            this.nonActiveCustomerdoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.nonActiveCustomerdoughnut.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Last 5 Days', 'Last Month', 'Last Year'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.fiveinactive, this.graphData.monthinactive, this.graphData.yearinactive],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 1)',
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
    };
    DashboardPage.prototype.bindActiveCustomerChart = function (mode) {
        if (mode == 0) {
            this.activeCustomerdoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.activeCustomerdoughnut.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Last 5 Days', 'Last Month', 'Last Year'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.fiveactive, this.graphData.monthactive, this.graphData.yearactive],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(230, 245, 19, 1)',
                                'rgba(75, 192, 192, 1)',
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
        else {
            this.activeCustomerdoughnutChart.destroy();
            this.activeCustomerdoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.activeCustomerdoughnut.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Last 5 Days', 'Last Month', 'Last Year'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.fiveactive, this.graphData.monthactive, this.graphData.yearactive],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(75, 192, 192, 1)'
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
    };
    DashboardPage.prototype.bindProductGraph = function (mode) {
        if (mode == 0) {
            this.productdoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.productDoughnutCanvas.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Delivered', 'Viewed', 'Interested'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.totaldelivered, this.graphData.totalviewed, this.graphData.totalinterested],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(255, 206, 86, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(255, 206, 86, 1)'
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
        else {
            this.productdoughnutChart.destroy();
            this.productdoughnutChart = new __WEBPACK_IMPORTED_MODULE_10_chart_js__["Chart"](this.productDoughnutCanvas.nativeElement, {
                type: 'doughnut',
                data: {
                    labels: ['Delivered', 'Viewed', 'Interested'],
                    datasets: [{
                            label: '',
                            data: [this.graphData.totaldelivered, this.graphData.totalviewed, this.graphData.totalinterested],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(255, 206, 86, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(230, 245, 19, 0.2)',
                                'rgba(255, 206, 86, 1)'
                            ],
                            borderWidth: 1
                        }]
                }
            });
        }
    };
    DashboardPage.prototype.getCustomCount = function () {
        var _this = this;
        this.usersProvider.getuser()
            .then(function (result) {
            _this.userCount = result.data.length;
        });
        this.productsProvider.getProducts()
            .then(function (result) {
            _this.productCount = result.data.length;
        });
        this.memberProvider.getMembers()
            .then(function (result) {
            _this.memberCount = result.data.length;
        });
        this.messagesProvider.getmessages()
            .then(function (result) {
            _this.messageCount = result.data.length;
        });
        this.settingsProvider.getcustomercomplaint()
            .then(function (result) {
            _this.contactUsMessagesCount = result.data.length;
        });
        this.customersProvider.getAllCustomers().then(function (result) {
            _this.customerCount = result.data.length;
            _this.customersData = result;
            _this.getBlockedCustCount();
            _this.getfailedPasscodeCustCount();
            _this.getBlockedPasscodeCustomers();
        });
    };
    DashboardPage.prototype.getBlockedCustCount = function () {
        var returnval = this.customersData.data.filter(function (obj) { return obj.status == 'Blocked'; });
        this.blockedCustomerCount = returnval.length;
    };
    DashboardPage.prototype.getfailedPasscodeCustCount = function () {
        var returnval = this.customersData.data.filter(function (obj) { return obj.status == 'OTPAWAIT'; });
        this.failedPasscodeCustomerCount = returnval.length;
    };
    DashboardPage.prototype.getBlockedPasscodeCustomers = function () {
        var returnval = this.customersData.data.filter(function (obj) { return obj.status == 'OTPFAILED'; });
        this.blockedPasscode = returnval.length;
    };
    DashboardPage.prototype.bindTransactions = function () {
        var self = this;
        this.dashboardProvider.getTransaction().then(function (data) {
            self.graphData.transactiontotal = data.total;
            self.graphData.transactionfive = data.five;
            self.graphData.transactionmonth = data.month;
            self.graphData.transactionyear = data.year;
            self.graphData.transactionfivetotal = data.fiveamount;
            self.graphData.transactionmonthtotal = data.monthamount;
            self.graphData.transactionyeartotal = data.yearamount;
        }).catch(function (err) {
            console.log(err);
        });
    };
    // bindCalls() {
    //     let self = this;
    //     this.dashboardProvider.getCalls().then((data: any) => {
    //         self.graphData.callstotal = data.total;
    //         self.graphData.callsfive = data.five;
    //         self.graphData.callsmonth = data.month;
    //         self.graphData.callsyear = data.year
    //     }).catch((err) => {
    //         console.log(err);
    //     })
    // }
    DashboardPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.event.subscribe("contactUsMessages:loaded", function (contactUsLength) {
        });
        setTimeout(function () {
            _this.getPlatformGraphData();
            _this.bindTransactions();
            //this.bindCalls();
        }, 1000);
        this.globalProvider.pageLoading();
        setTimeout(function () {
            _this.bindProductGraph(0);
            _this.bindActiveCustomerChart(0);
            _this.bindNonActiveCustomerChart(0);
            _this.bindOnBoardingChart(0);
            //this.bindCallChart(0);
            _this.bindPushMessageChart(0);
            _this.bindTransactionChart(0);
        }, 5000);
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
        }
        // let count = {
        //     users_id: 2
        // }
        var self = this;
        self.productsOptions = {};
        self.cdr.detectChanges();
        self.productsOptions = {};
        self.cdr.detectChanges();
        self.pushOptions = {};
        self.cdr.detectChanges();
        self.transactionsOptions = {};
        self.cdr.detectChanges();
    };
    DashboardPage.prototype.segmentChanged = function (event) {
        var _this = this;
        switch (this.channel) {
            // case "all":
            //     this.globalProvider.pageLoading();
            //     this.bindGraphCount("all");
            //     setTimeout(() => {
            //         this.bindActiveCustomerChart(1);
            //         this.bindNonActiveCustomerChart(1);
            //         this.bindOnBoardingChart(1);
            //         this.bindCallChart(1);
            //         this.bindTransactionChart(1);
            //         this.bindProductGraph(1);
            //         this.bindPushMessageChart(1);
            //     }, 1000);
            //     break;
            case "mobileApp":
                this.globalProvider.pageLoading();
                this.bindGraphCount("mobile");
                setTimeout(function () {
                    _this.bindProductGraph(1);
                    _this.bindPushMessageChart(1);
                    _this.bindActiveCustomerChart(1);
                    _this.bindNonActiveCustomerChart(1);
                    _this.bindOnBoardingChart(1);
                }, 1000);
                break;
            // case "onlineApp":
            //     this.globalProvider.pageLoading();
            //     this.bindGraphCount("online");
            //     setTimeout(() => {
            //         this.bindActiveCustomerChart(1);
            //         this.bindNonActiveCustomerChart(1);
            //         this.bindOnBoardingChart(1);
            //     }, 1000);
            //     break;
            // case "telephoneConnect":
            //     this.globalProvider.pageLoading();
            //     this.bindGraphCount("telephone");
            //     setTimeout(() => {
            //         this.bindCallChart(1);
            //         this.bindTransactionChart(1);
            //     }, 1000);
            //     break;
            default:
                break;
        }
    };
    DashboardPage.prototype.customersClicked = function (event) {
        this.navCtrl.setRoot("CustomersPage", { filter: event });
    };
    DashboardPage.prototype.productsClicked = function (event) {
        this.navCtrl.setRoot("ProductsPage");
    };
    DashboardPage.prototype.pushClicked = function (event) {
        // console.log(JSON.stringify(event));
    };
    DashboardPage.prototype.transactionsClicked = function (event) {
        // console.log(JSON.stringify(event));
    };
    DashboardPage.prototype.activityClicked = function (event) {
        // console.log(JSON.stringify(event));
    };
    DashboardPage.prototype.memberAction = function () {
        var self = this;
        if (self.oauthProvider.user_role.members == 0) {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        }
        else {
            this.navCtrl.push('MembersPage');
        }
    };
    DashboardPage.prototype.userAction = function () {
        var self = this;
        if (self.oauthProvider.user_role.staffusers == 0) {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        }
        else {
            this.navCtrl.push('UsersPage');
        }
    };
    DashboardPage.prototype.productAction = function () {
        var self = this;
        if (self.oauthProvider.user_role.products == 0) {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        }
        else {
            this.navCtrl.push('ProductsPage');
        }
    };
    DashboardPage.prototype.messagesAction = function () {
        var self = this;
        if (self.oauthProvider.user_role.pushmessages == 0) {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        }
        else {
            this.navCtrl.push('PushMessagingPage');
        }
    };
    DashboardPage.prototype.activeCustomers = function () {
        // let self = this;
        // if (self.oauthProvider.user_role.customers == 0) {
        //     let toast = self.toastCtrl.create({
        //         message: 'You are not authorised to access this page',
        //         duration: 4000,
        //         position: 'right',
        //         cssClass: 'success',
        //     })
        //     toast.present();
        // } else {
        //     this.navCtrl.push('CustomersPage');
        // }
    };
    // nonactiveCustomers() {
    //     let self = this;
    //     if (self.oauthProvider.user_role.customers == 0) {
    //         let toast = self.toastCtrl.create({
    //             message: 'You are not authorised to access this page',
    //             duration: 4000,
    //             position: 'right',
    //             cssClass: 'success',
    //         })
    //         toast.present();
    //     } else {
    //         this.navCtrl.push('CustomersPage');
    //     }
    // }
    DashboardPage.prototype.addProductAction = function () {
        this.navCtrl.push('ProductPage');
    };
    DashboardPage.prototype.addUserAction = function () {
        this.navCtrl.push('UserPage');
    };
    DashboardPage.prototype.addMemberAction = function () {
        this.navCtrl.push('MemberPage');
    };
    DashboardPage.prototype.addMessageAction = function () {
        this.navCtrl.push('PushMessagePage');
    };
    DashboardPage.prototype.addCustomerAction = function () {
        var self = this;
        if (self.oauthProvider.user_role.customers == 0) {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        }
        else {
            this.navCtrl.push('CustomersPage');
        }
    };
    DashboardPage.prototype.addContactUsFailedMessages = function () {
        this.navCtrl.push("Messages", { "issend": "contactus" });
    };
    DashboardPage.prototype.addContactUsMessages = function () {
        this.navCtrl.push("Messages", { "msg": "contactus" });
    };
    DashboardPage.prototype.blockedPinCustomers = function () {
        var self = this;
        if (self.oauthProvider.user_role.customers == 0) {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        }
        else {
            this.navCtrl.push("CustomersPage", { blocked: "Blocked" });
        }
    };
    DashboardPage.prototype.blockedPasscodeCustomers = function () {
        this.navCtrl.push("CustomersPage", { otp: "OTP BLOCKED" });
    };
    DashboardPage.prototype.failedPasscodeCustomers = function () {
        this.navCtrl.push("CustomersPage", { failedPasscode: "FAILED PASSCODE" });
    };
    // listcustomers(status, period) {
    //     this.navCtrl.push("CustomersPage", { customerstatus: this.graphData.active, type: period, channel: this.channel })
    // }
    DashboardPage.prototype.listcustomers = function (status, period) {
        var self = this;
        if (self.oauthProvider.user_role.customers == 0) {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        }
        else {
            this.navCtrl.push("CustomersPage", { customerstatus: status, type: period, channel: this.channel });
            console.log(this.graphData.active, "this.graphData.active");
        }
    };
    DashboardPage.prototype.transactionsList = function (status, period) {
        this.navCtrl.push("TransactionPage", { status: status, type: period, channel: this.channel });
    };
    DashboardPage.prototype.callsList = function (period) {
        this.navCtrl.push("CustomerCallPage", { period: period });
    };
    DashboardPage.prototype.productsList = function () {
        var self = this;
        if (self.oauthProvider.user_role.products == 0) {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        }
        else {
            this.navCtrl.push('ProductsPage');
        }
    };
    DashboardPage.prototype.messagesList = function () {
        var self = this;
        if (self.oauthProvider.user_role.pushmessages == 0) {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                // showCloseButton: true,
                // closeButtonText: "OK",
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        }
        else {
            this.navCtrl.push('PushMessagingPage');
        }
    };
    return DashboardPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["_12" /* ViewChild */])('productDoughnutCanvas'),
    __metadata("design:type", Object)
], DashboardPage.prototype, "productDoughnutCanvas", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["_12" /* ViewChild */])('activeCustomerdoughnut'),
    __metadata("design:type", Object)
], DashboardPage.prototype, "activeCustomerdoughnut", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["_12" /* ViewChild */])('nonActiveCustomerdoughnut'),
    __metadata("design:type", Object)
], DashboardPage.prototype, "nonActiveCustomerdoughnut", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["_12" /* ViewChild */])('onboardingdoughnut'),
    __metadata("design:type", Object)
], DashboardPage.prototype, "onboardingdoughnut", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["_12" /* ViewChild */])('transactiondoughnut'),
    __metadata("design:type", Object)
], DashboardPage.prototype, "transactiondoughnut", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["_12" /* ViewChild */])('callsDoughnut'),
    __metadata("design:type", Object)
], DashboardPage.prototype, "callsDoughnut", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["_12" /* ViewChild */])('pushMessagesDoughnut'),
    __metadata("design:type", Object)
], DashboardPage.prototype, "pushMessagesDoughnut", void 0);
DashboardPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_9_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["n" /* Component */])({
        selector: 'page-dashboard',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\dashboard\dashboard.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(\'Dashboard | Consectus\')}} </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col col-3>\n\n        <ion-card class="ion-card-analytics mini-card" (click)="addCustomerAction()">\n\n          <ion-card-header text-center>\n\n            <div class="side-header">{{settingsProvider.getLabel(\'Customers\')}}</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="people" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right class="commonCount">\n\n              {{customerCount}}\n\n              <!-- <ion-icon name="arrow-dropright" class="right-icon"></ion-icon> -->\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col col-3>\n\n        <ion-card class="ion-card-analytics mini-card" (click)="addContactUsMessages()">\n\n          <ion-card-header text-center>\n\n            <div class="side-header">{{settingsProvider.getLabel(\'Contact Us Messages\')}}</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="chatboxes" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right class="commonCount">\n\n              {{contactUsMessagesCount}}\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col col-3>\n\n        <ion-card class="ion-card-analytics mini-card" (click)="addContactUsFailedMessages()">\n\n          <!--addContactUsFailedMessages-->\n\n          <ion-card-header text-center>\n\n            <div class="side-header">{{settingsProvider.getLabel(\'Contact Us Failed To Notify\')}}</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="chatboxes" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right class="commonCount">\n\n              0\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col col-3>\n\n        <ion-card class="ion-card-analytics mini-card" (click)="blockedPinCustomers() ">\n\n          <ion-card-header text-center>\n\n            <div class="side-header">{{settingsProvider.getLabel(\'Blocked Customer\')}}</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="warning" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right class="commonCount">\n\n              {{blockedCustomerCount}}\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n      <!-- <ion-col col-2>\n\n        <ion-card class="ion-card-analytics mini-card" (click)="blockedPasscodeCustomers()">\n\n          <ion-card-header text-center>\n\n            <div class="side-header">{{settingsProvider.getLabel(\'Blocked Passcode\')}}</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="warning" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right class="commonCount">\n\n              {{blockedPasscode}}\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col col-2>\n\n        <ion-card class="ion-card-analytics mini-card" (click)="failedPasscodeCustomers()">\n\n          <ion-card-header text-center>\n\n            <div class="side-header">{{settingsProvider.getLabel(\'Awaiting Passcode\')}}</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="warning" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right class="commonCount">\n\n              {{failedPasscodeCustomerCount}}\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col> -->\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-4>\n\n        <ion-card class="ion-card user-card" (click)="userAction()">\n\n          <ion-card-header text-center style>\n\n            <div class="card-header">{{settingsProvider.getLabel(\'Staff Users\') }}</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="people" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right>\n\n              <div class="count">{{userCount}}</div>\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col col-4>\n\n        <ion-card class="ion-card products-card" (click)="productAction()">\n\n          <ion-card-header text-center>\n\n            <div class="card-header"> {{settingsProvider.getLabel(\'Products\')}}</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="cart" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right>\n\n              <div class="count">{{productCount}}</div>\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n      <!-- <ion-col col-3>\n\n        <ion-card class="ion-card members-card" (click)="memberAction()">\n\n          <ion-card-header text-center>\n\n            <div class="person-add">{{settingsProvider.getLabel(\'Member Offers\')}}</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="person-add" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right>\n\n              <div class="count">{{memberCount}}</div>\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col> -->\n\n      <ion-col col-4>\n\n        <ion-card class="ion-card messages-card" (click)="messagesAction()">\n\n          <ion-card-header text-center>\n\n            <div class="card-header">{{settingsProvider.getLabel("Push Messages")}}</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="chatboxes" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right>\n\n              <div class="count">{{messageCount}}</div>\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <ion-card-header text-center>\n\n          <div class="graph-header">{{settingsProvider.getLabel(\'Platform Statistics\')}}</div>\n\n        </ion-card-header>\n\n        <ion-card class="ion-card-analytics" style="height: 87%;padding-bottom: 6%;">\n\n          <ion-row>\n\n            <ion-col class="segmentbutton">\n\n              <ion-segment [(ngModel)]="channel" color="primary" (ionChange)="segmentChanged($event)">\n\n                <!-- <ion-segment-button value="all">\n\n                  {{settingsProvider.getLabel("All")}}\n\n                </ion-segment-button> -->\n\n                <ion-segment-button value="mobileApp">\n\n                  {{settingsProvider.getLabel("Mobile App Channel")}}\n\n                </ion-segment-button>\n\n                <!-- <ion-segment-button value="onlineApp">\n\n                  {{settingsProvider.getLabel("Online App Channel")}}\n\n                </ion-segment-button>\n\n                <ion-segment-button value="telephoneConnect">\n\n                  {{settingsProvider.getLabel("Telephone Connect Channel")}}\n\n                </ion-segment-button> -->\n\n              </ion-segment>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col col-4 *ngIf="channel == \'all\' || channel==\'onlineApp\' || channel==\'mobileApp\'" text-center>\n\n              <ion-card class="anaylticsGraph">\n\n                <ion-card-header>\n\n                  {{settingsProvider.getLabel("Active Customers")}}\n\n                  <div class="headerCount" style="font-size: 20px;">\n\n                    {{graphData.active}}\n\n                  </div>\n\n                </ion-card-header>\n\n                <ion-card-content>\n\n                  <div (click)="listcustomers(\'active\',\'five\')" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Last 5 Days")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.fiveactive}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="listcustomers(\'active\',\'month\')" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Last Month")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.monthactive}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="listcustomers(\'active\',\'year\')" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Last Year")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.yearactive}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n\n\n                  <canvas #activeCustomerdoughnut></canvas>\n\n                </ion-card-content>\n\n              </ion-card>\n\n            </ion-col>\n\n\n\n            <ion-col col-4 *ngIf="channel == \'all\' || channel==\'onlineApp\' || channel==\'mobileApp\'" text-center>\n\n              <ion-card class="anaylticsGraph">\n\n                <ion-card-header>\n\n                  {{settingsProvider.getLabel("Non Active Customers")}}\n\n                  <div class="headerCount" style="font-size: 20px;">\n\n                    {{graphData.inactive}}\n\n                  </div>\n\n                </ion-card-header>\n\n                <ion-card-content>\n\n                  <div (click)="listcustomers(\'inactive\',\'five\')" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Last 3 Months")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.fiveinactive}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="listcustomers(\'inactive\',\'month\')" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Last 6 Months")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.monthinactive}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="listcustomers(\'inactive\',\'year\')" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Last Year")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.yearinactive}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <canvas #nonActiveCustomerdoughnut></canvas>\n\n                </ion-card-content>\n\n              </ion-card>\n\n            </ion-col>\n\n\n\n            <ion-col col-4 *ngIf="channel == \'all\' || channel==\'onlineApp\' || channel==\'mobileApp\'" text-center>\n\n              <ion-card class="anaylticsGraph">\n\n                <ion-card-header>\n\n                  {{settingsProvider.getLabel("Onboarding Existing Customers")}}\n\n                  <div class="headerCount" style="font-size: 20px;">\n\n                    {{graphData.onboard}}\n\n                  </div>\n\n                </ion-card-header>\n\n                <ion-card-content>\n\n                  <div (click)="listcustomers(\'onboard\',\'five\')" class="count-div">\n\n                    <span float-left>Last 5 Days</span>\n\n                    <span float-right>\n\n                      {{graphData.fiveonboard}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="listcustomers(\'onboard\',\'month\')" class="count-div">\n\n                    <span float-left>Last Month</span>\n\n                    <span float-right>\n\n                      {{graphData.monthonboard}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="listcustomers(\'onboard\',\'year\')" class="count-div">\n\n                    <span float-left>Last Year</span>\n\n                    <span float-right>\n\n                      {{graphData.yearonboard}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <canvas #onboardingdoughnut></canvas>\n\n                </ion-card-content>\n\n              </ion-card>\n\n            </ion-col>\n\n\n\n\n\n\n\n            <ion-col col-4 *ngIf="channel == \'all\' || channel == \'onlineApp\' || channel == \'mobileApp\' ||  channel==\'telephoneConnect\'"\n\n              text-center>\n\n              <ion-card class="anaylticsGraph">\n\n                <ion-card-header>\n\n                  {{settingsProvider.getLabel("Transactions")}}\n\n                  <div class="headerCount" style="font-size: 20px;">\n\n                    {{graphData.transactiontotal}}\n\n                  </div>\n\n                </ion-card-header>\n\n                <ion-card-content>\n\n                  <div (click)="transactionsList(\'\',\'five\')" class="count-div">\n\n                    <span float-right style="margin-left:2%;">\n\n                      Amount: {{graphData.transactionfivetotal| currency: \'GBP\':true:\'1.2-3\'}}\n\n                    </span>\n\n                    <div class="count-div">\n\n                      <span float-left>{{settingsProvider.getLabel("Last 5 Days")}}</span>\n\n                      <span float-right>\n\n                        Transactions : {{graphData.transactionfive }} |\n\n                      </span>\n\n                      <div style="clear:both;"></div>\n\n                    </div>\n\n                  </div>\n\n                  <div (click)="transactionsList(\'\',\'month\')" class="count-div">\n\n                    <span float-right style="margin-left:2%;">\n\n                      Amount: {{graphData.transactionmonthtotal | currency: \'GBP\':true:\'1.2-3\'}}\n\n                    </span>\n\n                    <div class="count-div">\n\n                      <span float-left>{{settingsProvider.getLabel("Last Month")}}</span>\n\n                      <span float-right>\n\n                        Transactions : {{graphData.transactionmonth}} |\n\n                      </span>\n\n                      <div style="clear:both;"></div>\n\n                    </div>\n\n                  </div>\n\n                  <div (click)="transactionsList(\'\',\'year\')" class="count-div">\n\n                    <span float-right style="margin-left:2%;">\n\n                      Amount: {{graphData.transactionyeartotal | currency: \'GBP\':true:\'1.2-3\'}}\n\n                    </span>\n\n                    <div class="count-div">\n\n                      <span float-left>{{settingsProvider.getLabel("Last Year")}}</span>\n\n                      <span float-right>\n\n                        Transactions : {{graphData.transactionyear}} |\n\n                      </span>\n\n                      <div style="clear:both;"></div>\n\n                    </div>\n\n                  </div>\n\n                  <canvas #transactiondoughnut></canvas>\n\n                </ion-card-content>\n\n              </ion-card>\n\n            </ion-col>\n\n\n\n            <!-- <ion-col col-4 *ngIf="channel == \'all\' ||  channel==\'telephoneConnect\'" text-center>\n\n              <ion-card class="anaylticsGraph">\n\n                <ion-card-header>\n\n                  {{settingsProvider.getLabel("Calls")}}\n\n                  <div class="headerCount" style="font-size: 20px;">\n\n                    {{graphData.callstotal}}\n\n                  </div>\n\n                </ion-card-header>\n\n                <ion-card-content>\n\n                  <div (click)="callsList(\'five\')" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Last 5 Days")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.callsfive}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="callsList(\'month\')" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Last Month")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.callsmonth}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="callsList(\'year\')" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Last Year")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.callsyear}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <canvas #callsDoughnut></canvas>\n\n                </ion-card-content>\n\n              </ion-card>\n\n            </ion-col> -->\n\n            <ion-col col-4 *ngIf="channel == \'all\' ||  channel==\'mobileApp\'" text-center>\n\n              <ion-card class="anaylticsGraph">\n\n                <ion-card-header>\n\n                  {{settingsProvider.getLabel("Products")}}\n\n                  <div class="headerCount" style="font-size: 20px;">\n\n                    {{graphData.productcount}}\n\n                  </div>\n\n                </ion-card-header>\n\n                <ion-card-content>\n\n                  <div (click)="productsList()" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Delivered")}}\n\n                    </span>\n\n                    <span float-right>\n\n                      {{graphData.totaldelivered}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="productsList()" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Viewed")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.totalviewed}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="productsList()" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Interested")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.totalinterested}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <canvas #productDoughnutCanvas></canvas>\n\n                </ion-card-content>\n\n              </ion-card>\n\n            </ion-col>\n\n            <ion-col col-4 *ngIf="channel == \'all\' ||  channel==\'mobileApp\'" text-center>\n\n              <ion-card class="anaylticsGraph">\n\n                <ion-card-header>\n\n                  {{settingsProvider.getLabel("Push Messages")}}\n\n                  <div class="headerCount" style="font-size: 20px;">\n\n                    {{graphData.messagecount}}\n\n                  </div>\n\n                </ion-card-header>\n\n                <ion-card-content>\n\n                  <div (click)="messagesList()" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Delivered")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.messagetotaldelivered}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="messagesList()" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Viewed")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.messagetotalviewed}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <div (click)="messagesList()" class="count-div">\n\n                    <span float-left>{{settingsProvider.getLabel("Interested")}}</span>\n\n                    <span float-right>\n\n                      {{graphData.messagetotalinterested}}\n\n                    </span>\n\n                    <div style="clear:both;"></div>\n\n                  </div>\n\n                  <canvas #pushMessagesDoughnut></canvas>\n\n                </ion-card-content>\n\n              </ion-card>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\dashboard\dashboard.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_customers_customers__["a" /* CustomersProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1__providers_dashboard_dashboard__["a" /* DashboardProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_products_products__["a" /* ProductsProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_push_messaging_push_messaging__["a" /* PushMessagingProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_member_member__["a" /* MemberProvider */],
        __WEBPACK_IMPORTED_MODULE_11__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_8__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_12__providers_users_users__["a" /* UsersProvider */],
        __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_13__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_14__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], DashboardPage);

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LoginPage = (function () {
    function LoginPage(oauthProvider, toastCtrl, languageProvider, events, navCtrl, navParams, viewCtrl, globalProvider, loadauditProvider, settingsProvider, masterpermissionProvider) {
        var _this = this;
        this.oauthProvider = oauthProvider;
        this.toastCtrl = toastCtrl;
        this.languageProvider = languageProvider;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.globalProvider = globalProvider;
        this.loadauditProvider = loadauditProvider;
        this.settingsProvider = settingsProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.username = 'demo@consectus.com';
        this.password = '123456';
        this.resultdata = null;
        this.languageString = "en";
        this.languageString1 = "en";
        this.resultdata1 = null;
        this.events.subscribe("consectus:language", function (language) {
            _this.languageString = language;
            console.log("subscribe language", _this.languageString);
        });
        console.log("masterpermission");
    }
    LoginPage.prototype.adminLogin = function () {
        var _this = this;
        var self = this;
        console.log(this.username + ' : ' + this.password);
        this.oauthProvider.adminLogin(this.username, this.password)
            .then(function (result) {
            console.log(result);
            if (result.status) {
                var message = _this.username + "has been logged in successfully";
                var auditUser = {
                    "users_id": result.data.users_id,
                    "customer_devices_id": 0,
                    "usertype": "staff",
                    "uuid": "",
                    "appname": "hub",
                    "event_type": "add",
                    "screen": "login",
                    "trail_details": message
                };
                _this.loadauditProvider.loadaudit(auditUser);
                console.log("dashboard");
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel("Welcome to Consectus Dashboard"),
                    cssClass: 'success',
                    duration: 1000,
                    position: 'bottom'
                });
                toast.present();
                self.navCtrl.setRoot("DashboardPage");
            }
            else {
                console.log(result.message);
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel(result.message),
                    cssClass: 'danger',
                    duration: 1000,
                    position: 'bottom'
                });
                toast.present();
            }
        })
            .catch(function (error) {
            console.log(JSON.stringify(error));
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel("Invalid Username or Password"),
                cssClass: 'error',
                duration: 5000,
                position: 'bottom'
            });
            toast.present();
        });
    };
    LoginPage.prototype.ionViewDidEnter = function () {
        console.log("Hello World");
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        if (!this.oauthProvider.user) {
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
        }
    };
    LoginPage.prototype.getMenuPage = function () {
        var self = this;
    };
    LoginPage.prototype.resetPassword = function () {
        var self = this;
        self.navCtrl.push("ForgotPasswordPage", { ispassword: true });
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["f" /* IonicPage */])({ name: "LoginPage" }),
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\login\login.html"*/'<ion-content>\n\n  <div class="valign-center">\n\n    <div class="login-logo" text-center>\n\n      <img src="assets/images/logo-blue(2).png" height="58">\n\n    </div>\n\n    <ion-card class="login">\n\n      <ion-card-content padding>\n\n        <ion-grid>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked style="font-size:20px !important;">{{settingsProvider.getLabel(\'Username\')}}</ion-label>\n\n              <ion-input style="font-size:15px !important;" type="text" value="" [(ngModel)]="username"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked style="font-size:20px !important;">{{settingsProvider.getLabel(\'Password\')}}</ion-label>\n\n              <ion-input style="font-size:15px !important;" type="password" [(ngModel)]="password"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <button ion-button float-right round (click)="adminLogin()">\n\n                <span class="button-text">{{settingsProvider.getLabel(\'Login\')}}</span>\n\n              </button>\n\n              <div float-left class="forgotPassword" (click)="resetPassword()">\n\n                <span class="button-text">{{settingsProvider.getLabel(\'Forgot Password?\')}}</span>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\login\login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConsectusHeaderModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__consectus_header__ = __webpack_require__(494);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConsectusHeaderModule = (function () {
    function ConsectusHeaderModule() {
    }
    return ConsectusHeaderModule;
}());
ConsectusHeaderModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_0__consectus_header__["a" /* ConsectusHeaderComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_0__consectus_header__["a" /* ConsectusHeaderComponent */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_0__consectus_header__["a" /* ConsectusHeaderComponent */]
        ]
    })
], ConsectusHeaderModule);

//# sourceMappingURL=consectus-header.module.js.map

/***/ }),

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPageModule", function() { return ProductPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_bar_chart_bar_chart_module__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_module__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_upload_button_upload_button_module__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__product__ = __webpack_require__(495);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ProductPageModule = (function () {
    function ProductPageModule() {
    }
    return ProductPageModule;
}());
ProductPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_6__product__["b" /* ProductPage */],
            __WEBPACK_IMPORTED_MODULE_6__product__["a" /* AddActionModal */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_6__product__["b" /* ProductPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_upload_button_upload_button_module__["a" /* UploadButtonComponentModule */],
            __WEBPACK_IMPORTED_MODULE_1__components_table_table_module__["a" /* TableComponentModule */],
            __WEBPACK_IMPORTED_MODULE_2__login_login_module__["LoginPageModule"],
            __WEBPACK_IMPORTED_MODULE_0__components_bar_chart_bar_chart_module__["a" /* BarChartComponentModule */]
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_6__product__["b" /* ProductPage */],
            __WEBPACK_IMPORTED_MODULE_6__product__["a" /* AddActionModal */],
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_6__product__["b" /* ProductPage */],
            __WEBPACK_IMPORTED_MODULE_6__product__["a" /* AddActionModal */]
        ]
    })
], ProductPageModule);

//# sourceMappingURL=product.module.js.map

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPageModule", function() { return ProductsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__login_login_module__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__product_product_module__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__products__ = __webpack_require__(496);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ProductsPageModule = (function () {
    function ProductsPageModule() {
    }
    return ProductsPageModule;
}());
ProductsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__products__["a" /* ProductsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_5__products__["a" /* ProductsPage */]),
            __WEBPACK_IMPORTED_MODULE_0__login_login_module__["LoginPageModule"],
            __WEBPACK_IMPORTED_MODULE_1__product_product_module__["ProductPageModule"],
            __WEBPACK_IMPORTED_MODULE_2__components_table_table_module__["a" /* TableComponentModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_5__products__["a" /* ProductsPage */]
        ]
    })
], ProductsPageModule);

//# sourceMappingURL=products.module.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerPageModule", function() { return CustomerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__customer__ = __webpack_require__(497);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CustomerPageModule = (function () {
    function CustomerPageModule() {
    }
    return CustomerPageModule;
}());
CustomerPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__customer__["a" /* CustomerPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__customer__["a" /* CustomerPage */]),
            __WEBPACK_IMPORTED_MODULE_0__components_table_table_module__["a" /* TableComponentModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_3__customer__["a" /* CustomerPage */]
        ]
    })
], CustomerPageModule);

//# sourceMappingURL=customer.module.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersPageModule", function() { return CustomersPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__customer_customer_module__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__customers__ = __webpack_require__(498);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var CustomersPageModule = (function () {
    function CustomersPageModule() {
    }
    return CustomersPageModule;
}());
CustomersPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__customers__["a" /* CustomersPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__customers__["a" /* CustomersPage */]),
            __WEBPACK_IMPORTED_MODULE_0__customer_customer_module__["CustomerPageModule"],
            __WEBPACK_IMPORTED_MODULE_1__components_table_table_module__["a" /* TableComponentModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_4__customers__["a" /* CustomersPage */],
        ]
    })
], CustomersPageModule);

//# sourceMappingURL=customers.module.js.map

/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MembersPageModule", function() { return MembersPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__members__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__member_member_module__ = __webpack_require__(332);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var MembersPageModule = (function () {
    function MembersPageModule() {
    }
    return MembersPageModule;
}());
MembersPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__members__["a" /* MembersPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__members__["a" /* MembersPage */]),
            __WEBPACK_IMPORTED_MODULE_4__member_member_module__["MemberPageModule"],
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], MembersPageModule);

//# sourceMappingURL=members.module.js.map

/***/ }),

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberPageModule", function() { return MemberPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_upload_button_bk_upload_button_module__ = __webpack_require__(500);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_bar_chart_bar_chart_module__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login_module__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__member__ = __webpack_require__(502);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var MemberPageModule = (function () {
    function MemberPageModule() {
    }
    return MemberPageModule;
}());
MemberPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_6__member__["b" /* MemberPage */],
            __WEBPACK_IMPORTED_MODULE_6__member__["a" /* MemberActionModal */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_6__member__["b" /* MemberPage */]),
            __WEBPACK_IMPORTED_MODULE_1__components_upload_button_bk_upload_button_module__["a" /* UploadButtonComponentModule */],
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */],
            __WEBPACK_IMPORTED_MODULE_5__login_login_module__["LoginPageModule"],
            __WEBPACK_IMPORTED_MODULE_2__components_bar_chart_bar_chart_module__["a" /* BarChartComponentModule */]
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_6__member__["a" /* MemberActionModal */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_6__member__["b" /* MemberPage */],
            __WEBPACK_IMPORTED_MODULE_6__member__["a" /* MemberActionModal */]
        ]
    })
], MemberPageModule);

//# sourceMappingURL=member.module.js.map

/***/ }),

/***/ 333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestPageModule", function() { return TestPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__test__ = __webpack_require__(503);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TestPageModule = (function () {
    function TestPageModule() {
    }
    return TestPageModule;
}());
TestPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__test__["a" /* TestPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__test__["a" /* TestPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__test__["a" /* TestPage */],
        ]
    })
], TestPageModule);

//# sourceMappingURL=test.module.js.map

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__upload_button_upload_button_module__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pagination_pagination_module__ = __webpack_require__(487);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__table__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var TableComponentModule = (function () {
    function TableComponentModule() {
    }
    return TableComponentModule;
}());
TableComponentModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__table__["a" /* TableComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* IonicModule */],
            __WEBPACK_IMPORTED_MODULE_1__pagination_pagination_module__["a" /* PaginationComponentModule */],
            __WEBPACK_IMPORTED_MODULE_0__upload_button_upload_button_module__["a" /* UploadButtonComponentModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_4__table__["a" /* TableComponent */]
        ]
    })
], TableComponentModule);

//# sourceMappingURL=table.module.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadauditProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoadauditProvider = (function () {
    function LoadauditProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello LoadauditProvider Provider');
    }
    LoadauditProvider.prototype.loadaudit = function (member) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.loadaudit(member)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    LoadauditProvider.prototype.getaudittrailbyuserid = function (userid) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getaudittrailbyuserid(userid)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return LoadauditProvider;
}());
LoadauditProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], LoadauditProvider);

//# sourceMappingURL=loadaudit.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserRolesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserRolesProvider = (function () {
    function UserRolesProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello UserRolesProvider Provider');
    }
    UserRolesProvider.prototype.getUserRoles = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getUserRoles()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    UserRolesProvider.prototype.addrole = function (userData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addrole(userData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    UserRolesProvider.prototype.roleSetting = function (updateUserData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.roleSetting(updateUserData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return UserRolesProvider;
}());
UserRolesProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], UserRolesProvider);

//# sourceMappingURL=user-roles.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewAccountProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewAccountProvider = (function () {
    function NewAccountProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello NewAccountProvider Provider');
    }
    NewAccountProvider.prototype.getnewaccountslist = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getnewaccountslist()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.getnewaccountmailerlist = function (product) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getnewaccountmailerlist(product)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.updatenewaccountstatus = function (customerIds, status) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updatenewaccountstatus(customerIds, status)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.restartregistrationbyaccountsidv2 = function (naccountid, state_status) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.restartregistrationbyaccountsidv2(naccountid, state_status)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.blockexistingcustomer = function (mailerBlock) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.blockexistingcustomer(mailerBlock)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.getnewaccountaudits = function (users_id) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getnewaccountaudits(users_id)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.getnewaccountdetails = function (new_accounts_id) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getnewaccountdetails(new_accounts_id)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.generateLetter = function (newaccntid) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.generateLetter(newaccntid)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.openPdfLetter = function (newaccntid) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.openPdfLetter(newaccntid)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.generateLetterForAll = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.generateLetterForAll()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.generateletterforonboard = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.generateletterforonboard()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewAccountProvider.prototype.sendLetterMailer = function (url, newaccountdata) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.sendMailerLetter(url, newaccountdata)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return NewAccountProvider;
}());
NewAccountProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], NewAccountProvider);

//# sourceMappingURL=new-account.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavingAccountRulesListProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SavingAccountRulesListProvider = (function () {
    function SavingAccountRulesListProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello SavingAccountRulesListProvider Provider');
    }
    SavingAccountRulesListProvider.prototype.getaccountrules = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getaccountrules()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SavingAccountRulesListProvider.prototype.updateaccountrules = function (SavingRulesData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateaccountrules(SavingRulesData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SavingAccountRulesListProvider.prototype.getAccountTypes = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getaccounttypes()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SavingAccountRulesListProvider.prototype.addRulesEngine = function (accountData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addRulesEngine(accountData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SavingAccountRulesListProvider.prototype.updateRulesEngine = function (accountData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateRulesEngine(accountData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SavingAccountRulesListProvider.prototype.addaccounttypes = function (SavingRulesData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addaccounttypes(SavingRulesData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SavingAccountRulesListProvider.prototype.updateaccounttypes = function (AccountTypeData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateaccounttypes(AccountTypeData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return SavingAccountRulesListProvider;
}());
SavingAccountRulesListProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], SavingAccountRulesListProvider);

//# sourceMappingURL=saving-account-rules-list.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppSettingProvider = (function () {
    function AppSettingProvider(gateway, http) {
        this.gateway = gateway;
        this.http = http;
    }
    AppSettingProvider.prototype.getAllVersion = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAllAppVersions().then(function (data) {
                resolve(data);
            });
        });
    };
    AppSettingProvider.prototype.getAppVerisonByDevice = function (device) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAppVerisonByDevice(device).then(function (data) {
                resolve(data);
            });
        });
    };
    AppSettingProvider.prototype.getAppData = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAppData().then(function (data) {
                resolve(data);
            });
        });
    };
    AppSettingProvider.prototype.getappversion = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getappversion().then(function (data) {
                resolve(data);
            });
        });
    };
    AppSettingProvider.prototype.addappversion = function (deviceData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addappversion(deviceData).then(function (data) {
                resolve(data);
            });
        });
    };
    AppSettingProvider.prototype.addAppVersion = function (deviceData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addAppVersion(deviceData).then(function (data) {
                resolve(data);
            });
        });
    };
    return AppSettingProvider;
}());
AppSettingProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__["a" /* GatewayClientProvider */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
], AppSettingProvider);

//# sourceMappingURL=app-setting.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnboardingSetupProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OnboardingSetupProvider = (function () {
    function OnboardingSetupProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello OnboardingSetupProvider Provider');
    }
    OnboardingSetupProvider.prototype.getonboardingprocess = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getonboardingprocess()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    OnboardingSetupProvider.prototype.getloginprocess = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getloginprocess()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    OnboardingSetupProvider.prototype.getregistrationfieldsv1 = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getregistrationfieldsv1()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    OnboardingSetupProvider.prototype.updateloginprocess = function (loginData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateloginprocess(loginData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    OnboardingSetupProvider.prototype.updateonboardingprocess = function (onboardingData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateonboardingprocess(onboardingData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return OnboardingSetupProvider;
}());
OnboardingSetupProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], OnboardingSetupProvider);

//# sourceMappingURL=onboarding-setup.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqCategoryProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the FaqCategoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var FaqCategoryProvider = (function () {
    function FaqCategoryProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello FaqCategoryProvider Provider');
    }
    FaqCategoryProvider.prototype.getallcategories = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getallcategories()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    FaqCategoryProvider.prototype.activateFaqCategory = function (category) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.activateFaqCategory(category)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    FaqCategoryProvider.prototype.deleteCategory = function (category) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteCategory(category)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    FaqCategoryProvider.prototype.addcategories = function (addCategoryData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addcategories(addCategoryData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    FaqCategoryProvider.prototype.editcategory = function (updatefaqData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.editcategory(updatefaqData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return FaqCategoryProvider;
}());
FaqCategoryProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], FaqCategoryProvider);

//# sourceMappingURL=faq-category.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavingAccountRulesMilestoneProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SavingAccountRulesMilestoneProvider = (function () {
    function SavingAccountRulesMilestoneProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello SavingAccountRulesMilestoneProvider Provider');
    }
    SavingAccountRulesMilestoneProvider.prototype.getgoalmilestones = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getgoalmilestones()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SavingAccountRulesMilestoneProvider.prototype.addgoalmilestone = function (milestoneData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addgoalmilestone(milestoneData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SavingAccountRulesMilestoneProvider.prototype.updategoalmilestone = function (UpdateMilestoneData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updategoalmilestone(UpdateMilestoneData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SavingAccountRulesMilestoneProvider.prototype.deleteMilestone = function (milestone) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteMilestone(milestone)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    SavingAccountRulesMilestoneProvider.prototype.activateMilestone = function (milestone) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.activateMilestone(milestone)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return SavingAccountRulesMilestoneProvider;
}());
SavingAccountRulesMilestoneProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], SavingAccountRulesMilestoneProvider);

//# sourceMappingURL=saving-account-rules-milestone.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewsProvider = (function () {
    function NewsProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello NewsProvider Provider');
    }
    NewsProvider.prototype.getNews = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getNews()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewsProvider.prototype.createNews = function (news) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.createNews(news)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewsProvider.prototype.updateNews = function (news) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateNews(news)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    NewsProvider.prototype.deleteNews = function (news) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteNews(news)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return NewsProvider;
}());
NewsProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], NewsProvider);

//# sourceMappingURL=news.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductAppProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ProductAppProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ProductAppProvider = (function () {
    function ProductAppProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello ProductAppProvider Provider');
    }
    ProductAppProvider.prototype.getnewcustomerlist = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getnewcustomerlist()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    ProductAppProvider.prototype.getproductapplicationdetails = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getproductapplicationdetails()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    ProductAppProvider.prototype.getnewaccountaudits = function (users_id) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getnewaccountaudits(users_id)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return ProductAppProvider;
}());
ProductAppProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], ProductAppProvider);

//# sourceMappingURL=product-app.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TransactionProvider = (function () {
    function TransactionProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello TransactionProvider Provider');
    }
    TransactionProvider.prototype.getyearlytransactions = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getyearlytransactions()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    TransactionProvider.prototype.updatecustomeraccounttransations = function (transactionOptions) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updatecustomeraccounttransations(transactionOptions)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return TransactionProvider;
}());
TransactionProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], TransactionProvider);

//# sourceMappingURL=transaction.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppversionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppversionProvider = (function () {
    function AppversionProvider(gateway, http) {
        this.gateway = gateway;
        this.http = http;
        console.log('Hello AppversionProvider Provider');
    }
    AppversionProvider.prototype.saveVersion = function (appVersionData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.saveVersion(appVersionData)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return AppversionProvider;
}());
AppversionProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
], AppversionProvider);

//# sourceMappingURL=appversion.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChangePasswordProvider = (function () {
    function ChangePasswordProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello ChangePasswordProvider Provider');
    }
    ChangePasswordProvider.prototype.changepassword = function (pwd) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.changepassword(pwd)
                .then(function (data) {
                resolve(data);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    return ChangePasswordProvider;
}());
ChangePasswordProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], ChangePasswordProvider);

//# sourceMappingURL=change-password.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MessagesProvider = (function () {
    function MessagesProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello MessagesProvider Provider');
    }
    MessagesProvider.prototype.getcontactusmessagebyid = function (customer_complaint_id) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getcontactusmessagebyid(customer_complaint_id)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    MessagesProvider.prototype.sendmessagetocustomer = function (customer_complaint_id) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.sendmessagetocustomer(customer_complaint_id)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    MessagesProvider.prototype.complaintresolved = function (customer_complaint_id) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.complaintresolved(customer_complaint_id)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return MessagesProvider;
}());
MessagesProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], MessagesProvider);

//# sourceMappingURL=messages.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RpaDashboardProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the RpatestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RpaDashboardProvider = (function () {
    function RpaDashboardProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
    }
    RpaDashboardProvider.prototype.getAverageRpa = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.gateway.getAverageRpa()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    RpaDashboardProvider.prototype.getfindtotalagent = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.gateway.getfindtotalagent()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    RpaDashboardProvider.prototype.pingAgent = function (RpaData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.gateway.pingAgent(RpaData)
                .then(function (data) {
                resolve(data);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    RpaDashboardProvider.prototype.SaveAgentdata = function (RpaData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.gateway.SaveAgentdata(RpaData)
                .then(function (data) {
                resolve(data);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    RpaDashboardProvider.prototype.DeleteAgent = function (RpaData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.gateway.DeleteAgent(RpaData)
                .then(function (data) {
                resolve(data);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    RpaDashboardProvider.prototype.getAgentData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.gateway.getAgentData()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    RpaDashboardProvider.prototype.AllAgentList = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.gateway.AllAgentList()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    RpaDashboardProvider.prototype.GetActionPath = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.gateway.GetActionPath()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return RpaDashboardProvider;
}());
RpaDashboardProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], RpaDashboardProvider);

//# sourceMappingURL=rpa-dashboard.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(411);


// enableProdMode();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_settings_settings_module__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_reports_reports_module__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_push_messaging_push_messaging_module__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_products_products_module__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_dashboard_dashboard_module__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login_module__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_customers_customers_module__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_members_members_module__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_test_test_module__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_network__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_consectus_header_consectus_header_module__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__app_component__ = __webpack_require__(554);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_status_bar__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_splash_screen__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_session_session__ = __webpack_require__(555);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_push_messaging_push_messaging__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__providers_database_database__ = __webpack_require__(556);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_customer_customer__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers_products_products__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_dashboard_dashboard__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_customers_customers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_utils_utils__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_member_member__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_app_setting_app_setting__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_news_news__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__providers_appversion_appversion__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__providers_saving_account_rules_list_saving_account_rules_list__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__providers_new_saving_account_milestone_new_saving_account_milestone__ = __webpack_require__(557);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__providers_saving_account_rules_milestone_saving_account_rules_milestone__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__providers_transaction_transaction__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__providers_user_roles_user_roles__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__providers_complaint_details_subject_complaint_details_subject__ = __webpack_require__(558);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__providers_users_users__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__providers_change_password_change_password__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__providers_new_account_new_account__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__providers_messages_messages__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__providers_onboarding_setup_onboarding_setup__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__providers_rpadashboard_rpa_dashboard__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__providers_faq_category_faq_category__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__providers_product_app_product_app__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















































var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_13__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_14_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */], { mode: 'ios' }, {
                links: [
                    { loadChildren: '../pages/branch/branch.module#BranchPageModule', name: 'BranchPage', segment: 'branch', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../components/change-page-size-popover/change-page-size-popover.module#ChangePageSizePopoverPageModule', name: 'ChangePageSizePopoverPage', segment: 'change-page-size-popover', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/reports/reports.module#ReportsPageModule', name: 'ReportsPage', segment: 'reports', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/push-message/push-message.module#PushMessagePageModule', name: 'PushMessagePage', segment: 'push-message', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/push-messaging/push-messaging.module#PushMessagingPageModule', name: 'PushMessagingPage', segment: 'push-messaging', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/product/product.module#ProductPageModule', name: 'ProductPage', segment: 'product', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/products/products.module#ProductsPageModule', name: 'ProductsPage', segment: 'products', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/customer/customer.module#CustomerPageModule', name: 'CustomerPage', segment: 'customer', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/customers/customers.module#CustomersPageModule', name: 'CustomersPage', segment: 'customers', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/members/members.module#MembersPageModule', name: 'MembersPage', segment: 'members', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/member/member.module#MemberPageModule', name: 'MemberPage', segment: 'member', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/test/test.module#TestPageModule', name: 'TestPage', segment: 'test', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/app-setting/app-setting.module#AppSettingPageModule', name: 'AppSettingPage', segment: 'app-setting', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/appversion/appversion.module#AppversionPageModule', name: 'AppversionPage', segment: 'appversion', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/call-detail/call-detail.module#CallDetailPageModule', name: 'CallDetailPage', segment: 'call-detail', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/change-password/change-password.module#ChangePasswordPageModule', name: 'ChangePasswordPage', segment: 'change-password', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/complaint-details/complaint-details.module#ComplaintDetailsPageModule', name: 'ComplaintDetailsPage', segment: 'complaint-details', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/complaint-details-subject/complaint-details-subject.module#ComplaintDetailsSubjectPageModule', name: 'ComplaintDetailsSubjectPage', segment: 'complaint-details-subject', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/contact-us/contact-us.module#ContactUsPageModule', name: 'ContactUsPage', segment: 'contact-us', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/contact-us-message/contact-us-message.module#ContactUsMessagePageModule', name: 'ContactUsMessagePage', segment: 'contact-us-message', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/contacts-messages/contacts-messages.module#ContactsMessagesPageModule', name: 'ContactsMessagesPage', segment: 'contacts-messages', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/customer-call/customer-call.module#CustomerCallPageModule', name: 'CustomerCallPage', segment: 'customer-call', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/faq-category/faq-category.module#FaqCategoryPageModule', name: 'FaqCategoryPage', segment: 'faq-category', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/faq-category-detail/faq-category-detail.module#FaqCategoryDetailPageModule', name: 'FaqCategoryDetailPage', segment: 'faq-category-detail', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/faqs/faqs.module#FaqsPageModule', name: 'FaqsPage', segment: 'faqs', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/forgot-password/forgot-password.module#ForgotPasswordPageModule', name: 'ForgotPasswordPage', segment: 'forgot-password', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/labels/labels.module#LabelsPageModule', name: 'LabelsPage', segment: 'labels', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/languages/languages.module#LanguagesPageModule', name: 'LanguagesPage', segment: 'languages', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/marketing-prefrences/marketing-prefrences.module#MarketingPrefrencesPageModule', name: 'MarketingPrefrencesPage', segment: 'marketing-prefrences', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/messages/messages.module#MessagesPageModule', name: 'Messages', segment: 'messages', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/new-account/new-account.module#NewAccountPageModule', name: 'NewAccountPage', segment: 'new-account', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/new-account-view/new-account-view.module#NewAccountViewPageModule', name: 'NewAccountViewPage', segment: 'new-account-view', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/new-savings-milestone/new-savings-milestone.module#NewSavingsMilestonePageModule', name: 'NewSavingsMilestonePage', segment: 'new-savings-milestone', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/news/news.module#NewsPageModule', name: 'NewsPage', segment: 'news', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/news-list/news-list.module#NewsListPageModule', name: 'NewsListPage', segment: 'news-list', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/onboarding-setup/onboarding-setup.module#OnboardingSetupPageModule', name: 'OnboardingSetupPage', segment: 'onboarding-setup', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/product-app-details/product-app-details.module#ProductAppDetailsPageModule', name: 'ProductAppDetailsPage', segment: 'product-app-details', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/product-application/product-application.module#ProductApplicationPageModule', name: 'ProductApplicationPage', segment: 'product-application', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/rpa-dashboard/rpa-dashboard.module#RpaDashboardPageModule', name: 'RpaDashboardPage', segment: 'rpa-dashboard', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/saving-account-rules/saving-account-rules.module#SavingAccountRulesPageModule', name: 'SavingAccountRulesPage', segment: 'saving-account-rules', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/saving-account-rules-milestone/saving-account-rules-milestone.module#SavingAccountRulesMilestonePageModule', name: 'SavingAccountRulesMilestonePage', segment: 'saving-account-rules-milestone', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/saving-account-type/saving-account-type.module#SavingAccountTypePageModule', name: 'SavingAccountTypePage', segment: 'saving-account-type', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/saving-accout-rules-list/saving-accout-rules-list.module#SavingAccoutRulesListPageModule', name: 'SavingAccoutRulesListPage', segment: 'saving-accout-rules-list', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/transaction/transaction.module#TransactionPageModule', name: 'TransactionPage', segment: 'transaction', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/transaction-details/transaction-details.module#TransactionDetailsPageModule', name: 'TransactionDetailsPage', segment: 'transaction-details', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/updateagent/updateagent.module#UpdateagentPageModule', name: 'UpdateagentPage', segment: 'updateagent', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/user/user.module#UserPageModule', name: 'UserPage', segment: 'user', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/user-access/user-access.module#UserAccessPageModule', name: 'UserAccessPage', segment: 'user-access', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/users/users.module#UsersPageModule', name: 'UsersPage', segment: 'users', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_10__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_8__pages_test_test_module__["TestPageModule"],
            __WEBPACK_IMPORTED_MODULE_5__pages_login_login_module__["LoginPageModule"],
            __WEBPACK_IMPORTED_MODULE_4__pages_dashboard_dashboard_module__["DashboardPageModule"],
            __WEBPACK_IMPORTED_MODULE_3__pages_products_products_module__["ProductsPageModule"],
            __WEBPACK_IMPORTED_MODULE_6__pages_customers_customers_module__["CustomersPageModule"],
            __WEBPACK_IMPORTED_MODULE_2__pages_push_messaging_push_messaging_module__["PushMessagingPageModule"],
            __WEBPACK_IMPORTED_MODULE_1__pages_reports_reports_module__["ReportsPageModule"],
            __WEBPACK_IMPORTED_MODULE_0__pages_settings_settings_module__["SettingsPageModule"],
            __WEBPACK_IMPORTED_MODULE_7__pages_members_members_module__["MembersPageModule"],
            __WEBPACK_IMPORTED_MODULE_12__components_consectus_header_consectus_header_module__["a" /* ConsectusHeaderModule */],
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_14_ionic_angular__["c" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_16__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_17__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_13__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_14_ionic_angular__["d" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_18__providers_session_session__["a" /* SessionProvider */],
            __WEBPACK_IMPORTED_MODULE_19__providers_gateway_client_gateway_client__["a" /* GatewayClientProvider */],
            __WEBPACK_IMPORTED_MODULE_20__providers_oauth_oauth__["a" /* OauthProvider */],
            __WEBPACK_IMPORTED_MODULE_21__providers_language_language__["a" /* LanguageProvider */],
            __WEBPACK_IMPORTED_MODULE_22__providers_push_messaging_push_messaging__["a" /* PushMessagingProvider */],
            __WEBPACK_IMPORTED_MODULE_23__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_24__providers_customer_customer__["a" /* CustomerProvider */],
            __WEBPACK_IMPORTED_MODULE_25__providers_products_products__["a" /* ProductsProvider */],
            __WEBPACK_IMPORTED_MODULE_26__providers_dashboard_dashboard__["a" /* DashboardProvider */],
            __WEBPACK_IMPORTED_MODULE_27__providers_customers_customers__["a" /* CustomersProvider */],
            __WEBPACK_IMPORTED_MODULE_28__providers_settings_settings__["a" /* SettingsProvider */],
            __WEBPACK_IMPORTED_MODULE_29__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_30__providers_member_member__["a" /* MemberProvider */],
            __WEBPACK_IMPORTED_MODULE_31__providers_app_setting_app_setting__["a" /* AppSettingProvider */],
            __WEBPACK_IMPORTED_MODULE_32__providers_news_news__["a" /* NewsProvider */],
            __WEBPACK_IMPORTED_MODULE_33__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_33__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_34__providers_appversion_appversion__["a" /* AppversionProvider */],
            __WEBPACK_IMPORTED_MODULE_35__providers_saving_account_rules_list_saving_account_rules_list__["a" /* SavingAccountRulesListProvider */],
            __WEBPACK_IMPORTED_MODULE_36__providers_new_saving_account_milestone_new_saving_account_milestone__["a" /* NewSavingAccountMilestoneProvider */],
            __WEBPACK_IMPORTED_MODULE_37__providers_saving_account_rules_milestone_saving_account_rules_milestone__["a" /* SavingAccountRulesMilestoneProvider */],
            __WEBPACK_IMPORTED_MODULE_38__providers_transaction_transaction__["a" /* TransactionProvider */],
            __WEBPACK_IMPORTED_MODULE_39__providers_user_roles_user_roles__["a" /* UserRolesProvider */],
            __WEBPACK_IMPORTED_MODULE_40__providers_complaint_details_subject_complaint_details_subject__["a" /* ComplaintDetailsSubjectProvider */],
            __WEBPACK_IMPORTED_MODULE_41__providers_users_users__["a" /* UsersProvider */],
            __WEBPACK_IMPORTED_MODULE_42__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
            __WEBPACK_IMPORTED_MODULE_43__providers_change_password_change_password__["a" /* ChangePasswordProvider */],
            __WEBPACK_IMPORTED_MODULE_44__providers_new_account_new_account__["a" /* NewAccountProvider */],
            __WEBPACK_IMPORTED_MODULE_45__providers_messages_messages__["a" /* MessagesProvider */],
            __WEBPACK_IMPORTED_MODULE_46__providers_onboarding_setup_onboarding_setup__["a" /* OnboardingSetupProvider */],
            __WEBPACK_IMPORTED_MODULE_47__providers_rpadashboard_rpa_dashboard__["a" /* RpaDashboardProvider */],
            __WEBPACK_IMPORTED_MODULE_48__providers_faq_category_faq_category__["a" /* FaqCategoryProvider */],
            __WEBPACK_IMPORTED_MODULE_49__providers_product_app_product_app__["a" /* ProductAppProvider */],
            __WEBPACK_IMPORTED_MODULE_50__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BranchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { LoginPage } from './../login/login';







var BranchPage = (function () {
    function BranchPage(oauthProvider, languageProvider, domSanitizer, alertCtrl, navCtrl, navParams, settingsProvider, toastCtrl, viewCtrl, masterpermissionProvider) {
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.domSanitizer = domSanitizer;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.settingsProvider = settingsProvider;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.branch = {
            name: "",
            address_line1: "",
            address_line2: "",
            address_line3: "",
            address_line4: "",
            tel: "",
            fax: "",
            email: "",
            opening_hours: [],
            latitude: "",
            longitude: "",
            postalcode: ""
        };
        this.languageString = "en";
        this.resultdata = null;
        this.updateMap = false;
        this.new = this.navParams.get('creatNew');
        console.log(this.new, 'new true');
        if (this.navParams.get('branch') == null) {
            if (this.navParams.get('duplicate')) {
                this.branch = this.navParams.get('duplicate');
                this.branch.id = new Date().getTime().toString();
            }
            else {
                this.branch = {
                    id: new Date().getTime().toString(),
                    name: '',
                    address_line1: '',
                    address_line2: '',
                    address_line3: '',
                    address_line4: '',
                    tel: '',
                    email: '',
                    fax: '',
                    opening_hours: [],
                    latitude: '',
                    longitude: '',
                    postalcode: ''
                };
            }
            this.title = "Create New Branch";
            this.new = true;
        }
        else {
            this.branch = this.navParams.get('branch');
            console.log(this.branch, "Branchewre");
            this.new = false;
            this.title = "Update Branch Details";
            this.latitude = this.branch.latitude;
            this.longitude = this.branch.longitude;
        }
        this.callback = this.navParams.get("callback");
    }
    BranchPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("appsettings")) {
            return true;
        }
        else {
            return false;
        }
    };
    BranchPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    BranchPage.prototype.ionViewDidEnter = function () {
        this.getPostalcode(this.latLang);
    };
    BranchPage.prototype.getPostalcode = function (latLang) {
        var self = this;
        this.settingsProvider.getLatLong(latLang)
            .then(function (result) {
            console.log(result, "postalcode");
        }).catch(function (error) {
        });
    };
    BranchPage.prototype.addHours = function () {
        this.branch.opening_hours.push({ days: this.days, hours: this.hours });
        this.days = "";
        this.hours = "";
        this.dirty();
    };
    BranchPage.prototype.deleteHours = function (op, index) {
        var self = this;
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel('Confirm Delete'),
            message: this.settingsProvider.getLabel('Are you sure you want to delete the hours?'),
            buttons: [
                {
                    text: this.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.settingsProvider.getLabel('Delete'),
                    handler: function (data) {
                        self.branch.opening_hours.splice(index, 1);
                        self.dirty();
                    }
                }
            ]
        });
        alert.present();
    };
    BranchPage.prototype.delete = function () {
        var self = this;
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel('Confirm Delete'),
            message: this.settingsProvider.getLabel('Are you sure you want to delete this Branch'),
            buttons: [
                {
                    text: this.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function (data) {
                        self.callback(self.branch, true)
                            .then(function () {
                            self.navCtrl.pop();
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    BranchPage.prototype.save = function () {
        var self = this;
        console.log(self.branch.name, "BranchName");
        // var reg = new RegExp("^[-+]?[0-9]{1,7}(\.[0-9]+)?$");
        if (self.branch.name == "" || self.branch.name == null) {
            this.errorAlert("Please enter name");
        }
        else if (self.branch.address_line1 == "" || self.branch.address_line1 == null) {
            this.errorAlert("Please enter address_line1");
        }
        else if (self.branch.address_line2 == "" || self.branch.address_line2 == null) {
            this.errorAlert("Please enter address_line2");
        }
        else if (self.branch.address_line3 == "" || self.branch.address_line3 == null) {
            this.errorAlert("Please enter address_line3");
        }
        else if (self.branch.tel == "" || self.branch.tel == null) {
            this.errorAlert("Please enter tel");
        }
        else if (self.branch.opening_hours == "" || self.branch.opening_hours == null) {
            this.errorAlert("Please enter opening_hours");
        }
        else if (this.branch.latitude == "" || self.branch.latitude == null) {
            this.errorAlert("Please enter latitude");
        }
        else if (this.branch.longitude == "" || self.branch.longitude == null) {
            this.errorAlert("Please enter longitude");
        }
        else if (this.branch.postalcode == "" || self.branch.postalcode == null) {
            this.errorAlert("Please enter postalcode");
        }
        else {
            if (this.new) {
                self.branch = {
                    "name": self.branch.name,
                    "address_line1": self.branch.address_line1,
                    "address_line2": self.branch.address_line2,
                    "address_line3": self.branch.address_line3,
                    "address_line4": self.branch.address_line4,
                    "tel": self.branch.tel,
                    "fax": self.branch.fax,
                    "email": self.branch.email,
                    "opening_hours": self.branch.opening_hours,
                    "latitude": self.branch.latitude,
                    "longitude": self.branch.longitude,
                    "createdby": 2,
                    "isactive": 1,
                    "postalcode": self.branch.postalcode,
                };
                console.log(self.branch, "branchData");
            }
            else {
                var self_1 = this;
                var alert_1 = this.alertCtrl.create({
                    title: this.settingsProvider.getLabel("Confirm Save"),
                    message: this.settingsProvider.getLabel("Are you sure you want to save this branch?"),
                    buttons: [
                        {
                            text: this.settingsProvider.getLabel("Cancel"),
                            role: this.settingsProvider.getLabel("OK"),
                            handler: function (data) {
                                self_1.settingsProvider.updateBranch(self_1.branch).then(function (result) {
                                    console.log("result", result);
                                });
                            }
                        },
                        {
                            text: this.settingsProvider.getLabel("Save"),
                            handler: function (data) {
                                self_1.callback(self_1.branch)
                                    .then(function () {
                                    self_1.navCtrl.pop();
                                });
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        }
    };
    BranchPage.prototype.dirty = function () {
        this.dirtyFlag = true;
    };
    BranchPage.prototype.dirtyMap = function () {
        this.dirtyFlag = true;
        this.updateMap = true;
    };
    BranchPage.prototype.getGoogleEmbed = function () {
        var googleApiKey = 'AIzaSyAnZISmOigAIooC3ej3BLGVizMPsFcSE1I';
        return "https://www.google.com/maps/embed/v1/place?q=" + this.branch.latitude + "," + this.branch.longitude + "&key=" + googleApiKey;
    };
    BranchPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    return BranchPage;
}());
BranchPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* IonicPage */])({ name: "BranchPage" }),
    Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["n" /* Component */])({
        selector: 'page-branch',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\branch\branch.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons start>\n\n    </ion-buttons>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid class="table-cell" *ngIf="branch">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="save()" *ngIf="dirtyFlag">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Branch Name\')}}</ion-label>\n\n                <ion-input type="text" [(ngModel)]="branch.name" placeholder="{{settingsProvider.getLabel(\'Branch Name\')}}" (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Address Line 1\')}}</ion-label>\n\n                <ion-input type="text" [(ngModel)]="branch.address_line1" placeholder="{{settingsProvider.getLabel(\'Address Line 1\')}}" (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Address Line 2\')}}</ion-label>\n\n                <ion-input type="text" [(ngModel)]="branch.address_line2" placeholder="{{settingsProvider.getLabel(\'Address Line 2\')}}" (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Address Line 3\')}}</ion-label>\n\n                <ion-input type="text" [(ngModel)]="branch.address_line3" placeholder="{{settingsProvider.getLabel(\'Address Line 3\')}}" (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Address Line 4\')}}</ion-label>\n\n                <ion-input type="text" [(ngModel)]="branch.address_line4" placeholder="{{settingsProvider.getLabel(\'Address Line 4\')}}" (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Branch Telephone\')}}</ion-label>\n\n                <ion-input type="number" [(ngModel)]="branch.tel" placeholder="{{settingsProvider.getLabel(\'Branch Telephone\')}}" (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Branch Fax\')}}</ion-label>\n\n                <ion-input type="tel" [(ngModel)]="branch.fax" placeholder="{{settingsProvider.getLabel(\'Branch Fax\')}}" (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Branch Email\')}}</ion-label>\n\n                <ion-input type="email" [(ngModel)]="branch.email" placeholder="{{settingsProvider.getLabel(\'Branch Email\')}}" (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Postal Code\')}}</ion-label>\n\n                <ion-input type="tel" [(ngModel)]="branch.postalcode" placeholder="{{settingsProvider.getLabel(\'Postal Code\')}}" (input)="dirty()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col padding>\n\n              <ion-grid class="table-grid border radius">\n\n                <ion-row class="header">\n\n                  <ion-col>{{settingsProvider.getLabel("Opening Hours")}}</ion-col>\n\n                </ion-row>\n\n                <ion-row *ngFor="let op of branch.opening_hours; let i = index;">\n\n                  <ion-col col>\n\n                    {{op.days}}\n\n                  </ion-col>\n\n                  <ion-col>\n\n                    {{op.hours}}\n\n                  </ion-col>\n\n                  <ion-col>\n\n                    <button ion-button color="danger" clear icon-only (click)="deleteHours(op, index)">\n\n                      <ion-icon name="trash"></ion-icon>\n\n                    </button>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-input type="text" [(ngModel)]="days" placeholder="{{settingsProvider.getLabel(\'Enter Day or Days\')}}"></ion-input>\n\n                  </ion-col>\n\n                  <ion-col>\n\n                    <ion-input type="text" [(ngModel)]="hours" placeholder="{{settingsProvider.getLabel(\'Enter Hours\')}}"></ion-input>\n\n                  </ion-col>\n\n                  <ion-col>\n\n                    <button ion-button small (click)="addHours()">{{settingsProvider.getLabel("Add Days & Hours")}}</button>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid>\n\n                <ion-row align-items-center>\n\n                  <ion-col>\n\n                    <ion-item>\n\n                      <ion-label stacked>{{settingsProvider.getLabel(\'Latitude\')}}</ion-label>\n\n                      <ion-input type="number" [(ngModel)]="branch.latitude" placeholder="{{settingsProvider.getLabel(\'Latitude\')}}" (input)="dirtyMap()"></ion-input>\n\n                    </ion-item>\n\n                    <ion-item>\n\n                      <ion-label stacked>{{settingsProvider.getLabel(\'Longitude\')}}</ion-label>\n\n                      <ion-input type="number" [(ngModel)]="branch.longitude" placeholder="{{settingsProvider.getLabel(\'Longitude\')}}" (input)="dirtyMap()"></ion-input>\n\n                    </ion-item>\n\n                  </ion-col>\n\n                  <ion-col text-center>\n\n                    <small *ngIf="!updateMap">\n\n                      <a [href]="domSanitizer.bypassSecurityTrustResourceUrl(getGoogleEmbed())" style="color:#0000FF;text-align:left" target="_blank">{{settingsProvider.getLabel(\'See Bigger Map\')}}</a>\n\n                    </small>\n\n                    <iframe *ngIf="!updateMap && branch.latitude != null && branch.longitude != null && branch.latitude.toString().length>0 && branch.longitude.toString().length>0"\n\n                      width="100%" height="200px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" [src]="domSanitizer.bypassSecurityTrustResourceUrl(getGoogleEmbed())"></iframe>\n\n                  </ion-col>\n\n                </ion-row>\n\n\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\branch\branch.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], BranchPage);

//# sourceMappingURL=branch.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomersProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CustomersProvider = (function () {
    function CustomersProvider(gateway, http) {
        this.gateway = gateway;
        this.http = http;
    }
    CustomersProvider.prototype.getDashbaordData = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getCustomers()
                .then(function (data) {
                var res = {
                    NEW: 0,
                    OTP: 0,
                    BLOCKED: 0,
                    SETPIN: 0,
                    SETUP: 0
                };
                for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                    var item = data_1[_i];
                    res[item.status] += 1;
                }
                resolve(res);
            });
        });
    };
    CustomersProvider.prototype.getCustomers = function (filter) {
        if (filter === void 0) { filter = null; }
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAllCustomers()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomersProvider.prototype.getCustomersById = function (filter) {
        if (filter === void 0) { filter = null; }
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getCustomersById(filter)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomersProvider.prototype.blockCustomer = function (item) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.blockCustomer(item)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomersProvider.prototype.getBlockedCustomers = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAllCustomers()
                .then(function (result) {
                resolve(result);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // getPasscodeCustomers(item) {
    //   let self = this;
    //   return new Promise(function (resolve, reject) {
    //     self.gateway.getAllCustomers()
    //       .then((result: any) => {
    //         let returnval = result.data.filter((obj) => obj.status == 'OTPAWAIT')
    //         console.log(returnval);
    //         resolve(returnval);
    //       }).catch((err) => {
    //         reject(err);
    //       })
    //   })
    // }
    // getfailedPasscodeCustomers(item) {
    //   let self = this;
    //   return new Promise(function (resolve, reject) {
    //     self.gateway.getAllCustomers()
    //       .then((result: any) => {
    //         let returnval = result.data.filter((obj) => obj.status == 'OTPFAILED')
    //         console.log(returnval);
    //         resolve(returnval);
    //       }).catch((err) => {
    //         reject(err);
    //       })
    //   })
    // }
    CustomersProvider.prototype.getAllCustomers = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAllCustomers().then(function (result) {
                resolve(result);
            });
        });
    };
    CustomersProvider.prototype.getregistrationfields = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getregistrationfields().then(function (result) {
                resolve(result);
            });
        });
    };
    CustomersProvider.prototype.getAllDashboardFilterCustomers = function (status, type, channel) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getAllCustomers().then(function (result) {
                resolve(result);
            });
        });
    };
    CustomersProvider.prototype.unBlockCustomer = function (item) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.unBlockCustomer(item)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomersProvider.prototype.registerCustomer = function (item) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.registerCustomer(item)
                .then(function () {
                resolve();
            });
        });
    };
    CustomersProvider.prototype.resendPasscode = function (item) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.resendPasscode(item)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomersProvider.prototype.getCustomerCount = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getCustomerCount()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomersProvider.prototype.getComplaints = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getComplaints()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomersProvider.prototype.complaintResolved = function (resolvedcomplaint) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.complaintResolved(resolvedcomplaint)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    CustomersProvider.prototype.getCalls = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getCalls()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    return CustomersProvider;
}());
CustomersProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__["a" /* GatewayClientProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__["a" /* GatewayClientProvider */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object])
], CustomersProvider);

var _a, _b;
//# sourceMappingURL=customers.js.map

/***/ }),

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePageSizePopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_utils_utils__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ChangePageSizePopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChangePageSizePopoverPage = (function () {
    function ChangePageSizePopoverPage(viewCtrl, navParams, utilsProvider) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.utilsProvider = utilsProvider;
        if (navParams.get("pageSize"))
            this.pageSize = new Number(navParams.get('pageSize')).valueOf();
        else
            this.pageSize = 10;
    }
    ChangePageSizePopoverPage.prototype.close = function () {
        this.utilsProvider.show("Please wait");
        this.viewCtrl.dismiss({ pageSize: new Number(this.pageSize).valueOf() });
    };
    return ChangePageSizePopoverPage;
}());
ChangePageSizePopoverPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])({ name: "ChangePageSizePopoverPage" }),
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-change-page-size-popover',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\change-page-size-popover\change-page-size-popover.html"*/'<ion-list radio-group [(ngModel)]="pageSize">\n\n  <ion-list-header>\n\n    <b>Rows Per Page</b>\n\n  </ion-list-header>\n\n  <ion-item>\n\n    <ion-label>10</ion-label>\n\n    <ion-radio value="10" (ionSelect)="close()" ></ion-radio>\n\n  </ion-item>\n\n  <ion-item>\n\n    <ion-label>20</ion-label>\n\n    <ion-radio value="20" (ionSelect)="close()" ></ion-radio>\n\n  </ion-item>\n\n  <ion-item>\n\n    <ion-label>50</ion-label>\n\n    <ion-radio value="50" (ionSelect)="close()" ></ion-radio>\n\n  </ion-item>\n\n  <ion-item>\n\n    <ion-label>100</ion-label>\n\n    <ion-radio value="100" (ionSelect)="close()" ></ion-radio>\n\n  </ion-item>\n\n  <ion-item>\n\n    <ion-label>500</ion-label>\n\n    <ion-radio value="500" (ionSelect)="close()" ></ion-radio>\n\n  </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\change-page-size-popover\change-page-size-popover.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_0__providers_utils_utils__["a" /* UtilsProvider */]])
], ChangePageSizePopoverPage);

//# sourceMappingURL=change-page-size-popover.js.map

/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReportsPage = (function () {
    function ReportsPage(languageProvider, navCtrl, navParams) {
        this.languageProvider = languageProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ReportsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReportsPage');
    };
    return ReportsPage;
}());
ReportsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-reports',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\reports\reports.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title   padding-vertical>{{settingsProvider.getLabel(\'Reports\')}} | Consectus</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\reports\reports.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */]])
], ReportsPage);

//# sourceMappingURL=reports.js.map

/***/ }),

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadButtonComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the UploadButtonComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var UploadButtonComponent = (function () {
    function UploadButtonComponent(renderer) {
        this.renderer = renderer;
        this.options = {
            title: "Upload",
            type: "csv",
            btnType: 'clear',
            btnSize: 'small',
            btnColor: 'primary',
            btnCallback: null // callback fundtion to call with the data
        };
    }
    UploadButtonComponent.prototype.callback = function (event) {
        // trigger click event of hidden input
        var clickEvent = new MouseEvent("click", { bubbles: true });
        this.renderer.invokeElementMethod(this.nativeInputBtn.nativeElement, "dispatchEvent", [clickEvent]);
    };
    UploadButtonComponent.prototype.ionViewDidLoad = function () {
    };
    /**
     * Callback which is executed after files from native popup are selected.
     * @param  {Event}    event change event containing selected files
     */
    UploadButtonComponent.prototype.filesAdded = function (event) {
        var self = this;
        var files = this.nativeInputBtn.nativeElement.files;
        if (files.length > 0) {
            var f = files[0];
            if (this.options.type == 'image') {
                if (f.type.match('image.*')) {
                    var imagereader = new FileReader();
                    // Closure to capture the file information.
                    imagereader.onload = (function (theFile) {
                        return function (e) {
                            // Render thumbnail.
                            console.log('image content: ' + e.target.result.length);
                            self.nativeInputBtn.nativeElement.value = '';
                            self.options.btnCallback(e);
                        };
                    })(f);
                    // Read in the image file as a data URL.
                    imagereader.readAsDataURL(f);
                }
                else {
                    self.options.btnCallback(null);
                }
            }
            else if (this.options.type == 'csv') {
                var filereader = new FileReader();
                // Closure to capture the file information.
                filereader.onload = (function (theFile) {
                    return function (e) {
                        // Render thumbnail.
                        console.log('csv content: ' + e.target.result.length);
                        var index = e.target.result.indexOf('base64,');
                        if (index > 0) {
                            self.nativeInputBtn.nativeElement.value = '';
                            self.options.btnCallback(atob(e.target.result.substr(index + 7)));
                        }
                        else {
                            self.nativeInputBtn.nativeElement.value = '';
                            self.options.btnCallback(e.target.result);
                        }
                    };
                })(f);
                // Read in the image file as a data URL.
                filereader.readAsDataURL(f);
            }
            else if (this.options.type == 'json') {
                var filereader_1 = new FileReader();
                // Closure to capture the file information.
                filereader_1.onload = (function (theFile) {
                    return function (e) {
                        // Render thumbnail.
                        console.log('json content: ' + e.target.result.length);
                        var index = e.target.result.indexOf('base64,');
                        if (index > 0) {
                            self.nativeInputBtn.nativeElement.value = '';
                            self.options.btnCallback(atob(e.target.result.substr(index + 7)));
                        }
                        else {
                            self.nativeInputBtn.nativeElement.value = '';
                            self.options.btnCallback(e.target.result);
                        }
                    };
                })(f);
                // Read in the image file as a data URL.
                filereader_1.readAsDataURL(f);
            }
            else {
                var reader = new FileReader();
                // Closure to capture the file information.
                reader.onload = (function (theFile) {
                    return function (e) {
                        // Render thumbnail.
                        console.log('file content: ' + e.target.result.length);
                        self.nativeInputBtn.nativeElement.value = '';
                        self.options.btnCallback(atob(e.target.result));
                    };
                })(f);
                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }
        }
    };
    return UploadButtonComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewChild */])("nativefilebutton"),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
], UploadButtonComponent.prototype, "nativeInputBtn", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], UploadButtonComponent.prototype, "options", void 0);
UploadButtonComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'upload-button',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\upload-button\upload-button.html"*/'<button ion-button round [color]="options.color" [small]="options.btnSize === \'small\'" [large]="options.btnSize === \'large\'" [clear]="options.btnType === \'clear\'" [round]="options.btnType === \'round\'" (click)="callback($event)" clear>\n\n  <ion-icon name="{{name}}"></ion-icon>\n\n  {{options.title}}\n\n</button>\n\n<input type="file" (change)="filesAdded($event)" style="display: none" #nativefilebutton />'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\upload-button\upload-button.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Z" /* Renderer */]])
], UploadButtonComponent);

//# sourceMappingURL=upload-button.js.map

/***/ }),

/***/ 438:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BarChartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the BarChartComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var BarChartComponent = (function () {
    function BarChartComponent(languageProvider) {
        this.clickAction = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["w" /* EventEmitter */]();
    }
    BarChartComponent.prototype.ngOnInit = function () {
        // let self = this;
        // this.barChart = new Chart(this.barChartCanvas.nativeElement, {
        //   type: 'bar',
        //   data: self.data,
        //   options: self.options
        // });
    };
    BarChartComponent.prototype.ngOnChanges = function () {
        var self = this;
        if (this.options) {
            self.options["onClick"] = function (event) {
                var activeElement = self.barChart.getElementAtEvent(event);
                if (activeElement.length > 0) {
                    console.log("now calling clickAction method: " + activeElement[0]._model.label);
                    self.clickAction.emit(activeElement[0]._model.label);
                }
            };
            self.options.legend = {
                display: false
            };
        }
        if (this.width) {
            this.barChartCanvas.nativeElement.getContext("2d").canvas.width = this.width;
            console.log("settings width: " + this.width);
        }
        if (this.height) {
            this.barChartCanvas.nativeElement.getContext("2d").canvas.height = this.height;
            console.log("settings height: " + this.height);
        }
        this.barChart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barChartCanvas.nativeElement, {
            type: 'bar',
            data: self.data,
            options: self.options
        });
    };
    return BarChartComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", String)
], BarChartComponent.prototype, "title", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], BarChartComponent.prototype, "data", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], BarChartComponent.prototype, "options", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], BarChartComponent.prototype, "clickAction", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_12" /* ViewChild */])('barchartcanvas'),
    __metadata("design:type", Object)
], BarChartComponent.prototype, "barChartCanvas", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], BarChartComponent.prototype, "width", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], BarChartComponent.prototype, "height", void 0);
BarChartComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'bar-chart',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\bar-chart\bar-chart.html"*/'\n\n<ion-card>\n\n  <ion-card-header>\n\n    {{title}}\n\n  </ion-card-header>\n\n  <ion-card-content>\n\n    <canvas #barchartcanvas></canvas>\n\n  </ion-card-content>\n\n</ion-card>\n\n'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\bar-chart\bar-chart.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_language_language__["a" /* LanguageProvider */]])
], BarChartComponent);

//# sourceMappingURL=bar-chart.js.map

/***/ }),

/***/ 467:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 200,
	"./af.js": 200,
	"./ar": 201,
	"./ar-dz": 202,
	"./ar-dz.js": 202,
	"./ar-kw": 203,
	"./ar-kw.js": 203,
	"./ar-ly": 204,
	"./ar-ly.js": 204,
	"./ar-ma": 205,
	"./ar-ma.js": 205,
	"./ar-sa": 206,
	"./ar-sa.js": 206,
	"./ar-tn": 207,
	"./ar-tn.js": 207,
	"./ar.js": 201,
	"./az": 208,
	"./az.js": 208,
	"./be": 209,
	"./be.js": 209,
	"./bg": 210,
	"./bg.js": 210,
	"./bm": 211,
	"./bm.js": 211,
	"./bn": 212,
	"./bn.js": 212,
	"./bo": 213,
	"./bo.js": 213,
	"./br": 214,
	"./br.js": 214,
	"./bs": 215,
	"./bs.js": 215,
	"./ca": 216,
	"./ca.js": 216,
	"./cs": 217,
	"./cs.js": 217,
	"./cv": 218,
	"./cv.js": 218,
	"./cy": 219,
	"./cy.js": 219,
	"./da": 220,
	"./da.js": 220,
	"./de": 221,
	"./de-at": 222,
	"./de-at.js": 222,
	"./de-ch": 223,
	"./de-ch.js": 223,
	"./de.js": 221,
	"./dv": 224,
	"./dv.js": 224,
	"./el": 225,
	"./el.js": 225,
	"./en-au": 226,
	"./en-au.js": 226,
	"./en-ca": 227,
	"./en-ca.js": 227,
	"./en-gb": 228,
	"./en-gb.js": 228,
	"./en-ie": 229,
	"./en-ie.js": 229,
	"./en-il": 230,
	"./en-il.js": 230,
	"./en-nz": 231,
	"./en-nz.js": 231,
	"./eo": 232,
	"./eo.js": 232,
	"./es": 233,
	"./es-do": 234,
	"./es-do.js": 234,
	"./es-us": 235,
	"./es-us.js": 235,
	"./es.js": 233,
	"./et": 236,
	"./et.js": 236,
	"./eu": 237,
	"./eu.js": 237,
	"./fa": 238,
	"./fa.js": 238,
	"./fi": 239,
	"./fi.js": 239,
	"./fo": 240,
	"./fo.js": 240,
	"./fr": 241,
	"./fr-ca": 242,
	"./fr-ca.js": 242,
	"./fr-ch": 243,
	"./fr-ch.js": 243,
	"./fr.js": 241,
	"./fy": 244,
	"./fy.js": 244,
	"./gd": 245,
	"./gd.js": 245,
	"./gl": 246,
	"./gl.js": 246,
	"./gom-latn": 247,
	"./gom-latn.js": 247,
	"./gu": 248,
	"./gu.js": 248,
	"./he": 249,
	"./he.js": 249,
	"./hi": 250,
	"./hi.js": 250,
	"./hr": 251,
	"./hr.js": 251,
	"./hu": 252,
	"./hu.js": 252,
	"./hy-am": 253,
	"./hy-am.js": 253,
	"./id": 254,
	"./id.js": 254,
	"./is": 255,
	"./is.js": 255,
	"./it": 256,
	"./it.js": 256,
	"./ja": 257,
	"./ja.js": 257,
	"./jv": 258,
	"./jv.js": 258,
	"./ka": 259,
	"./ka.js": 259,
	"./kk": 260,
	"./kk.js": 260,
	"./km": 261,
	"./km.js": 261,
	"./kn": 262,
	"./kn.js": 262,
	"./ko": 263,
	"./ko.js": 263,
	"./ky": 264,
	"./ky.js": 264,
	"./lb": 265,
	"./lb.js": 265,
	"./lo": 266,
	"./lo.js": 266,
	"./lt": 267,
	"./lt.js": 267,
	"./lv": 268,
	"./lv.js": 268,
	"./me": 269,
	"./me.js": 269,
	"./mi": 270,
	"./mi.js": 270,
	"./mk": 271,
	"./mk.js": 271,
	"./ml": 272,
	"./ml.js": 272,
	"./mn": 273,
	"./mn.js": 273,
	"./mr": 274,
	"./mr.js": 274,
	"./ms": 275,
	"./ms-my": 276,
	"./ms-my.js": 276,
	"./ms.js": 275,
	"./mt": 277,
	"./mt.js": 277,
	"./my": 278,
	"./my.js": 278,
	"./nb": 279,
	"./nb.js": 279,
	"./ne": 280,
	"./ne.js": 280,
	"./nl": 281,
	"./nl-be": 282,
	"./nl-be.js": 282,
	"./nl.js": 281,
	"./nn": 283,
	"./nn.js": 283,
	"./pa-in": 284,
	"./pa-in.js": 284,
	"./pl": 285,
	"./pl.js": 285,
	"./pt": 286,
	"./pt-br": 287,
	"./pt-br.js": 287,
	"./pt.js": 286,
	"./ro": 288,
	"./ro.js": 288,
	"./ru": 289,
	"./ru.js": 289,
	"./sd": 290,
	"./sd.js": 290,
	"./se": 291,
	"./se.js": 291,
	"./si": 292,
	"./si.js": 292,
	"./sk": 293,
	"./sk.js": 293,
	"./sl": 294,
	"./sl.js": 294,
	"./sq": 295,
	"./sq.js": 295,
	"./sr": 296,
	"./sr-cyrl": 297,
	"./sr-cyrl.js": 297,
	"./sr.js": 296,
	"./ss": 298,
	"./ss.js": 298,
	"./sv": 299,
	"./sv.js": 299,
	"./sw": 300,
	"./sw.js": 300,
	"./ta": 301,
	"./ta.js": 301,
	"./te": 302,
	"./te.js": 302,
	"./tet": 303,
	"./tet.js": 303,
	"./tg": 304,
	"./tg.js": 304,
	"./th": 305,
	"./th.js": 305,
	"./tl-ph": 306,
	"./tl-ph.js": 306,
	"./tlh": 307,
	"./tlh.js": 307,
	"./tr": 308,
	"./tr.js": 308,
	"./tzl": 309,
	"./tzl.js": 309,
	"./tzm": 310,
	"./tzm-latn": 311,
	"./tzm-latn.js": 311,
	"./tzm.js": 310,
	"./ug-cn": 312,
	"./ug-cn.js": 312,
	"./uk": 313,
	"./uk.js": 313,
	"./ur": 314,
	"./ur.js": 314,
	"./uz": 315,
	"./uz-latn": 316,
	"./uz-latn.js": 316,
	"./uz.js": 315,
	"./vi": 317,
	"./vi.js": 317,
	"./x-pseudo": 318,
	"./x-pseudo.js": 318,
	"./yo": 319,
	"./yo.js": 319,
	"./zh-cn": 320,
	"./zh-cn.js": 320,
	"./zh-hk": 321,
	"./zh-hk.js": 321,
	"./zh-tw": 322,
	"./zh-tw.js": 322
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 467;

/***/ }),

/***/ 486:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return PushMessagePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddActionModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AddBulletModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_products_products__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_push_messaging_push_messaging__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_customers_customers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var PushMessagePage = (function () {
    function PushMessagePage(languageProvider, oauthProvider, customersProvider, messagesProvider, pushMessagingProvider, productsProvider, domSanitizer, alertCtrl, modalCtrl, toastCtrl, viewCtrl, settingsProvider, navCtrl, masterpermissionProvider, navParams) {
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.customersProvider = customersProvider;
        this.messagesProvider = messagesProvider;
        this.pushMessagingProvider = pushMessagingProvider;
        this.productsProvider = productsProvider;
        this.domSanitizer = domSanitizer;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.settingsProvider = settingsProvider;
        this.navCtrl = navCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.navParams = navParams;
        this.deviceType = 'ios';
        this.new = false;
        this.resultdata = null;
        this.languageString = "";
        this.targetColumns = {
            "customerId": { title: "CustomerId", search: true },
            "status": { title: "Status" },
        };
        this.all_customers = false;
        this.todayDate = new Date();
        this.canUpdate = false;
        this.isdelivered = true;
        this.products = [];
        this.targetedCustomers = {
            data: [],
            filteredData: [],
            columns: {
                "customers_id": { title: "Consectus ID", search: true },
                "customername": { title: "Customer Name", search: true },
                "memberid": { title: "Member ID", search: true },
                "isdelivered": { title: "Status", format: "function", fn: this.pushMessageStatus.bind(this), search: true }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.showCustomer.bind(this),
            uploadOptions: {},
        };
        this.isUpdate = false;
        var self = this;
        this.callback = navParams.get('callback');
        this.customersProvider.getCustomerCount()
            .then(function (count) {
            self.customerCount = count;
        });
        this.productsProvider.getProducts()
            .then(function (data) {
            self.products = data;
        });
        console.log(navParams.get('messagedata'), " ");
        if (!navParams.get('messagedata')) {
            self.message = {
                id: new Date().getTime().toString(),
                "title": null,
                "body": null,
                "actions": [],
                "bullets": null,
                "css": "",
                "type": "system",
                "date": this.todayDate.toISOString(),
                "target": null,
                "total": this.customerCount,
                "delivered": 0,
                "viewed": 0,
                "interested": 0,
                "status": "new",
                "productId": null,
                "messagebutton": []
            };
            self.title = "Create New Push Message";
            self.new = true;
        }
        else {
            this.message = JSON.parse(JSON.stringify(navParams.get('messagedata')));
            console.log(this.message.pushmessages_id, "pushmessages_id");
            if (this.message.status == "new") {
                this.title = "Edit Push Message";
                this.canUpdate = true;
            }
            else {
                this.title = "View Push Message (Read ONLY)";
            }
            if (!this.message.actions)
                this.message['actions'] = [];
            if (!this.message.bullets)
                this.message['bullets'] = [];
            if (!this.message.productId)
                this.message['productId'] = null;
            this.isUpdate = true;
        }
    }
    PushMessagePage.prototype.showCustomer = function () {
    };
    PushMessagePage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("pushmessages")) {
            return true;
        }
        else {
            return false;
        }
    };
    PushMessagePage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        else {
            this.updatePreview();
            // let self = this;
        }
        this.customersData = {
            labels: ["Total", "Delivered", "Viewed", "Interested"],
            datasets: [{
                    label: '',
                    data: [this.message.total, this.message.delivered, this.message.viewed, this.message.interested],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                    ],
                    borderWidth: 1
                }]
        };
        this.customersOptions = {
            legend: {
                display: false
            },
        };
        console.log(this.message.pushmessages_id, "Messagepushmessages_id");
        this.getcustomerbymessagesid();
    };
    PushMessagePage.prototype.getcustomerbymessagesid = function () {
        var _this = this;
        if (this.message.pushmessages_id) {
            var self_1 = this;
            var pushMessaging = {
                "pushmessages_id": this.message.pushmessages_id
            };
            this.pushMessagingProvider.getcustomerbymessagesid(pushMessaging)
                .then(function (result) {
                self_1.targetedCustomers.data = result.data;
                console.log(self_1.targetedCustomers.data.length, "message length");
                if (self_1.targetedCustomers.data.length == 0) {
                    _this.isdelivered = true;
                }
                else {
                    _this.isdelivered = false;
                }
                self_1.targetedCustomers = Object.assign({}, self_1.targetedCustomers);
            });
        }
    };
    PushMessagePage.prototype.pushMessageStatus = function (data) {
        var str = "";
        if (data == "Yes") {
            str = "Delivered";
        }
        else {
            str = "Not Delivered";
        }
        return str;
    };
    PushMessagePage.prototype.dirty = function () {
        this.dirtyFlag = true;
        this.updatePreview();
    };
    PushMessagePage.prototype.updatePreview = function () {
        var _this = this;
        this.settingsProvider.getBankLookAndFeel().then(function (lookandfeel) {
            var color = lookandfeel.primary_color;
            var title = "";
            if (_this.message.title) {
                title = "<h1>" + _this.message.title + "</h1>";
            }
            var body = "";
            if (_this.message.message) {
                body = "<p>" + _this.message.message + "</p>";
            }
            var buttons = "";
            if (_this.message.messagebutton !== null) {
                for (var _i = 0, _a = _this.message.messagebutton; _i < _a.length; _i++) {
                    var action = _a[_i];
                    buttons += "\n            <button ion-button small round>" + action.title + "</button>\n          ";
                }
            }
            var ionitem = "";
            if (_this.message.bullets !== null) {
                for (var _b = 0, _c = _this.message.bullets; _b < _c.length; _b++) {
                    var bullet = _c[_b];
                    ionitem += "\n            <li>" + bullet.bulletline + "</li>\n          ";
                }
            }
            switch (_this.deviceType) {
                case 'ios':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n          <html>\n            <head>\n              <style>\n                @font-face {\n                  font-family: 'Ionicons';\n                  src: url(assets/fonts/ionicons.woff2);\n                }\n                body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n                .nav-bar{position:fixed; height: 60px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n                .logo{height: 60%; width: auto;}\n                .content{position:fixed;height: 438px; overflow: scroll; margin-top: 60px; min-width: 300px;}\n                .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n                .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n                .tab:not(:last-child){border-right: 1px solid #fff}\n                .icon {\n                  margin-top: 5px;\n                  width: inherit;\n                  font-size: 1.4em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                  height: 30px;\n                }\n                .tab-title {\n                  position: absolute;\n                  bottom: 5px;\n                  width: inherit;\n                  font-size: 0.7em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                }\n                div.icon-cash:before{ content: \"\\f143\"; }\n                div.icon-pin:before{ content: \"\\f1e4\";}\n                div.icon-pricetags:before{ content: \"\\f48e\"; }\n                div.icon-text:before{ content: \"\\f24f\"; }\n                div.icon-settings::before{ content: \"\\f20d\"; }\n                .padding{ padding: 10px; }\n                .card {border: 1px solid #dedede; overflow: hidden; }\n                .list{\n                  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='%23c8c7cc'/></svg>\");\n                  padding-right: 32px;\n                  background-position: right 14px center;\n                  background-repeat: no-repeat;\n                  background-size: 14px 14px;\n                  padding-left: 10px;\n                  padding: 5px;\n                  border: 1px solid #efefee;\n                  margin-bottom: 20px\n                }\n                h1, h2, p{color: " + color + " }\n                div.card h1{font-size: 1.5em; font-weight: bold; }\n                h2{font-size: 1.2em; font-weight: bold; }\n                div.list h1{font-size: 1.1em; font-weight: bold;}\n                button{\n                  padding: 5px;\n                  border-radius: 10px;\n                  background-color: " + color + ";\n                  color: white;\n                  display: block;\n                }\n              </style>\n            </head>\n            <body>\n              <div class=\"nav-bar\">\n                <img class=\"statusbar-img \" src=\"assets/images/ios-statusbar.png\" width=\"320\">\n                <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n              </div>\n              <div class=\"content padding\">\n                Preview List:\n                <div class=\"list\">\n                  " + title + "\n                </div>\n                Full View:\n                <div class=\"card padding\">\n                  " + title + "\n                  " + body + "\n                  <ul>\n                  " + ionitem + "\n                  </ul>\n                  " + buttons + "\n                </div>\n              </div>\n              <div class=\"tab-bar\">\n                <div class=\"tab\">\n                  <div class=\"icon icon-cash\"></div><div class=\"tab-title\">Accounts</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pin\"></div><div class=\"tab-title\">Near Me</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pricetags\"></div><div class=\"tab-title\">Products</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-text\"></div><div class=\"tab-title\">Messages</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-settings\"></div><div class=\"tab-title\">Settings</div>\n                </div>\n              </div>\n            </body>\n            </html>\n          ");
                    break;
                case 'android':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n          <html>\n            <head>\n\n              <style>\n                @font-face {\n                  font-family: 'Ionicons';\n                  src: url(assets/fonts/ionicons.woff2);\n                }\n                body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n                .nav-bar{position:fixed; height: 70px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n                .logo{height: 60%; width: auto;}\n                .content{position:fixed;height: 438px; overflow: scroll; margin-top: 70px; min-width: 300px;}\n                .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n                .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n                .tab:not(:last-child){border-right: 1px solid #fff}\n                .icon {\n                  margin-top: 5px;\n                  width: inherit;\n                  font-size: 1.4em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                  height: 30px;\n                }\n                .tab-title {\n                  position: absolute;\n                  bottom: 5px;\n                  width: inherit;\n                  font-size: 0.7em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                }\n                div.icon-cash:before{ content: \"\\f143\"; }\n                div.icon-pin:before{ content: \"\\f1e4\";}\n                div.icon-pricetags:before{ content: \"\\f48e\"; }\n                div.icon-text:before{ content: \"\\f24f\"; }\n                div.icon-settings::before{ content: \"\\f20d\"; }\n                .padding{ padding: 10px; }\n                .card {border: 1px solid #dedede; overflow: hidden; }\n                .list{\n                  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='%23c8c7cc'/></svg>\");\n                  padding-right: 32px;\n                  background-position: right 14px center;\n                  background-repeat: no-repeat;\n                  background-size: 14px 14px;\n                  padding-left: 10px;\n                  padding: 5px;\n                  border: 1px solid #efefee;\n                  margin-bottom: 20px\n                }\n                h1, h2, p{color: " + color + " }\n                div.card h1{font-size: 1.5em; font-weight: bold; }\n                h2{font-size: 1.2em; font-weight: bold; }\n                div.list h1{font-size: 1.1em; font-weight: bold;}\n                button{\n                  padding: 5px;\n                  border-radius: 10px;\n                  background-color: " + color + ";\n                  color: white;\n                  display: block;\n                }\n              </style>\n            </head>\n            <body>\n              <div class=\"nav-bar\">\n                <img class=\"statusbar-img \" src=\"assets/images/android-statusbar.png\" width=\"320\">\n                <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n              </div>\n              <div class=\"content padding\">\n                Preview List:\n                <div class=\"list\">\n                  " + title + "\n                </div>\n                Full View:\n                <div class=\"card padding\">\n                  " + title + "\n                  " + body + "\n                  " + ionitem + "\n                  " + buttons + "\n                </div>\n              </div>\n              <div class=\"tab-bar\">\n                <div class=\"tab\">\n                  <div class=\"icon icon-cash\"></div><div class=\"tab-title\">Accounts</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pin\"></div><div class=\"tab-title\">Near Me</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pricetags\"></div><div class=\"tab-title\">Products</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-text\"></div><div class=\"tab-title\">Messages</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-settings\"></div><div class=\"tab-title\">Settings</div>\n                </div>\n              </div>\n            </body>\n            </html>\n\n\n          ");
                    break;
                case 'windows':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n          <html>\n            <head>\n\n              <style>\n                @font-face {\n                  font-family: 'Ionicons';\n                  src: url(assets/fonts/ionicons.woff2);\n                }\n                body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n                .nav-bar{position:fixed; height: 60px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n                .logo{height: 60%; width: auto;}\n                .content{position:fixed;height: 438px; overflow: scroll; margin-top: 60px; min-width: 300px;}\n                .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n                .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n                .tab:not(:last-child){border-right: 1px solid #fff}\n                .icon {\n                  margin-top: 5px;\n                  width: inherit;\n                  font-size: 1.4em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                  height: 30px;\n                }\n                .tab-title {\n                  position: absolute;\n                  bottom: 5px;\n                  width: inherit;\n                  font-size: 0.7em;\n                  color: #ffffff;\n                  font-family: \"Ionicons\";\n                }\n                div.icon-cash:before{ content: \"\\f143\"; }\n                div.icon-pin:before{ content: \"\\f1e4\";}\n                div.icon-pricetags:before{ content: \"\\f48e\"; }\n                div.icon-text:before{ content: \"\\f24f\"; }\n                div.icon-settings::before{ content: \"\\f20d\"; }\n                .padding{ padding: 10px; }\n                .card {border: 1px solid #dedede; overflow: hidden; }\n                .list{\n                  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='%23c8c7cc'/></svg>\");\n                  padding-right: 32px;\n                  background-position: right 14px center;\n                  background-repeat: no-repeat;\n                  background-size: 14px 14px;\n                  padding-left: 10px;\n                  padding: 5px;\n                  border: 1px solid #efefee;\n                  margin-bottom: 20px\n                }\n                h1, h2, p{color: " + color + " }\n                div.card h1{font-size: 1.5em; font-weight: bold; }\n                h2{font-size: 1.2em; font-weight: bold; }\n                div.list h1{font-size: 1.1em; font-weight: bold;}\n                button{\n                  padding: 5px;\n                  border-radius: 10px;\n                  background-color: " + color + ";\n                  color: white;\n                  display: block;\n                }\n              </style>\n            </head>\n            <body>\n              <div class=\"nav-bar\">\n                <img class=\"statusbar-img \" src=\"assets/images/wp-statusbar.png\" width=\"320\">\n                <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n              </div>\n              <div class=\"content padding\">\n                Preview List:\n                <div class=\"list\">\n                  " + title + "\n                </div>\n                Full View:\n                <div class=\"card padding\">\n                  " + title + "\n                  " + body + "\n                  " + ionitem + "\n                  " + buttons + "\n                </div>\n             </div>\n              <div class=\"tab-bar\">\n                <div class=\"tab\">\n                  <div class=\"icon icon-cash\"></div><div class=\"tab-title\">Accounts</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pin\"></div><div class=\"tab-title\">Near Me</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pricetags\"></div><div class=\"tab-title\">Products</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-text\"></div><div class=\"tab-title\">Messages</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-settings\"></div><div class=\"tab-title\">Settings</div>\n                </div>\n              </div>\n            </body>\n            </html>\n            ");
                    break;
                default:
                    break;
            }
        });
    };
    PushMessagePage.prototype.pushButton = function () {
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel("Confirm"),
            message: self.settingsProvider.getLabel("Are you sure want to push this Message?"),
            buttons: [
                {
                    text: self.settingsProvider.getLabel("Cancel"),
                    role: self.settingsProvider.getLabel("cancel"),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: self.settingsProvider.getLabel("Ok"),
                    handler: function (data) {
                        self.productsProvider.push_Message(self.message)
                            .then(function (data) {
                            console.log(data);
                            var toast = self.toastCtrl.create({
                                message: self.settingsProvider.getLabel("Notification send successfully"),
                                duration: 3000,
                                position: 'right'
                            });
                            toast.present();
                            self.viewCtrl.dismiss();
                        }).catch(function (err) {
                            console.log(err);
                        });
                    }
                }
            ],
            enableBackdropDismiss: true
        });
        alert.present();
    };
    // onFileChange(csv: any) {
    //   this.fileReaded = csv.target.files[0];
    //   let reader: FileReader = new FileReader();
    //   reader.readAsText(this.fileReaded);
    //   reader.onload = (e) => {
    //     let csv: string = reader.result;
    //     let allTextLines = csv.split(/\r|\n|\r/);
    //     let headers = allTextLines[0].split(',');
    //     let lines = [];
    //     let tarr = [];
    //     for (let i = 0; i < allTextLines.length; i++) {
    //       // split content based on comma  
    //       let data = allTextLines[i].split(',');
    //       if (data.length === headers.length) {
    //         if (i > 0) {
    //           let obj = {
    //             customerId: data[0].replace(/\"/g, ""),
    //             selected: false,
    //             status: data[1].replace(/\"/g, "")
    //           }
    //           this.message.target.push(obj);
    //         }
    //         // }
    //         // log each row to see output  
    //         lines.push(tarr);
    //       }
    //     }
    //     this.targetedCustomers.data = this.message.target;
    //     console.log(this.message.target, "messageTarget");
    //     this.targetedCustomers = Object.assign({}, this.targetedCustomers);
    //   }
    // }
    PushMessagePage.prototype.onFileChange = function (csv) {
        var _this = this;
        var csvcustomer = [];
        var file = csv.target.files[0];
        var self = this;
        var formData = new FormData();
        formData.append("file-to-upload", file);
        formData.append("pushmessages_id", this.message.pushmessages_id);
        this.pushMessagingProvider.updateMessage_TargetCustomer(formData)
            .then(function (result) {
            self.getcustomerbymessagesid();
            self.csvfile.nativeElement.value = "";
            if (result.status) {
                var toast = self.toastCtrl.create({
                    message: result.msg,
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                _this.getcustomerbymessagesid;
            }
        }).catch(function (err) {
            console.log(err, "erorrrrr");
            self.csvfile.nativeElement.value = "";
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel("No Records found as Member ID is fake."),
                cssClass: 'error',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    };
    PushMessagePage.prototype.downloadCsv = function () {
        var csvContent = "MemberID";
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvContent);
        hiddenElement.target = '_blank';
        hiddenElement.download = this.getCsvFileName();
        hiddenElement.click();
    };
    PushMessagePage.prototype.getCsvFileName = function () {
        return "consectus-" + new Date().getTime() + ".csv";
    };
    PushMessagePage.prototype.uploadFile = function (data) {
        console.log(data);
        var lines = data.split("\n");
        if (lines.length > 0) {
            //remove header if it exists
            if (lines.length >= 2) {
                if (lines[0].toLowerCase().indexOf("customerid") == 0) {
                    lines = lines.slice(1);
                }
            }
            //now cleanup columns
            var newlines = Array();
            for (var _i = 0, lines_1 = lines; _i < lines_1.length; _i++) {
                var l = lines_1[_i];
                var cid = l.split(",")[0];
                if (cid.indexOf("\"") == 0)
                    cid = cid.substr(1);
                if (cid.indexOf("\"") == cid.length - 1)
                    cid = cid.substr(0, cid.length - 1);
                newlines.push(cid);
            }
            var self_2 = this;
            var customers = [];
            this.customersProvider.getCustomers()
                .then(function (data) {
                for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                    var c = data_1[_i];
                    if (newlines.includes(c.customerId)) {
                        customers.push({
                            customerId: c.customerId,
                            status: "pending"
                        });
                    }
                }
                var msg = "Total of " + customers.length + " customers found";
                if (customers.length != newlines.length) {
                    msg = "Out of " + newlines.length + " customer ids, " + (newlines.length - customers.length) + " could not be found in our customer list.";
                }
                var toast = self_2.toastCtrl.create({
                    message: msg,
                    duration: 5000,
                    position: 'right',
                    cssClass: 'success'
                });
                toast.present();
                self_2.message['target'] = customers;
                self_2.message['total'] = customers.length;
                self_2.message['delivered'] = 0;
                self_2.message['viewed'] = 0;
                self_2.message['interested'] = 0;
                self_2.dirty();
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel('Error'),
                message: this.settingsProvider.getLabel("Uploaded file is empty"),
                buttons: [
                    {
                        role: 'cancel',
                        text: 'OK',
                        handler: function (data) {
                        }
                    }
                ]
            });
            alert_1.present();
        }
        this.targetedCustomers.data = this.message.target;
        this.targetedCustomers = Object.assign({}, this.targetedCustomers);
    };
    PushMessagePage.prototype.addBulletPoint = function () {
        var self = this;
        var modal = this.modalCtrl.create(AddBulletModal);
        modal.onDidDismiss(function (data) {
            if (data) {
                self.message.bullets.push(data);
                self.dirty();
            }
        });
        modal.present();
    };
    PushMessagePage.prototype.addAction = function () {
        var self = this;
        var modal = this.modalCtrl.create(AddActionModal);
        modal.onDidDismiss(function (data) {
            if (data) {
                self.message.messagebutton.push(data);
                self.dirty();
            }
        });
        modal.present();
    };
    PushMessagePage.prototype.deleteAction = function (action, id) {
        var self = this;
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel('Confirm Delete'),
            message: this.settingsProvider.getLabel('Are you sure you want to delete') + ' ' + action.title,
            buttons: [
                {
                    text: this.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function (data) {
                        var actions = [];
                        for (var _i = 0, _a = self.message.messagebutton; _i < _a.length; _i++) {
                            var a = _a[_i];
                            console.log(a.id + " --- " + id);
                            if (a.id != id) {
                                actions.push(a);
                            }
                        }
                        self.message.messagebutton = actions;
                        self.dirty();
                    }
                }
            ]
        });
        alert.present();
    };
    PushMessagePage.prototype.deletebullet = function (bullet, id) {
        var self = this;
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel('Confirm Delete'),
            message: this.settingsProvider.getLabel('Are you sure you want to delete') + ' ' + bullet.title,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function (data) {
                        var bullets = [];
                        for (var _i = 0, _a = self.message.bullets; _i < _a.length; _i++) {
                            var a = _a[_i];
                            console.log(a.id + " --- " + id);
                            if (a.id != id) {
                                bullets.push(a);
                            }
                        }
                        self.message.bullets = bullets;
                        self.dirty();
                    }
                }
            ]
        });
        alert.present();
    };
    PushMessagePage.prototype.toggleAllCustomers = function (event) {
        if (this.all_customers) {
            this.message.target = null;
            this.message['total'] = this.customerCount;
        }
        else {
            this.message['total'] = 0;
        }
        this.dirty();
    };
    PushMessagePage.prototype.save = function () {
        var _this = this;
        var msg = [];
        if (this.message.title) {
            if (this.message.title.length <= 0)
                msg.push(this.settingsProvider.getLabel("Message Title cannot be blank"));
        }
        else {
            msg.push("Message Title cannot be blank");
        }
        if (new Date(this.message.date) < this.todayDate)
            msg.push(this.settingsProvider.getLabel("Date cannot be in the past."));
        if (this.message.type == 'product' && !this.message.productId) {
            msg.push(this.settingsProvider.getLabel("Select Product to Push"));
        }
        if (msg.length > 0) {
            var alert_2 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel('Error'),
                message: msg.join("<br/>"),
                buttons: [
                    {
                        role: this.settingsProvider.getLabel('Cancel'),
                        text: 'OK',
                        handler: function (data) {
                        }
                    }
                ]
            });
            alert_2.present();
        }
        else {
            var self_3 = this;
            var alert_3 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel('Confirm Save'),
                message: this.settingsProvider.getLabel('Are you sure you want to Save this message?'),
                buttons: [
                    {
                        text: this.settingsProvider.getLabel('Cancel'),
                        role: 'cancel',
                        handler: function (data) {
                        }
                    },
                    {
                        text: this.settingsProvider.getLabel('Save'),
                        handler: function (data) {
                            console.log("message", _this.message);
                            self_3.callback(self_3.message)
                                .then(function () {
                                self_3.navCtrl.pop();
                            });
                        }
                    }
                ]
            });
            alert_3.present();
        }
    };
    PushMessagePage.prototype.delete = function () {
        var self = this;
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel('Confirm Delete Message'),
            message: this.settingsProvider.getLabel('Are you sure you want to delete this message?'),
            buttons: [
                {
                    text: this.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                    handler: function (data) {
                    }
                },
                {
                    text: this.settingsProvider.getLabel('Delete Message'),
                    handler: function (data) {
                        self.messagesProvider.deleteMessage(self.message);
                        self.navCtrl.pop();
                    }
                }
            ]
        });
        alert.present();
    };
    PushMessagePage.prototype.deleteMessage = function () {
    };
    PushMessagePage.prototype.downloadPdf = function () {
        window.open('http://www.orimi.com/pdf-test.pdf', '_system', 'location=yes');
    };
    return PushMessagePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_7__angular_core__["_12" /* ViewChild */])("fileInput"),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7__angular_core__["u" /* ElementRef */])
], PushMessagePage.prototype, "csvfile", void 0);
PushMessagePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_8_ionic_angular__["f" /* IonicPage */])({ name: "PushMessagePage" }),
    Object(__WEBPACK_IMPORTED_MODULE_7__angular_core__["n" /* Component */])({
        selector: 'page-push-message',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\push-message\push-message.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <button small ion-button color="primary" (click)="pushButton()">{{settingsProvider.getLabel(\'Push\')}}</button>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row *ngIf="new">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="save()" *ngIf="dirtyFlag">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row *ngIf="!new && canUpdate">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="danger" (click)="delete()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Delete Push\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-6>\n\n        <ion-grid class="table-cell">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row class="header">\n\n                  <ion-col>\n\n                    {{settingsProvider.getLabel(\'Message Content\')}}\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-list>\n\n                      <ion-item>\n\n                        <ion-label stacked>{{settingsProvider.getLabel(\'Title\')}}</ion-label>\n\n                        <ion-input type="text" [(ngModel)]="message.title" placeholder="{{settingsProvider.getLabel(\'Title\')}}" (input)="dirty()"\n\n                          [disabled]="!canUpdate && !new"></ion-input>\n\n                      </ion-item>\n\n                      <ion-item>\n\n                        <ion-label stacked>{{settingsProvider.getLabel(\'Body\')}}</ion-label>\n\n                        <ion-textarea [(ngModel)]="message.message" placeholder="{{settingsProvider.getLabel(\'Body\')}}" (input)="dirty()" [disabled]="!canUpdate && !new"></ion-textarea>\n\n                      </ion-item>\n\n                    </ion-list>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <p float-right>{{settingsProvider.getLabel(\'Note: You can use HTML Tags in the body\')}}</p>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row align-items-center class="header">\n\n                  <ion-col>{{settingsProvider.getLabel(\'Action Buttons\')}}</ion-col>\n\n                </ion-row>\n\n                <ion-row *ngFor="let action of message.messagebutton ">\n\n                  <ion-col text-nowrap>\n\n                    <button ion-button color="danger" clear icon-only float-right *ngIf="!isUpdate" (click)="deleteAction(action, action.id)">\n\n                      <ion-icon name="trash"></ion-icon>\n\n                    </button>\n\n                    <h5>{{action.title}}</h5>\n\n                    <div float-left *ngIf="action.type==\'url\'">{{settingsProvider.getLabel(\'URL\')}} &nbsp;</div>\n\n                    <div float-left *ngIf="action.type==\'call\'">{{settingsProvider.getLabel(\'Call Number\')}} &nbsp;\n\n                    </div>\n\n                    <div float-left *ngIf="action.type==\'callback\'">{{settingsProvider.getLabel(\'Callback Request\')}} &nbsp;\n\n                    </div>\n\n                    <a *ngIf="action.type==\'url\'" [href]="action.action" [target]="\'_blank\'">{{action.action}}</a>\n\n                    <div *ngIf="action.type!=\'url\'">{{action.action}}</div>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <button small ion-button (click)="addAction()" [disabled]="!canUpdate && !new">{{settingsProvider.getLabel(\'Add Action\')}}</button>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n      <ion-col col-6>\n\n        <ion-grid class="table-cell">\n\n          <ion-row justify-content-center>\n\n            <ion-col text-center text-nowrap padding>\n\n              <div class="header">{{settingsProvider.getLabel(\'Live Message Preview\')}}</div>\n\n              <ion-segment [(ngModel)]="deviceType" padding-top padding-bottom>\n\n                <ion-segment-button value="ios">iOS</ion-segment-button>\n\n                <ion-segment-button value="android">Android</ion-segment-button>\n\n                <!-- <ion-segment-button value="windows">Windows</ion-segment-button> -->\n\n              </ion-segment>\n\n              <div class="preview" id="demo-device-ios" *ngIf="deviceType==\'ios\'">\n\n                <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n              </div>\n\n              <div class="preview" id="demo-device-android" *ngIf="deviceType==\'android\'">\n\n                <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n              </div>\n\n              <div class="preview" id="demo-device-windows" *ngIf="deviceType==\'windows\'">\n\n                <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-cell">\n\n          <ion-row justify-content-center>\n\n            <ion-col text-center text-nowrap padding>\n\n              <div class="header">\n\n                {{settingsProvider.getLabel(\'Targeted Customers\')}}\n\n\n\n\n\n              </div>\n\n              <div class="button_labels">\n\n                <label for="uploadFile">{{settingsProvider.getLabel(\'Upload CSV\')}}</label>\n\n                <input type="file" id="uploadFile" (change)="onFileChange($event)" #fileInput>\n\n                <label for="uploadFile" style=" margin-left: 0px;" *ngIf="isdelivered">{{settingsProvider.getLabel(\'Target all Customers\')}}</label>\n\n                <!-- <label style="margin-left: 0px;" *ngIf="!isdelivered">{{settingsProvider.getLabel(\'Resend\')}}</label> -->\n\n                <!-- <a ion-button small clear (click)="downloadPdf()" style="margin-top: 10px; float:right;">\n\n                  {{settingsProvider.getLabel(\'Download PDF\')}}\n\n                </a> -->\n\n                <a (click)="downloadCsv()" ion-button small clear style="margin-top: 10px; float:right;">\n\n                  {{settingsProvider.getLabel(\'Download Sample CSV\')}}\n\n                </a>\n\n\n\n              </div>\n\n              <table-component [options]="targetedCustomers"></table-component>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\push-message\push-message.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_customers_customers__["a" /* CustomersProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_push_messaging_push_messaging__["a" /* PushMessagingProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_push_messaging_push_messaging__["a" /* PushMessagingProvider */],
        __WEBPACK_IMPORTED_MODULE_1__providers_products_products__["a" /* ProductsProvider */],
        __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["l" /* NavParams */]])
], PushMessagePage);

var AddActionModal = (function () {
    function AddActionModal(alertCtrl, languageProvider, viewCtrl, navCtrl, settingsProvider) {
        this.alertCtrl = alertCtrl;
        this.languageProvider = languageProvider;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.settingsProvider = settingsProvider;
        this.title = "";
        this.type = "";
        this.action = "";
        this.languageString = "en";
        this.resultdata = null;
    }
    AddActionModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddActionModal.prototype.save = function () {
        var errors = [];
        if (this.title.length == 0) {
            errors.push(this.settingsProvider.getLabel("Title cannot be blank"));
        }
        if (this.type.length == 0) {
            errors.push("Type must be selected");
        }
        if (this.action.length == 0 && this.type != "callback") {
            errors.push(this.settingsProvider.getLabel("Action must be provided."));
        }
        if (errors.length > 0) {
            var alert_4 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel("Confirm Delete"),
                message: errors.join('<br/> '),
                buttons: [
                    {
                        text: this.settingsProvider.getLabel('OK'),
                        role: 'cancel',
                        handler: function (data) {
                        }
                    }
                ]
            });
            alert_4.present();
        }
        else {
            this.viewCtrl.dismiss({ title: this.title, type: this.type, action: this.action });
        }
    };
    return AddActionModal;
}());
AddActionModal = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_7__angular_core__["n" /* Component */])({
        selector: 'add-action',
        template: "\n  <ion-header color=\"secondary\">\n    <ion-navbar>\n      <ion-buttons start>\n        <button ion-button icon-left (click)=\"dismiss()\" style=\"color:white\">\n          <ion-icon name=\"arrow-round-back\"></ion-icon>\n          {{settingsProvider.getLabel('Back')}}\n        </button>\n      </ion-buttons>\n      <ion-title>{{settingsProvider.getLabel(titleName)}}</ion-title>\n      <ion-buttons end>\n        <button ion-button icon-left (click)=\"save()\" style=\"color:white\">\n          <ion-icon name=\"ios-checkmark\"></ion-icon>\n          {{settingsProvider.getLabel('Save')}}\n        </button>\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>\n  <ion-content padding>\n      <ion-list padding>\n        <ion-item padding>\n          <ion-label stacked>{{settingsProvider.getLabel('Title')}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"title\"></ion-input>\n        </ion-item>\n        <ion-item style=\"padding-right:16px; padding-bottom:10px;\">\n          <ion-label stacked>{{settingsProvider.getLabel('Type')}}</ion-label>\n          <ion-select [(ngModel)]=\"type\">\n            <ion-option value=\"url\">URL</ion-option>\n            <ion-option value=\"call\">Call Number</ion-option>\n            <ion-option value=\"callback\">Call Back</ion-option>\n          </ion-select>\n        </ion-item>\n        <ion-item *ngIf=\"type!='callback'\" style=\"padding-right:16px; padding-bottom:16px;\">\n          <ion-label stacked>{{settingsProvider.getLabel('Action')}}</ion-label>\n          <ion-input [(ngModel)]=\"action\"></ion-input>\n        </ion-item>\n      </ion-list>\n  </ion-content>\n  "
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_settings_settings__["a" /* SettingsProvider */]])
], AddActionModal);

var AddBulletModal = (function () {
    function AddBulletModal(alertCtrl, languageProvider, viewCtrl, navCtrl, settingsProvider) {
        this.alertCtrl = alertCtrl;
        this.languageProvider = languageProvider;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.settingsProvider = settingsProvider;
        this.bulletline = "";
        this.languageString = "en";
        this.resultdata = null;
    }
    AddBulletModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddBulletModal.prototype.save = function () {
        var errors = [];
        if (this.bulletline.length == 0) {
            errors.push(this.settingsProvider.getLabel("Title cannot be blank"));
        }
        if (errors.length > 0) {
            var alert_5 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel('Confirm Delete'),
                message: errors.join('<br/> '),
                buttons: [
                    {
                        text: this.settingsProvider.getLabel('OK'),
                        role: 'cancel',
                    }
                ]
            });
            alert_5.present();
        }
        else {
            this.viewCtrl.dismiss({ bulletline: this.bulletline });
        }
    };
    return AddBulletModal;
}());
AddBulletModal = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_7__angular_core__["n" /* Component */])({
        selector: 'add-bullet',
        template: "\n  <ion-header color=\"secondary\">\n    <ion-navbar>\n      <ion-buttons start>\n        <button ion-button icon-left (click)=\"dismiss()\" style=\"color:white\">\n          <ion-icon name=\"arrow-round-back\"></ion-icon>\n          {{settingsProvider.getLabel('Back')}}\n        </button>\n      </ion-buttons>\n      <ion-title>{{settingsProvider.getLabel('Add Bullet')}}</ion-title>\n      <ion-buttons end>\n        <button ion-button icon-left (click)=\"save()\" style=\"color:white\">\n          <ion-icon name=\"done-all\"></ion-icon>\n          {{settingsProvider.getLabel('Add Bullet')}}\n        </button>\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>\n  <ion-content padding>\n      <ion-list>\n        <ion-item>\n          <ion-label stacked>{{settingsProvider.getLabel('Bullet')}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"bulletline\"></ion-input>\n        </ion-item>\n      </ion-list>\n      <br/>\n  </ion-content>\n  "
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_settings_settings__["a" /* SettingsProvider */]])
], AddBulletModal);

//# sourceMappingURL=push-message.js.map

/***/ }),

/***/ 487:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaginationComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pagination__ = __webpack_require__(488);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__change_page_size_popover_change_page_size_popover_module__ = __webpack_require__(193);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PaginationComponentModule = (function () {
    function PaginationComponentModule() {
    }
    return PaginationComponentModule;
}());
PaginationComponentModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__pagination__["a" /* PaginationComponent */]
        ],
        entryComponents: [],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicModule */],
            __WEBPACK_IMPORTED_MODULE_3__change_page_size_popover_change_page_size_popover_module__["ChangePageSizePopoverPageModule"]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__pagination__["a" /* PaginationComponent */]
        ]
    })
], PaginationComponentModule);

//# sourceMappingURL=pagination.module.js.map

/***/ }),

/***/ 488:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Pagination */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaginationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_utils_utils__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Pagination = (function () {
    function Pagination() {
    }
    return Pagination;
}());

var PaginationComponent = (function () {
    function PaginationComponent(popoverCtrl, utilsProvider) {
        this.popoverCtrl = popoverCtrl;
        this.utilsProvider = utilsProvider;
        this.clickBeginning = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["w" /* EventEmitter */]();
        this.clickPrevious = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["w" /* EventEmitter */]();
        this.clickNext = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["w" /* EventEmitter */]();
        this.clickEnd = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["w" /* EventEmitter */]();
        this.pageSizeChanged = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["w" /* EventEmitter */]();
        this.showRowCount = false;
    }
    Object.defineProperty(PaginationComponent.prototype, "currentPageItemsMin", {
        get: function () {
            return Math.max(1, (this.pagination.page - 1) * this.pagination.pageSize);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "currentPageItemsMax", {
        get: function () {
            return Math.min((this.pagination.page) * this.pagination.pageSize, this.maxItems);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "maxItems", {
        get: function () {
            return this.pagination.rowCount;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "currentPage", {
        get: function () {
            return this.pagination.page;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "totalPages", {
        get: function () {
            return this.pagination.pageCount;
        },
        enumerable: true,
        configurable: true
    });
    PaginationComponent.prototype.toggle = function () {
        if (this.showRowCount)
            this.showRowCount = false;
        else
            this.showRowCount = true;
    };
    PaginationComponent.prototype.changePageSize = function (ev) {
        var self = this;
        var popover = this.popoverCtrl.create("ChangePageSizePopoverPage", { pageSize: this.pagination.pageSize });
        popover.onDidDismiss(function (data) {
            if (data) {
                if (data.pageSize != self.pagination.pageSize) {
                    self.pagination.pageSize = data.pageSize;
                    self.pageSizeChanged.next(data.pageSize);
                }
                else {
                    self.utilsProvider.hide();
                }
            }
        });
        popover.present({
            ev: ev
        });
    };
    return PaginationComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", Pagination)
], PaginationComponent.prototype, "pagination", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", String)
], PaginationComponent.prototype, "color", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], PaginationComponent.prototype, "small", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], PaginationComponent.prototype, "large", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], PaginationComponent.prototype, "clickBeginning", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], PaginationComponent.prototype, "clickPrevious", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], PaginationComponent.prototype, "clickNext", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], PaginationComponent.prototype, "clickEnd", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], PaginationComponent.prototype, "pageSizeChanged", void 0);
PaginationComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'pagination',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\pagination\pagination.html"*/'<button ion-button [large]="large!=\'undefined\'" [small]="small!=\'undefined\'"  [color]="color" (click)="changePageSize($event)">\n\n  Showing {{pagination.pageSize}} per page\n\n</button>\n\n<button ion-button (click)="clickBeginning.next()" icon-only [color]="color" [disabled]="pagination.page === 1"  [large]="large!=\'undefined\'" [small]="small!=\'undefined\'">\n\n  <ion-icon name="rewind"></ion-icon>\n\n</button>\n\n<button ion-button (click)="clickPrevious.next()" icon-only [color]="color" [disabled]="pagination.page === 1"  [large]="large!=\'undefined\'" [small]="small!=\'undefined\'">\n\n  <ion-icon name="arrow-dropleft"></ion-icon>\n\n</button>\n\n<button *ngIf="showRowCount" (tap)="toggle()" ion-button pagination-center-text  [large]="large!=\'undefined\'" [small]="small!=\'undefined\'">{{ currentPageItemsMin }}-{{ currentPageItemsMax }} of {{ maxItems }}</button>\n\n<button *ngIf="!showRowCount" (tap)="toggle()"  ion-button pagination-center-text  [large]="large!=\'undefined\'" [small]="small!=\'undefined\'">Page {{ currentPage }} of {{ totalPages }}</button>\n\n<button ion-button (click)="clickNext.next()" icon-only [color]="color" [disabled]="pagination.page >= pagination.pageCount"  [large]="large!=\'undefined\'" [small]="small!=\'undefined\'">\n\n  <ion-icon name="arrow-dropright"></ion-icon>\n\n</button>\n\n<button ion-button (click)="clickEnd.next()" icon-only [color]="color" [disabled]="pagination.page >= pagination.pageCount"  [large]="large!=\'undefined\'" [small]="small!=\'undefined\'">\n\n  <ion-icon name="fastforward"></ion-icon>\n\n</button>\n\n'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\pagination\pagination.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* PopoverController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_utils_utils__["a" /* UtilsProvider */]])
], PaginationComponent);

//# sourceMappingURL=pagination.js.map

/***/ }),

/***/ 489:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushMessagingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_push_messaging_push_messaging__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_table_table__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var PushMessagingPage = (function () {
    function PushMessagingPage(languageProvider, oauthProvider, messagesProvider, pushMessagingProvider, modalCtrl, toastCtrl, navCtrl, navParams, globalProvider, loadauditProvider, events, settingsProvider, viewCtrl, masterpermissionProvider) {
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.messagesProvider = messagesProvider;
        this.pushMessagingProvider = pushMessagingProvider;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.globalProvider = globalProvider;
        this.loadauditProvider = loadauditProvider;
        this.events = events;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.currentTab = "activePushMessages";
        this.messagingOptions = {
            data: [],
            filteredData: [],
            columns: {
                "title": { title: "Title", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "create_date": { title: "Start Date", mobile: true, format: 'date' },
                "target": { title: "Target", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "delivered": { title: "Delivered", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "viewed": { title: "Viewed", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "interested": { title: "Interested", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.delete.bind(this),
                    color: 'danger'
                },
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Push Message",
                addAction: this.create.bind(this),
            },
            tapRow: this.edit.bind(this),
            uploadOptions: {},
        };
        this.messaging1Options = {
            data: [],
            filteredData: [],
            columns: {
                "title": { title: "Title", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "create_date": { title: "Start Date", mobile: true, format: 'date' },
                "target": { title: "Target", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "delivered": { title: "Delivered", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "viewed": { title: "Viewed", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "interested": { title: "Interested", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Active",
                    action: this.activate.bind(this),
                    color: 'primary'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Push Message",
                addAction: this.create.bind(this),
            },
            tapRow: this.edit.bind(this),
            uploadOptions: {},
        };
        this.new = false;
        this.canUpdate = false;
        this.data = [];
        this.resultdata = null;
        this.languageString = "en";
        this.addActionTitle = this.settingsProvider.getLabel("Create New Push Message");
        this.messageList = navParams.get("message");
        var self = this;
        if (!navParams.get('message')) {
            //add adata
            self.message = {
                "customers_id": null,
                "title": null,
                "message": null,
                "passwd": null,
                "bulletpoint": null,
                "bannerurl": null,
                "messagetype": null,
                "messagebutton": null,
                "isactive  ": null
            };
            self.title = settingsProvider.getLabel("Update Complaint Details");
            self.new = true;
        }
        else {
            //View Data
            self.message = JSON.parse(JSON.stringify(navParams.get('message')));
            self.messageList = self.message;
            console.log("thismessages", this.message);
            self.canUpdate = true;
            self.title = settingsProvider.getLabel("View Push Message Details");
        }
    }
    PushMessagingPage.prototype.commonFunction = function (nullData) {
        var str = "";
        if (nullData == 0) {
            str = '--';
        }
        else if (nullData == null) {
            str = '--';
        }
        else if (nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    PushMessagingPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("pushmessages")) {
            return true;
        }
        else {
            return false;
        }
    };
    PushMessagingPage.prototype.edit = function (updatemsg) {
        this.navCtrl.push("PushMessagePage", { messagedata: updatemsg });
    };
    PushMessagingPage.prototype.create = function () {
        this.navCtrl.push("PushMessagePage", { callback: this.addMessage.bind(this) });
    };
    PushMessagingPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    PushMessagingPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        switch (this.currentTab) {
            case 'activePushMessages':
                this.getmessages();
                break;
            case 'archivePushMessages':
                this.getArchivemessages();
                break;
            default:
                break;
        }
    };
    PushMessagingPage.prototype.segmentChanged = function (event) {
        console.log("eventValue", event._value);
        switch (event._value) {
            case 'activePushMessages':
                this.getmessages();
                break;
            case 'archivePushMessages':
                this.getArchivemessages();
                break;
            default:
                break;
        }
    };
    PushMessagingPage.prototype.getmessages = function () {
        var _this = this;
        var self = this;
        this.pushMessagingProvider.getmessages()
            .then(function (result) {
            self.messagingOptions.data = _this.archive(result.data, 1);
            self.messagingOptions = Object.assign({}, self.messagingOptions);
        }).catch(function (error) {
        });
    };
    PushMessagingPage.prototype.getArchivemessages = function () {
        var _this = this;
        var self = this;
        this.pushMessagingProvider.getmessages()
            .then(function (result) {
            self.messaging1Options.data = _this.archive(result.data, 0);
            console.log("resultData", result.data);
            self.messaging1Options = Object.assign({}, self.messaging1Options);
        }).catch(function (error) {
        });
    };
    PushMessagingPage.prototype.delete = function (items) {
        var _this = this;
        console.log("items", items);
        var d = [];
        var self = this;
        console.log("delete product");
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            d.push(item.pushmessages_id);
        }
        var prodcutIds = d.join(',');
        self.messagesProvider.deletepushMessage(prodcutIds).then(function (result) {
            self.getmessages();
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
            var message = "Push Messages are activatd for this user in Push Messaging";
            var auditUser = {
                "users_id": _this.oauthProvider.user[0].users_id,
                "customer_devices_id": 0,
                "usertype": "staff",
                "uuid": "",
                "appname": "hub",
                "event_type": "view",
                "screen": "push-messaging",
                "trail_details": message
            };
            _this.loadauditProvider.loadaudit(auditUser)
                .then(function (data) {
            });
        });
    };
    PushMessagingPage.prototype.activate = function (productsId) {
        var _this = this;
        var d = [];
        var self = this;
        console.log("delete Push Message");
        for (var _i = 0, productsId_1 = productsId; _i < productsId_1.length; _i++) {
            var item = productsId_1[_i];
            d.push(item.pushmessages_id);
        }
        var messageIds = d.join(',');
        self.messagesProvider.activatePushMessage(messageIds).then(function (result) {
            self.getArchivemessages();
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            var message = "Push Messages are activatd for this user in Push Messaging";
            var auditUser = {
                "users_id": _this.oauthProvider.user[0].users_id,
                "customer_devices_id": 0,
                "usertype": "staff",
                "uuid": "",
                "appname": "hub",
                "event_type": "view",
                "screen": "push-messaging",
                "trail_details": message
            };
            _this.loadauditProvider.loadaudit(auditUser)
                .then(function (data) {
            });
        });
    };
    PushMessagingPage.prototype.getRowClass = function (item) {
        return item.type;
    };
    PushMessagingPage.prototype.updatemessage = function (message) {
        var self = this;
        message.isactive = 1;
        return new Promise(function (resolve, reject) {
            self.pushMessagingProvider.updatemessage(message)
                .then(function () {
                var toast = self.toastCtrl.create({
                    message: 'Push Message was updated successfully',
                    duration: 3000,
                    position: 'right'
                });
                toast.present();
                resolve();
            });
        });
    };
    PushMessagingPage.prototype.addMessage = function (message) {
        var self = this;
        message.isactive = 1;
        return new Promise(function (resolve, reject) {
            self.pushMessagingProvider.addMessage(message)
                .then(function () {
                var toast = self.toastCtrl.create({
                    message: 'Push Message was added successfully',
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                resolve();
            });
        });
    };
    PushMessagingPage.prototype.archive = function (archiveData, activestatus) {
        var archive = [];
        for (var i in archiveData) {
            if (archiveData[i].isactive == activestatus) {
                archive.push(archiveData[i]);
            }
        }
        return archive;
    };
    return PushMessagingPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["_12" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2__components_table_table__["a" /* TableComponent */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__components_table_table__["a" /* TableComponent */])
], PushMessagingPage.prototype, "table", void 0);
PushMessagingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["f" /* IonicPage */])({ name: "PushMessagingPage" }),
    Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["n" /* Component */])({
        selector: 'page-push-messaging',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\push-messaging\push-messaging.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel("Marketing Push Messaging Management | Consectus")}} </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n    <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n        <ion-segment-button value="activePushMessages">\n\n          {{settingsProvider.getLabel(\'Active\')}}\n\n        </ion-segment-button>\n\n        <ion-segment-button value="archivePushMessages">\n\n          {{settingsProvider.getLabel(\'Archive\')}}\n\n        </ion-segment-button>\n\n      </ion-segment >\n\n\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'activePushMessages\'">\n\n      <ion-row>\n\n        <ion-col>\n\n          <ion-grid class="table-simple">\n\n            <ion-row>\n\n              <ion-col>\n\n                <table-component [options]="messagingOptions"></table-component>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-grid>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n    <ion-grid class="table-cell" *ngIf="currentTab == \'archivePushMessages\'">\n\n      <ion-row>\n\n        <ion-col>\n\n          <ion-grid class="table-simple">\n\n            <ion-row>\n\n              <ion-col>\n\n                <table-component [options]="messaging1Options"></table-component>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-grid>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\push-messaging\push-messaging.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_0__providers_push_messaging_push_messaging__["a" /* PushMessagingProvider */],
        __WEBPACK_IMPORTED_MODULE_0__providers_push_messaging_push_messaging__["a" /* PushMessagingProvider */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_7__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], PushMessagingPage);

//# sourceMappingURL=push-messaging.js.map

/***/ }),

/***/ 490:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChartComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__chart__ = __webpack_require__(491);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ChartComponentModule = (function () {
    function ChartComponentModule() {
    }
    return ChartComponentModule;
}());
ChartComponentModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_0__chart__["a" /* ChartComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */],
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_0__chart__["a" /* ChartComponent */]
        ]
    })
], ChartComponentModule);

//# sourceMappingURL=chart.module.js.map

/***/ }),

/***/ 491:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ChartComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var ChartComponent = (function () {
    function ChartComponent(languageProvider) {
        this.languageProvider = languageProvider;
        this.clickAction = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["w" /* EventEmitter */]();
    }
    ChartComponent.prototype.ngOnInit = function () {
        // let self = this;
        // this.barChart = new Chart(this.barChartCanvas.nativeElement, {
        //   type: 'bar',
        //   data: self.data,
        //   options: self.options
        // });
    };
    ChartComponent.prototype.ngOnChanges = function () {
        var self = this;
        if (this.options) {
            self.options["onClick"] = function (event) {
                var activeElement = self.chart.getElementAtEvent(event);
                if (activeElement.length > 0) {
                    console.log("now calling clickAction method: " + activeElement[0]._model.label);
                    self.clickAction.emit(activeElement[0]._model.label);
                }
            };
            self.options.legend = {
                display: false
            };
        }
        if (this.width) {
            this.chartCanvas.nativeElement.getContext("2d").canvas.width = this.width;
            console.log("settings width: " + this.width);
        }
        if (this.height) {
            this.chartCanvas.nativeElement.getContext("2d").canvas.height = this.height;
            console.log("settings height: " + this.height);
        }
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartCanvas.nativeElement, this.options);
    };
    return ChartComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], ChartComponent.prototype, "options", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], ChartComponent.prototype, "clickAction", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_12" /* ViewChild */])('chartcanvas'),
    __metadata("design:type", Object)
], ChartComponent.prototype, "chartCanvas", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], ChartComponent.prototype, "width", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], ChartComponent.prototype, "height", void 0);
ChartComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'chart',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\chart\chart.html"*/'<ion-card>\n\n  <ion-card-content>\n\n    <canvas #chartcanvas></canvas>\n\n  </ion-card-content>\n\n</ion-card>\n\n'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\chart\chart.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_language_language__["a" /* LanguageProvider */]])
], ChartComponent);

//# sourceMappingURL=chart.js.map

/***/ }),

/***/ 492:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DoughnutComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__doughnut__ = __webpack_require__(493);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DoughnutComponentModule = (function () {
    function DoughnutComponentModule() {
    }
    return DoughnutComponentModule;
}());
DoughnutComponentModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_0__doughnut__["a" /* DoughnutComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */],
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_0__doughnut__["a" /* DoughnutComponent */]
        ]
    })
], DoughnutComponentModule);

//# sourceMappingURL=doughnut.module.js.map

/***/ }),

/***/ 493:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DoughnutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chart_js__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the DoughnutComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var DoughnutComponent = (function () {
    function DoughnutComponent(languageProvider) {
        this.languageProvider = languageProvider;
        this.clickAction = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    DoughnutComponent.prototype.ngOnChanges = function () {
        var self = this;
        if (this.options) {
            self.options["onClick"] = function (event) {
                var activeElement = self.doughnut.getElementAtEvent(event);
                if (activeElement.length > 0) {
                    console.log("now calling clickAction method: " + activeElement[0]._model.label);
                    self.clickAction.emit(activeElement[0]._model.label);
                }
            };
            self.options['cutoutPercentage'] = 70;
        }
        this.doughnut = new __WEBPACK_IMPORTED_MODULE_1_chart_js__["Chart"](this.canvas.nativeElement, {
            type: 'doughnut',
            data: self.data,
            options: self.options
        });
    };
    return DoughnutComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
    __metadata("design:type", String)
], DoughnutComponent.prototype, "title", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], DoughnutComponent.prototype, "data", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], DoughnutComponent.prototype, "options", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], DoughnutComponent.prototype, "clickAction", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewChild */])('doughnutcanvas'),
    __metadata("design:type", Object)
], DoughnutComponent.prototype, "canvas", void 0);
DoughnutComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'doughnut',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\doughnut\doughnut.html"*/'<ion-card>\n\n  <ion-card-header>\n\n    {{title}}\n\n  </ion-card-header>\n\n  <ion-card-content padding>\n\n      <canvas #doughnutcanvas></canvas>\n\n  </ion-card-content>\n\n</ion-card>\n\n'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\doughnut\doughnut.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */]])
], DoughnutComponent);

//# sourceMappingURL=doughnut.js.map

/***/ }),

/***/ 494:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConsectusHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_language_language__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ConsectusHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ConsectusHeaderComponent = (function () {
    function ConsectusHeaderComponent(languageProvider) {
        this.languageProvider = languageProvider;
        console.log('Hello ConsectusHeaderComponent Component');
        this.text = 'Hello World';
    }
    return ConsectusHeaderComponent;
}());
ConsectusHeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'consectus-header',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\consectus-header\consectus-header.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{languageProvider.get(\'Dashboard\')}} | Consectus</ion-title>\n\n  </ion-navbar>\n\n</ion-header>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\consectus-header\consectus-header.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_language_language__["a" /* LanguageProvider */]])
], ConsectusHeaderComponent);

//# sourceMappingURL=consectus-header.js.map

/***/ }),

/***/ 495:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ProductPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddActionModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_customers_customers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_products_products__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











// declare var LocalFileSystem: any;
// declare var navigator: any;
var ProductPage = (function () {
    function ProductPage(languageProvider, oauthProvider, customersProvider, domSanitizer, cdr, alertCtrl, modalCtrl, viewCtrl, settingsProvider, productsProvider, toastCtrl, navCtrl, navParams, globalProvider, loadauditProvider, masterpermissionProvider) {
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.customersProvider = customersProvider;
        this.domSanitizer = domSanitizer;
        this.cdr = cdr;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.settingsProvider = settingsProvider;
        this.productsProvider = productsProvider;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.globalProvider = globalProvider;
        this.loadauditProvider = loadauditProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.accountTypeId = false;
        this.deviceType = 'ios';
        this.new = false;
        this.isdelivered = true;
        this.isEdge = false;
        this.productOptions = {
            data: [],
            filteredData: [],
            columns: {
                "customers_id": { title: "Consectus ID", search: true },
                "customername": { title: "Customer Name", search: true },
                "memberid": { title: "Member ID", search: true },
                "isdelivered": { title: "Status", format: 'function', fn: this.productStatus.bind(this), search: true }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.showCustomer.bind(this),
            uploadOptions: {},
        };
        this.csvData = [];
        this.isupdate = false;
        this.languageString = "en";
        this.resultdata = null;
        console.log(navigator.appVersion, "navigator");
        this.isEdge = (navigator.appVersion.indexOf('Edge') === -1) ? false : true;
        var self = this;
        this.callback = navParams.get('callback');
        this.customersProvider.getCustomerCount()
            .then(function (count) {
            self.customerCount = count;
        });
        if (!navParams.get('product')) {
            var sd = new Date();
            var ed = new Date();
            ed.setMonth(sd.getMonth() + 6);
            self.product = {
                id: new Date().getTime().toString(),
                "title": null,
                "sub_title": null,
                "description": null,
                "bannerurl": null,
                "imagepath": null,
                "productbutton": [],
                "start_date": sd.toISOString(),
                "end_date": ed.toISOString(),
                "target": 0,
                "total": this.customerCount,
                "account_types_id": ["1"
                ],
                "accountname": ""
            };
            self.title = "Create New Product";
            self.new = true;
        }
        else {
            this.title = "Update Product";
            this.isupdate = true;
            // self.productType = self.updateData.account_types_id;
            this.product = JSON.parse(JSON.stringify(navParams.get('product')));
            if (this.product.bannerurl == null || this.product.bannerurl == "") {
                this.product.imagepath = null;
                this.product.bannerurl = null;
            }
            else {
                this.product.imagepath = self.globalProvider.getBaseUrl() + this.product.bannerurl;
            }
            this.image = self.globalProvider.getBaseUrl() + this.product.bannerurl;
            console.log("product", this.product);
            this.getcustomerbyproductid();
        }
    }
    ProductPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("products")) {
            return true;
        }
        else {
            return false;
        }
    };
    ProductPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        else {
            this.updatePreview();
            // let self = this;
        }
        this.customersData = {
            labels: ["Total", "Delivered", "Viewed", "Interested"],
            datasets: [{
                    label: '',
                    data: [this.product.total, this.product.delivered, this.product.viewed, this.product.interested],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                    ],
                    borderWidth: 1
                }]
        };
        this.customersOptions = {
            legend: {
                display: false
            },
        };
    };
    ProductPage.prototype.ionViewDidEnter = function () {
        this.getProducts();
    };
    ProductPage.prototype.getProducts = function () {
        var self = this;
        this.productsProvider.getaccounttypes()
            .then(function (result) {
            // if (result.status) {
            self.productTypes = result.data;
            console.log(self.productTypes, "accountype");
            // }
        }).catch(function (error) {
        });
    };
    ProductPage.prototype.productStatus = function (data) {
        var str = "";
        if (data == "Yes") {
            str = "Delivered";
        }
        else {
            str = "Not Delivered";
        }
        return str;
    };
    ProductPage.prototype.getcustomerbyproductid = function () {
        var _this = this;
        var self = this;
        var productId = {
            products_id: this.product.products_id
        };
        self.productsProvider.getcustomerbyproductid(productId).then(function (result) {
            console.log("result", result);
            self.productOptions.data = result.data;
            if (self.productOptions.data.length == 0) {
                _this.isdelivered = true;
            }
            else {
                _this.isdelivered = false;
            }
            self.productOptions = Object.assign({}, self.productOptions);
        }).catch(function (error) {
            console.log(error);
        });
    };
    ProductPage.prototype.downloadPdf = function () {
        window.open('http://www.orimi.com/pdf-test.pdf', '_system', 'location=yes');
    };
    ProductPage.prototype.updatePreview = function () {
        var _this = this;
        this.settingsProvider.getBankLookAndFeel().then(function (lookandfeel) {
            var color = lookandfeel.primary_color;
            var image = "";
            if (_this.base_64_image) {
                image = "<img src=\"" + _this.image + "\" width=\"278px\">";
            }
            else if (_this.product.bannerurl) {
                image = "<img src=\"" + _this.image + "\" width=\"278px\">";
            }
            var title = "";
            if (_this.product.title) {
                title = "<h1>" + _this.product.title + "</h1>";
            }
            var sub_title = "";
            if (_this.product.sub_title) {
                sub_title = "<h2>" + _this.product.sub_title + "</h2>";
            }
            var account_types_id = "";
            if (_this.product.account_types_id) {
                for (var _i = 0, _a = _this.product.account_types_id; _i < _a.length; _i++) {
                    var typeaccout = _a[_i];
                    account_types_id += "\n          <h2>" + typeaccout + "</h2>";
                }
            }
            var body = "";
            if (_this.product.description) {
                body = "<p>" + _this.product.description + "</p>";
            }
            var buttons = "";
            if (_this.product.productbutton) {
                for (var _b = 0, _c = _this.product.productbutton; _b < _c.length; _b++) {
                    var action = _c[_b];
                    buttons += "\n            <button ion-button small round>" + action.title + "</button>        ";
                }
            }
            switch (_this.deviceType) {
                case 'ios':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n          <html>\n          <head>\n          <style>\n          @font-face {\n            font-family: 'Ionicons';\n            src: url(assets/fonts/ionicons.woff2);\n          }\n          body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n          .nav-bar{position:fixed; height: 70px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n          .logo{height: 60%; width: auto;}\n          .content{position:fixed;height: 438px; overflow: scroll; margin-top: 70px; min-width: 300px;}\n          .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n          .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n          .tab:not(:last-child){border-right: 1px solid #fff}\n          .icon {\n            margin-top: 5px;\n            width: inherit;\n            font-size: 1.4em;\n            color: #ffffff;\n            font-family: \"Ionicons\";\n            height: 30px;\n          }\n          .tab-title {\n            position: absolute;\n            bottom: 5px;\n            width: inherit;\n            font-size: 0.7em;\n            color: #ffffff;\n            font-family: \"Ionicons\";\n          }\n          div.icon-cash:before{ content: \"\\f143\"; }\n          div.icon-pin:before{ content: \"\\f1e4\";}\n          div.icon-pricetags:before{ content: \"\\f48e\"; }\n          div.icon-text:before{ content: \"\\f24f\"; }\n          div.icon-settings::before{ content: \"\\f20d\"; }\n          .padding{ padding: 10px; }\n          .card {border: 1px solid #dedede; overflow: hidden; }\n          .list{\n            background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='%23c8c7cc'/></svg>\");\n            padding-right: 32px;\n            background-position: right 14px center;\n            background-repeat: no-repeat;\n            background-size: 14px 14px;\n            padding-left: 10px;\n            padding: 5px;\n            border: 1px solid #efefee;\n            margin-bottom: 20px\n          }\n          h1, h2, p{color: " + color + " }\n          div.card h1{font-size: 1.5em; font-weight: bold; }\n          h2{font-size: 1.2em; font-weight: bold; }\n          div.list h1{font-size: 1.1em; font-weight: bold;}\n          button{\n            padding: 5px;\n            border-radius: 10px;\n            background-color: " + color + ";\n            color: white;\n            display: block;\n          }\n        </style>\n          </head>\n          <body>\n            <div class=\"nav-bar\">\n              <img class=\"statusbar-img \" src=\"assets/images/ios-statusbar.png\" width=\"320\">\n              <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n            </div>\n            <div class=\"content padding\">\n              <div class=\"card padding\">\n                " + image + "\n                " + title + "\n                " + sub_title + "\n                " + body + "\n                " + buttons + "\n                \n              </div>\n            </div>\n            <div class=\"tab-bar\">\n                <div class=\"tab\">\n                  <div class=\"icon icon-cash\"></div><div class=\"tab-title\">Accounts</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pin\"></div><div class=\"tab-title\">Near Me</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pricetags\"></div><div class=\"tab-title\">Products</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-text\"></div><div class=\"tab-title\">Messages</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-settings\"></div><div class=\"tab-title\">Settings</div>\n                </div>\n              </div>\n          </body>\n          </html>\n        ");
                    break;
                case 'android':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n          <html>\n          <head>\n          <style>\n          @font-face {\n            font-family: 'Ionicons';\n            src: url(assets/fonts/ionicons.woff2);\n          }\n          body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n          .nav-bar{position:fixed; height: 70px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n          .logo{height: 60%; width: auto;}\n          .content{position:fixed;height: 438px; overflow: scroll; margin-top: 70px; min-width: 300px;}\n          .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n          .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n          .tab:not(:last-child){border-right: 1px solid #fff}\n          .icon {\n            margin-top: 5px;\n            width: inherit;\n            font-size: 1.4em;\n            color: #ffffff;\n            font-family: \"Ionicons\";\n            height: 30px;\n          }\n          .tab-title {\n            position: absolute;\n            bottom: 5px;\n            width: inherit;\n            font-size: 0.7em;\n            color: #ffffff;\n            font-family: \"Ionicons\";\n          }\n          div.icon-cash:before{ content: \"\\f143\"; }\n          div.icon-pin:before{ content: \"\\f1e4\";}\n          div.icon-pricetags:before{ content: \"\\f48e\"; }\n          div.icon-text:before{ content: \"\\f24f\"; }\n          div.icon-settings::before{ content: \"\\f20d\"; }\n          .padding{ padding: 10px; }\n          .card {border: 1px solid #dedede; overflow: hidden; }\n          .list{\n            background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='%23c8c7cc'/></svg>\");\n            padding-right: 32px;\n            background-position: right 14px center;\n            background-repeat: no-repeat;\n            background-size: 14px 14px;\n            padding-left: 10px;\n            padding: 5px;\n            border: 1px solid #efefee;\n            margin-bottom: 20px\n          }\n          h1, h2, p{color: " + color + " }\n          div.card h1{font-size: 1.5em; font-weight: bold; }\n          h2{font-size: 1.2em; font-weight: bold; }\n          div.list h1{font-size: 1.1em; font-weight: bold;}\n          button{\n            padding: 5px;\n            border-radius: 10px;\n            background-color: " + color + ";\n            color: white;\n            display: block;\n          }\n        </style>\n          </head>\n          <body>\n            <div class=\"nav-bar\">\n              <img class=\"statusbar-img \" src=\"assets/images/ios-statusbar.png\" width=\"320\">\n              <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n            </div>\n            <div class=\"content padding\">\n              <div class=\"card padding\">\n                " + image + "\n                " + title + "\n                " + sub_title + "\n                " + body + "\n                " + buttons + "\n                \n              </div>\n            </div>\n            <div class=\"tab-bar\">\n                <div class=\"tab\">\n                  <div class=\"icon icon-cash\"></div><div class=\"tab-title\">Accounts</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pin\"></div><div class=\"tab-title\">Near Me</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pricetags\"></div><div class=\"tab-title\">Products</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-text\"></div><div class=\"tab-title\">Messages</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-settings\"></div><div class=\"tab-title\">Settings</div>\n                </div>\n              </div>\n          </body>\n          </html>\n          ");
                    break;
                default:
                    break;
            }
            _this.cdr.detectChanges();
        });
    };
    ProductPage.prototype.dirty = function () {
        this.dirtyFlag = true;
        this.updatePreview();
    };
    ProductPage.prototype.uploadImage = function (event) {
        this.base_64_image = event.target.result;
        this.dirty();
    };
    ProductPage.prototype.removeImage = function () {
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel("Confirm Delete"),
            message: self.settingsProvider.getLabel("Are you sure you want to delete this Image?"),
            buttons: [
                {
                    text: self.settingsProvider.getLabel("Cancel"),
                    role: self.settingsProvider.getLabel("cancel"),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: self.settingsProvider.getLabel("Delete"),
                    handler: function (data) {
                        console.log(self.base_64_image, "removeImage");
                        self.product.bannerurl = null;
                        self.base_64_image = null;
                        self.product.imagepath = null;
                        // self.product.image = null;
                        self.dirty();
                    }
                }
            ]
        });
        alert.present();
    };
    ProductPage.prototype.addAction = function () {
        var self = this;
        var modal = this.modalCtrl.create(AddActionModal); //,{"status":true}
        modal.onDidDismiss(function (data) {
            if (data) {
                self.product.productbutton.push(data);
                self.dirty();
            }
        });
        modal.present();
    };
    ProductPage.prototype.changeListener = function ($event) {
        console.log(this.globalProvider.getBaseUrl(), "getBaseUrl");
        var self = this;
        var formData = new FormData();
        formData.append("file-to-upload", $event.target.files[0]);
        this.productsProvider.upload_Image(formData)
            .then(function (data) {
            var fileurl = self.globalProvider.getBaseUrl() + data.filename;
            self.product.bannerurl = data.filename;
            self.product.imagepath = fileurl;
            self.image = fileurl;
            self.updatePreview();
            console.log(fileurl, "fileurl");
        }).catch(function (err) {
            console.log(err, "erorrrrr");
        });
    };
    ProductPage.prototype.renderImage = function (file) {
        var self = this;
        // generate a new FileReader object
        var reader = new window.FileReader();
        // inject an image with the src url
        reader.onload = function (event) {
            var the_url = event.target.result;
            console.log("url", the_url);
            self.product.bannerurl = the_url;
            self.updatePreview();
            // self.viewCtrl.dismiss({image: the_url});
        };
        // when the file is read it triggers the onload event above.
        reader.readAsDataURL(file);
    };
    ProductPage.prototype.deleteAction = function (action, title) {
        var self = this;
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Confirm Delete"),
            message: this.settingsProvider.getLabel("Are you sure you want to delete") + " " + action.title + "?",
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Cancel"),
                    role: this.settingsProvider.getLabel("cancel"),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.settingsProvider.getLabel("Delete"),
                    handler: function (data) {
                        var actions = [];
                        for (var _i = 0, _a = self.product.productbutton; _i < _a.length; _i++) {
                            var a = _a[_i];
                            console.log(a.title + " --- " + title);
                            if (a.title !== title) {
                                actions.push(a);
                            }
                        }
                        self.product.productbutton = actions;
                        self.dirty();
                    }
                }
            ]
        });
        alert.present();
    };
    ProductPage.prototype.save = function () {
        var msg = [];
        if (this.product.title.length <= 0)
            msg.push(this.settingsProvider.getLabel("Product Title cannot be blank"));
        if (new Date(this.product.start_date) >= new Date(this.product.end_date))
            msg.push("End Date must be after start date.");
        if (msg.length > 0) {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                message: msg.join("<br/>"),
                buttons: [
                    {
                        role: 'cancel',
                        text: this.settingsProvider.getLabel('Ok'),
                        handler: function (data) {
                        }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            var self_1 = this;
            var alert_2 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel("Confirm Save"),
                message: this.settingsProvider.getLabel("Are you sure you want to Save this product?"),
                buttons: [
                    {
                        text: this.settingsProvider.getLabel("Cancel"),
                        role: this.settingsProvider.getLabel("cancel"),
                        handler: function (data) {
                        }
                    },
                    {
                        text: this.settingsProvider.getLabel("Ok"),
                        role: this.settingsProvider.getLabel("Ok"),
                        handler: function (data) {
                            // self.navCtrl.pop();
                            self_1.callback(self_1.product);
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    ProductPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    ProductPage.prototype.showCampaign = function (campaign) {
        if (campaign === void 0) { campaign = null; }
        // this.navCtrl.push("ProductCampaignPage", {campaign: campaign, callback: this.saveProductCampaign.bind(this)});
    };
    ProductPage.prototype.onFileChange = function (csv) {
        var _this = this;
        var csvcustomer = [];
        var file = csv.target.files[0];
        var self = this;
        var formData = new FormData();
        formData.append("file-to-upload", file);
        formData.append("products_id", this.product.products_id);
        this.productsProvider.updateProduct_TargetCustomer(formData)
            .then(function (result) {
            self.getcustomerbyproductid();
            self.csvfile.nativeElement.value = "";
            if (result.status) {
                var toast = self.toastCtrl.create({
                    message: result.msg,
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                _this.getcustomerbyproductid;
            }
        }).catch(function (err) {
            console.log(err, "erorrrrr");
            self.csvfile.nativeElement.value = "";
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel("No Records found as Member ID is fake."),
                cssClass: 'error',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    };
    ProductPage.prototype.downloadCsv = function () {
        var csvContent = "MemberID\n";
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvContent);
        hiddenElement.target = '_blank';
        hiddenElement.download = this.getCsvFileName();
        hiddenElement.click();
    };
    ProductPage.prototype.getCsvFileName = function () {
        return "consectus-" + new Date().getTime() + ".csv";
    };
    ProductPage.prototype.showCustomer = function (item) { };
    ProductPage.prototype.editAction = function (action, title) {
        var self = this;
        var modal = this.modalCtrl.create(AddActionModal, { action: action }); //,{"status":true}
        modal.onDidDismiss(function (data) {
            console.log("on dismissdata", data);
            var arr = [];
            if (data) {
                for (var _i = 0, _a = self.product.productbutton; _i < _a.length; _i++) {
                    var p = _a[_i];
                    if (p.title == title) {
                        arr.push(data);
                    }
                    else {
                        arr.push(p);
                        self.dirty();
                    }
                }
                self.product.productbutton = arr;
            }
            else {
            }
        });
        modal.present();
    };
    ProductPage.prototype.onAccTypeSelect = function () {
        console.log(this.account_types_id, "account_types_id 123");
        this.dirty();
    };
    // onAccTypeSelect() {
    //   console.log("log", this.productType);
    //   this.productType.slice(0, 1);
    //   console.log("log", this.productType);
    // }
    // if (this.accountRuleType == "selected") {
    //   this.errorAlert("Please select account type");
    // } else {
    //   self.addRuleEngine();
    // }
    ProductPage.prototype.saveAsClone = function () {
        var self = this;
        if (this.productTypes == "selected") {
            this.errorAlert("Please select account type");
        }
        else {
            return new Promise(function (resolve, reject) {
                self.productsProvider.addProduct(self.product)
                    .then(function () {
                    var toast = self.toastCtrl.create({
                        message: self.settingsProvider.getLabel("Product was added successfully"),
                        duration: 3000,
                        position: 'right'
                    });
                    toast.present();
                    self.viewCtrl.dismiss();
                    resolve();
                });
            });
        }
    };
    ProductPage.prototype.pushButton = function () {
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel("Confirm"),
            message: self.settingsProvider.getLabel("Are you sure want to push this product?"),
            buttons: [
                {
                    text: self.settingsProvider.getLabel("Cancel"),
                    role: self.settingsProvider.getLabel("cancel"),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: self.settingsProvider.getLabel("Ok"),
                    handler: function (data) {
                        self.productsProvider.push_Product(self.product)
                            .then(function (data) {
                            console.log(data);
                            var toast = self.toastCtrl.create({
                                message: self.settingsProvider.getLabel("Notification send successfully"),
                                duration: 3000,
                                position: 'right'
                            });
                            toast.present();
                            self.viewCtrl.dismiss();
                        }).catch(function (err) {
                            console.log(err);
                        });
                    }
                }
            ],
            enableBackdropDismiss: true
        });
        alert.present();
    };
    return ProductPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["_12" /* ViewChild */])("fileInput"),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4__angular_core__["u" /* ElementRef */])
], ProductPage.prototype, "csvfile", void 0);
ProductPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* IonicPage */])({ name: "ProductPage" }),
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["n" /* Component */])({
        selector: 'page-product',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\product\product.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons start>\n\n    </ion-buttons>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <button *ngIf="product.products_id!=undefined" small ion-button color="primary"\n\n            (click)="pushButton()">{{settingsProvider.getLabel(\'Push\')}}</button>\n\n          <ion-buttons end>\n\n\n\n            <button ion-button small round icon-left color="primary" (click)="save()" *ngIf="dirtyFlag">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-cell">\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row class="header">\n\n                  <ion-col>\n\n                    {{settingsProvider.getLabel("Account Type")}}\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-select [(ngModel)]="product.account_types_id" (ionChange)="onAccTypeSelect()"\n\n                      (input)="dirty()">\n\n                      <ion-option selected value="selected">Please Select Account Type</ion-option>\n\n                      <ion-option *ngFor="let product of productTypes" [value]="product.account_types_id">\n\n                        {{product.accountname}}</ion-option>\n\n                    </ion-select>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row class="header">\n\n                  <ion-col>\n\n                    {{settingsProvider.getLabel(\'Banner Image\')}}\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row padding>\n\n                  <ion-col>\n\n                    <img class="imgUpload" *ngIf="product.imagepath" style="width:50%; height:50%"\n\n                      [src]="product.imagepath" alt="">\n\n                    <label style="border-radius:5px;"> {{settingsProvider.getLabel(\'Select Image\')}}\n\n                      <input type="file" id="File" accept="image/*" id="imageupload" (change)="changeListener($event)"\n\n                        (input)="dirty()">\n\n                    </label>\n\n                    <div padding>\n\n                      <span\n\n                        *ngIf="!product.imagepath">{{settingsProvider.getLabel("Add a banner Image to the product by uploading an image")}}</span>\n\n                      <button float-right ion-button round small color="danger" (click)="removeImage()"\n\n                        *ngIf="product.imagepath">{{settingsProvider.getLabel("Remove Image")}}</button>\n\n                    </div>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row align-items-center>\n\n                  <ion-col>\n\n\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row class="header">\n\n                  <ion-col>\n\n                    {{settingsProvider.getLabel(\'Product content\')}}\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-list>\n\n                      <ion-item>\n\n                        <ion-label stacked>{{settingsProvider.getLabel(\'Title\')}}</ion-label>\n\n                        <ion-input type="text" [(ngModel)]="product.title"\n\n                          placeholder="{{settingsProvider.getLabel(\'Product Title\')}}" (input)="dirty()"></ion-input>\n\n                      </ion-item>\n\n                      <ion-item>\n\n                        <ion-label stacked>{{settingsProvider.getLabel(\'Sub Title\')}}</ion-label>\n\n                        <ion-input type="text" [(ngModel)]="product.sub_title"\n\n                          placeholder="{{settingsProvider.getLabel(\'Product Sub Title\')}}" (input)="dirty()">\n\n                        </ion-input>\n\n                      </ion-item>\n\n                      <ion-item>\n\n                        <ion-label stacked>{{settingsProvider.getLabel(\'Body\')}}</ion-label>\n\n                        <ion-textarea [(ngModel)]="product.description"\n\n                          placeholder="{{settingsProvider.getLabel(\'Body\')}}" (input)="dirty()"></ion-textarea>\n\n                      </ion-item>\n\n                    </ion-list>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <p float-right>{{settingsProvider.getLabel(\'Note: You can use HTML Tags in the body\')}}</p>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row align-items-center class="header">\n\n                  <ion-col>{{settingsProvider.getLabel(\'Action Buttons\')}}</ion-col>\n\n                </ion-row>\n\n                <ion-row *ngFor="let action of product.productbutton">\n\n                  <ion-col text-nowrap>\n\n                    <button ion-button color="danger" clear icon-only float-right\n\n                      (click)="deleteAction(action, action.title)">\n\n                      <ion-icon name="trash"></ion-icon>\n\n                    </button>\n\n                    <button ion-button clear icon-only float-right (click)="editAction(action, action.title)">\n\n                      <ion-icon name="create"></ion-icon>\n\n                    </button>\n\n                    <h5>{{action.title}}</h5>\n\n                    <div float-left *ngIf="action.type==\'url\'">{{settingsProvider.getLabel(\'URL\')}} &nbsp;</div>\n\n                    <div float-left *ngIf="action.type==\'call\'">{{settingsProvider.getLabel(\'Call Number\')}} &nbsp;\n\n                    </div>\n\n                    <div float-left *ngIf="action.type==\'callback\'">{{settingsProvider.getLabel(\'Callback Request\')}}\n\n                      &nbsp;\n\n                    </div>\n\n                    <a *ngIf="action.type==\'url\'" [href]="action.action" [target]="\'_blank\'">{{action.action}}</a>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <button small ion-button (click)="addAction()">{{settingsProvider.getLabel("Add Action")}}</button>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n\n\n      <ion-col col-6 *ngIf="isEdge ==true">\n\n        <ion-grid class="table-cell">\n\n          <ion-row justify-content-center>\n\n            <ion-col text-center text-nowrap padding>\n\n              <div class="header">{{settingsProvider.getLabel(\'Live Product Preview\')}}</div>\n\n              <ion-segment [(ngModel)]="deviceType" padding-top padding-bottom>\n\n                <ion-segment-button value="ios">{{settingsProvider.getLabel("iOS")}}</ion-segment-button>\n\n                <ion-segment-button value="android">{{settingsProvider.getLabel("Android")}}</ion-segment-button>\n\n              </ion-segment>\n\n              <img src="https://img.icons8.com/color/48/000000/error.png">\n\n              <h3>Preview feature is not compatible with the Edge Browser.</h3>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n\n\n      <ion-col col-6 *ngIf="isEdge ==false">\n\n        <ion-grid class="table-cell">\n\n          <ion-row justify-content-center>\n\n            <ion-col text-center text-nowrap padding>\n\n              <div class="header">{{settingsProvider.getLabel(\'Live Product Preview\')}}</div>\n\n              <ion-segment [(ngModel)]="deviceType" padding-top padding-bottom>\n\n                <ion-segment-button value="ios">{{settingsProvider.getLabel("iOS")}}</ion-segment-button>\n\n                <ion-segment-button value="android">{{settingsProvider.getLabel("Android")}}</ion-segment-button>\n\n              </ion-segment>\n\n              <div class="preview" id="demo-device-ios" *ngIf="deviceType==\'ios\'">\n\n                <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n              </div>\n\n              <div class="preview" id="demo-device-android" *ngIf="deviceType==\'android\'">\n\n                <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n\n\n    <ion-row *ngIf="!new">\n\n      <ion-col>\n\n        <ion-grid class="table-cell">\n\n          <ion-row justify-content-center>\n\n            <ion-col text-center text-nowrap padding>\n\n              <div class="header">\n\n                {{settingsProvider.getLabel(\'Targeted Customers\')}}\n\n              </div>\n\n\n\n              <a (click)="downloadCsv()" ion-button small clear style="margin-top: 10px; float:right;">\n\n                {{settingsProvider.getLabel(\'Download Sample CSV\')}}\n\n              </a>\n\n\n\n              <div class="button_labels">\n\n                <label for="uploadFile">{{settingsProvider.getLabel(\'Upload CSV\')}}</label>\n\n                <input type="file" id="uploadFile" (change)="onFileChange($event)" #fileInput>\n\n                <label for="uploadFile" style=" margin-left: 0px;"\n\n                  *ngIf="isdelivered">{{settingsProvider.getLabel(\'Target all Customers\')}}</label>\n\n              </div>\n\n\n\n              <table-component [options]="productOptions"></table-component>\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\product\product.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1__providers_customers_customers__["a" /* CustomersProvider */],
        __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_4__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_products_products__["a" /* ProductsProvider */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_9__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_10__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], ProductPage);

var AddActionModal = (function () {
    function AddActionModal(alertCtrl, languageProvider, viewCtrl, navCtrl, navParams, settingsProvider) {
        this.alertCtrl = alertCtrl;
        this.languageProvider = languageProvider;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.settingsProvider = settingsProvider;
        this.title = "";
        this.type = "";
        this.titleName = "Add Action";
        this.languageString = "en";
        this.resultdata = null;
        this.action = navParams.get("action");
        if (this.action !== undefined) {
            console.log("action", this.action);
            this.title = this.action.title;
            this.type = this.action.type;
            this.action = this.action.action;
            this.titleName = "Update Action";
        }
    }
    AddActionModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddActionModal.prototype.save = function () {
        var errors = [];
        if (this.title.length == 0) {
            errors.push(this.settingsProvider.getLabel("Title cannot be blank"));
        }
        if (this.type.length == 0) {
            errors.push(this.settingsProvider.getLabel("Type must be selected"));
        }
        if (this.action == undefined && this.type !== "callback") {
            errors.push(this.settingsProvider.getLabel("Action must be provided"));
        }
        if (errors.length > 0) {
            var alert_3 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel("Error"),
                message: errors.join('<br/> '),
                buttons: [
                    {
                        text: this.settingsProvider.getLabel("Ok"),
                        role: this.settingsProvider.getLabel("cancel"),
                    }
                ]
            });
            alert_3.present();
        }
        else {
            this.viewCtrl.dismiss({ title: this.title, type: this.type, action: this.action });
        }
    };
    return AddActionModal;
}());
AddActionModal = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["n" /* Component */])({
        selector: 'add-action',
        template: "\n  <ion-header color=\"secondary\">\n    <ion-navbar>\n      <ion-buttons start>\n        <button ion-button icon-left (click)=\"dismiss()\" style=\"color:white\">\n          <ion-icon name=\"arrow-round-back\"></ion-icon>\n          {{settingsProvider.getLabel('Back')}}\n        </button>\n      </ion-buttons>\n      <ion-title>{{settingsProvider.getLabel(titleName)}}</ion-title>\n      <ion-buttons end>\n        <button ion-button icon-left (click)=\"save()\" style=\"color:white\">\n          <ion-icon name=\"ios-checkmark\"></ion-icon>\n          {{settingsProvider.getLabel('Save')}}\n        </button>\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>\n  <ion-content padding>\n      <ion-list padding>\n        <ion-item padding>\n          <ion-label stacked>{{settingsProvider.getLabel('Title')}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"title\"></ion-input>\n        </ion-item>\n        <ion-item style=\"padding-right:16px; padding-bottom:10px;\">\n          <ion-label stacked>{{settingsProvider.getLabel('Type')}}</ion-label>\n          <ion-select [(ngModel)]=\"type\">\n            <ion-option value=\"url\">URL</ion-option>\n            <ion-option value=\"call\">Call Number</ion-option>\n            <ion-option value=\"callback\">Call Back</ion-option>\n          </ion-select>\n        </ion-item>\n        <ion-item *ngIf=\"type!='callback'\" style=\"padding-right:16px; padding-bottom:16px;\">\n          <ion-label stacked>{{settingsProvider.getLabel('Action')}}</ion-label>\n          <ion-input [(ngModel)]=\"action\"></ion-input>\n        </ion-item>\n      </ion-list>\n  </ion-content>\n  "
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_0__providers_settings_settings__["a" /* SettingsProvider */]])
], AddActionModal);

//# sourceMappingURL=product.js.map

/***/ }),

/***/ 496:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_table_table__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_products_products__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ProductsPage = (function () {
    function ProductsPage(languageProvider, oauthProvider, productsProvider, modalCtrl, toastCtrl, navCtrl, navParams, globalProvider, loadauditProvider, events, settingsProvider, viewCtrl, masterpermissionProvider) {
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.productsProvider = productsProvider;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.globalProvider = globalProvider;
        this.loadauditProvider = loadauditProvider;
        this.events = events;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.productOptions = {
            data: [],
            filteredData: [],
            columns: {
                "title": { title: "Title", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "sub_title": { title: "Sub Title", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                // "description": { title: "Description", mobile: true, col: 3, format: 'function', fn: this.commonFunction.bind(this) },
                "target": { title: "Target", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "delivered": { title: "Delivered", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "viewed": { title: "Viewed", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "interested": { title: "Interested", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.delete.bind(this),
                    color: "danger"
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Product",
                addAction: this.create.bind(this),
            },
            tapRow: this.edit.bind(this),
            // resetFilter: null,
            uploadOptions: {},
        };
        this.product1Options = {
            data: [],
            filteredData: [],
            columns: {
                "title": { title: "Title", search: true, mobile: true, col: 2, format: 'function', fn: this.commonFunction.bind(this) },
                "sub_title": { title: "Sub Title", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "description": { title: "Description", mobile: true, col: 3, format: 'function', fn: this.commonFunction.bind(this) },
                "target": { title: "Target", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "delivered": { title: "Delivered", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "viewed": { title: "Viewed", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "interested": { title: "Interested", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Activate",
                    action: this.activate.bind(this),
                    color: 'primary'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Product",
                addAction: this.create.bind(this),
            },
            tapRow: this.edit.bind(this),
            uploadOptions: {},
        };
        this.currentTab = "activeProducts";
        this.addActionTitle = "Add Product";
        this.data = [];
        this.languageString = "en";
        this.resultdata = null;
        this.title = "Products Management | Consectus";
    }
    ProductsPage.prototype.commonFunction = function (nullData) {
        var str = "";
        if (nullData == 0) {
            str = '--';
        }
        else if (nullData == null) {
            str = '--';
        }
        else if (nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    ProductsPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("products")) {
            return true;
        }
        else {
            return false;
        }
    };
    ProductsPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    ProductsPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        var self = this;
        this.getArchiveProducts();
        if (self.oauthProvider.user_role.products) {
            self.getProducts();
        }
        else {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.setRoot("DashboardPage");
        }
    };
    ProductsPage.prototype.getProducts = function () {
        var _this = this;
        var self = this;
        this.productsProvider.getproduct()
            .then(function (result) {
            self.productOptions.data = _this.archive(result.data, 1);
            self.productOptions = Object.assign({}, self.productOptions);
        }).catch(function (error) {
        });
    };
    ProductsPage.prototype.getArchiveProducts = function () {
        var _this = this;
        var self = this;
        this.productsProvider.getproduct()
            .then(function (result) {
            self.product1Options.data = _this.archive(result.data, 0);
            self.product1Options = Object.assign({}, self.product1Options);
        }).catch(function (error) {
        });
    };
    ProductsPage.prototype.segmentChanged = function (event) {
        console.log("eventValue", event._value);
        switch (event._value) {
            case 'activeProducts':
                this.getProducts();
                break;
            case 'archiveProducts':
                this.getArchiveProducts();
                break;
            default:
                break;
        }
    };
    ProductsPage.prototype.edit = function (item) {
        this.navCtrl.push("ProductPage", { product: item, callback: this.updateProduct.bind(this) });
    };
    ProductsPage.prototype.delete = function (items) {
        var _this = this;
        console.log("items", items);
        var d = [];
        var self = this;
        console.log("delete product");
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            d.push(item.products_id);
        }
        var prodcutIds = d.join(',');
        self.productsProvider.deleteProduct(prodcutIds).then(function (result) {
            self.getProducts();
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
            var message = "Product is Archived";
            ;
            var auditUser = {
                "users_id": _this.oauthProvider.user[0].users_id,
                "customer_devices_id": 0,
                "usertype": "staff",
                "uuid": "",
                "appname": "hub",
                "event_type": "view",
                "screen": "products",
                "trail_details": message
            };
            _this.loadauditProvider.loadaudit(auditUser)
                .then(function (data) {
            });
        });
    };
    ProductsPage.prototype.activate = function (productsId) {
        var _this = this;
        var d = [];
        var self = this;
        console.log("delete product");
        for (var _i = 0, productsId_1 = productsId; _i < productsId_1.length; _i++) {
            var item = productsId_1[_i];
            d.push(item.products_id);
        }
        var prodcutIds = d.join(',');
        self.productsProvider.activateProduct(prodcutIds).then(function (result) {
            if (result.status) {
                self.getArchiveProducts();
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel(result.message),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                var message = "Product is Activated";
                ;
                var auditUser = {
                    "users_id": _this.oauthProvider.user[0].users_id,
                    "customer_devices_id": 0,
                    "usertype": "staff",
                    "uuid": "",
                    "appname": "hub",
                    "event_type": "view",
                    "screen": "products",
                    "trail_details": message
                };
                _this.loadauditProvider.loadaudit(auditUser)
                    .then(function (data) {
                });
            }
        });
    };
    ProductsPage.prototype.push = function (item) {
        console.log("push product");
    };
    ProductsPage.prototype.create = function () {
        this.navCtrl.push("ProductPage", { callback: this.addProduct.bind(this) });
    };
    ProductsPage.prototype.updateProduct = function (product) {
        var self = this;
        product.isactive = 1;
        return new Promise(function (resolve, reject) {
            var _this = this;
            console.log(JSON.stringify(product), "product123");
            self.productsProvider.updateProduct(product)
                .then(function () {
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel('Product was updated successfully'),
                    duration: 3000,
                    position: 'bottom',
                    cssClass: 'success',
                });
                toast.present();
                _this;
                resolve();
            });
        });
    };
    ProductsPage.prototype.addProduct = function (product) {
        var self = this;
        product.isactive = 1;
        console.log(JSON.stringify(product));
        return new Promise(function (resolve, reject) {
            self.productsProvider.addProduct(product)
                .then(function () {
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel('Product was added successfully\nPlease update product with target customer data.'),
                    duration: 3000,
                    position: 'right',
                    cssClass: "success"
                });
                toast.present();
                resolve();
                self.navCtrl.pop();
            });
        });
    };
    ProductsPage.prototype.archive = function (archiveData, activestatus) {
        var archive = [];
        for (var i in archiveData) {
            if (archiveData[i].isactive == activestatus) {
                archive.push(archiveData[i]);
            }
        }
        return archive;
    };
    return ProductsPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["_12" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_0__components_table_table__["a" /* TableComponent */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__components_table_table__["a" /* TableComponent */])
], ProductsPage.prototype, "table", void 0);
ProductsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["n" /* Component */])({
        selector: 'page-products',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\products\products.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(title)}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n    <ion-segment-button value="activeProducts">\n\n      {{settingsProvider.getLabel(\'Active\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="archiveProducts">\n\n      {{settingsProvider.getLabel(\'Archive\')}}\n\n    </ion-segment-button>\n\n  </ion-segment>\n\n\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'activeProducts\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <table-component [options]="productOptions"></table-component>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'archiveProducts\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <table-component [options]="product1Options"></table-component>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\products\products.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_products_products__["a" /* ProductsProvider */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_8__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], ProductsPage);

//# sourceMappingURL=products.js.map

/***/ }),

/***/ 497:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_customer_customer__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var CustomerPage = (function () {
    function CustomerPage(navCtrl, navParams, oauthProvider, alertCtrl, toastCtrl, cdr, customerProvider, events, languageProvider, loadauditProvider, global, settingsProvider, viewCtrl, masterpermissionProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.oauthProvider = oauthProvider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.cdr = cdr;
        this.customerProvider = customerProvider;
        this.events = events;
        this.languageProvider = languageProvider;
        this.loadauditProvider = loadauditProvider;
        this.global = global;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.customer = { accounts: [] };
        this.transactions = {};
        this.auditlogs = [];
        this.regularSavings = [];
        this.cheques = [];
        this.accounts = [];
        this.savingGoals = [];
        this.customerDetailsData = [];
        this.options1 = {
            data: [],
            filteredData: [],
            columns: {
                "amount": { title: "Amount", format: 'currency' },
                "transferaccount": { title: "Transfer Account", search: true },
                "transactiondate": { title: "Transaction Date", format: "function", fn: this.formatDateTimeDate.bind(this), search: true },
                "status": { title: "Status", search: true },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [
                {
                    title: "Delete",
                    color: 'danger'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editCustomer.bind(this),
            columnClass: "table-column",
            resetFilter: null,
            uploadOptions: {},
        };
        this.audit_options = {
            data: [],
            filteredData: [],
            columns: {
                "appname": { title: "App Name", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "event_type": { title: "Event Type", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "screen": { title: "Screen Name", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "trail_details": { title: "Trail Details", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "create_date": { title: "Date", format: 'function', fn: this.formatDateTimeDate.bind(this), search: true },
                "devicetype": { title: "Device Type", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                // "customer_devices_id": { title: "Customer Device ID", search: true },
                "uuid": { title: "UUID", search: true, format: 'function', fn: this.commonFunction.bind(this) },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [
                {
                    title: "Delete",
                    color: 'danger'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editCustomer.bind(this),
            uploadOptions: {},
        };
        this.savinggoal_options = {
            data: [],
            filteredData: [],
            columns: {
                "title": { title: "Title", search: true },
                "target_date": { title: "Target Date", format: 'function', fn: this.formatDateTimeDate.bind(this) },
                "amount": { title: "Amount", search: true, format: "currency" },
                "target_month": { title: "Target Amount", search: true },
                "isactive": { title: "Status", format: 'function', fn: this.getStatus.bind(this) },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editCustomer.bind(this),
            uploadOptions: {}
        };
        this.regular_saving_option = {
            data: [],
            filteredData: [],
            columns: {
                "amount": { title: "Amount", format: "currency", search: true },
                "account_number": { title: "Account", search: true },
                "current_account": { title: "Current Account", search: true },
                "reference": { title: "Reference" },
                "num_months": { title: "No. of Months" },
                "create_date": { title: "Created Date", format: 'function', fn: this.formatDateTimeDate.bind(this) },
                "modified_date": { title: "Modified Date", format: 'function', fn: this.formatDateTimeDate.bind(this) },
                "isactive": { title: "Status", format: 'function', fn: this.getStatus.bind(this) }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [
                {
                    title: "Delete",
                    color: 'danger'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editCustomer.bind(this),
            uploadOptions: {}
        };
        this.cheque_option = {
            data: [],
            filteredData: [],
            columns: {
                "payee": { title: "Payee", search: true },
                "amount": { title: "Amount", format: "currency", search: true },
                "reference": { title: "Reference", search: true },
                "frontimage": { title: "Front Image", format: 'function', fn: this.getImage.bind(this) },
                "backimage": { title: "Back Image", format: 'function', fn: this.getBackImage.bind(this) },
                "deposited_date": { title: "Deposit Date", format: 'function', fn: this.formatDate.bind(this) },
                "isactive": { title: "Status", format: 'function', fn: this.getStatus.bind(this) }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [
                {
                    title: "Delete",
                    color: 'danger'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editCustomer.bind(this),
            uploadOptions: {}
        };
        this.customerData = {
            id: new Date().getTime().toString(),
            customerId: "",
            firstname: "",
            lastname: "",
            dob: new Date().toISOString(),
            accountId: "",
            mobile: "",
            status: ""
        };
        this.type = 'password';
        this.showPass = false;
        this.resultdata = null;
        this.languageString = "en";
        this.events.subscribe("consectus:language", function (language) {
            _this.languageString = language;
            console.log("subscribe language", _this.languageString);
        });
        this.getregistrationfields = navParams.get('getregistrationfields');
        this.customer = this.navParams.get('customer');
        // this.customer = JSON.parse(JSON.stringify(navParams.get('customer')));
        console.log("customer", this.customer);
        if (this.customer) {
            this.customer.passcode = this.generatePassword();
            this.title = "View Customer Details";
            this.name = this.customer.firstname + " " + this.customer.lastname;
            this.accounts = this.customer.accounts;
            this.currenTab = "savings";
            console.log("accounts", this.accounts);
            var self_1 = this;
            self_1.customerProvider.getAuditLogs(this.customer)
                .then(function (data) {
                if (data != null) {
                    self_1.auditlogs = data.sort(function (a, b) {
                        if (new Date(a.date) < new Date(b.date))
                            return -1;
                        else if (new Date(a.date) > new Date(b.date))
                            return 1;
                        else
                            return 0;
                    });
                    self_1.audit_options.data = self_1.auditlogs;
                    _this.audit_options = Object.assign({}, self_1.audit_options);
                }
            });
            self_1.customerProvider.getAllCheques(this.customer)
                .then(function (data) {
                if (data != null) {
                    self_1.cheques = data;
                }
            });
            console.log("cheque_option", self_1.cheque_option);
        }
        else {
            this.customer = {
                firstmame: "",
                lastname: "",
                dob: new Date().toISOString(),
                mobile: "",
                customer_status: "",
                emailid: ""
            };
            console.log("custmerData", this.customerData);
        }
    }
    CustomerPage.prototype.commonFunction = function (nullData) {
        var str = "";
        if (nullData == 0) {
            str = '--';
        }
        else if (nullData == null) {
            str = '--';
        }
        else if (nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    CustomerPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("customers")) {
            return true;
        }
        else {
            return false;
        }
    };
    CustomerPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        else {
            var self_2 = this;
            var message = "Viewed " + this.customer.customers_id + " in Customer";
            var auditUser = {
                "users_id": this.oauthProvider.user[0].users_id,
                "customer_devices_id": 0,
                "usertype": "staff",
                "uuid": "",
                "appname": "hub",
                "event_type": "view",
                "screen": "customer",
                "trail_details": message
            };
            this.loadauditProvider.loadaudit(auditUser)
                .then(function (data) {
            });
        }
    };
    CustomerPage.prototype.formatDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    CustomerPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        this.getCustomerDetails(this.customer);
        if (this.accounts) {
            this.acc = this.accounts.filter(function (item) {
                return item.account_number == _this.currenTab;
            });
            console.log("Entry of account", this.acc[0]);
            this.getTransactions(this.acc[0]);
        }
    };
    // generate passcode for the customer pascode
    CustomerPage.prototype.generatePassword = function () {
        var length = 3, charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    };
    CustomerPage.prototype.segmentChanged = function (event) {
        console.log("event", this.currenTab);
        if (this.currenTab == "saving") {
            for (var i = 0; i < this.accounts.length; i++) {
                if (this.accounts[i].account_number == event.value) {
                    this.getTransactions(this.accounts[i]);
                }
            }
            this.cdr.detectChanges();
        }
        else {
            this.customerAcc = null;
        }
    };
    CustomerPage.prototype.selectAccount = function (account) {
        this.customerAcc = account;
    };
    CustomerPage.prototype.setAccount = function () {
        var self = this;
        if (self.customerAcc !== null) {
            console.log(self.customerAcc.transactions, "888888888");
            self.options1 = Object.assign({}, self.options1);
        }
        else {
            self.options1.data = [];
            self.options1 = Object.assign({}, self.options1);
        }
    };
    CustomerPage.prototype.getCustomerDetails = function (customer) {
        var _this = this;
        var self = this;
        self.customerProvider.getcustomerdetailsbycustomerid(customer).then(function (result) {
            if (result.status) {
                self.customerDetailsData = result.data;
                if (self.customerDetailsData.regularsavings.length > 0) {
                    self.regular_saving_option.data = self.customerDetailsData.regularsavings;
                    self.regular_saving_option = Object.assign({}, self.regular_saving_option);
                }
                if (self.customerDetailsData.savingsgoals.length > 0) {
                    self.savinggoal_options.data = self.customerDetailsData.savingsgoals;
                    self.savinggoal_options = Object.assign({}, self.savinggoal_options);
                }
                if (self.customerDetailsData.cheques.length > 0) {
                    self.cheque_option.data = self.customerDetailsData.cheques;
                    self.cheque_option = Object.assign({}, self.cheque_option);
                }
                if (self.customerDetailsData.audit) {
                    self.audit_options.data = self.customerDetailsData.audit;
                    _this.audit_options = Object.assign({}, self.audit_options);
                }
                console.log("customerDetailsData", self.customerDetailsData);
            }
        }).catch(function (error) {
            console.log("error", error);
        });
    };
    CustomerPage.prototype.getImage = function (image) {
        var ret = '<img class="imgUpload"  style="width:50%; height:50%" src ="' + image + '" alt="Click here to view Image">';
        return ret;
    };
    CustomerPage.prototype.savigGoalImageFn = function (image) {
        var ret = '<img class="imgUpload"  style="width:50%; height:50%" src ="' + image + '" alt="Click here to view Image">';
        return ret;
    };
    CustomerPage.prototype.getBackImage = function (image) {
        var ret = '<img class="imgUpload"  style="width:50%; height:50%" src ="' + image + '" alt="Click here to view Image">';
        return ret;
    };
    CustomerPage.prototype.getColumns = function (account) {
        if (account.type.toLowerCase() == "savings") {
            return {
                "date": { title: "Date", format: 'date', search: true },
                "description": { title: "Reference", search: true, col: 4 },
                "amount": { title: "Amount", format: 'currency' },
                "comments": { title: "Comments", search: true, col: 4 },
            };
        }
        else if (account.type.toLowerCase() == "mortgage") {
            return {
                "date": { title: "Date", format: 'date', search: true },
                "description": { title: "Reference", search: true, col: 4 },
                "amount": { title: "Amount", format: 'currency' },
                "comments": { title: "Comments", search: false, col: 4 },
            };
        }
        else if (account.type.toLowerCase() == "current") {
            return {
                "date": { title: "Date", format: 'date', search: true },
                "description": { title: "Reference", search: true, col: 4 },
                "amount": { title: "Amount", format: 'currency' },
                "comments": { title: "Comments", search: false, col: 4 },
            };
        }
    };
    CustomerPage.prototype.getTransactions = function (account) {
        var _this = this;
        console.log("account", account);
        var self = this;
        if (this.accounts.id) {
            self.customerProvider.getAccountTransactions(account).then(function (data) {
                _this.transactions[account.id] = data.sort(function (a, b) {
                    if (new Date(a.date) < new Date(b.date))
                        return -1;
                    else if (new Date(a.date) > new Date(b.date))
                        return 1;
                    else
                        return 0;
                });
                console.log("transactions[account.id]", _this.transactions[account.id]);
                self.options1.data = _this.transactions[account.id];
                _this.options1 = Object.assign({}, self.options1);
            });
        }
    };
    CustomerPage.prototype.register = function () {
        var _this = this;
        var self = this;
        this.alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Register Customer"),
            message: this.settingsProvider.getLabel("Are you sure you want to approve the customer registration?"),
            buttons: [
                {
                    text: this.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                },
                {
                    text: this.settingsProvider.getLabel('Register'),
                    handler: function (data) {
                        self.customer.status = "OTP";
                        self.cdr.detectChanges();
                        var toast = self.toastCtrl.create({
                            message: _this.settingsProvider.getLabel("Customer registration accepted"),
                            cssClass: 'success',
                            duration: 1000,
                            position: 'bottom'
                        });
                        toast.present();
                    }
                }
            ]
        });
        this.alert.present();
    };
    CustomerPage.prototype.resendPasscode = function () {
        var _this = this;
        var self = this;
        this.alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Send Passcode"),
            message: this.settingsProvider.getLabel("Would you like to send a new Passcode via SMS to this customer?"),
            buttons: [
                {
                    text: this.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                },
                {
                    text: this.settingsProvider.getLabel('Send Passcode'),
                    handler: function (data) {
                        _this.customer.passcode = _this.generatePassword();
                        self.resendPasscodeToMobile(_this.customer);
                        self.cdr.detectChanges();
                    }
                }
            ]
        });
        this.alert.present();
    };
    CustomerPage.prototype.resendPasscodeToMobile = function (customer) {
        var _this = this;
        var self = this;
        self.customerProvider.resendPasscode(customer.customers_id.toString()).then(function (result) {
            if (result.status) {
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel(result.message),
                    cssClass: 'success',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
        }).catch(function (error) {
            console.log("error", error);
        });
    };
    // deleteAction(action, title) {
    //   let self = this;
    //   let alert = this.alertCtrl.create({
    //     title: this.settingsProvider.getLabel("Confirm Delete"),
    //     message: this.settingsProvider.getLabel("Are you sure you want to delete") + " " + action.title + "?",
    //     buttons: [
    //       {
    //         text: this.settingsProvider.getLabel("Cancel"),
    //         role: this.settingsProvider.getLabel("cancel"),
    //         handler: data => {
    //           console.log('Cancel clicked');
    //         }
    //       },
    //       {
    //         text: this.settingsProvider.getLabel("Delete"),
    //         handler: data => {
    //           var actions = [];
    //           for (let a of self.customer.productbutton) {
    //             console.log(a.title + " --- " + title);
    //             if (a.title !== title) {
    //               actions.push(a);
    //             }
    //           }
    //           self.customer.productbutton = actions;
    //           self.dirty();
    //         }
    //       }
    //     ]
    //   });
    //   alert.present();
    // }
    // editAction(action, title) {
    //   let self = this;
    //   let modal = this.modalCtrl.create(AddActionModal, { action: action });//,{"status":true}
    //   modal.onDidDismiss((data) => {
    //     console.log("on dismissdata", data);
    //     let arr = [];
    //     if (data) {
    //       for (let p of self.product.productbutton) {
    //         if (p.title == title) {
    //           arr.push(data);
    //         } else {
    //           arr.push(p);
    //           self.dirty();
    //         }
    //       }
    //       self.product.productbutton = arr;
    //     } else {
    //     }
    //   });
    //   modal.present();
    // }
    // dirty() {
    //   this.dirtyFlag = true;
    //   this.updatePreview();
    // }
    CustomerPage.prototype.activateCustomer = function (customer) {
        var _this = this;
        var self = this;
        self.customerProvider.unBlockCustomer(customer.customers_id.toString()).then(function (result) {
            if (result.status) {
                self.customer.status = "ACTIVE";
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel(result.message),
                    cssClass: 'success',
                    duration: 3000,
                    position: 'right'
                });
                toast.present();
                self.navCtrl.pop();
            }
        }).catch(function (error) {
            console.log("error", error);
        });
        ;
    };
    CustomerPage.prototype.blockCustomer = function (customer, data) {
        var _this = this;
        var self = this;
        var block_customer = {
            customers_id: customer.customers_id.toString(),
            customermobile: self.customer.customermobile,
            logindatetime: self.customer.logindatetime,
            name: data,
            status: "BLOCKED",
        };
        console.log(block_customer, "block_customerblock_customer");
        self.customerProvider.blockCustomer(block_customer).then(function (result) {
            if (result.status) {
                self.customer.status = "BLOCKED";
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel(result.message),
                    cssClass: 'success',
                    duration: 2000,
                    position: 'bottom'
                });
                toast.present();
                self.navCtrl.pop();
            }
        }).catch(function (error) {
            console.log("error", error);
        });
        ;
    };
    CustomerPage.prototype.generatePasscode = function () {
        // var d = [];
        var self = this;
        var customerId = self.customer.customers_id;
        self.customerProvider.resendPasscode(customerId).then(function (result) {
            if (result.status) {
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel(result.message),
                    duration: 2000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
            }
        }).catch(function (error) {
            console.log("error", error);
        });
        ;
    };
    CustomerPage.prototype.block = function () {
        var _this = this;
        var self = this;
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Select a reason to block this customer"),
            // message: this.settingsProvider.getLabel("Select a reason to block this customer"),
            inputs: [
                {
                    name: 'forgotPin',
                    type: 'radio',
                    label: 'Customer forgot pin',
                    value: 'forgotPin',
                    checked: true
                },
                {
                    name: 'custRequest',
                    type: 'radio',
                    label: 'Customer request',
                    value: 'custRequest'
                },
                {
                    name: 'securityPurposes',
                    type: 'radio',
                    label: 'Security purposes',
                    value: 'securityPurposes'
                },
                {
                    name: 'other',
                    type: 'radio',
                    label: 'Other',
                    value: 'other'
                },
            ],
            buttons: [
                {
                    text: this.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                },
                {
                    text: this.settingsProvider.getLabel('Block'),
                    handler: function (data) {
                        self.blockCustomer(_this.customer, data);
                        self.cdr.detectChanges();
                        console.log(data, "DATANAME");
                    }
                }
            ]
        });
        alert.present();
    };
    CustomerPage.prototype.activate = function () {
        var _this = this;
        var self = this;
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Activate Customer"),
            message: this.settingsProvider.getLabel("Would you like to activate this customer's mobile app access?"),
            buttons: [
                {
                    text: this.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                },
                {
                    text: this.settingsProvider.getLabel('Activate'),
                    handler: function (data) {
                        _this.activateCustomer(_this.customer);
                        self.cdr.detectChanges();
                    }
                }
            ]
        });
        alert.present();
    };
    CustomerPage.prototype.formatDateTimeDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            var hh = newdate.getHours();
            var min = newdate.getMinutes().toString().length == 1 ? "0" + newdate.getMinutes().toString() : newdate.getMinutes();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            // newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    CustomerPage.prototype.editCustomer = function () {
    };
    CustomerPage.prototype.showPassword = function () {
        console.log(this.showPass);
        var self = this;
        self.showPass = !self.showPass;
        if (this.showPass) {
            var self_3 = this;
            var alert_1 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel("Reveal Passcode"),
                message: this.settingsProvider.getLabel("Are you sure you want to reveal this passoword?"),
                buttons: [
                    {
                        text: this.settingsProvider.getLabel('Cancel'),
                        role: 'cancel',
                    },
                    {
                        text: this.settingsProvider.getLabel('Ok'),
                        handler: function (data) {
                            self_3.type = "text";
                        }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            self.type = 'password';
        }
    };
    CustomerPage.prototype.deleteSavingGoals = function (savingGoals) {
        var _this = this;
        console.log(savingGoals);
        var d = [];
        var self = this;
        for (var _i = 0, savingGoals_1 = savingGoals; _i < savingGoals_1.length; _i++) {
            var savingGoal = savingGoals_1[_i];
            d.push(this.customerProvider.deleteSavingGoals(savingGoal));
        }
        Promise.all(d)
            .then(function () {
            var toast = self.toastCtrl.create({
                message: savingGoals.length == 1 ? _this.settingsProvider.getLabel('Saving Goal was Deleted successfully') : savingGoals.length + " " + _this.settingsProvider.getLabel('Saving Goal were Deleted successfully'),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
            self.loadSavingGoalData();
        });
    };
    CustomerPage.prototype.loadSavingGoalData = function () {
        var _this = this;
        var self = this;
        self.customerProvider.getSavingGoals(this.customer)
            .then(function (data) {
            self.savingGoals = data;
            self.savinggoal_options.data = self.savingGoals;
            _this.savinggoal_options = Object.assign({}, self.savinggoal_options);
        });
    };
    CustomerPage.prototype.getStatus = function (status) {
        if (status == "1") {
            status = this.settingsProvider.getLabel("Active");
        }
        else {
            status = this.settingsProvider.getLabel("Inactive");
        }
        return (status);
    };
    CustomerPage.prototype.formatDateTime = function (date) {
        var str = "";
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            var hh = newdate.getHours();
            var min = newdate.getMinutes().toString().length == 1 ? "0" + newdate.getMinutes().toString() : newdate.getMinutes();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            return (newdate);
        }
        else {
            str = '--';
            return (str);
        }
    };
    return CustomerPage;
}());
CustomerPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* IonicPage */])({ name: "CustomerPage" }),
    Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["n" /* Component */])({
        selector: 'page-customer',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\customer\customer.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(title)}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid *ngIf="customer">\n\n    <ion-row align-items-start>\n\n      <ion-col>\n\n        <ion-card padding>\n\n          <ion-row *ngIf="customer.firstname && customer.lastname">\n\n            <ion-col>\n\n              <ion-label fixed>{{settingsProvider.getLabel("Name")}}</ion-label>\n\n              <ion-input [(ngModel)]="customer.firstname +\' \'+ customer.lastname" disabled></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row *ngIf="customer.status">\n\n            <ion-col>\n\n              <ion-label fixed>{{settingsProvider.getLabel("Status")}}</ion-label>\n\n              <ion-input [(ngModel)]="customer.status" disabled></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row *ngIf="customer.mobile">\n\n            <ion-col>\n\n              <ion-label fixed>{{settingsProvider.getLabel(\'Mobile Number\')}}</ion-label>\n\n              <ion-input [(ngModel)]="customer.mobile" disabled></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row *ngIf="customer.platform">\n\n            <ion-col>\n\n              <ion-label fixed>{{settingsProvider.getLabel("Platform")}}</ion-label>\n\n              <ion-input [(ngModel)]="customer.platform" disabled></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row *ngIf="customer.customermobile ">\n\n            <ion-col>\n\n              <ion-label fixed>{{settingsProvider.getLabel(\'Other Mobile Number\')}}</ion-label>\n\n              <ion-input [(ngModel)]="customer.customermobile " disabled></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col>\n\n        <ion-card padding>\n\n          <ion-row *ngIf="customer.memberid">\n\n            <ion-col>\n\n              <ion-label fixed>{{settingsProvider.getLabel(\'Member ID\')}}</ion-label>\n\n              <ion-input [(ngModel)]="customer.memberid" disabled></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row *ngIf="customer.dob">\n\n            <ion-col>\n\n              <ion-label fixed>{{settingsProvider.getLabel(\'Date of Birth\')}}</ion-label>\n\n              <ion-datetime displayFormat="DD/MM/YYYY" pickerFormat="DD/MM/YYYY" [(ngModel)]="customer.dob" disabled></ion-datetime>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row *ngIf="customer.customersid">\n\n            <ion-col>\n\n              <ion-label fixed>{{settingsProvider.getLabel(\'Consectus ID\')}}</ion-label>\n\n              <ion-input [(ngModel)]="customer.customersid" disabled></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row *ngIf="customer.logindatetime">\n\n            <ion-col>\n\n              <ion-label fixed>{{settingsProvider.getLabel(\'Last Login\')}}</ion-label>\n\n              <ion-input [(ngModel)]="customer.logindatetime" disabled></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n            <ion-col margin style="padding: 10px;">\n\n              <button *ngIf="customer.status==\'NEW\'" ion-button round small (click)="register()">{{settingsProvider.getLabel(\'Register\')}}</button>\n\n              <button *ngIf="customer.status==\'RESENDOTP\'" ion-button round small (click)="resendPasscode()">{{settingsProvider.getLabel(\'Resend Passcode\')}}\n\n              </button>\n\n              <button *ngIf="customer.status==\'Blocked\' || customer.status==\'New Existing Registration Blocked\' || customer.status==\'Existing Registration Blocked\'"\n\n                ion-button round small (click)="activate()">{{settingsProvider.getLabel(\'Activate\')}}</button>\n\n              <button *ngIf="customer.status==\'Existing Active\' || customer.status ==\'New Existing Active\' || customer.status ==\'Active\'"\n\n                ion-button round small color="danger" (click)="block()">{{settingsProvider.getLabel(\'Block\')}}</button>\n\n\n\n              <!-- <ion-item *ngIf="customer.status==\'Existing Active\' || customer.status ==\'New Existing Active\' || customer.status ==\'Active\'">\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Block\')}}</ion-label>\n\n                <ion-select name="customerAccount" (ionChange)="setAccount() ">\n\n                  <ion-option selected="true" (click)="block()">Select Block Reason</ion-option>\n\n                  <ion-option>Customer forgot pin</ion-option>\n\n                  <ion-option>Customer request</ion-option>\n\n                  <ion-option>Security purposes</ion-option>\n\n                  <ion-option>Other</ion-option>\n\n                </ion-select>\n\n              </ion-item> -->\n\n\n\n              <!-- <ion-row *ngFor="let action of product.productbutton">\n\n                <ion-col text-nowrap>\n\n                  <button ion-button color="danger" clear icon-only float-right (click)="deleteAction(action, action.title)">\n\n                    <ion-icon name="trash"></ion-icon>\n\n                  </button>\n\n                  <button ion-button clear icon-only float-right (click)="editAction(action, action.title)">\n\n                    <ion-icon name="create"></ion-icon>\n\n                  </button>\n\n                  <h5>{{action.title}}</h5>\n\n                  <div float-left *ngIf="action.type==\'url\'">{{settingsProvider.getLabel(\'URL\')}} &nbsp;</div>\n\n                  <div float-left *ngIf="action.type==\'call\'">{{settingsProvider.getLabel(\'Call Number\')}} &nbsp;\n\n                  </div>\n\n                  <div float-left *ngIf="action.type==\'callback\'">{{settingsProvider.getLabel(\'Callback Request\')}} &nbsp;\n\n                  </div>\n\n                  <a *ngIf="action.type==\'url\'" [href]="action.action" [target]="\'_blank\'">{{action.action}}</a>\n\n                </ion-col>\n\n              </ion-row> -->\n\n\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid>\n\n    <ion-row *ngIf="customer" align-items-center>\n\n      <ion-col class="tabs" col-12>\n\n        <ion-segment [(ngModel)]="currenTab" (ionChange)="segmentChanged($event)">\n\n          <ion-segment-button value="savings">\n\n            {{settingsProvider.getLabel(\'Savings\')}}\n\n          </ion-segment-button>\n\n          <ion-segment-button value="auditlogs" *ngIf="customer !== undefined">\n\n            {{settingsProvider.getLabel(\'Audit Logs\')}}\n\n          </ion-segment-button>\n\n          <!-- <ion-segment-button value="savingoals">\n\n            {{settingsProvider.getLabel(\'Saving Goals\')}}\n\n          </ion-segment-button>\n\n          <ion-segment-button value="regularSaving">\n\n            {{settingsProvider.getLabel(\'Regular Saving\')}}\n\n          </ion-segment-button>\n\n          <ion-segment-button value="cheque">\n\n            {{settingsProvider.getLabel(\'Cheques\')}}\n\n          </ion-segment-button> -->\n\n        </ion-segment>\n\n        <ion-grid>\n\n          <ion-row *ngIf=" currenTab==\'savings\'">\n\n            <ion-col col-3>\n\n              <ion-card padding>\n\n                <ion-item>\n\n                  <ion-label stacked>{{settingsProvider.getLabel(\'Select Account\')}}</ion-label>\n\n                  <ion-select name=" customerAccount " (ionChange)="setAccount() ">\n\n                    <ion-option selected="true" (ionSelect)="selectAccount(null) ">Select Account</ion-option>\n\n                    <ion-option *ngFor="let account of customerDetailsData.accounts" (ionSelect)="selectAccount(account) ">\n\n                      {{account.accountname}} {{account.account_number}}\n\n                    </ion-option>\n\n                  </ion-select>\n\n                </ion-item>\n\n              </ion-card>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n        <ion-grid>\n\n          <ion-row>\n\n            <ion-col col-6>\n\n              <ion-card>\n\n                <div *ngIf=" currenTab==\'savings\' && customerAcc ">\n\n                  <table-component [options]="options1 "></table-component>\n\n                </div>\n\n              </ion-card>\n\n            </ion-col>\n\n            <ion-col col-6 *ngIf="customerAcc " padding>\n\n              <ion-card padding>\n\n                <ion-row *ngIf="customerAcc.accountname">\n\n                  <ion-col>\n\n                    <ion-label fixed>{{settingsProvider.getLabel(\'Account Name\')}}</ion-label>\n\n                    <ion-input [disabled]="true " [(ngModel)]="customerAcc.accountname"></ion-input>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row *ngIf="customerAcc.account_number">\n\n                  <ion-col>\n\n                    <ion-label fixed>{{settingsProvider.getLabel(\'Account Number\')}}</ion-label>\n\n                    <ion-input [disabled]="true " [(ngModel)]="customerAcc.account_number"></ion-input>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row *ngIf="customerAcc.sortcode">\n\n                  <ion-col>\n\n                    <ion-label fixed>{{settingsProvider.getLabel(\'Account Sort Code\')}}</ion-label>\n\n                    <ion-input [disabled]="true " [(ngModel)]="customerAcc.sortcode"></ion-input>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-card>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n\n\n        <div *ngIf="currenTab==\'auditlogs\' ">\n\n          <table-component [options]="audit_options "></table-component>\n\n        </div>\n\n        <div *ngIf="currenTab==\'savingoals\' ">\n\n          <table-component [options]="savinggoal_options "></table-component>\n\n        </div>\n\n        <div *ngIf="currenTab==\'regularSaving\' ">\n\n          <table-component [options]="regular_saving_option "></table-component>\n\n        </div>\n\n        <div *ngIf="currenTab==\'cheque\' ">\n\n          <table-component [options]="cheque_option "></table-component>\n\n        </div>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\customer\customer.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_1__providers_customer_customer__["a" /* CustomerProvider */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], CustomerPage);

//# sourceMappingURL=customer.js.map

/***/ }),

/***/ 498:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_customers_customers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_dashboard_dashboard__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_customer_customer__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var CustomersPage = (function () {
    function CustomersPage(oauthProvider, customersProvider, languageProvider, events, customerProvider, cdr, renderer, navCtrl, alertCtrl, dashboardProvider, toastCtrl, navParams, loadauditProvider, globalProvider, settingsProvider, viewCtrl, masterpermissionProvider) {
        this.oauthProvider = oauthProvider;
        this.customersProvider = customersProvider;
        this.languageProvider = languageProvider;
        this.events = events;
        this.customerProvider = customerProvider;
        this.cdr = cdr;
        this.renderer = renderer;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.dashboardProvider = dashboardProvider;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.loadauditProvider = loadauditProvider;
        this.globalProvider = globalProvider;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.customerOptions = {
            data: [],
            filteredData: [],
            columns: {
                "customersid": { title: "Consectus ID", search: true, format: 'function', fn: this.commonBlankFunction.bind(this) },
                "memberid": { title: "Member ID", search: true, format: 'function', fn: this.commonBlankFunction.bind(this) },
                "firstname": { title: "First Name", search: true, mobile: true, format: 'function', fn: this.commonBlankFunction.bind(this) },
                "lastname": { title: "Last Name", search: true, format: 'function', fn: this.commonBlankFunction.bind(this) },
                "dob": { title: "Date of Birth", format: 'date', search: true, fn: this.formatDateTimeDate.bind(this) },
                "emailid": { title: "Email ID", search: true, col: 1, format: 'function', fn: this.commonBlankFunction.bind(this) },
                "platform": { title: "Platform", search: true, format: 'function', fn: this.commonBlankFunction.bind(this) },
                "mobile": { title: "Mobile", search: true, format: 'function', fn: this.commonBlankFunction.bind(this) },
                "logindatetime": { title: "Last Login", format: 'function', fn: this.formatDateTimeDate.bind(this) },
                "status": { title: "Status", search: true, format: 'function', fn: this.commonBlankFunction.bind(this) },
            },
            pagination: true,
            filter: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Block",
                    action: this.block.bind(this),
                    color: 'danger'
                },
                {
                    title: "Activate",
                    action: this.activate.bind(this),
                    color: 'primary'
                },
                {
                    title: "Reset",
                    action: this.myFunction.bind(this),
                    color: 'dark'
                },
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editCustomer.bind(this),
            uploadOptions: {},
        };
        this.statusLabels = {
            "New": "NEW", "Passcode Sent": "OTP", "Blocked": "BLOCKED", "OTP": "OTP", "Ready": "SETUP"
        };
        this.resultdata = null;
        this.languageString = "en";
        this.events.subscribe("customers:loaded", this.ionViewDidLoad.bind(this));
        this.filter = navParams.get("filter");
        this.BLOCKED = navParams.get("blocked");
        this.OTP = navParams.get("otp");
        this.FAILEDPASSCODE = navParams.get("failedPasscode");
        this.customerstatus = navParams.get("customerstatus");
        console.log(this.customerstatus, "this.customerstatus");
        this.period = navParams.get("type");
        this.channel = navParams.get("channel");
        if (this.filter)
            this.filter = this.statusLabels[this.filter];
    }
    CustomersPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("customers")) {
            return true;
        }
        else {
            return false;
        }
    };
    CustomersPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        var self = this;
        console.log("wkbfwjefjewbfne", this.oauthProvider.user);
        // if (!this.oauthProvider.user) {
        //   this.navCtrl.setRoot("LoginPage");
        // } else {
        //   if (this.BLOCKED == "Blocked") {
        //     console.log(this.BLOCKED, "hi blocked customers");
        //     this.getAllCustomers();
        //   }
        // else if (this.OTP == "OTP BLOCKED") {
        //  self.customersProvider.getPasscodeCustomers(this.OTP).then((result: any) => {
        //     if (result.length > 0) {
        //       self.customerOptions.data = result.data;
        //       self.customerOptions = Object.assign({}, self.customerOptions);
        //       self.cdr.detectChanges();
        //     }
        //   })
        // }
        // else if (this.FAILEDPASSCODE == "FAILED PASSCODE") {
        // self.customersProvider.getfailedPasscodeCustomers(this.FAILEDPASSCODE).then((result: any) => {
        //     if (result.length > 0) {
        //       self.customerOptions.data = result.data;
        //       self.customerOptions = Object.assign({}, self.customerOptions);
        //       self.cdr.detectChanges();
        //     }
        //   })
        // }
        if (this.customerstatus != undefined && this.period != undefined) {
            self.dashboardProvider.getDashboardFilterCustomers(this.customerstatus, this.period, this.channel)
                .then(function (res) {
                console.log("responsedatattttttt", res);
                self.customerOptions.data = res;
                self.customerOptions = Object.assign({}, self.customerOptions);
                self.cdr.detectChanges();
            }).catch(function (error) {
                console.log(error, self.customerOptions.data, "dacd");
            });
        }
        else {
            self.customersProvider.getregistrationfields().then(function (registerData) {
                self.getregistrationfields = registerData.data;
                self.customerOptions = Object.assign({}, self.customerOptions);
                self.cdr.detectChanges();
            }).catch(function (error) {
            });
            this.getAllCustomers();
        }
    };
    CustomersPage.prototype.ionViewDidLoad = function () {
        var self = this;
        var message = "Viewed " + this.customerOptions.customersid + " in Customers";
        console.log(this.customerstatus, this.period, "this.customerStatusLoad");
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "view",
            "screen": "customers",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
        });
    };
    CustomersPage.prototype.register = function (items) {
        console.log("register");
    };
    CustomersPage.prototype.resendPasscode = function (items) {
        var _this = this;
        console.log("Resend PAsscode");
        var d = [];
        var self = this;
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            d.push(item.customers_id);
        }
        var userIds = d.join(',');
        self.customersProvider.resendPasscode(userIds).then(function (result) {
            if (result.status) {
                self.getAllCustomers();
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel(result.message),
                    cssClass: 'success',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
        });
    };
    CustomersPage.prototype.activate = function (items) {
        var _this = this;
        console.log("activate code");
        var d = [];
        var self = this;
        for (var _i = 0, items_2 = items; _i < items_2.length; _i++) {
            var item = items_2[_i];
            d.push(item.customers_id);
        }
        var userIds = d.join(',');
        self.customersProvider.unBlockCustomer(userIds).then(function (result) {
            _this.getAllCustomers();
            if (result.status) {
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel(result.message),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                var message = "Activated customer in Customers";
                var auditUser = {
                    "users_id": _this.oauthProvider.user[0].users_id,
                    "customer_devices_id": 0,
                    "usertype": "staff",
                    "uuid": "",
                    "appname": "hub",
                    "event_type": "view",
                    "screen": "customers",
                    "trail_details": message
                };
                _this.loadauditProvider.loadaudit(auditUser)
                    .then(function (data) {
                });
            }
        });
    };
    CustomersPage.prototype.refresh = function () {
        this.ionViewDidEnter();
    };
    CustomersPage.prototype.block = function (items) {
        var _this = this;
        var d = [];
        console.log(items);
        var self = this;
        for (var _i = 0, items_3 = items; _i < items_3.length; _i++) {
            var item = items_3[_i];
            d.push(item.customers_id);
        }
        var userIds = d.join(',');
        var objblock = [];
        for (var i = 0; i < items.length; i++) {
            var obj = {
                "customers_id": items[i].customers_id,
                "customermobile": "",
                "logindatetime": "",
                "name": "custRequest",
                "status": "BLOCKED"
            };
            objblock.push(obj);
        }
        self.customersProvider.blockCustomer(objblock).then(function (result) {
            if (result.status) {
                _this.getAllCustomers();
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel(result.data),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'danger',
                });
                toast.present();
                var message = "BLocked customer in Customers";
                var auditUser = {
                    "users_id": _this.oauthProvider.user[0].users_id,
                    "customer_devices_id": 0,
                    "usertype": "staff",
                    "uuid": "",
                    "appname": "hub",
                    "event_type": "view",
                    "screen": "user-access",
                    "trail_details": message
                };
                _this.loadauditProvider.loadaudit(auditUser)
                    .then(function (data) {
                });
            }
        });
    };
    CustomersPage.prototype.getAllCustomers = function () {
        var _this = this;
        var self = this;
        self.customersProvider.getAllCustomers().then(function (res) {
            if (_this.BLOCKED == "Blocked") {
                self.customerOptions.data = res.data.filter(function (obj) { return obj.status == 'Blocked'; });
                console.log(_this.BLOCKED, "hi blocked customers 3");
                _this.nullData = res.data;
            }
            else if (_this.OTP == "OTP BLOCKED") {
                self.customerOptions.data = res.data.filter(function (obj) { return obj.status == 'OTPAWAIT'; });
            }
            else if (_this.FAILEDPASSCODE == "FAILED PASSCODE") {
                self.customerOptions.data = res.data.filter(function (obj) { return obj.status == 'OTPFAILED'; });
            }
            else {
                self.customerOptions.data = res.data;
            }
            self.customerOptions = Object.assign({}, self.customerOptions);
            self.cdr.detectChanges();
        }).catch(function (error) {
        });
    };
    CustomersPage.prototype.clearFilter = function () {
        this.filter = null;
        this.ionViewDidLoad();
    };
    CustomersPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Error"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    CustomersPage.prototype.addCustomer = function () {
        console.log("Add customer");
        this.navCtrl.push("CustomerPage");
    };
    CustomersPage.prototype.editCustomer = function (item) {
        console.log(item, "thisItem");
        item.logindatetime = this.formatDateTime(item.logindatetime);
        this.navCtrl.push("CustomerPage", { customer: item, getregistrationfields: this.getregistrationfields });
    };
    CustomersPage.prototype.showCustomer = function (item) {
        this.navCtrl.push("CustomerPage", { customer: item });
    };
    CustomersPage.prototype.selectAccount = function (accNo) {
    };
    CustomersPage.prototype.formatDateTime = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            var hh = newdate.getHours();
            var min = newdate.getMinutes().toString().length == 1 ? "0" + newdate.getMinutes().toString() : newdate.getMinutes();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            return (newdate);
        }
    };
    CustomersPage.prototype.formatDateTimeDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            var hh = newdate.getHours();
            var min = newdate.getMinutes().toString().length == 1 ? "0" + newdate.getMinutes().toString() : newdate.getMinutes();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            // newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    CustomersPage.prototype.commonBlankFunction = function (nullData) {
        var str = "";
        if (nullData == 0) {
            str = '--';
        }
        else if (nullData == null) {
            str = '--';
        }
        else if (nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    CustomersPage.prototype.commonFunctionActive = function (nullData) {
        var str = "";
        if (nullData == "Active") {
            str = '<img class="imgActiveCust" src ="assets/member_images/Reset.jpg"  onclick="myFunction()" alt="">';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    CustomersPage.prototype.myFunction = function (resultdata) {
        var _this = this;
        var self = this;
        // if (this.nullData.customer_id != "") {
        //   this.navCtrl.push("CustomerPage");
        // }
        // else {
        // }
        if (resultdata.length == 1) {
            var alert = this.alertCtrl.create();
            alert.setTitle('Please select');
            alert.addInput({
                type: 'radio',
                label: 'Existing Customer using Registered Mobile number',
                value: 'initial',
            });
            alert.addInput({
                type: 'radio',
                label: 'Existing Customer using unregistered ( New ) Mobile number',
                value: 'new'
            });
            alert.addInput({
                type: 'radio',
                label: 'First Time App Registration',
                value: 'reset'
            });
            alert.addInput({
                type: 'radio',
                label: 'Delete Data',
                value: 'delete'
            });
            alert.addButton('Cancel');
            alert.addButton({
                text: 'Ok',
                handler: function (data) {
                    var resetdata = {};
                    console.log(data, "sfashfasgfasifgasfugasfgasfashgasfj");
                    if (data == "initial") {
                        resetdata = {
                            "mode": 1,
                            "bankcustomers_id": resultdata[0].bankcustomers_id
                        };
                        _this.reset(resetdata);
                    }
                    else if (data == "new") {
                        resetdata = {
                            "mode": 2,
                            "bankcustomers_id": resultdata[0].bankcustomers_id
                        };
                        _this.reset(resetdata);
                    }
                    else if (data == "reset") {
                        resetdata = {
                            "mode": 3,
                            "bankcustomers_id": resultdata[0].bankcustomers_id
                        };
                        _this.reset(resetdata);
                    }
                    else if (data == "delete") {
                        resetdata = {
                            "mode": 4,
                            "bankcustomers_id": resultdata[0].bankcustomers_id
                        };
                        _this.reset(resetdata);
                    }
                }
            });
            alert.present();
        }
        else {
            var toast = self.toastCtrl.create({
                message: "Please Select one Customer only.",
                duration: 3000,
                position: 'right',
                cssClass: 'error',
            });
            toast.present();
        }
    };
    CustomersPage.prototype.reset = function (resetdata) {
        var self = this;
        self.customerProvider.resetcustomer(resetdata).then(function (resultdata) {
            var toast = self.toastCtrl.create({
                message: "Customer Data reset successfully.",
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.customersProvider.getAllCustomers().then(function (res) {
                self.customerOptions.data = res.data;
                self.customerOptions = Object.assign({}, self.customerOptions);
                self.cdr.detectChanges();
            }).catch(function (error) {
            });
        }).catch(function (error) {
        });
    };
    return CustomersPage;
}());
CustomersPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* IonicPage */])({ name: 'CustomersPage' }),
    Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["n" /* Component */])({
        selector: 'page-customers',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\customers\customers.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(\'Customer Management | Consectus\')}} </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <table-component [options]="customerOptions"></table-component>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\customers\customers.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__["a" /* OauthProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__["a" /* OauthProvider */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__providers_customers_customers__["a" /* CustomersProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__providers_customers_customers__["a" /* CustomersProvider */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* Events */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_9__providers_customer_customer__["a" /* CustomerProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9__providers_customer_customer__["a" /* CustomerProvider */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["k" /* ChangeDetectorRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_core__["k" /* ChangeDetectorRef */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["Z" /* Renderer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_core__["Z" /* Renderer */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["k" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["k" /* NavController */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_5__providers_dashboard_dashboard__["a" /* DashboardProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__providers_dashboard_dashboard__["a" /* DashboardProvider */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* ToastController */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavParams */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_7__providers_global_global__["a" /* GlobalProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__providers_global_global__["a" /* GlobalProvider */]) === "function" && _p || Object, typeof (_q = typeof __WEBPACK_IMPORTED_MODULE_8__providers_settings_settings__["a" /* SettingsProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__providers_settings_settings__["a" /* SettingsProvider */]) === "function" && _q || Object, typeof (_r = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* ViewController */]) === "function" && _r || Object, typeof (_s = typeof __WEBPACK_IMPORTED_MODULE_10__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_10__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]) === "function" && _s || Object])
], CustomersPage);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s;
//# sourceMappingURL=customers.js.map

/***/ }),

/***/ 499:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MembersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_table_table__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_member_member__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MembersPage = (function () {
    function MembersPage(oauthProvider, languageProvider, memberProvider, toastCtrl, cdr, navCtrl, settingsProvider, navParams, alertCtrl, globalProvider, loadauditProvider, events, viewCtrl, masterpermissionProvider) {
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.memberProvider = memberProvider;
        this.toastCtrl = toastCtrl;
        this.cdr = cdr;
        this.navCtrl = navCtrl;
        this.settingsProvider = settingsProvider;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.globalProvider = globalProvider;
        this.loadauditProvider = loadauditProvider;
        this.events = events;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.currentTab = "activeMembers";
        this.data = [];
        this.memberOptions = {
            data: [],
            filteredData: [],
            columns: {
                // "image": { title: "Image", format: 'function', fn: this.getMemberImage.bind(this) },
                "title": { title: "Title", search: true, mobile: true, col: 2, format: 'function', fn: this.commonFunction.bind(this) },
                "sub_title": { title: "Sub Title", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "description": { title: "Description", mobile: true, col: 3, format: 'function', fn: this.commonFunction.bind(this) },
                "totaltarget": { title: "Target", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "totaldelivered": { title: "Delivered", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "totalread": { title: "Viewed", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "interested": { title: "Interested", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.deleteMembers.bind(this),
                    color: "danger"
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Member Offers",
                addAction: this.createMembers.bind(this),
            },
            tapRow: this.editMembers.bind(this),
            // resetFilter: null,
            uploadOptions: {},
        };
        this.member1Options = {
            data: [],
            filteredData: [],
            columns: {
                "title": { title: "Title", search: true, mobile: true, col: 2, format: 'function', fn: this.commonFunction.bind(this) },
                "sub_title": { title: "Sub Title", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "description": { title: "Description", mobile: true, col: 3, format: 'function', fn: this.commonFunction.bind(this) },
                "totaltarget": { title: "Target", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "totaldelivered": { title: "Delivered", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "totalread": { title: "Viewed", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "interested": { title: "Interested", mobile: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Activate",
                    action: this.activateMembers.bind(this),
                    color: "primary"
                },
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Member Offers",
                addAction: this.createMembers.bind(this),
            },
            tapRow: this.editMembers.bind(this),
            // resetFilter: null,
            uploadOptions: {},
        };
        this.resultdata = null;
        this.languageString = "en";
    }
    MembersPage.prototype.commonFunction = function (nullData) {
        var str = "";
        if (nullData == 0) {
            str = '--';
        }
        else if (nullData == null) {
            str = '--';
        }
        else if (nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    MembersPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("members")) {
            return true;
        }
        else {
            return false;
        }
    };
    MembersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MembersPage');
        console.log('ionViewDidLoad FaqsPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    MembersPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        var self = this;
        self.getArchivemembers();
        if (self.oauthProvider.user_role.members) {
            self.getMembers();
        }
        else {
            self.navCtrl.setRoot("DashboardPage");
        }
    };
    MembersPage.prototype.segmentChanged = function (event) {
        console.log("eventValue", event._value);
        switch (event._value) {
            case 'activeMembers':
                this.getMembers();
                break;
            case 'archiveMembers':
                this.getArchivemembers();
                break;
            default:
                break;
        }
    };
    MembersPage.prototype.getMembers = function () {
        var _this = this;
        var self = this;
        this.memberProvider.getmember()
            .then(function (result) {
            console.log("result", result);
            self.memberOptions.data = _this.archive(result.data, 1);
            self.memberOptions = Object.assign({}, self.memberOptions);
        }).catch(function (error) {
        });
    };
    MembersPage.prototype.getArchivemembers = function () {
        var _this = this;
        var self = this;
        this.memberProvider.getmember()
            .then(function (result) {
            self.member1Options.data = _this.archive(result.data, 0);
            self.member1Options = Object.assign({}, self.member1Options);
        }).catch(function (error) {
        });
    };
    MembersPage.prototype.archive = function (archiveData, activeData) {
        var archive = [];
        for (var i in archiveData) {
            if (archiveData[i].isactive == activeData) {
                archive.push(archiveData[i]);
            }
        }
        return archive;
    };
    MembersPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    MembersPage.prototype.editMembers = function (member) {
        console.log("member", member);
        this.navCtrl.push('MemberPage', { member: member, callback: this.updateMember.bind(this) });
    };
    MembersPage.prototype.deleteMembers = function (memberData) {
        var _this = this;
        var d = [];
        var self = this;
        for (var _i = 0, memberData_1 = memberData; _i < memberData_1.length; _i++) {
            var item = memberData_1[_i];
            d.push(item.member_id);
        }
        var memberIds = d.join(',');
        self.memberProvider.deleteMembers(memberIds).then(function (result) {
            if (result.status) {
                self.getMembers();
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel(result.message),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'danger',
                });
                toast.present();
                var message = "Member is Deleted";
                ;
                var auditUser = {
                    "users_id": _this.oauthProvider.user[0].users_id,
                    "customer_devices_id": 0,
                    "usertype": "staff",
                    "uuid": "",
                    "appname": "hub",
                    "event_type": "view",
                    "screen": "members",
                    "trail_details": message
                };
                _this.loadauditProvider.loadaudit(auditUser)
                    .then(function (data) {
                });
            }
        });
    };
    MembersPage.prototype.activateMembers = function (membersData) {
        var _this = this;
        var d = [];
        var self = this;
        for (var _i = 0, membersData_1 = membersData; _i < membersData_1.length; _i++) {
            var item = membersData_1[_i];
            d.push(item.member_id);
        }
        var memberIds = d.join(',');
        self.memberProvider.activateMembers(memberIds).then(function (result) {
            if (result.status) {
                self.getArchivemembers();
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel(result.message),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                var message = "Member is Activated";
                ;
                var auditUser = {
                    "users_id": _this.oauthProvider.user[0].users_id,
                    "customer_devices_id": 0,
                    "usertype": "staff",
                    "uuid": "",
                    "appname": "hub",
                    "event_type": "view",
                    "screen": "members",
                    "trail_details": message
                };
                _this.loadauditProvider.loadaudit(auditUser)
                    .then(function (data) {
                });
            }
        });
    };
    MembersPage.prototype.updateMember = function (member) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.memberProvider.updateMember(member)
                .then(function () {
                var toast = self.toastCtrl.create({
                    message: 'Member was updated successfully',
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success'
                });
                toast.present();
                resolve();
            });
        });
    };
    MembersPage.prototype.addMessage = function (member) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.memberProvider.createMember(member)
                .then(function () {
                var toast = self.toastCtrl.create({
                    message: 'Member was added successfully',
                    duration: 3000,
                    position: 'right'
                });
                toast.present();
                resolve();
            });
        });
    };
    MembersPage.prototype.createMembers = function () {
        this.navCtrl.push('MemberPage', { callback: this.addMessage.bind(this) });
    };
    MembersPage.prototype.loadMembersData = function () {
        var self = this;
        this.memberProvider.getMembers()
            .then(function (result) {
            self.memberOptions.data = result;
            self.memberOptions = Object.assign({}, self.memberOptions);
        });
    };
    MembersPage.prototype.getMemberImage = function (memberimage) {
        var ret = '<img class="imgUpload"  style="width:25%; height:25%" src ="' + memberimage + '" alt="Click here to upload Image">';
        return ret;
    };
    return MembersPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["_12" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_0__components_table_table__["a" /* TableComponent */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__components_table_table__["a" /* TableComponent */])
], MembersPage.prototype, "table", void 0);
MembersPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["n" /* Component */])({
        selector: 'page-members',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\members\members.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel("Member Offers Management | Consectus")}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n    <ion-segment-button value="activeMembers">\n\n      {{settingsProvider.getLabel(\'Active\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="archiveMembers">\n\n      {{settingsProvider.getLabel(\'Archive\')}}\n\n    </ion-segment-button>\n\n  </ion-segment>\n\n\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'activeMembers\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <table-component [options]="memberOptions"></table-component>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'archiveMembers\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <table-component [options]="member1Options"></table-component>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\members\members.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_member_member__["a" /* MemberProvider */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_9__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], MembersPage);

//# sourceMappingURL=members.js.map

/***/ }),

/***/ 500:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadButtonComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__upload_button__ = __webpack_require__(501);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UploadButtonComponentModule = (function () {
    function UploadButtonComponentModule() {
    }
    return UploadButtonComponentModule;
}());
UploadButtonComponentModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_0__upload_button__["a" /* UploadButtonComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */],
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_0__upload_button__["a" /* UploadButtonComponent */]
        ]
    })
], UploadButtonComponentModule);

//# sourceMappingURL=upload-button.module.js.map

/***/ }),

/***/ 501:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadButtonComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the UploadButtonComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var UploadButtonComponent = (function () {
    function UploadButtonComponent(renderer) {
        this.renderer = renderer;
        this.type = "file";
    }
    UploadButtonComponent.prototype.callback = function (event) {
        // trigger click event of hidden input
        var clickEvent = new MouseEvent("click", { bubbles: true });
        this.renderer.invokeElementMethod(this.nativeInputBtn.nativeElement, "dispatchEvent", [clickEvent]);
    };
    UploadButtonComponent.prototype.ionViewDidLoad = function () {
    };
    /**
     * Callback which is executed after files from native popup are selected.
     * @param  {Event}    event change event containing selected files
     */
    UploadButtonComponent.prototype.filesAdded = function (event) {
        var self = this;
        var files = this.nativeInputBtn.nativeElement.files;
        if (files.length > 0) {
            var f = files[0];
            if (this.type == 'image') {
                if (f.type.match('image.*')) {
                    var imagereader = new FileReader();
                    // Closure to capture the file information.
                    imagereader.onload = (function (theFile) {
                        return function (e) {
                            // Render thumbnail.
                            console.log('image content: ' + e.target.result.length);
                            self.nativeInputBtn.nativeElement.value = '';
                            self.btnCallback(e);
                        };
                    })(f);
                    // Read in the image file as a data URL.
                    imagereader.readAsDataURL(f);
                }
                else {
                    self.btnCallback(null);
                }
            }
            else if (this.type == 'csv') {
                var filereader = new FileReader();
                // Closure to capture the file information.
                filereader.onload = (function (theFile) {
                    return function (e) {
                        // Render thumbnail.
                        console.log('csv content: ' + e.target.result.length);
                        var index = e.target.result.indexOf('base64,');
                        if (index > 0) {
                            var d = atob(e.target.result.substr(index + 7)).split("\n");
                            self.nativeInputBtn.nativeElement.value = '';
                            self.btnCallback(atob(e.target.result.substr(index + 7)));
                        }
                        else {
                            self.nativeInputBtn.nativeElement.value = '';
                            self.btnCallback(e.target.result);
                        }
                    };
                })(f);
                // Read in the image file as a data URL.
                filereader.readAsDataURL(f);
            }
            else if (this.type == 'json') {
                var filereader_1 = new FileReader();
                // Closure to capture the file information.
                filereader_1.onload = (function (theFile) {
                    return function (e) {
                        // Render thumbnail.
                        console.log('csv content: ' + e.target.result.length);
                        var index = e.target.result.indexOf('base64,');
                        if (index > 0) {
                            var d = atob(e.target.result.substr(index + 7)).split("\n");
                            self.nativeInputBtn.nativeElement.value = '';
                            self.btnCallback(atob(e.target.result.substr(index + 7)));
                        }
                        else {
                            self.nativeInputBtn.nativeElement.value = '';
                            self.btnCallback(e.target.result);
                        }
                    };
                })(f);
                // Read in the image file as a data URL.
                filereader_1.readAsDataURL(f);
            }
            else {
                var reader = new FileReader();
                // Closure to capture the file information.
                reader.onload = (function (theFile) {
                    return function (e) {
                        // Render thumbnail.
                        console.log('file content: ' + e.target.result.length);
                        self.nativeInputBtn.nativeElement.value = '';
                        self.btnCallback(atob(e.target.result));
                    };
                })(f);
                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }
        }
    };
    return UploadButtonComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewChild */])("nativefilebutton"),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
], UploadButtonComponent.prototype, "nativeInputBtn", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
    __metadata("design:type", String)
], UploadButtonComponent.prototype, "type", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
    __metadata("design:type", Function)
], UploadButtonComponent.prototype, "btnCallback", void 0);
UploadButtonComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'upload-button',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\upload-button-bk\upload-button.html"*/'<button ion-button round [color]="color" [small]="small!=undefined" [large]="large!=undefined" [clear]="clear!=undefined" [round]="round!=undefined" (click)="callback($event)" clear>\n\n  <ion-icon name="{{name}}"></ion-icon>\n\n  {{title}}\n\n</button>\n\n<input type="file" (change)="filesAdded($event)" style="display: none" #nativefilebutton />'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\upload-button-bk\upload-button.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Z" /* Renderer */]])
], UploadButtonComponent);

//# sourceMappingURL=upload-button.js.map

/***/ }),

/***/ 502:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MemberPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MemberActionModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_member_member__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_customers_customers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_push_messaging_push_messaging__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var MemberPage = (function () {
    //end - Targeted Members Code
    function MemberPage(navCtrl, navParams, memberProvider, languageProvider, settingsProvider, oauthProvider, domSanitizer, toastCtrl, viewCtrl, modalCtrl, alertCtrl, cdr, customersProvider, pushMessagingProvider, loadauditProvider, globalProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.memberProvider = memberProvider;
        this.languageProvider = languageProvider;
        this.settingsProvider = settingsProvider;
        this.oauthProvider = oauthProvider;
        this.domSanitizer = domSanitizer;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.cdr = cdr;
        this.customersProvider = customersProvider;
        this.pushMessagingProvider = pushMessagingProvider;
        this.loadauditProvider = loadauditProvider;
        this.globalProvider = globalProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.image = null;
        this.member = {
            id: new Date().getTime().toString(),
            title: "",
            body: "",
            image: "",
            actions: [
                {
                    type: "",
                    title: "",
                    action: ""
                }
            ],
            css: "",
            new: true
        };
        this.urldata = {
            title: "",
            type: "",
            action: ""
        };
        this.deviceType = 'ios';
        this.languageString = "en";
        this.resultdata = null;
        this.all_customers = false;
        this.todayDate = new Date();
        this.isdelivered = true;
        //start - Targeted Members Code
        this.targetedCustomers = {
            data: [],
            filteredData: [],
            columns: {
                "customers_id": { title: "Consectus ID", search: true },
                "customername": { title: "Customer Name", search: true },
                "memberid": { title: "Member ID", search: true },
                "isdelivered": { title: "Status", format: "function", fn: this.memberStatus.bind(this), search: true }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.showCustomer.bind(this),
            uploadOptions: {},
        };
        this.callback = navParams.get('callback');
        if (!navParams.get('member')) {
            this.member = {
                title: "",
                sub_title: "",
                description: "",
                bannerurl: "",
                bulletpoint: [],
                memberbutton: [],
                createdby: 2,
                isactive: 1
            };
            this.title = "Add Member Offers";
            this.new = true;
        }
        else {
            this.title = "Update Member Offers";
            this.member = JSON.parse(JSON.stringify(navParams.get('member')));
            this.member.imagepath = this.globalProvider.getBaseUrl() + this.member.bannerurl;
            this.dirtyFlag = true;
        }
    }
    MemberPage.prototype.showCustomer = function () {
    };
    MemberPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("members")) {
            return true;
        }
        else {
            return false;
        }
    };
    MemberPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        else {
            this.updatePreview();
        }
        console.log('ionViewDidLoad MemberPage');
        //start - Targeted Members Code
        console.log(this.member.member_id, "member_id");
        if (this.member.member_id) {
            var self_1 = this;
            var pushMember = {
                "member_id": this.member.member_id
            };
            this.memberProvider.getcustomerlistbymemberid(pushMember)
                .then(function (result) {
                self_1.targetedCustomers.data = result.data;
                if (self_1.targetedCustomers.data.length == 0) {
                    _this.isdelivered = true;
                }
                else {
                    _this.isdelivered = false;
                }
                self_1.targetedCustomers = Object.assign({}, self_1.targetedCustomers);
            });
        }
    };
    MemberPage.prototype.pushButton = function () {
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel("Confirm"),
            message: self.settingsProvider.getLabel("Are you sure want to push this member offer?"),
            buttons: [
                {
                    text: self.settingsProvider.getLabel("Cancel"),
                    role: self.settingsProvider.getLabel("cancel"),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: self.settingsProvider.getLabel("Ok"),
                    handler: function (data) {
                    }
                }
            ],
            enableBackdropDismiss: true
        });
        alert.present();
    };
    MemberPage.prototype.onFileChange = function (csv) {
        var _this = this;
        this.fileReaded = csv.target.files[0];
        var reader = new FileReader();
        reader.readAsText(this.fileReaded);
        reader.onload = function (e) {
            // let csv: string = reader.result;
            var allTextLines = csv.split(/\r|\n|\r/);
            var headers = allTextLines[0].split(',');
            var lines = [];
            var tarr = [];
            for (var i = 0; i < allTextLines.length; i++) {
                var data = allTextLines[i].split(',');
                if (data.length === headers.length) {
                    if (i > 0) {
                        var obj = {
                            customerId: data[0].replace(/\"/g, ""),
                            selected: false,
                            status: data[1].replace(/\"/g, "")
                        };
                        _this.member.target.push(obj);
                    }
                    lines.push(tarr);
                }
            }
            _this.targetedCustomers.data = _this.member.target;
            console.log(_this.member.target, "messageTarget");
            _this.targetedCustomers = Object.assign({}, _this.targetedCustomers);
        };
    };
    MemberPage.prototype.downloadCsv = function () {
        var csvContent = "CustomerID MemberID";
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvContent);
        hiddenElement.target = '_blank';
        hiddenElement.download = this.getCsvFileName();
        hiddenElement.click();
    };
    MemberPage.prototype.downloadPdf = function () {
        window.open('http://www.orimi.com/pdf-test.pdf', '_system', 'location=yes');
    };
    MemberPage.prototype.getCsvFileName = function () {
        return "consectus-" + new Date().getTime() + ".csv";
    };
    MemberPage.prototype.uploadFile = function (data) {
        console.log(data);
        var lines = data.split("\n");
        if (lines.length > 0) {
            //remove header if it exists
            if (lines.length >= 2) {
                if (lines[0].toLowerCase().indexOf("customerid") == 0) {
                    lines = lines.slice(1);
                }
            }
            //now cleanup columns
            var newlines = Array();
            for (var _i = 0, lines_1 = lines; _i < lines_1.length; _i++) {
                var l = lines_1[_i];
                var cid = l.split(",")[0];
                if (cid.indexOf("\"") == 0)
                    cid = cid.substr(1);
                if (cid.indexOf("\"") == cid.length - 1)
                    cid = cid.substr(0, cid.length - 1);
                newlines.push(cid);
            }
            var self_2 = this;
            var customers = [];
            this.customersProvider.getCustomers()
                .then(function (data) {
                for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                    var c = data_1[_i];
                    if (newlines.includes(c.customerId)) {
                        customers.push({
                            customerId: c.customerId,
                            status: "pending"
                        });
                    }
                }
                var msg = "Total of " + customers.length + " customers found";
                if (customers.length != newlines.length) {
                    msg = "Out of " + newlines.length + " customer ids, " + (newlines.length - customers.length) + " could not be found in our customer list.";
                }
                var toast = self_2.toastCtrl.create({
                    message: msg,
                    duration: 5000,
                    position: 'right',
                    cssClass: 'success'
                });
                toast.present();
                self_2.member['target'] = customers;
                self_2.member['total'] = customers.length;
                self_2.member['delivered'] = 0;
                self_2.member['viewed'] = 0;
                self_2.member['interested'] = 0;
                self_2.dirty();
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel('Error'),
                message: this.settingsProvider.getLabel("Uploaded file is empty"),
                buttons: [
                    {
                        role: 'cancel',
                        text: 'OK',
                        handler: function (data) {
                        }
                    }
                ]
            });
            alert_1.present();
        }
        this.targetedCustomers.data = this.member.target;
        this.targetedCustomers = Object.assign({}, this.targetedCustomers);
    };
    MemberPage.prototype.ionViewDidEnter = function () { this.settingsProvider.getAppLabels(this.viewCtrl.name); };
    MemberPage.prototype.dirty = function () {
        this.dirtyFlag = true;
        this.updatePreview();
    };
    MemberPage.prototype.uploadImage = function (event) {
        this.base_64_image = event.target.result;
        this.dirty();
    };
    MemberPage.prototype.removeImage = function () {
        var self = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to delete Image',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function (data) {
                        self.member.bannerurl = null;
                        self.base_64_image = null;
                        self.member.imagepath = null;
                        self.dirty();
                    }
                }
            ]
        });
        alert.present();
    };
    MemberPage.prototype.updatePreview = function () {
        var _this = this;
        console.log("member", this.member);
        this.settingsProvider.getBankLookAndFeel().then(function (lookandfeel) {
            var color = lookandfeel.primary_color;
            var image = "";
            if (_this.base_64_image) {
                image = "<img src=\"" + _this.image + "\" width=\"278px\">";
            }
            else if (_this.member.bannerurl) {
                image = "<img src=\"" + _this.image + "\" width=\"278px\">";
            }
            var title = "";
            if (_this.member.title) {
                title = "<h1>" + _this.member.title + "</h1>";
            }
            var sub_title = "";
            if (_this.member.sub_title) {
                sub_title = "<h2>" + _this.member.sub_title + "</h2>";
            }
            var description = "";
            if (_this.member.description) {
                description = "<p>" + _this.member.description + "</p>";
            }
            var memberbutton = "";
            if (_this.member.memberbutton !== null) {
                for (var _i = 0, _a = _this.member.memberbutton; _i < _a.length; _i++) {
                    var action = _a[_i];
                    memberbutton += "\n            <button ion-button small round>" + action.title + "</button>        ";
                }
            }
            switch (_this.deviceType) {
                case 'ios':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n            <html>\n              <head>\n                <style>\n                  @font-face {\n                    font-family: 'Ionicons';\n                    src: url(assets/fonts/ionicons.woff2);\n                  }\n                  body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n                  .nav-bar{position:fixed; height: 60px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n                  .logo{height: 60%; width: auto;}\n                  .content{position:fixed;height: 438px; overflow: scroll; margin-top: 60px; min-width: 300px;}\n                  .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n                  .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n                  .tab:not(:last-child){border-right: 1px solid #fff}\n                  .icon {\n                    margin-top: 5px;\n                    width: inherit;\n                    font-size: 1.4em;\n                    color: #ffffff;\n                    font-family: \"Ionicons\";\n                    height: 30px;\n                  }\n                  .tab-title {\n                    position: absolute;\n                    bottom: 5px;\n                    width: inherit;\n                    font-size: 0.7em;\n                    color: #ffffff;\n                    font-family: \"Ionicons\";\n                  }\n                  div.icon-cash:before{ content: \"\\f143\"; }\n                  div.icon-pin:before{ content: \"\\f1e4\";}\n                  div.icon-pricetags:before{ content: \"\\f48e\"; }\n                  div.icon-text:before{ content: \"\\f24f\"; }\n                  div.icon-settings::before{ content: \"\\f20d\"; }\n                  .padding{ padding: 10px; }\n                  .card {border: 1px solid #dedede; overflow: hidden; }\n                  h1, h2, p{color: " + color + " }\n                  h1{font-size: 1.5em; font-weight: normal;}\n                  h2{font-size: 1.2em; font-weight: normal;}\n                  button{\n                    padding: 5px;\n                    border-radius: 10px;\n                    background-color: " + color + ";\n                    color: white;\n                    display: block;\n                  }\n                </style>\n              </head>\n              <body>\n                <div class=\"nav-bar\">\n                  <img class=\"statusbar-img \" src=\"assets/images/ios-statusbar.png\" width=\"320\">\n                  <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n                </div>\n                <div class=\"content padding\">\n                  <div class=\"card padding\">\n                      " + image + "\n                      " + title + "   \n                      " + sub_title + "           \n                      " + description + "\n                      " + memberbutton + "\n                      \n                  </div>\n                </div>\n                <div class=\"tab-bar\">\n                <div class=\"tab\">\n                  <div class=\"icon icon-cash\"></div><div class=\"tab-title\">Accounts</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pin\"></div><div class=\"tab-title\">Near Me</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-pricetags\"></div><div class=\"tab-title\">Products</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-text\"></div><div class=\"tab-title\">Messages</div>\n                </div>\n                <div class=\"tab\">\n                  <div class=\"icon icon-settings\"></div><div class=\"tab-title\">Settings</div>\n                </div>\n              </div>\n              </body>\n              </html>\n            ");
                    break;
                case 'android':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n            <html>\n              <head>\n\n                <style>\n                  @font-face {\n                    font-family: 'Ionicons';\n                    src: url(assets/fonts/ionicons.woff2);\n                  }\n                  body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n                  .nav-bar{position:fixed; height: 70px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n                  .logo{height: 60%; width: auto;}\n                  .content{position:fixed;height: 438px; overflow: scroll; margin-top: 70px; min-width: 300px;}\n                  .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n                  .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n                  .tab:not(:last-child){border-right: 1px solid #fff}\n                  .icon {\n                    margin-top: 5px;\n                    width: inherit;\n                    font-size: 1.4em;\n                    color: #ffffff;\n                    font-family: \"Ionicons\";\n                    height: 30px;\n                  }\n                  .tab-title {\n                    position: absolute;\n                    bottom: 5px;\n                    width: inherit;\n                    font-size: 0.7em;\n                    color: #ffffff;\n                    font-family: \"Ionicons\";\n                  }\n                  div.icon-cash:before{ content: \"\\f143\"; }\n                  div.icon-pin:before{ content: \"\\f1e4\";}\n                  div.icon-pricetags:before{ content: \"\\f48e\"; }\n                  div.icon-text:before{ content: \"\\f24f\"; }\n                  div.icon-settings::before{ content: \"\\f20d\"; }\n                  .padding{ padding: 10px; }\n                  .card {border: 1px solid #dedede; overflow: hidden; }\n                  h1, h2, p{color: " + color + " }\n                  h1{font-size: 1.5em; font-weight: normal;}\n                  h2{font-size: 1.2em; font-weight: normal;}\n                  button{\n                    padding: 5px;\n                    border-radius: 10px;\n                    background-color: " + color + ";\n                    color: white;\n                    display: block;\n                  }\n                </style>\n              </head>\n              <body>\n                <div class=\"nav-bar\">\n                  <img class=\"statusbar-img \" src=\"assets/images/android-statusbar.png\" width=\"320\">\n                  <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n                </div>\n                <div class=\"content padding\">\n                  <div class=\"card padding\">\n                    " + image + "\n                    " + title + "\n                    " + sub_title + "\n                    " + description + "\n                    " + memberbutton + "\n                    \n                  </div>\n                </div>\n                <div class=\"tab-bar\">\n                  <div class=\"tab\">\n                    <div class=\"icon icon-cash\"></div><div class=\"tab-title\">Accounts</div>\n                  </div>\n                  <div class=\"tab\">\n                    <div class=\"icon icon-pin\"></div><div class=\"tab-title\">Near Me</div>\n                  </div>\n                  <div class=\"tab\">\n                    <div class=\"icon icon-pricetags\"></div><div class=\"tab-title\">Products</div>\n                  </div>\n                  <div class=\"tab\">\n                    <div class=\"icon icon-text\"></div><div class=\"tab-title\">Messages</div>\n                  </div>\n                  <div class=\"tab\">\n                    <div class=\"icon icon-settings\"></div><div class=\"tab-title\">Settings</div>\n                  </div>\n                </div>\n              </body>\n              </html>\n            ");
                    break;
                case 'windows':
                    _this.previewHtml = _this.domSanitizer.bypassSecurityTrustHtml("\n            <html>\n              <head>\n\n                <style>\n                  @font-face {\n                    font-family: 'Ionicons';\n                    src: url(assets/fonts/ionicons.woff2);\n                  }\n                  body {background-color: " + lookandfeel.page_background_color + "; color: " + lookandfeel.text_color + "; margin: 0px;font-family: Roboto, 'Helvetica Neue', sans-serif;}\n                  .nav-bar{position:fixed; height: 60px; background-color: #fefefe; text-align: center; border-bottom: 1px solid #ccc}\n                  .logo{height: 60%; width: auto;}\n                  .content{position:fixed;height: 438px; overflow: scroll; margin-top: 60px; min-width: 300px;}\n                  .tab-bar{position:fixed; bottom: 0px; height: 50px; background-color: " + color + "}\n                  .tab{ width: 63px; height: 100%; float:left; text-align:center;}\n                  .tab:not(:last-child){border-right: 1px solid #fff}\n                  .icon {\n                    margin-top: 5px;\n                    width: inherit;\n                    font-size: 1.4em;\n                    color: #ffffff;\n                    font-family: \"Ionicons\";\n                    height: 30px;\n                  }\n                  .tab-title {\n                    position: absolute;\n                    bottom: 5px;\n                    width: inherit;\n                    font-size: 0.7em;\n                    color: #ffffff;\n                    font-family: \"Ionicons\";\n                  }\n                  div.icon-cash:before{ content: \"\\f143\"; }\n                  div.icon-pin:before{ content: \"\\f1e4\";}\n                  div.icon-pricetags:before{ content: \"\\f48e\"; }\n                  div.icon-text:before{ content: \"\\f24f\"; }\n                  div.icon-settings::before{ content: \"\\f20d\"; }\n                  .padding{ padding: 10px; }\n                  .card {border: 1px solid #dedede; overflow: hidden; }\n                  h1, h2, p{color: " + color + " }\n                  h1{font-size: 1.5em; font-weight: normal;}\n                  h2{font-size: 1.2em; font-weight: normal;}\n                  button{\n                    padding: 5px;\n                    border-radius: 10px;\n                    background-color: " + color + ";\n                    color: white;\n                    display: block;\n                  }\n                </style>\n              </head>\n              <body>\n                <div class=\"nav-bar\">\n                  <img class=\"statusbar-img \" src=\"assets/images/wp-statusbar.png\" width=\"320\">\n                  <img class=\"logo\" src=\"assets/images/logo.png\" width=\"200px\">\n                </div>\n                <div class=\"content padding\">\n                  <div class=\"card padding\">\n                    " + image + "\n                    " + title + "\n                    " + sub_title + "\n                    " + description + "\n                    " + memberbutton + "\n                    \n                  </div>\n                </div>\n                <div class=\"tab-bar\">\n                  <div class=\"tab\">\n                    <div class=\"icon icon-cash\"></div><div class=\"tab-title\">Accounts</div>\n                  </div>\n                  <div class=\"tab\">\n                    <div class=\"icon icon-pin\"></div><div class=\"tab-title\">Near Me</div>\n                  </div>\n                  <div class=\"tab\">\n                    <div class=\"icon icon-pricetags\"></div><div class=\"tab-title\">Products</div>\n                  </div>\n                  <div class=\"tab\">\n                    <div class=\"icon icon-text\"></div><div class=\"tab-title\">Messages</div>\n                  </div>\n                  <div class=\"tab\">\n                    <div class=\"icon icon-settings\"></div><div class=\"tab-title\">Settings</div>\n                  </div>\n                </div>\n              </body>\n              </html>\n              ");
                    break;
                default:
                    break;
            }
            _this.cdr.detectChanges();
        });
    };
    MemberPage.prototype.changeListener = function ($event) {
        var self = this;
        var formData = new FormData();
        formData.append("file-to-upload", $event.target.files[0]);
        this.memberProvider.upload_Image(formData)
            .then(function (data) {
            var fileurl = self.globalProvider.getBaseUrl() + data.filename;
            self.member.bannerurl = data.filename;
            self.member.imagepath = fileurl;
            self.image = fileurl;
            self.updatePreview();
        }).catch(function (err) {
            console.log(err, "err");
        });
    };
    MemberPage.prototype.renderImage = function (file) {
        var self = this;
        // generate a new FileReader object
        var reader = new window.FileReader();
        // inject an image with the src url
        reader.onload = function (event) {
            var the_url = event.target.result;
            console.log("url", the_url);
            self.member.bannerurl = the_url;
            self.updatePreview();
            // self.viewCtrl.dismiss({image: the_url});
        };
        // when the file is read it triggers the onload event above.
        reader.readAsDataURL(file);
    };
    MemberPage.prototype.addAction = function () {
        var self = this;
        var modal = this.modalCtrl.create(MemberActionModal);
        modal.onDidDismiss(function (data) {
            if (data) {
                console.log("data", data);
                self.member.memberbutton = [];
                console.log(self.member.memberbutton);
                self.member.memberbutton.push(data);
                self.dirty();
            }
        });
        modal.present();
    };
    MemberPage.prototype.deleteAction = function (action, id) {
        var self = this;
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel('Confirm Delete'),
            message: this.settingsProvider.getLabel('Are you sure you want to delete') + ' ' + action.title,
            buttons: [
                {
                    text: this.settingsProvider.getLabel('Cancel'),
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function (data) {
                        var actions = [];
                        for (var _i = 0, _a = self.member.memberbutton; _i < _a.length; _i++) {
                            var a = _a[_i];
                            console.log(a.id + " --- " + id);
                            if (a.id != id) {
                                actions.push(a);
                            }
                        }
                        self.member.memberbutton = actions;
                        self.dirty();
                    }
                }
            ]
        });
        alert.present();
    };
    MemberPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Error"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    MemberPage.prototype.memberStatus = function (data) {
        var str = "";
        if (data == "Yes") {
            str = "Delivered";
        }
        else {
            str = "Not Delivered";
        }
        return str;
    };
    MemberPage.prototype.save = function () {
        var msg = [];
        if (this.member.title) {
            if (this.member.title.length <= 0)
                msg.push("Member Title cannot be blank");
        }
        else {
            msg.push("Member Title cannot be blank");
        }
        if (new Date(this.member.date) < this.todayDate)
            msg.push("Date cannot be in the past.");
        if (this.member.type == 'product' && !this.member.productId) {
            msg.push("Select Product to Push");
        }
        if (msg.length > 0) {
            var alert_2 = this.alertCtrl.create({
                title: 'Error',
                message: msg.join("<br/>"),
                buttons: [
                    {
                        role: 'cancel',
                        text: 'OK',
                        handler: function (data) {
                        }
                    }
                ]
            });
            alert_2.present();
        }
        else {
            var self_3 = this;
            var alert_3 = this.alertCtrl.create({
                title: 'Confirm Save',
                message: 'Are you sure you want to Save this Member',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function (data) {
                        }
                    },
                    {
                        text: 'Save',
                        handler: function (data) {
                            self_3.callback(self_3.member)
                                .then(function () {
                                self_3.navCtrl.pop();
                            });
                        }
                    }
                ]
            });
            alert_3.present();
        }
    };
    return MemberPage;
}());
MemberPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* IonicPage */])({ name: "MemberPage" }),
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["n" /* Component */])({
        selector: 'page-member',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\member\member.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons start>\n\n    </ion-buttons>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <button small ion-button color="primary" (click)="pushButton()" [disabled]="member.totaltarget">{{settingsProvider.getLabel(\'Push\')}}</button>\n\n\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="save()" *ngIf="dirtyFlag">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col col-6>\n\n              <ion-grid class="table-cell">\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-grid class="table-simple">\n\n                      <ion-row class="header">\n\n                        <ion-col>\n\n                          {{settingsProvider.getLabel(\'Banner Images\')}}\n\n                        </ion-col>\n\n                      </ion-row>\n\n                      <ion-row padding>\n\n                        <ion-col col-6>\n\n                          <img class="imgUpload" *ngIf="member.bannerurl" style="width:50%; height:50%" [src]="member.imagepath" alt="">\n\n                          <label style="border-radius:5px;"> {{settingsProvider.getLabel(\'Select Image\')}}\n\n                            <input type="file" id="File" accept="image/*" id="imageupload" (change)="changeListener($event)">\n\n                          </label>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                      <ion-row align-items-center>\n\n                        <ion-col>\n\n                          <div padding>\n\n                            <span *ngIf="!member.bannerurl">{{settingsProvider.getLabel("Add a banner Image to the member by uploading an image")}}</span>\n\n                            <button float-right ion-button round small color="danger" (click)="removeImage()" *ngIf="member.bannerurl">{{settingsProvider.getLabel("Remove Image")}}\n\n                            </button>\n\n                          </div>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                    </ion-grid>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-grid class="table-simple">\n\n                      <ion-row class="header">\n\n                        <ion-col>\n\n                          {{settingsProvider.getLabel(\'Member Content\')}}\n\n                        </ion-col>\n\n                      </ion-row>\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <ion-list>\n\n                            <ion-item>\n\n                              <ion-label stacked>{{settingsProvider.getLabel(\'Title\')}}</ion-label>\n\n                              <ion-input type="text" [(ngModel)]="member.title" placeholder="{{settingsProvider.getLabel(\'Member Title\')}}" (input)="dirty()"></ion-input>\n\n                            </ion-item>\n\n                            <ion-item>\n\n                              <ion-label stacked>{{settingsProvider.getLabel(\'Sub Title\')}}</ion-label>\n\n                              <ion-input type="text" [(ngModel)]="member.sub_title" placeholder="{{settingsProvider.getLabel(\'Product Sub Title\')}}" (input)="dirty()"></ion-input>\n\n                            </ion-item>\n\n                            <ion-item>\n\n                              <ion-label stacked>{{settingsProvider.getLabel(\'Description\')}}</ion-label>\n\n                              <ion-textarea [(ngModel)]="member.description" placeholder="{{settingsProvider.getLabel(\'Body\')}}" (input)="dirty()"></ion-textarea>\n\n                            </ion-item>\n\n                          </ion-list>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <p float-right>{{settingsProvider.getLabel(\'Note: You can use HTML Tags in the body\')}}</p>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                    </ion-grid>\n\n                  </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-grid class="table-simple">\n\n                      <ion-row align-items-center class="header">\n\n                        <ion-col>{{settingsProvider.getLabel(\'Action Buttons\')}}</ion-col>\n\n                      </ion-row>\n\n                      <ion-row *ngFor="let action of member.memberbutton">\n\n                        <ion-col text-nowrap>\n\n                          <button ion-button color="danger" clear icon-only float-right (click)="deleteAction(action, action.id)">\n\n                            <ion-icon name="trash"></ion-icon>\n\n                          </button>\n\n                          <h5>{{action.title}}</h5>\n\n                          <div float-left *ngIf="action.type==\'url\'">{{settingsProvider.getLabel(\'URL\')}} &nbsp;</div>\n\n                          <div float-left *ngIf="action.type==\'call\'">{{settingsProvider.getLabel(\'Call Number\')}} &nbsp;\n\n                          </div>\n\n                          <div float-left *ngIf="action.type==\'callback\'">{{settingsProvider.getLabel(\'Callback Request\')}} &nbsp;</div>\n\n                          <a *ngIf="action.type==\'url\'" [href]="action.action" [target]="\'_blank\'">{{action.action}}</a>\n\n                          <div *ngIf="action.type!=\'url\'">{{action.action}}</div>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <button small ion-button (click)="addAction()">Add Action</button>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                    </ion-grid>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n            <ion-col col-6>\n\n              <ion-grid class="table-cell">\n\n                <ion-row justify-content-center>\n\n                  <ion-col text-center text-nowrap padding>\n\n                    <div class="header">{{settingsProvider.getLabel(\'Live Member Preview\')}}</div>\n\n                    <ion-segment [(ngModel)]="deviceType" padding-top padding-bottom>\n\n                      <ion-segment-button value="ios">{{settingsProvider.getLabel("iOS")}}</ion-segment-button>\n\n                      <ion-segment-button value="android">{{settingsProvider.getLabel("Android")}}</ion-segment-button>\n\n                      <ion-segment-button value="windows">{{settingsProvider.getLabel("Windows")}}</ion-segment-button>\n\n                    </ion-segment>\n\n                    <div class="preview" id="demo-device-ios" *ngIf="deviceType==\'ios\'">\n\n                      <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n                    </div>\n\n                    <div class="preview" id="demo-device-android" *ngIf="deviceType==\'android\'">\n\n                      <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n                    </div>\n\n                    <div class="preview" id="demo-device-windows" *ngIf="deviceType==\'windows\'">\n\n                      <iframe [srcdoc]="previewHtml" frameborder="0"></iframe>\n\n                    </div>\n\n                  </ion-col>\n\n                </ion-row>\n\n\n\n                <!--Targeted Customers-->\n\n\n\n\n\n\n\n              </ion-grid>\n\n            </ion-col>\n\n\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-cell">\n\n          <ion-row justify-content-center>\n\n            <ion-col text-center text-nowrap padding>\n\n              <div class="header">\n\n                {{settingsProvider.getLabel(\'Targeted Customers\')}}\n\n              </div>\n\n              <div class="button_labels">\n\n                <label for="uploadFile">{{settingsProvider.getLabel(\'Upload CSV\')}}</label>\n\n                <input type="file" id="uploadFile" (change)="onFileChange($event)" #fileInput>\n\n                <label for="uploadFile" style="margin-left: 0px;" *ngIf="isdelivered">{{languageProvider.get(\'Target all Customers\')}}</label>\n\n                <label style="margin-left: 0px;" *ngIf="!isdelivered">{{languageProvider.get(\'Resend\')}}</label>\n\n                <label for="uploadFile" style=" margin-left: 0px;" *ngIf="isdelivered">{{settingsProvider.getLabel(\'Target all Customers\')}}</label>\n\n                <!-- <label style="margin-left: 0px;" *ngIf="!isdelivered">{{settingsProvider.getLabel(\'Resend\')}}</label> -->\n\n                <!-- <button ion-button small (click)="downloadCsv()">\n\n                  {{settingsProvider.getLabel(\'Download Sample CSV\')}}\n\n                </button> -->\n\n                <a ion-button small clear (click)="downloadPdf()" style="margin-top: 10px; float:right;">\n\n                  {{settingsProvider.getLabel(\'Download PDF\')}}\n\n                </a>\n\n                <a (click)="downloadCsv()" ion-button small clear style="margin-top: 10px; float:right;">\n\n                  {{settingsProvider.getLabel(\'Download Sample CSV\')}}\n\n                </a>\n\n\n\n              </div>\n\n              <table-component [options]="targetedCustomers"></table-component>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\member\member.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_0__providers_member_member__["a" /* MemberProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_7__providers_customers_customers__["a" /* CustomersProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_push_messaging_push_messaging__["a" /* PushMessagingProvider */],
        __WEBPACK_IMPORTED_MODULE_9__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_10__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_11__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], MemberPage);

var MemberActionModal = (function () {
    function MemberActionModal(alertCtrl, languageProvider, settingsProvider, viewCtrl, navCtrl) {
        this.alertCtrl = alertCtrl;
        this.languageProvider = languageProvider;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.title = "";
        this.type = "";
        this.action = "";
        this.languageString = "en";
        this.resultdata = null;
    }
    MemberActionModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    MemberActionModal.prototype.save = function () {
        var errors = [];
        var self = this;
        if (this.title.length == 0) {
            errors.push(self.settingsProvider.getLabel("Title cannot be blank"));
        }
        if (this.type.length == 0) {
            errors.push("Type must be selected");
        }
        if (this.action.length == 0 && this.type != "callback") {
            errors.push(this.settingsProvider.getLabel("Action must be provided."));
        }
        if (errors.length > 0) {
            var alert_4 = this.alertCtrl.create({
                title: this.settingsProvider.getLabel("Confirm Delete"),
                message: errors.join('<br/> '),
                buttons: [
                    {
                        text: this.settingsProvider.getLabel('OK'),
                        role: 'cancel',
                    }
                ]
            });
            alert_4.present();
        }
        else {
            this.viewCtrl.dismiss({ title: this.title, type: this.type, action: this.action });
        }
    };
    return MemberActionModal;
}());
MemberActionModal = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["n" /* Component */])({
        selector: 'member-action',
        template: "<ion-header color=\"secondary\">\n  <ion-navbar>\n    <ion-buttons start>\n      <button ion-button icon-left (click)=\"dismiss()\" style=\"color:white\">\n        <ion-icon name=\"arrow-round-back\"></ion-icon>\n        {{settingsProvider.getLabel('Back')}}\n      </button>\n    </ion-buttons>\n    <ion-title>{{settingsProvider.getLabel('Add Action')}}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-left (click)=\"save()\" style=\"color:white\">\n        <ion-icon name=\"done-all\"></ion-icon>\n        {{settingsProvider.getLabel('Add Action')}}\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <ion-list>\n      <ion-item>\n        <ion-label stacked>{{settingsProvider.getLabel('Title')}}</ion-label>\n        <ion-input type=\"text\" [(ngModel)]=\"title\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>{{settingsProvider.getLabel('Type')}}</ion-label>\n        <ion-select [(ngModel)]=\"type\">\n          <ion-option value=\"url\">URL</ion-option>\n          <ion-option value=\"call\">Call Number</ion-option>\n          <ion-option value=\"callback\">Call Back</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item *ngIf=\"type!='callback'\">\n        <ion-label stacked>{{settingsProvider.getLabel('Action')}}</ion-label>\n        <ion-input [(ngModel)]=\"action\"></ion-input>\n      </ion-item>\n    </ion-list>\n"
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* NavController */]])
], MemberActionModal);

//# sourceMappingURL=member.js.map

/***/ }),

/***/ 503:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TestPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TestPage = (function () {
    function TestPage(languageProvider, navCtrl, navParams) {
        this.languageProvider = languageProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TestPage.prototype.ionViewDidLoad = function () {
    };
    return TestPage;
}());
TestPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])({ name: 'TestPage' }),
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-test',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\test\test.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{\'Test Page\'}} | Consectus</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\test\test.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */]])
], TestPage);

//# sourceMappingURL=test.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_utils_utils__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { Pagination } from './../pagination/pagination';


/**
 * options: {
  data: [],  Original data Array of objects that is to be used to create the list of items to display
  filteredData: [] this is the data that is used to display the table ... this has all the search applied
  columns: { // list of properties of the data object to show in columns
    title: shows as column title
    search: true or false to allow searching in this column
    format: data / currency / count / number / function --- check getValue method below
    mobile: true or false ... to show on mobile device if it is cordova app
    col: nn force col-nn to be output to force width of the column
      "name": {title: "Product Name", search: true, mobile: true, col: 4 },
      "unit": {title: "Unit", search: true, mobile: false, col: 2 },
      "sales_price": {title: "Price", search: false, mobile: false, col: 2 },
      "stock": {title: "Stock", search: false, mobile: false, col: 2 },
    },
  pagination: true, // to display pagination control on top right or not
  search: true, // do display search bar on top or not
  filter: null, // initial filter to be applied ... useful when click of chart etc
                  needs data to be filtered by default e.g. show all active customers or blocked customers
                  the filter will be "BLOCKED" or "ACTIVE"
  select: true, // allow user to select records using a checkbox on left column
  selectedActions: [], // actions that are available to click when records are selected
  download: true, // allow user ot download CSV or JSON files ... link at the bottom of the table
  btnSize: "small", // size of buttons to show
  pageSize: 13, // default page size
  rowActions: [], // each row can have a number of actions per row
    title: title of button to show,
    actoin: function reference to invoke
  { title: "Add", action: fn_reference}

  rowClass: "table-row", // additional class names to add to rows
  columnClass: "table-column", //addiitonal class names to add to columns
  add: { // overall Add Button to be shown on top right corner to enable create new record
    addActionTitle: null, // title of button
    addAction: null, // function to call when clicked
  },
  tapRow: null,  // function to be called when item is tapped ... like detail push
  resetFilter: null, // function to call when filter is reset or cleared
  uploadOptions: { // allow users to upload data ... work in progress ... empty object will do.
    title: "Upload", // title of button
    type: "csv", //type of file format image, csv, json
    btnType: 'clear', //clear, round
    btnSize: 'small', //small, large
    btnColor: 'primary', //primary, secondary ... ionic color values
    btnCallback: null // callback with data when uploaded
  }
 * pagination: true
 */
var TableComponent = (function () {
    function TableComponent(domSanitizer, platform, renderer, utilsProvider) {
        this.domSanitizer = domSanitizer;
        this.platform = platform;
        this.renderer = renderer;
        this.utilsProvider = utilsProvider;
        this.options = {
            data: [],
            filteredData: [],
            pagination: true,
            search: true,
            filter: null,
            select: true,
            download: true,
            btnSize: "small",
            pageSize: 13,
            columns: [],
            selectedActions: [],
            allActions: [],
            rowActions: [],
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: null,
                addAction: null,
            },
            tapRow: null,
            resetFilter: null,
            uploadOptions: {}
        };
        this.csvFileName = "export-" + new Date().getTime() + ".csv";
        this.jsonFileName = "export-" + new Date().getTime() + ".json";
        this.filteredData = [];
        this.selected = false;
        this.selectedCount = 0;
        this.paginationInfo = {
            page: 1,
            pageSize: 10,
            rowCount: 0,
            pageCount: 1
        };
        this.pageData = [];
        this.selectPage = false;
        this.sortColumns = {};
        this.valid = true;
        this.msg = [];
        this.actionCols = "";
        this.columnKeys = [];
    }
    TableComponent.prototype.getColumnFields = function () {
        if (this.options.columns) {
            var keys = [];
            for (var _i = 0, _a = Object.keys(this.options.columns); _i < _a.length; _i++) {
                var key = _a[_i];
                if (this.platform.is("cordova")) {
                    if (this.options.columns[key].hide == undefined && this.options.columns[key].mobile) {
                        keys.push(key);
                    }
                }
                else {
                    // if (this.options.columns[key].hide == undefined && this.options.columns[key].mobile){
                    if (this.options.columns[key].hide == undefined) {
                        keys.push(key);
                    }
                }
            }
            return keys;
        }
        else {
            return {};
        }
    };
    TableComponent.prototype.ngOnInit = function () {
        if (this.options.allActions === undefined)
            this.options.allActions = [];
        if (this.options.pageSize != null)
            this.pageS = new Number(this.options.pageSize).valueOf();
        else
            this.pageS = 10;
        //standardise page size
        switch (this.pageS) {
            case 5:
            case 10:
            case 20:
            case 50:
            case 100:
                break;
            default:
                this.pageS = 10;
                break;
        }
        if (this.options.columns == null) {
            this.valid = false;
            this.msg.push("No Columns");
        }
        else {
            if (this.options.data == null) {
                this.valid = false;
                this.msg.push("No Data");
            }
            else if (typeof this.options.data != 'object') {
                this.valid = false;
                this.msg.push("Data not an array of objects");
            }
            this.paginationInfo.page = 1;
            this.paginationInfo.pageSize = this.pageS;
            this.paginationInfo.rowCount = this.filterData.length;
            this.paginationInfo.pageCount = Math.ceil(this.paginationInfo.rowCount / this.paginationInfo.pageSize);
            if (this.paginationInfo.pageCount == 0) {
                this.paginationInfo.pageCount = this.paginationInfo.pageCount + 1;
            }
        }
        this.columnKeys = this.getColumnFields();
    };
    TableComponent.prototype.ngOnChanges = function (changes) {
        // this.options = changes.options.currentValue;
        this.filterData(this.options.filter);
    };
    TableComponent.prototype.getRowClass = function (item) {
        if (this.options.rowClass != null)
            return this.options.rowClass;
        else
            return '';
    };
    TableComponent.prototype.getColumnClass = function (item) {
        if (this.options.columnClass != null)
            return this.options.columnClass;
        else
            return '';
    };
    TableComponent.prototype.addActionClicked = function (action) {
        action();
    };
    TableComponent.prototype.pageSizeChanged = function (data) {
        this.pageS = data;
        this.filterData('');
        this.utilsProvider.hide();
    };
    TableComponent.prototype.changePage = function (i) {
        this.setPageInfo(i);
        // this.cdr.detectChanges();
    };
    TableComponent.prototype.setPageInfo = function (i) {
        this.paginationInfo.page = i;
        this.paginationInfo.rowCount = this.options.filteredData.length;
        if (this.paginationInfo.pageSize == undefined)
            this.paginationInfo.pageSize = this.options.pageSize;
        this.paginationInfo.pageCount = Math.ceil(this.paginationInfo.rowCount / this.paginationInfo.pageSize);
        var startIndex = (this.paginationInfo.page - 1) * this.paginationInfo.pageSize;
        this.pageData = this.options.filteredData.slice(startIndex, startIndex + this.paginationInfo.pageSize);
        // this.cdr.detectChanges();
        if (this.paginationInfo.pageCount == 0) {
            this.paginationInfo.pageCount = this.paginationInfo.pageCount + 1;
        }
    };
    TableComponent.prototype.filterData = function (val) {
        var self = this;
        if (this.options.filter === null)
            val = '';
        else
            val = this.options.filter;
        if (val && val.trim().length > 0) {
            val = val.trim().split(' ');
            this.options.filteredData = this.options.data.filter(function (item) {
                var ret = {};
                for (var _i = 0, val_1 = val; _i < val_1.length; _i++) {
                    var v = val_1[_i];
                    ret[v] = false;
                }
                for (var _a = 0, _b = Object.keys(self.options.columns); _a < _b.length; _a++) {
                    var key = _b[_a];
                    if (item[key] !== null && item[key] !== undefined) {
                        if (self.options.columns[key].search) {
                            for (var _c = 0, val_2 = val; _c < val_2.length; _c++) {
                                var v = val_2[_c];
                                item[key] = item[key].toString();
                                ret[v] = ret[v] || item[key].toLowerCase().indexOf(v.toLowerCase()) > -1;
                            }
                        }
                    }
                }
                var outcome = true;
                for (var _d = 0, val_3 = val; _d < val_3.length; _d++) {
                    var v = val_3[_d];
                    outcome = outcome && ret[v];
                }
                return outcome;
            });
        }
        else {
            this.options.filteredData = this.options.data.slice(0);
        }
        this.sort();
        this.paginationInfo.page = 1;
        this.paginationInfo.pageSize = this.pageS;
        this.paginationInfo.rowCount = this.options.filteredData.length;
        this.paginationInfo.pageCount = Math.ceil(this.paginationInfo.rowCount / this.paginationInfo.pageSize);
        if (this.paginationInfo.pageCount == 0) {
            this.paginationInfo.pageCount = this.paginationInfo.pageCount + 1;
        }
        this.setPageInfo(this.paginationInfo.page);
        this.selectAll();
    };
    TableComponent.prototype.searchEvent = function (ev) {
        var self = this;
        var val = ev.target.value;
        this.filterData(val);
        clearTimeout(this.blurEvent);
        this.blurEvent = setTimeout(function () {
            self.renderer.invokeElementMethod(ev.target, 'blur');
        }, 5000);
    };
    TableComponent.prototype.clearFilter = function () {
        this.options.filter = null;
        this.options.resetFilter.next();
    };
    TableComponent.prototype.selectAll = function () {
        // var startIndex = (this.paginationInfo.page-1)*this.paginationInfo.pageSize;
        this.selectedCount = 0;
        for (var i = 0; i < this.options.filteredData.length; i++) {
            if (this.selectAllRecords) {
                this.options.filteredData[i].selected = true;
                this.selectedCount += 1;
            }
            else {
                this.options.filteredData[i].selected = false;
            }
        }
    };
    TableComponent.prototype.selectRecord = function (event, i) {
        this.selected = false;
        this.selectedCount = 0;
        for (var _i = 0, _a = this.options.filteredData; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.selected) {
                this.selected = true;
                this.selectedCount += 1;
            }
        }
        // this.cdr.detectChanges();
    };
    TableComponent.prototype.sort = function () {
        var self = this;
        var _loop_1 = function (key) {
            this_1.options.filteredData = this_1.options.filteredData.sort(function (a, b) {
                if (self.sortColumns[key] > 0) {
                    if (self.options.columns[key]['format'] == 'date') {
                        if (new Date(a[key]) < new Date(b[key]))
                            return -1;
                        else if (new Date(a[key]) > new Date(b[key]))
                            return 1;
                        else
                            return 0;
                    }
                    else if (self.options.columns[key]['format'] == 'date') {
                        if (new Number(a[key]) < new Number(b[key]))
                            return -1;
                        else if (new Number(a[key]) > new Number(b[key]))
                            return 1;
                        else
                            return 0;
                    }
                    else {
                        if (a[key] < b[key])
                            return -1;
                        else if (a[key] > b[key])
                            return 1;
                        else
                            return 0;
                    }
                }
                else if (self.sortColumns[key] < 0) {
                    if (self.options.columns[key]['format'] == 'date') {
                        if (new Date(a[key]) < new Date(b[key]))
                            return 1;
                        else if (new Date(a[key]) > new Date(b[key]))
                            return -1;
                        else
                            return 0;
                    }
                    else if (self.options.columns[key]['format'] == 'date') {
                        if (new Number(a[key]) < new Number(b[key]))
                            return 1;
                        else if (new Number(a[key]) > new Number(b[key]))
                            return -1;
                        else
                            return 0;
                    }
                    else {
                        if (a[key] < b[key])
                            return 1;
                        else if (a[key] > b[key])
                            return -1;
                        else
                            return 0;
                    }
                }
                else {
                    return 0;
                }
            });
        };
        var this_1 = this;
        for (var _i = 0, _a = Object.keys(this.options.columns); _i < _a.length; _i++) {
            var key = _a[_i];
            _loop_1(key);
        }
    };
    TableComponent.prototype.sortClicked = function (field) {
        for (var _i = 0, _a = Object.keys(this.options.columns); _i < _a.length; _i++) {
            var key = _a[_i];
            if (key == field) {
                this.filterData(this.searchBar.value);
                if (this.sortColumns[key] == 0 || !this.sortColumns[key]) {
                    this.sortColumns[key] = 1;
                }
                else if (this.sortColumns[key] == 1) {
                    this.sortColumns[key] = -1;
                }
                else {
                    this.sortColumns[key] = 0;
                }
            }
            else {
                this.sortColumns[key] = 0;
            }
        }
        this.sort();
        this.setPageInfo(1);
        // this.cdr.detectChanges();
    };
    TableComponent.prototype.allAction = function (action) {
        action.action();
    };
    TableComponent.prototype.selectedAction = function (action) {
        var arr = this.options.filteredData.filter(function (item) {
            return item.selected;
        });
        action.action(arr);
        this.selectAllRecords = false;
        this.selectAll();
    };
    TableComponent.prototype.getValue = function (value, format, column, item) {
        if (format === 'date') {
            if (value === null || value === '')
                return '';
            var d = new Date(value).toISOString();
            return d.substr(8, 2) + "/" + d.substr(5, 2) + "/" + d.substr(0, 4);
        }
        else if (format === 'currency') {
            if (value === null)
                return '';
            return new Number(value).toLocaleString("en-IN", {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
                useGrouping: false,
                style: 'currency',
                currency: 'GBP'
            });
        }
        else if (format === 'count') {
            if (value === null)
                return '';
            if (Array.isArray(value))
                return value.length;
            else
                return 0;
        }
        else if (format === 'number') {
            if (value === null)
                return '';
            return new Number(value).valueOf();
        }
        else if (format === 'function') {
            if (column.fn !== undefined)
                return column.fn(value, column, item);
            else
                return value;
        }
        else {
            return value;
        }
    };
    TableComponent.prototype.rowAction = function (action, item) {
        action.action(item);
    };
    TableComponent.prototype.downloadCsv = function () {
        var csvContent = "";
        if (this.options.filteredData.length > 0) {
            var columnNames = Object.keys(this.options.filteredData[0]);
            if (columnNames.indexOf('selected') >= 0) {
                columnNames.splice(columnNames.indexOf('selected'), 1);
            }
            csvContent += columnNames.join(',');
            csvContent += "\r";
            var cols = columnNames.length;
            for (var j = 0; j < this.options.filteredData.length; j++) {
                var item = this.options.filteredData[j];
                for (var i = 0; i < columnNames.length; i++) {
                    var key = columnNames[i];
                    var value = item[key] !== undefined ? "\"" + item[key] + "\"" : "\"\"";
                    csvContent += (i === cols - 1) ? value : value + ",";
                }
                csvContent += "\r";
            }
        }
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvContent);
        hiddenElement.target = '_blank';
        hiddenElement.download = this.getCsvFileName();
        hiddenElement.click();
    };
    TableComponent.prototype.downloadJson = function () {
        var jsonContent = JSON.stringify(this.options.filteredData);
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(jsonContent);
        hiddenElement.target = '_blank';
        hiddenElement.download = this.getJsonFileName();
        hiddenElement.click();
    };
    TableComponent.prototype.getCsvFileName = function () {
        return "consectus-" + new Date().getTime() + ".csv";
    };
    TableComponent.prototype.getJsonFileName = function () {
        return "consectus-" + new Date().getTime() + ".json";
    };
    TableComponent.prototype.swipe = function (event) {
    };
    return TableComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_12" /* ViewChild */])("searchBar"),
    __metadata("design:type", Object)
], TableComponent.prototype, "searchBar", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["E" /* Input */])(),
    __metadata("design:type", Object)
], TableComponent.prototype, "options", void 0);
TableComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
        selector: 'table-component',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\table\table.html"*/'<!-- Generated template for the TableComponent component -->\n\n<div *ngIf="!valid">\n\n  <div *ngFor="let m of msg">\n\n    {{m}}\n\n  </div>\n\n</div>\n\n<div *ngIf="valid">\n\n\n\n  <ion-searchbar *ngIf="options.search === true" #searchBar [(ngModel)]="options.filter" (ionInput)="searchEvent($event)"></ion-searchbar>\n\n  <div (swipe)="swipte($event)">\n\n    <ion-grid class="control">\n\n      <ion-row align-items-center *ngIf="options.pagination!=null || options.select!=null || options.add.addActionTitle!=null || options.filter!=null">\n\n        <ion-col *ngIf="selectedCount>0" col-auto>\n\n          <div ion-button color="light" [large]="options.btnSize === \'large\'" [small]="options.btnSize === \'small\'">\n\n            Selected: {{selectedCount}}\n\n          </div>\n\n          <button ion-button (click)="selectedAction(action)" *ngFor="let action of options.selectedActions" [color]="action.color"\n\n            [large]="options.btnSize === \'large\'" [small]="options.btnSize === \'small\'">{{action.title}}</button>\n\n        </ion-col>\n\n        <ion-col *ngIf="filter">\n\n          <button ion-button color="light" (click)="changePageSize($event)" [large]="options.btnSize === \'large\'" [small]="options.btnSize === \'small\'">\n\n            Showing: Status {{options.filter}}\n\n            <button margin-left small round ion-button color="danger" (click)="clearFilter()">\n\n              Clear\n\n            </button>\n\n          </button>\n\n        </ion-col>\n\n        <ion-col float-left *ngIf="options.add.addActionTitle">\n\n          <button ion-button float-left (click)="options.add.addAction(null)" [large]="options.btnSize === \'large\'" [small]="options.btnSize === \'small\'">\n\n            {{options.add.addActionTitle}}</button>\n\n        </ion-col>\n\n        <ion-col col-auto *ngIf="options.pagination">\n\n          <pagination color="light" (clickBeginning)="changePage(1)" (clickPrevious)="changePage(paginationInfo.page - 1)" (clickNext)="changePage(paginationInfo.page + 1)"\n\n            (clickEnd)="changePage(paginationInfo.pageCount)" (pageSizeChanged)="pageSizeChanged($event)" [attr.small]="options.btnSize === \'small\' ? \'small\' : null"\n\n            [attr.large]="options.btnSize === \'large\' ? \'large\' : null" [pagination]="paginationInfo" float-right>\n\n          </pagination>\n\n        </ion-col>\n\n        <ion-col float-right *ngIf="options.allActions.length > 0">\n\n          <button ion-button float-right (click)="allAction(action)" *ngFor="let action of options.allActions" [color]="action.color"\n\n            [large]="options.btnSize === \'large\'" [small]="options.btnSize === \'small\'">{{action.title}}</button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </div>\n\n  <ion-grid class="table">\n\n    <ion-row class="header" align-items-center>\n\n      <ion-col checkbox col-1 *ngIf="options.select">\n\n        <ion-label>All</ion-label>\n\n        <ion-checkbox mode="ios" [(ngModel)]="selectAllRecords" (ionChange)="selectAll()"></ion-checkbox>\n\n      </ion-col>\n\n      <ion-col *ngFor="let key of columnKeys" [attr.col-1]="options.columns[key].col==\'1\'? \'\': null" [attr.col-2]="options.columns[key].col==\'2\'? \'\': null"\n\n        [attr.col-3]="options.columns[key].col==\'3\'? \'\': null" [attr.col-4]="options.columns[key].col==\'4\'? \'\': null" [attr.col-5]="options.columns[key].col==\'5\'? \'\': null"\n\n        [attr.col-6]="options.columns[key].col==\'6\'? \'\': null" [attr.col-7]="options.columns[key].col==\'7\'? \'\': null" [attr.col-8]="options.columns[key].col==\'8\'? \'\': null"\n\n        [attr.col-9]="options.columns[key].col==\'9\'? \'\': null" [attr.col-10]="options.columns[key].col==\'10\'? \'\': null" [attr.col-11]="options.columns[key].col==\'11\'? \'\': null"\n\n        [attr.col-12]="options.columns[key].col==\'12\'? \'\': null">\n\n        <button ion-button clear icon-right (tap)="sortClicked(key)">\n\n          {{options.columns[key].title}}\n\n          <ion-icon *ngIf="sortColumns[key]>0" name="arrow-round-up"></ion-icon>\n\n          <ion-icon *ngIf="sortColumns[key]<0" name="arrow-round-down"></ion-icon>\n\n        </button>\n\n      </ion-col>\n\n      <ion-col *ngIf="options.rowActions.length>0" [attr.col-1]="actionCols==1? \'\': null" [attr.col-2]="actionCols==2? \'\': null"\n\n        [attr.col-3]="actionCols==3? \'\': null" [attr.col-4]="actionCols==4? \'\': null" [attr.col-5]="actionCols==5? \'\': null"\n\n        [attr.col-6]="actionCols==6? \'\': null" [attr.col-7]="actionCols==7? \'\': null" [attr.col-8]="actionCols==8? \'\': null"\n\n        [attr.col-9]="actionCols==9? \'\': null">\n\n        <button ion-button clear>Actions</button>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row *ngFor="let item of pageData; let i=index;" align-items-center class="select" [ngClass]="item.status==\'BLOCKED\'? \'blocked\':\'\'"\n\n      [class]="getRowClass(item)">\n\n      <ion-col col-1 *ngIf="options.select">\n\n        <ion-checkbox mode="ios" float-left [(ngModel)]="options.filteredData[(paginationInfo.page-1)*paginationInfo.pageSize+i].selected"\n\n          (ionChange)="selectRecord($event, i)"></ion-checkbox>\n\n      </ion-col>\n\n      <ion-col *ngFor="let key of columnKeys" (tap)="options.tapRow(item)" [attr.col-1]="options.columns[key].col==\'1\'? \'\': null"\n\n        [attr.col-2]="options.columns[key].col==\'2\'? \'\': null" [attr.col-3]="options.columns[key].col==\'3\'? \'\': null" [attr.col-4]="options.columns[key].col==\'4\'? \'\': null"\n\n        [attr.col-5]="options.columns[key].col==\'5\'? \'\': null" [attr.col-6]="options.columns[key].col==\'6\'? \'\': null" [attr.col-7]="options.columns[key].col==\'7\'? \'\': null"\n\n        [attr.col-8]="options.columns[key].col==\'8\'? \'\': null" [attr.col-9]="options.columns[key].col==\'9\'? \'\': null" [attr.col-10]="options.columns[key].col==\'10\'? \'\': null"\n\n        [attr.col-11]="options.columns[key].col==\'11\'? \'\': null" [attr.col-12]="options.columns[key].col==\'12\'? \'\': null" [attr.col-auto]="options.columns[key].col==\'auto\'? \'auto\': null"\n\n        [ngClass]="getColumnClass(item)" [innerHTML]="getValue(item[key], options.columns[key].format, options.columns[key], item)">\n\n      </ion-col>\n\n      <ion-col *ngIf="options.rowActions.length>0" [attr.col-1]="actionCols==1? \'\': null" [attr.col-2]="actionCols==2? \'\': null" [attr.col-3]="actionCols==3? \'\': null"\n\n        [attr.col-4]="actionCols==4? \'\': null" [attr.col-5]="actionCols==5? \'\': null" [attr.col-6]="actionCols==6? \'\': null"\n\n        [attr.col-7]="actionCols==7? \'\': null" [attr.col-8]="actionCols==8? \'\': null" [attr.col-9]="actionCols==9? \'\': null"\n\n        [attr.col-9]="actionCols==10? \'\': null" [attr.col-9]="actionCols==11? \'\': null" [attr.col-9]="actionCols==12? \'\': null"\n\n        text-nowrap>\n\n        <button ion-button small round [style.backgroundColor]="(action.color)?action.color:primary" *ngFor="let action of options.rowActions" (click)="rowAction(action, item)">{{action.title}}</button>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row align-items-center *ngIf="options.download != undefined && options.download === true" class="download">\n\n      <ion-col>\n\n        <upload-button *ngIf="options.uploadOptions.btnCallback !== undefined && options.uploadOptions.btnCallback !==  null" float-left\n\n          [options]="options.uploadOptions"></upload-button>\n\n        <button float-right ion-button (click)="downloadCsv()" [large]="options.btnSize === \'large\'" [small]="options.btnSize === \'small\'">Download CSV File</button>\n\n        <button float-right ion-button (click)="downloadJson()" [large]="options.btnSize === \'large\'" [small]="options.btnSize === \'small\'">Download JSON</button>\n\n        <!-- <div class="message" float-right>Right click and save as file on you local machine: </div> -->\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</div>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\components\table\table.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__angular_core__["Z" /* Renderer */],
        __WEBPACK_IMPORTED_MODULE_0__providers_utils_utils__["a" /* UtilsProvider */]])
], TableComponent);

//# sourceMappingURL=table.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushMessagingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PushMessagingProvider = (function () {
    function PushMessagingProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
    }
    PushMessagingProvider.prototype.getMessages = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getMessages()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    PushMessagingProvider.prototype.getmessages = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getmessages()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    PushMessagingProvider.prototype.addMessage = function (message) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addMessage(message)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    PushMessagingProvider.prototype.updatemessage = function (updatemsg) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updatemessage(updatemsg)
                .then(function () {
                resolve();
            });
        });
    };
    PushMessagingProvider.prototype.deleteMessage = function (message) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteMessage(message)
                .then(function () {
                resolve();
            });
        });
    };
    PushMessagingProvider.prototype.deletepushMessage = function (pushmessageId) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deletepushMessage(pushmessageId)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    PushMessagingProvider.prototype.activatePushMessage = function (pushmessageId) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.activatePushMessage(pushmessageId)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    PushMessagingProvider.prototype.getFailedMessage = function (result) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getFailedMessage(result)
                .then(function (contactUsMessages) {
                resolve(contactUsMessages);
            });
        });
    };
    PushMessagingProvider.prototype.getContactUsMessages = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getContactUsMessages()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    PushMessagingProvider.prototype.deleteContactUsMessage = function (contactus) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteContactUsMessage(contactus)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    PushMessagingProvider.prototype.getcustomerbymessagesid = function (pushMessaging) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getcustomerbymessagesid(pushMessaging)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    PushMessagingProvider.prototype.updateMessage_TargetCustomer = function (formData) {
        var self = this;
        var url = "/accounts/updatepushmessagecustomercsv";
        return new Promise(function (resolve, reject) {
            self.gateway.updateMessage_TargetCustomer(url, formData)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    return PushMessagingProvider;
}());
PushMessagingProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], PushMessagingProvider);

//# sourceMappingURL=push-messaging.js.map

/***/ }),

/***/ 553:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_table_table__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SettingsPage = (function () {
    function SettingsPage(oauthProvider, languageProvider, settingsProvider, alertCtrl, toastCtrl, viewCtrl, domSanitizer, cdr, navCtrl, navParams, globalProvider, masterpermissionProvider) {
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.settingsProvider = settingsProvider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.domSanitizer = domSanitizer;
        this.cdr = cdr;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.globalProvider = globalProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.currentTab1 = "activeBranches";
        this.currentTab2 = "activeFAQ";
        this.currentTabtnc = "onboarding";
        this.terms = {
            onboarding: "",
            newaccount: ""
        };
        this.title = "App Settings";
        this.currentTab = "branches";
        this.languageString = "en";
        this.resultdata = null;
        this.options1 = {
            data: [],
            filteredData: [],
            columns: {
                "title": { title: "Title", search: true, mobile: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "description": { title: "Description", search: true, mobile: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "category": { title: "Category", search: true, mobile: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "type": { title: "Type", mobile: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.deletefaq.bind(this),
                    color: 'danger'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add FAQs",
                addAction: this.createFaqs.bind(this),
            },
            tapRow: this.editFaqs.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
        this.options12 = {
            data: [],
            filteredData: [],
            columns: {
                "title": { title: "Title", search: true, mobile: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "description": { title: "Description", search: true, mobile: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "type": { title: "Type", mobile: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Activate",
                    action: this.activate.bind(this),
                    color: 'primary'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add FAQs",
                addAction: this.createFaqs.bind(this),
            },
            tapRow: this.editFaqs.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
        this.new = false;
        this.canUpdate = false;
        // onboarding: boolean = false;
        // newaccount: boolean = false;
        this.contactSubjectOptions = {
            data: [],
            filteredData: [],
            columns: {
                "customersid": { title: "Consectus ID", search: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "memberid": { title: "Member ID", search: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "customername": { title: "Customer Name", search: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "subject": { title: "Subject", search: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "description": { title: "Message", search: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "create_date": { title: "Date", search: true, format: 'function', fn: this.formatDate.bind(this) },
                "isresolved": { title: "Status", format: 'function', fn: this.commonFunction.bind(this), search: true },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.deleteContact.bind(this),
                    color: 'danger'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {},
            tapRow: this.editContactSubject.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
        this.branchOptions = {
            data: [],
            filteredData: [],
            columns: {
                "name": { title: "Name", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "address_line1": { hide: true, search: true, mobile: true },
                "address_line2": { hide: true, search: true, mobile: true },
                "address_line3": { hide: true, search: true, mobile: true },
                "address_line4": { hide: true, search: true, mobile: true },
                "opening_hours": {
                    title: "Opening Hours", format: "function", fn: function (value, column, item) {
                        var ret = "<div>";
                        for (var i = 0; i < value.length; i++) {
                            ret += value[i].days + " " + value[i].hours + "</div>";
                            if (i < value.length - 1)
                                ret += "<div>";
                        }
                        ret += "</div>";
                        return ret;
                    }, search: false, mobile: true
                },
                "tel": { title: "Telephone", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "fax": { title: "Fax", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "postalcode": { title: "Postal Code", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "email": { title: "Email Id", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
            },
            pagination: true,
            filter: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.delete.bind(this),
                    color: 'danger'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Branch",
                addAction: this.createBranch.bind(this),
            },
            tapRow: this.editBranch.bind(this),
            uploadOptions: {},
        };
        this.branch1Options = {
            data: [],
            filteredData: [],
            columns: {
                "name": { title: "Name", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "address_line1": { hide: true, search: true, mobile: true },
                "address_line2": { hide: true, search: true, mobile: true },
                "address_line3": { hide: true, search: true, mobile: true },
                "address_line4": { hide: true, search: true, mobile: true },
                "opening_hours": {
                    title: "Opening Hours", format: "function", fn: function (value, column, item) {
                        var ret = "<div>";
                        for (var i = 0; i < value.length; i++) {
                            ret += value[i].days + " " + value[i].hours + "</div>";
                            if (i < value.length - 1)
                                ret += "<div>";
                        }
                        ret += "</div>";
                        return ret;
                    }, search: false, mobile: true
                },
                "tel": { title: "Telephone", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "fax": { title: "Fax", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "postalcode ": { title: "Postal Code", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "email": { title: "Email Id", mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
            },
            pagination: true,
            filter: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Activate",
                    action: this.activatebranch.bind(this),
                    color: 'primary'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Branch",
                addAction: this.createBranch.bind(this),
            },
            tapRow: this.editBranch.bind(this),
            uploadOptions: {},
        };
        this.contactOptions = {
            data: [],
            filteredData: [],
            columns: {
                "subject": { title: "Subject", search: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "emailid": { title: "Email Id", search: true, format: 'function', fn: this.commonFunctionSettings.bind(this) },
                "issystem": { title: "System Defined", format: 'function', fn: this.commonFunctionSystemDfined.bind(this), search: true },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.deleteContact.bind(this),
                    color: 'danger'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Message Subject",
                addAction: this.createContactSubject.bind(this),
            },
            tapRow: this.editContacts.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
        this.banksmtp = {
            mail_smtp_id: 0,
            username: "",
            passwd: "",
            host: "",
            port: ""
        };
        this.contact = {
            "subject ": null,
            "emailid ": null
        };
        this.addActionTitle = this.settingsProvider.getLabel("Create New Push Message");
        this.branchSelectedActions = [
            {
                title: "Archive",
                action: this.deleteBranch.bind(this),
                color: "danger"
            }
        ];
        this.contactSelectedActions = [
            {
                title: "Archive",
                action: this.deleteContact.bind(this),
                color: "danger"
            }
        ];
        this.faqSelectedActions = [
            {
                title: "Archive",
                action: this.deleteFaqs.bind(this),
                color: "danger"
            }
        ];
        this.cotactsdata = [];
        this.branches = [];
        this.faqs = [];
        this.contacts = [];
        this.faqsData = {
            title: "",
            description: "",
            category: "app",
            type: "mobile"
        };
        this.contData = {
            subject: "",
            to: ""
        };
        this.dirtyBankDetailsFlag = false;
        this.dirtyLookAndFeelFlag = false;
        this.smtpFlag = false;
        this.bank = {
            savingsaccount: false,
            mortgageaccount: false,
            currentaccount: false,
            externalaccount: false
        };
        if (this.navParams.get("msg") !== undefined) {
            this.currentTab = this.navParams.get("msg");
        }
        if (this.navParams.get("issend") !== undefined) {
            this.currentTab = this.navParams.get("issend");
        }
        //Contact Us
        this.filter = navParams.get("filter");
        this.contactList = navParams.get("user");
        var self = this;
        if (!navParams.get('user')) {
            //add adata
            self.contact = {
                "subject ": null,
                "emailid ": null,
            };
            self.title = settingsProvider.getLabel("Update Staff");
            self.new = true;
        }
        else {
            //View Data
            self.contact = JSON.parse(JSON.stringify(navParams.get('user')));
        }
    }
    SettingsPage.prototype.createContactSubject = function () {
        this.navCtrl.push("ContactUsPage");
    };
    SettingsPage.prototype.editContactSubject = function (contactData) {
        console.log("contactData", contactData);
    };
    SettingsPage.prototype.createContact = function () {
        this.navCtrl.push("ComplaintDetailsSubjectPage");
    };
    SettingsPage.prototype.editContacts = function (contactData) {
        this.globalProvider.showLoader();
        this.navCtrl.push("ContactUsPage", { contactUsData: contactData });
    };
    SettingsPage.prototype.commonFunctionSettings = function (nullData) {
        var str = "";
        if (nullData == 0) {
            str = '--';
        }
        else if (nullData == null) {
            str = '--';
        }
        else if (nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    SettingsPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("appsettings")) {
            return true;
        }
        else {
            return false;
        }
    };
    SettingsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        var self = this;
        this.settingsProvider.getsmtp().then(function (data) {
            _this.banksmtp = data.data[0];
        });
    };
    SettingsPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        this.title = "App Setting | Consectus";
        console.log(this.currentTab, "currentTab");
        var self = this;
        if (self.oauthProvider.user_role.appsettings) {
            switch (this.currentTab) {
                case 'branches':
                    this.currentTab1 = "activeBranches";
                    this.loadBranchData();
                    this.loadBranchArchiveData();
                    break;
                case 'bankdetails':
                    this.loadBankDetails();
                    break;
                case 'lookandfeel':
                    this.loadLookAndFeelData();
                    break;
                case 'contactus':
                    this.loadContactsData();
                    break;
                case 'faqs':
                    this.loadFaqsData();
                    this.loadFaqsArchiveData();
                    break;
                case 'contacts':
                    this.loadSubjectData();
                    break;
                case 'termsCondition':
                    this.gettermsandcondition();
                    break;
                default:
                    break;
            }
        }
        else {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.setRoot("DashboardPage");
        }
    };
    SettingsPage.prototype.gettermsandcondition = function () {
        var self = this;
        // this.globalProvider.showLoader();
        this.settingsProvider.gettermsandcondition()
            .then(function (result) {
            if (result.data) {
                // this.globalProvider.hideLoader();
                self.terms = result.data;
            }
        }).catch(function (error) {
            // this.globalProvider.hideLoader();
        });
    };
    SettingsPage.prototype.delete = function (items) {
        console.log("items", items);
        var d = [];
        var self = this;
        console.log("delete product");
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            d.push(item.branches_id);
        }
        var branchId = d.join(',');
        self.settingsProvider.deletebranch(branchId).then(function (result) {
            self.loadBranchData();
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
        });
    };
    SettingsPage.prototype.activatebranch = function (branchesId) {
        var d = [];
        var self = this;
        console.log("delete product");
        for (var _i = 0, branchesId_1 = branchesId; _i < branchesId_1.length; _i++) {
            var item = branchesId_1[_i];
            d.push(item.branches_id);
        }
        var branchId = d.join(',');
        self.settingsProvider.activatebranch(branchId).then(function (result) {
            self.loadBranchArchiveData();
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    SettingsPage.prototype.deletefaq = function (items) {
        console.log("items", items);
        var d = [];
        var self = this;
        console.log("archive faq");
        for (var _i = 0, items_2 = items; _i < items_2.length; _i++) {
            var item = items_2[_i];
            d.push(item.faq_id);
        }
        var faqId = d.join(',');
        self.settingsProvider.deletefaq(faqId).then(function (result) {
            // self.getProducts();
            self.loadFaqsData();
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
        });
    };
    SettingsPage.prototype.activate = function (faqsId) {
        var _this = this;
        var d = [];
        var self = this;
        console.log("Activate FAQ's");
        for (var _i = 0, faqsId_1 = faqsId; _i < faqsId_1.length; _i++) {
            var item = faqsId_1[_i];
            d.push(item.faq_id);
        }
        var faqId = d.join(',');
        self.settingsProvider.activateFaq(faqId).then(function (result) {
            if (result.status) {
                _this.loadFaqsArchiveData();
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel(result.message),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
            }
        });
    };
    SettingsPage.prototype.loadBranchData = function () {
        var _this = this;
        var self = this;
        this.settingsProvider.getbranch()
            .then(function (result) {
            self.branches = result.data;
            self.branchOptions.data = _this.archive(result.data, 1);
            self.branchOptions = Object.assign({}, self.branchOptions);
            self.cdr.detectChanges();
            self.nullData = result.data;
        }).catch(function (error) {
        });
    };
    SettingsPage.prototype.loadBranchArchiveData = function () {
        var _this = this;
        var self = this;
        this.settingsProvider.getbranch()
            .then(function (result) {
            self.branch1Options.data = _this.archive(result.data, 0);
            self.branch1Options = Object.assign({}, self.branch1Options);
            self.cdr.detectChanges();
            self.nullData = result.data;
        }).catch(function (error) {
        });
    };
    SettingsPage.prototype.commonFunction = function (nullData) {
        // console.log(this.nullData, "nullDataData");
        var str = "";
        if (nullData == "" || nullData == null || nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    SettingsPage.prototype.loadFaqsData = function () {
        var _this = this;
        var self = this;
        this.settingsProvider.getFaqs().then(function (result) {
            console.log("result", result);
            if (result.status && result.data !== null) {
                self.faqs = result.data;
                self.options1.data = _this.archive(result.data, 1);
                self.options1 = Object.assign({}, self.options1);
            }
            else {
                self.errorAlert("Something Went wrong");
            }
        }).catch(function (error) {
            _this.globalProvider.hideLoader();
        });
    };
    SettingsPage.prototype.loadFaqsArchiveData = function () {
        var _this = this;
        var self = this;
        this.settingsProvider.getFaqs().then(function (result) {
            console.log("result", result);
            if (result.status && result.data !== null) {
                self.faqs = result.data;
                self.options12.data = _this.archive(result.data, 0);
                self.options12 = Object.assign({}, self.options12);
            }
            else {
                self.errorAlert("Something Went wrong");
            }
        }).catch(function (error) {
        });
    };
    SettingsPage.prototype.loadContactsData = function () {
        var _this = this;
        var self = this;
        this.globalProvider.showLoader();
        this.settingsProvider.getcustomercomplaint()
            .then(function (result) {
            _this.globalProvider.hideLoader();
            console.log(result, "result");
            if (_this.navParams.get("msg") !== undefined) {
                self.contactSubjectOptions.data = result.data;
                self.contactSubjectOptions = Object.assign({}, self.contactSubjectOptions);
            }
            else if (_this.navParams.get("issend") !== undefined) {
                self.contactSubjectOptions = Object.assign({}, self.contactSubjectOptions);
            }
            else {
                self.contactSubjectOptions.data = result.data;
                self.contactSubjectOptions = Object.assign({}, self.contactSubjectOptions);
            }
        }).catch(function (error) {
            _this.globalProvider.hideLoader();
        });
    };
    SettingsPage.prototype.loadSubjectData = function () {
        var _this = this;
        var self = this;
        this.globalProvider.showLoader();
        this.settingsProvider.getcontactsubject()
            .then(function (result) {
            _this.globalProvider.hideLoader();
            console.log(result, "result");
            self.contactOptions.data = result.data;
            self.contactOptions = Object.assign({}, self.contactOptions);
        }).catch(function (error) {
            _this.globalProvider.hideLoader();
        });
    };
    SettingsPage.prototype.commonFunctionSystemDfined = function (data) {
        var str = "";
        console.log(data, "dataisempty");
        if (data == 0) {
            str = '<img class="imgUpload"  style="width:20%; height:20%" src ="assets/member_images/close.jpg" alt="">';
        }
        else if (data == null) {
            str = '<img class="imgUpload"  style="width:20%; height:20%" src ="assets/member_images/close.jpg" alt="">';
        }
        else {
            str = '<img class="imgUpload"  style="width:20%; height:20%" src ="assets/member_images/1398911.png" alt="">';
        }
        return (str);
    };
    SettingsPage.prototype.loadBankDetails = function () {
        var _this = this;
        var self = this;
        this.globalProvider.showLoader();
        this.settingsProvider.getBankDetails()
            .then(function (result) {
            _this.globalProvider.hideLoader();
            if (result.status) {
                self.bank = result.data[0];
            }
        }).catch(function (error) {
            _this.globalProvider.hideLoader();
        });
    };
    SettingsPage.prototype.loadLookAndFeelData = function () {
        var _this = this;
        var self = this;
        this.globalProvider.showLoader();
        this.settingsProvider.getBankLookAndFeel()
            .then(function (data) {
            _this.globalProvider.hideLoader();
            self.lookandfeel = data;
        }).catch(function (error) {
            _this.globalProvider.hideLoader();
        });
    };
    SettingsPage.prototype.dirtyBankDetails = function () {
        this.dirtyBankDetailsFlag = true;
    };
    SettingsPage.prototype.dirtyLookAndFeel = function () {
        this.dirtyLookAndFeelFlag = true;
    };
    SettingsPage.prototype.banksmtpClick = function () {
        this.smtpFlag = true;
    };
    SettingsPage.prototype.uploadImage = function (event) {
        this.base_64_image = event.target.result;
        this.dirtyBankDetails();
    };
    SettingsPage.prototype.removeImage = function () {
        var self = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to delete Image',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function (data) {
                        self.bank.logo = null;
                        self.base_64_image = null;
                        self.dirtyBankDetails();
                    }
                }
            ]
        });
        alert.present();
    };
    SettingsPage.prototype.segmentChangedTNC = function (event) {
        switch (event._value) {
            case 'onboarding':
                this.gettermsandcondition();
                break;
            case 'newAccountopening':
                this.gettermsandcondition();
                break;
            default:
                break;
        }
    };
    SettingsPage.prototype.segmentChanged = function (event) {
        console.log("eventValue", event._value);
        switch (event._value) {
            case 'branches':
                this.loadBranchData();
                break;
            case 'bankdetails':
                this.loadBankDetails();
                break;
            case 'lookandfeel':
                this.loadLookAndFeelData();
                break;
            case 'contactus':
                this.loadContactsData();
                break;
            case 'faqs':
                this.loadFaqsData();
                break;
            case 'contacts':
                this.loadSubjectData();
                break;
            case 'newAccount':
                this.loadNewAccountList();
                break;
            case 'termsCondition':
                this.gettermsandcondition();
                break;
            default:
                break;
        }
    };
    SettingsPage.prototype.segmentChanged1 = function (event) {
        console.log("dsf", event._value);
        switch (event._value) {
            case 'activeBranches':
                this.loadBranchData();
                break;
            case 'archiveBranches':
                this.loadBranchArchiveData();
                break;
            default:
                break;
        }
    };
    SettingsPage.prototype.segmentChanged2 = function (event) {
        console.log("dsf", event._value);
        switch (event._value) {
            case 'activeFAQ':
                this.loadFaqsData();
                break;
            case 'archiveFAQ':
                this.loadFaqsArchiveData();
                break;
            default:
                break;
        }
    };
    SettingsPage.prototype.contactusData = function () {
        var self = this;
        this.settingsProvider.getContactusSubject().then(function (data) {
            self.cotactsdata = data;
            console.log("contactusdta", self.cotactsdata);
        });
    };
    SettingsPage.prototype.addSubject = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.settingsProvider.addSubject(JSON.parse(JSON.stringify(self.contData)))
                .then(function () {
                self.contData.subject = "";
                var toast = self.toastCtrl.create({
                    message: 'Subject  added successfully',
                    duration: 2000,
                    position: 'right',
                    cssClass: "success"
                });
                toast.present();
                resolve();
            });
        });
    };
    SettingsPage.prototype.editContact = function (contactusdata) {
        this.globalProvider.showLoader();
        this.navCtrl.push("ContactUsPage", { subject: contactusdata });
    };
    SettingsPage.prototype.deleteContact = function (contactusdata) {
        var _this = this;
        console.log(contactusdata);
        var d = [];
        var self = this;
        for (var _i = 0, contactusdata_1 = contactusdata; _i < contactusdata_1.length; _i++) {
            var contact = contactusdata_1[_i];
            d.push(this.settingsProvider.deleteContact(contact));
        }
        Promise.all(d)
            .then(function () {
            var toast = self.toastCtrl.create({
                message: contactusdata.length == 1 ? _this.settingsProvider.getLabel('Subject was Deleted successfully') : contactusdata.length + _this.settingsProvider.getLabel(' Subjects were Deleted successfully'),
                duration: 2000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
            self.loadContactsData();
        });
    };
    SettingsPage.prototype.createFaqs = function () {
        this.navCtrl.push('FaqsPage');
    };
    SettingsPage.prototype.editFaqs = function (faqs) {
        this.globalProvider.showLoader();
        this.navCtrl.push("FaqsPage", { faq: faqs });
    };
    SettingsPage.prototype.deleteFaqs = function (faqdata) {
        var d = [];
        var self = this;
        for (var _i = 0, faqdata_1 = faqdata; _i < faqdata_1.length; _i++) {
            var faq = faqdata_1[_i];
            d.push(this.settingsProvider.deleteFaqs(faq));
        }
        Promise.all(d)
            .then(function () {
            var toast = self.toastCtrl.create({
                message: faqdata.length == 1 ? self.settingsProvider.getLabel('Faq was Deleted successfully') : faqdata.length + self.settingsProvider.getLabel(" FAQ's were Deleted successfully"),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
            self.loadFaqsData();
        });
    };
    SettingsPage.prototype.createSubject = function () {
        this.navCtrl.push('ContactUsPage');
    };
    SettingsPage.prototype.deleteBranch = function (branches) {
        var d = [];
        var self = this;
        for (var _i = 0, branches_1 = branches; _i < branches_1.length; _i++) {
            var branch = branches_1[_i];
            d.push(this.settingsProvider.deleteBranch(branch));
        }
        Promise.all(d)
            .then(function () {
            var toast = self.toastCtrl.create({
                message: branches.length == 1 ? 'Branch was Deleted successfully' : branches.length + ' branches were Deleted successfully',
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
            self.loadBranchData();
        });
    };
    SettingsPage.prototype.createBranch = function () {
        this.navCtrl.push("BranchPage", { creatNew: true }); //, { callback: this.addBranch.bind(this) }
    };
    SettingsPage.prototype.editBranch = function (branch) {
        this.globalProvider.showLoader();
        this.navCtrl.push("BranchPage", { branch: branch, callback: this.updateBranch.bind(this) });
    };
    SettingsPage.prototype.updateBranch = function (branch, del) {
        if (del === void 0) { del = false; }
        var self = this;
        return new Promise(function (resolve, reject) {
            if (del) {
                self.settingsProvider.updateBranch(branch)
                    .then(function () {
                    var toast = self.toastCtrl.create({
                        message: 'Branch was Deleted successfully',
                        duration: 3000,
                        position: 'right',
                        cssClass: 'danger',
                    });
                    toast.present();
                    resolve();
                });
            }
            else {
                self.settingsProvider.updateBranch(branch)
                    .then(function () {
                    var toast = self.toastCtrl.create({
                        message: 'Branch was updated successfully',
                        duration: 3000,
                        position: 'right',
                        cssClass: 'success',
                    });
                    toast.present();
                    resolve();
                });
            }
        });
    };
    SettingsPage.prototype.saveBankDetails = function () {
        var self = this;
        console.log("this.bank", this.bank);
        this.bankData = {
            "users_id": 2,
            "banksettings_id": 1,
            "savingsaccount": 1,
            "mortgageaccount": 1,
            "currentaccount": 1,
            "externalaccount": 1,
            "menu_new_account": 1,
            "menu_existing_account": 1,
            "menu_prodcuts": 1,
            "menu_savinganalysis": 1,
            "menu_goals": 1,
            "menu_calculators": 1,
            "menu_cheque": 1,
            "menu_contactus": 1,
            "menu_thirdparty": 1,
            "menu_settings": 1,
            "menu_termsconditions": 1,
            "menu_branches": 1,
            "menu_paycontacts": 1,
            "menu_messages": 1,
            "menu_members": 1,
            "menu_saver": 1,
            "menu_regularsavings": 1
        };
        this.bank.savingsaccount = this.bank.savingsaccount ? 1 : 0;
        this.bank.externalaccount = this.bank.externalaccount ? 1 : 0;
        this.bank.menu_branches = this.bank.menu_branches ? 1 : 0;
        this.bank.menu_calculators = this.bank.menu_calculators ? 1 : 0;
        this.bank.menu_calculators = this.bank.menu_calculators ? 1 : 0;
        this.bank.menu_contactus = this.bank.menu_contactus ? 1 : 0;
        this.bank.menu_goals = this.bank.menu_goals ? 1 : 0;
        this.bank.menu_new_account = this.bank.menu_new_account ? 1 : 0;
        this.bank.menu_paycontacts = this.bank.menu_paycontacts ? 1 : 0;
        this.bank.menu_prodcuts = this.bank.menu_prodcuts ? 1 : 0;
        this.bank.menu_saver = this.bank.menu_saver ? 1 : 0;
        this.bank.menu_settings = this.bank.menu_settings ? 1 : 0;
        this.bank.menu_cheque = this.bank.menu_cheque ? 1 : 0;
        this.bank.menu_termsconditions = this.bank.menu_termsconditions ? 1 : 0;
        this.bank.mortgageaccount = this.bank.mortgageaccount ? 1 : 0;
        this.bank.menu_thirdparty = this.bank.menu_thirdparty ? 1 : 0;
        this.bank.menu_existing_account = this.bank.menu_existing_account ? 1 : 0;
        this.bank.menu_messages = this.bank.menu_messages ? 1 : 0;
        this.bankData.banksettings_id = 1;
        this.bankData.currentaccount = 1;
        this.bankData.menu_savinganalysis = 1;
        this.bankData.menu_members = 1;
        this.bankData.menu_regularsavings = 1;
        this.bankData.savingsaccount = this.bank.savingsaccount;
        this.bankData.externalaccount = this.bank.externalaccount;
        this.bankData.menu_branches = this.bank.menu_branches;
        this.bankData.menu_calculators = this.bank.menu_calculators;
        this.bankData.menu_calculators = this.bank.menu_calculators;
        this.bankData.menu_contactus = this.bank.menu_contactus;
        this.bankData.menu_goals = this.bank.menu_goals;
        this.bankData.menu_new_account = this.bank.menu_new_account;
        this.bankData.menu_paycontacts = this.bank.menu_paycontacts;
        this.bankData.menu_prodcuts = this.bank.menu_prodcuts;
        this.bankData.menu_saver = this.bank.menu_saver;
        this.bankData.menu_settings = this.bank.menu_settings;
        this.bankData.menu_cheque = this.bank.menu_cheque;
        this.bankData.menu_termsconditions = this.bank.menu_termsconditions;
        this.bankData.mortgageaccount = this.bank.mortgageaccount;
        this.bankData.menu_thirdparty = this.bank.menu_thirdparty;
        this.bankData.menu_existing_account = this.bank.menu_existing_account;
        this.bankData.menu_messages = this.bankData.menu_messages;
        this.settingsProvider.updateBankDetails(this.bankData)
            .then(function (data) {
            self.dirtyBankDetailsFlag = false;
            var toast = self.toastCtrl.create({
                message: 'Bank Details updated.',
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    SettingsPage.prototype.saveBankLookAndFeel = function () {
        var self = this;
        this.settingsProvider.updateBankLookAndFeel(this.lookandfeel)
            .then(function (data) {
            self.dirtyLookAndFeelFlag = false;
            var toast = self.toastCtrl.create({
                message: 'Bank Look and Feel updated.',
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    SettingsPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    SettingsPage.prototype.saveBankSTMP = function (addData, updateData) {
        if (this.banksmtp.mail_smtp_id == 0) {
            var self_1 = this;
            this.settingsProvider.addsmtp(addData)
                .then(function (result) {
                self_1.banksmtp.username = "";
                self_1.banksmtp.passwd = "";
                self_1.banksmtp.host = "";
                self_1.banksmtp.port = "";
                var toast = self_1.toastCtrl.create({
                    message: self_1.settingsProvider.getLabel('Data added successfully'),
                    duration: 3000,
                    position: 'right',
                    cssClass: "success"
                });
                toast.present();
            });
        }
        else {
            var self_2 = this;
            this.settingsProvider.updatesmtp(updateData)
                .then(function (result) {
                self_2.banksmtp.username = "";
                self_2.banksmtp.passwd = "";
                self_2.banksmtp.host = "";
                self_2.banksmtp.port = "";
                var toast = self_2.toastCtrl.create({
                    message: self_2.settingsProvider.getLabel('Data updated successfully'),
                    duration: 3000,
                    position: 'right',
                    cssClass: "success"
                });
                toast.present();
            });
        }
    };
    SettingsPage.prototype.archive = function (archiveData, activestatus) {
        var archive = [];
        for (var i in archiveData) {
            if (archiveData[i].isactive == activestatus) {
                archive.push(archiveData[i]);
            }
        }
        return archive;
    };
    SettingsPage.prototype.formatDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1;
            var yyyy = newdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    SettingsPage.prototype.loadNewAccountList = function () {
    };
    SettingsPage.prototype.UpdateOnboardingTermsConditions = function () {
        var _this = this;
        var self = this;
        if (self.terms.onboarding == "" || self.terms.onboarding == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter onboarding customers terms and conditions"));
        }
        else {
            this.settingsProvider.updateonboardingtnc(this.terms)
                .then(function (data) {
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel("Terms & Conditions updated successfully"),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
            });
        }
    };
    SettingsPage.prototype.UpdateNewAccOpeningTermsConditions = function () {
        var _this = this;
        var self = this;
        if (self.terms.newaccount == "" || self.terms.newaccount == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter new account opening terms and conditions"));
        }
        else {
            this.settingsProvider.updatenewaccountnc(this.terms)
                .then(function (data) {
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel("Terms & Conditions updated successfully"),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
            });
        }
        // let message = "Updated " + this.savingData.milestonename + " in New Savings Milestone";
        // let auditUser = {
        //   "users_id": this.oauthProvider.user[0].users_id,
        //   "customer_devices_id": 0,
        //   "usertype": "staff",
        //   "uuid": "",
        //   "appname": "hub",
        //   "event_type": "update",
        //   "screen": "new-savings-milestone",
        //   "trail_details": message
        // }
        // this.loadauditProvider.loadaudit(auditUser)
        //   .then((data) => {
        //     let toast = self.toastCtrl.create({
        //       message: this.settingsProvider.getLabel("Updated Audit Done"),
        //       duration: 10000,
        //       position: 'right',
        //       cssClass: 'success',
        //     })
        //     toast.present();
        //   })
    };
    return SettingsPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["_12" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2__components_table_table__["a" /* TableComponent */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__components_table_table__["a" /* TableComponent */])
], SettingsPage.prototype, "table", void 0);
SettingsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["f" /* IonicPage */])({ name: "SettingsPage" }),
    Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["n" /* Component */])({
        selector: 'page-settings',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\settings\settings.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(title)}} </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <!--Settings-->\n\n  <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n    <ion-segment-button value="branches">\n\n      {{settingsProvider.getLabel(\'Branches\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="faqs">\n\n      {{settingsProvider.getLabel(\'FAQs\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="mailsmtp">\n\n      {{settingsProvider.getLabel(\'Mail SMTP\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="contacts">\n\n      {{settingsProvider.getLabel(\'Contact Us Message Subject\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="termsCondition">\n\n      {{settingsProvider.getLabel(\'Terms & Condition\')}}\n\n    </ion-segment-button>\n\n  </ion-segment>\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'bankdetails\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="saveBankDetails()" *ngIf="dirtyBankDetailsFlag">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'faqs\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-segment [(ngModel)]="currentTab2" (ionChange)="segmentChanged2($event)">\n\n                <ion-segment-button value="activeFAQ">\n\n                  {{settingsProvider.getLabel(\'Active\')}}\n\n                </ion-segment-button>\n\n                <ion-segment-button value="archiveFAQ">\n\n                  {{settingsProvider.getLabel(\'Archive\')}}\n\n                </ion-segment-button>\n\n              </ion-segment>\n\n\n\n              <ion-grid class="table-cell" *ngIf="currentTab2 == \'activeFAQ\'">\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-grid class="table-simple">\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <table-component [options]="options1"></table-component>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                    </ion-grid>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n              <ion-grid class="table-cell" *ngIf="currentTab2 == \'archiveFAQ\'">\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-grid class="table-simple">\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <table-component [options]="options12"></table-component>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                    </ion-grid>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n  <!--BRANCHES-->\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'branches\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-segment [(ngModel)]="currentTab1" (ionChange)="segmentChanged1($event)">\n\n                <ion-segment-button value="activeBranches">\n\n                  {{settingsProvider.getLabel(\'Active\')}}\n\n                </ion-segment-button>\n\n                <ion-segment-button value="archiveBranches">\n\n                  {{settingsProvider.getLabel(\'Archive\')}}\n\n                </ion-segment-button>\n\n              </ion-segment>\n\n\n\n              <ion-grid class="table-cell" *ngIf="currentTab1 == \'activeBranches\'">\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-grid class="table-simple">\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <table-component [options]="branchOptions"></table-component>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                    </ion-grid>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n              <ion-grid class="table-cell" *ngIf="currentTab1 == \'archiveBranches\'">\n\n                <ion-row>\n\n                  <ion-col>\n\n                    <ion-grid class="table-simple">\n\n                      <ion-row>\n\n                        <ion-col>\n\n                          <table-component [options]="branch1Options"></table-component>\n\n                        </ion-col>\n\n                      </ion-row>\n\n                    </ion-grid>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'contactus\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <table-component [options]="contactSubjectOptions"></table-component>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'mailsmtp\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="saveBankSTMP()" *ngIf="smtpFlag">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Username\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="banksmtp.username" placeholder="{{settingsProvider.getLabel(\'username\')}}" (input)="banksmtpClick()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Password\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="banksmtp.passwd" placeholder="{{settingsProvider.getLabel(\'Password\')}}" (input)="banksmtpClick()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Host\')}}</ion-label>\n\n                <ion-input type="text" disabled [(ngModel)]="banksmtp.host" placeholder="{{settingsProvider.getLabel(\'Host\')}}" (input)="banksmtpClick()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-item>\n\n                <ion-label stacked>{{settingsProvider.getLabel(\'Port\')}}</ion-label>\n\n                <ion-input type="number" disabled [(ngModel)]="banksmtp.port" placeholder="{{settingsProvider.getLabel(\'Port\')}}" (input)="banksmtpClick()"></ion-input>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'contacts\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <table-component [options]="contactOptions"></table-component>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'termsCondition\'">\n\n\n\n    <!-- <ion-row *ngIf="new">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="UpdateOnboardingTermsConditions()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row *ngIf="canUpdate">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="UpdateNewAccOpeningTermsConditions()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row> -->\n\n\n\n    <ion-segment [(ngModel)]="currentTabtnc" (ionChange)="segmentChangedTNC($event)">\n\n      <ion-segment-button value="onboarding">\n\n        {{ settingsProvider.getLabel(\'Onboarding Customers\')}}\n\n      </ion-segment-button>\n\n      <ion-segment-button value="newAccountopening">\n\n        {{ settingsProvider.getLabel(\'New Account Opening\')}}\n\n      </ion-segment-button>\n\n    </ion-segment>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid>\n\n          <!-- <ion-item>\n\n            <ion-label stacked>{{languageProvider.get(\'Terms and Conditions\')}}</ion-label>\n\n            <ion-textarea type="text" [(ngModel)]="terms.textarea" style="margin-left: 5px !important;" placeholder="{{languageProvider.get(\'Terms and Conditions\')}}"></ion-textarea>\n\n          </ion-item> -->\n\n\n\n          <ion-grid class="table-cell" *ngIf="currentTabtnc == \'onboarding\'">\n\n\n\n            <ion-row *ngIf="new">\n\n              <ion-col>\n\n                <ion-toolbar>\n\n                  <ion-buttons end>\n\n                    <button ion-button small round icon-left color="primary" (click)="UpdateOnboardingTermsConditions()">\n\n                      <ion-icon name="checkmark-circle"></ion-icon>\n\n                      {{settingsProvider.getLabel(\'Save Changes\')}}\n\n                    </button>\n\n                  </ion-buttons>\n\n                </ion-toolbar>\n\n              </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row>\n\n              <ion-col>\n\n                <ion-grid class="table-simple">\n\n                  <ion-row>\n\n                    <ion-col>\n\n                      <ion-label>{{languageProvider.get("Terms and Conditions")}}</ion-label>\n\n                      <ion-textarea style="font-size: 13px;padding: 10px;" [(ngModel)]="terms.onboarding"></ion-textarea>\n\n                    </ion-col>\n\n                  </ion-row>\n\n                </ion-grid>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-grid>\n\n\n\n          <ion-grid class="table-cell" *ngIf="currentTabtnc == \'newAccountopening\'">\n\n            <ion-row *ngIf="new">\n\n              <ion-col>\n\n                <ion-toolbar>\n\n                  <ion-buttons end>\n\n                    <button ion-button small round icon-left color="primary" (click)="UpdateOnboardingTermsConditions()">\n\n                      <ion-icon name="checkmark-circle"></ion-icon>\n\n                      {{settingsProvider.getLabel(\'Save Changes\')}}\n\n                    </button>\n\n                  </ion-buttons>\n\n                </ion-toolbar>\n\n              </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row>\n\n              <ion-col>\n\n                <ion-grid class="table-simple">\n\n                  <ion-row>\n\n                    <ion-col>\n\n                      <ion-label>{{languageProvider.get("Terms and Conditions")}}</ion-label>\n\n                      <ion-textarea style="font-size: 13px;padding: 10px;" [(ngModel)]="terms.newaccount "></ion-textarea>\n\n                    </ion-col>\n\n                  </ion-row>\n\n                </ion-grid>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-grid>\n\n\n\n\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\settings\settings.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["c" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_5__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_7__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], SettingsPage);

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 554:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(378);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = (function () {
    function MyApp(oauthProvider, languageProvider, platform, statusBar, events, menu, cdr, splashScreen, settingsProvider, gateway) {
        var _this = this;
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.platform = platform;
        this.statusBar = statusBar;
        this.events = events;
        this.menu = menu;
        this.cdr = cdr;
        this.splashScreen = splashScreen;
        this.settingsProvider = settingsProvider;
        this.gateway = gateway;
        this.resultdata = null;
        this.rootPage = "LoginPage";
        this.hideMenu = false;
        this.languagestring = "en";
        this.languageString = "en";
        this.menuLabel = null;
        window.app = this;
        console.log("hii app");
        this.gateway.getAccessToken().then(function () {
            _this.initializeApp();
            // this.getSplitPane();
            _this.events.subscribe("consectus:userrole", function (data) {
                console.log("dataSub", data);
                _this.userAdmin = data;
            });
            _this.events.subscribe("consectus:labels.updated", function () {
                _this.settingsProvider.labelApi().then(function () {
                });
            });
            _this.events.subscribe("consectus:login", _this.loggedIn.bind(_this));
            _this.events.subscribe("consectus:logout", _this.loggedOut.bind(_this));
            _this.events.subscribe("consectus:menu", function () {
                _this.settingsProvider.getAppLabels("MenuPage");
            });
        }).catch(function (error) {
            if (error.code === 'ECONNREFUSED' || error.code === 'ENOTFOUND' || error.code === 'ECONNRESET' || error.code === 'ETIMEDOUT') {
                window.showApp(false);
            }
            else {
                window.showApp(false);
            }
        });
    }
    //Hide the splitpane according to device width
    MyApp.prototype.getSplitPane = function () {
        var statusNav = this.nav.getActive();
        if (statusNav.component.name == "LoginPage") {
            this.hideMenu = false;
        }
        else {
            this.hideMenu = true;
        }
        console.log("statusnav", statusNav);
        // if (statusNav == undefined) {
        //   this.splitPaneState = false;
        // } else {
        //   if (statusNav.component.name == "SplashPage") {
        //     this.splitPaneState = false;
        //   } else {
        //     if (this.platform.width() >= 768) {
        //       this.splitPaneState = true;
        //     } else {
        //       this.splitPaneState = "md";
        //     }
        //   }
        // }
        // return this.splitPaneState;
    };
    MyApp.prototype.loggedIn = function () {
        this.hideMenu = true;
    };
    MyApp.prototype.loggedOut = function () {
        this.nav.setRoot("LoginPage");
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        var language = [{ "en": "en" }];
        this.events.publish("consectus:language", language);
        this.platform.ready().then(function () {
            _this.settingsProvider.labelApi().then(function () {
                _this.splashScreen.hide();
                console.log("splash");
            });
        });
    };
    MyApp.prototype.logout = function () {
        var self = this;
        this.oauthProvider.logout()
            .then(function () {
            self.nav.setRoot("LoginPage");
        });
    };
    // login() {
    //   this.nav.setRoot("LoginPage");
    // }
    MyApp.prototype.test = function () {
        this.nav.setRoot("TestPage");
    };
    MyApp.prototype.dashboard = function () {
        this.nav.setRoot('DashboardPage');
    };
    MyApp.prototype.customers = function () {
        this.nav.setRoot("CustomersPage");
    };
    MyApp.prototype.products = function () {
        this.nav.setRoot('ProductsPage');
    };
    MyApp.prototype.push = function () {
        this.nav.setRoot('PushMessagingPage');
    };
    MyApp.prototype.reports = function () {
        this.nav.setRoot('ReportsPage');
    };
    MyApp.prototype.settings = function () {
        this.nav.setRoot('SettingsPage');
    };
    MyApp.prototype.users = function () {
        this.nav.setRoot('UsersPage');
    };
    MyApp.prototype.members = function () {
        this.nav.setRoot("MembersPage");
    };
    MyApp.prototype.appSetting = function () {
        this.nav.setRoot("AppSettingPage");
    };
    MyApp.prototype.news = function () {
        this.nav.setRoot("NewsListPage");
    };
    MyApp.prototype.savingsAccounts = function () {
        this.nav.setRoot("SavingAccoutRulesListPage");
    };
    MyApp.prototype.SavingAccountRulesMilestonePage = function () {
        this.nav.setRoot("SavingAccountRulesMilestonePage");
    };
    MyApp.prototype.TransactionPage = function () {
        this.nav.setRoot("TransactionPage");
    };
    MyApp.prototype.UserRolesPage = function () {
        this.nav.setRoot("UserRolesPage");
    };
    MyApp.prototype.complaint = function () {
        this.nav.setRoot("Messages");
    };
    MyApp.prototype.productApplication = function () {
        this.nav.setRoot("ProductApplicationPage");
    };
    MyApp.prototype.contactUs = function () {
        this.nav.setRoot("CustomerCallPage");
    };
    MyApp.prototype.languages = function () {
        this.nav.setRoot("LanguagesPage");
    };
    MyApp.prototype.labels = function () {
        this.nav.setRoot("LabelsPage");
    };
    MyApp.prototype.NewAccountPage = function () {
        this.nav.setRoot("NewAccountPage");
    };
    MyApp.prototype.ChangePasswordPage = function () {
        this.nav.setRoot("ChangePasswordPage");
    };
    MyApp.prototype.Onboarding = function () {
        this.nav.setRoot("OnboardingSetupPage");
    };
    MyApp.prototype.faqCategory = function () {
        this.nav.setRoot("FaqCategoryPage");
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["_12" /* ViewChild */])('mainNav'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* NavController */]) === "function" && _a || Object)
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\app\app.html"*/'<ion-menu *ngIf="hideMenu" [content]="mainNav" side="left" type="reveal" class="show-menu">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>{{settingsProvider.getLabel(\'Menu\')}}</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content #content>\n\n    <div *ngIf="oauthProvider.user" padding>\n\n      <h4>{{oauthProvider.user[0].name}}</h4>\n\n      <p>{{oauthProvider.user[0].emailid}}</p>\n\n    </div>\n\n    <ion-list>\n\n      <div>\n\n        <button icon-left menuClose ion-item (click)="dashboard()">\n\n          <ion-icon name="clipboard"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Dashboard\')}}\n\n        </button>\n\n      </div>\n\n      <div *ngIf="userAdmin.newaccount">\n\n        <button icon-left menuClose ion-item (click)="NewAccountPage()">\n\n          <ion-icon ios="ios-basket"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Onboarding Management\')}}\n\n        </button>\n\n      </div>\n\n      <div *ngIf="userAdmin.customers">\n\n        <!-- *ngIf="oauthProvider.user.roles.indexOf(\'super_admin\')>-1 || oauthProvider.user.roles.indexOf(\'on-boarding\')>-1" -->\n\n        <button icon-left menuClose ion-item (click)="customers()">\n\n          <ion-icon name="contacts"></ion-icon>\n\n          {{settingsProvider.getLabel("Customer Management")}}\n\n        </button>\n\n      </div>\n\n      <div *ngIf="userAdmin.staffusers">\n\n        <!-- *ngIf="oauthProvider.user.roles.indexOf(\'super_admin\')>-1 || oauthProvider.user.roles.indexOf(\'user_admin\')>-1" -->\n\n        <button icon-left menuClose ion-item (click)="users()">\n\n          <ion-icon name="people"></ion-icon>\n\n          {{settingsProvider.getLabel("Staff User Management")}}\n\n        </button>\n\n      </div>\n\n      <div *ngIf="userAdmin.products">\n\n        <!-- *ngIf="oauthProvider.user.roles.indexOf(\'super_admin\')>-1 || oauthProvider.user.roles.indexOf(\'marketing\')>-1" -->\n\n        <button icon-left menuClose ion-item (click)="products()">\n\n          <ion-icon name="cart"></ion-icon>\n\n          {{settingsProvider.getLabel("Product Management")}}\n\n        </button>\n\n      </div>\n\n      <div *ngIf="userAdmin.pushmessages">\n\n        <!-- *ngIf="oauthProvider.user.roles.indexOf( \'super_admin\')>-1 || oauthProvider.user.roles.indexOf(\'push\')>-1" -->\n\n        <button icon-left menuClose ion-item (click)="push()">\n\n          <ion-icon name="ios-notifications"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Marketing Push Messaging\')}}\n\n        </button>\n\n      </div>\n\n      <!-- <div *ngIf="userAdmin.members"> -->\n\n      <!-- *ngIf="oauthProvider.user.roles.indexOf(\'super_admin\')>-1 || oauthProvider.user.roles.indexOf(\'on-boarding\')>-1" -->\n\n      <!-- <button icon-left menuClose ion-item (click)="members()">\n\n          <ion-icon name="people"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Member Offers Management\')}}\n\n        </button>\n\n      </div> -->\n\n      <div *ngIf="oauthProvider.user">\n\n        <!-- *ngIf="oauthProvider.user.roles.indexOf(\'super_admin\')>-1 || oauthProvider.user.roles.indexOf(\'on-boarding\')>-1" -->\n\n        <button icon-left menuClose ion-item (click)="complaint()">\n\n          <ion-icon name="ios-mail"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Contact Us Messages\')}}\n\n        </button>\n\n      </div>\n\n      <div *ngIf="oauthProvider.user">\n\n        <!-- *ngIf="oauthProvider.user.roles.indexOf(\'super_admin\')>-1 || oauthProvider.user.roles.indexOf(\'on-boarding\')>-1" -->\n\n        <button icon-left menuClose ion-item (click)="productApplication()">\n\n          <ion-icon name="ios-mail"></ion-icon>\n\n          {{settingsProvider.getLabel(\'New Product Application\')}}\n\n        </button>\n\n      </div>\n\n      <div *ngIf="userAdmin.appsettings">\n\n        <!-- *ngIf="oauthProvider.user.roles.indexOf(\'super_admin\')>-1" -->\n\n        <button icon-left menuClose ion-item (click)="settings()">\n\n          <ion-icon name="construct"></ion-icon>\n\n          {{settingsProvider.getLabel(\'App Settings\')}}\n\n        </button>\n\n      </div>\n\n      <div *ngIf="userAdmin.admin">\n\n        <!-- <div *ngIf="oauthProvider.admin"> -->\n\n        <button icon-left menuClose ion-item (click)="appSetting()">\n\n          <ion-icon name="settings"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Super Admin\')}}\n\n        </button>\n\n      </div>\n\n\n\n      <!-- <button icon-left menuClose ion-item (click)="news()">\n\n        <ion-icon name="mail"></ion-icon>\n\n        {{settingsProvider.getLabel(\'Marketing News Management\')}}\n\n      </button> -->\n\n      <div *ngIf="userAdmin.rulesengine">\n\n        <button icon-left menuClose ion-item (click)="savingsAccounts()">\n\n          <ion-icon name="bookmark"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Account Types & Rules\')}}\n\n        </button>\n\n      </div>\n\n\n\n\n\n      <!-- <div *ngIf="userAdmin.goal_milestone">\n\n        <button icon-left menuClose ion-item (click)="SavingAccountRulesMilestonePage()">\n\n          <ion-icon name="football"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Savings Goals Milestone\')}}\n\n        </button>\n\n      </div> -->\n\n\n\n      <!-- <button icon-left menuClose ion-item (click)="TransactionPage()">\n\n        <ion-icon name="pricetags"></ion-icon>\n\n        {{settingsProvider.getLabel(\'Transactions\')}}\n\n      </button> -->\n\n      <!-- <button icon-left menuClose ion-item (click)="UserRolesPage()">\n\n        <ion-icon name="person-add"></ion-icon>\n\n        {{settingsProvider.getLabel(\'User Roles Management\')}}\n\n      </button> -->\n\n      <!-- <button icon-left menuClose ion-item (click)="complaint()">\n\n        <ion-icon name="information-circle"></ion-icon>\n\n        {{settingsProvider.getLabel(\'Complaint Messages\')}}\n\n      </button>\n\n      <button icon-left menuClose ion-item (click)="contactUs()">\n\n        <ion-icon name="call"></ion-icon>\n\n        {{settingsProvider.getLabel(\'Call\')}}\n\n      </button> -->\n\n      <!-- <button icon-left menuClose ion-item (click)="languages()">\n\n        <ion-icon name="text"></ion-icon>\n\n        {{settingsProvider.getLabel(\'Languages\')}}\n\n      </button> -->\n\n      <!-- <div *ngIf="oauthProvider.user">\n\n        <button icon-left *ngIf="oauthProvider.user.roles.indexOf(\'super_admin\')>-1 || oauthProvider.user.roles.indexOf(\'reports\')>-1"\n\n          menuClose ion-item (click)="reports()">\n\n          <ion-icon name="document"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Reports\')}}\n\n        </button>\n\n      </div> -->\n\n      <!-- <div *ngIf="userAdmin.labels">\n\n        <button icon-left menuClose ion-item (click)="Onboarding()">\n\n          <ion-icon ios="ios-text"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Onboarding Setup\')}}\n\n        </button>\n\n      </div> -->\n\n      <div *ngIf="userAdmin.labels">\n\n        <button icon-left menuClose ion-item (click)="labels()">\n\n          <ion-icon ios="ios-text"></ion-icon>\n\n          {{settingsProvider.getLabel(\'Label Management\')}}\n\n        </button>\n\n      </div>\n\n      <div *ngIf="userAdmin.labels">\n\n        <button icon-left menuClose ion-item (click)="faqCategory()">\n\n          <ion-icon ios="ios-text"></ion-icon>\n\n          {{settingsProvider.getLabel(\'FAQ Categories\')}}\n\n        </button>\n\n      </div>\n\n\n\n\n\n      <!-- <button icon-left menuClose ion-item (click)="ChangePasswordPage()">\n\n        <ion-icon ios="ios-text"></ion-icon>\n\n        {{settingsProvider.getLabel(\'Password\')}}\n\n      </button> -->\n\n      <button icon-right *ngIf="oauthProvider.user " menuClose ion-item (click)="logout() ">\n\n        {{settingsProvider.getLabel(\'Logout\')}}\n\n        <ion-icon name="log-out"></ion-icon>\n\n      </button>\n\n\n\n      <!-- <button icon-left *ngIf="!oauthProvider.user " menuClose ion-item (click)="login() ">\n\n        <ion-icon name="log-in"></ion-icon>\n\n        {{settingsProvider.getLabel(\'Login\')}}\n\n      </button> -->\n\n\n\n\n\n\n\n\n\n      <!-- <button icon-left menuClose ion-item (click)="customers() ">\n\n          <ion-icon name="log-in "></ion-icon>\n\n          {{settingsProvider.getLabel(\'Test Page\')}}\n\n      </button> -->\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage " main id="#mainNav " #mainNav swipeBackEnabled="false "></ion-nav>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__["a" /* OauthProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__["a" /* OauthProvider */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* Platform */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* Events */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* MenuController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* MenuController */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_4__angular_core__["k" /* ChangeDetectorRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_core__["k" /* ChangeDetectorRef */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_1__providers_gateway_client_gateway_client__["a" /* GatewayClientProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__providers_gateway_client_gateway_client__["a" /* GatewayClientProvider */]) === "function" && _l || Object])
], MyApp);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 555:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SessionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SessionProvider = (function () {
    function SessionProvider(http) {
        this.http = http;
        console.log('Hello SessionProvider Provider');
    }
    SessionProvider.prototype.login = function (username, password) {
    };
    return SessionProvider;
}());
SessionProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], SessionProvider);

//# sourceMappingURL=session.js.map

/***/ }),

/***/ 556:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DatabaseProvider = (function () {
    function DatabaseProvider(http) {
        this.http = http;
        console.log('Hello DatabaseProvider Provider');
    }
    return DatabaseProvider;
}());
DatabaseProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], DatabaseProvider);

//# sourceMappingURL=database.js.map

/***/ }),

/***/ 557:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewSavingAccountMilestoneProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NewSavingAccountMilestoneProvider = (function () {
    function NewSavingAccountMilestoneProvider(http) {
        this.http = http;
        console.log('Hello NewSavingAccountMilestoneProvider Provider');
    }
    NewSavingAccountMilestoneProvider.prototype.addgoalmilestone = function () {
    };
    return NewSavingAccountMilestoneProvider;
}());
NewSavingAccountMilestoneProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
], NewSavingAccountMilestoneProvider);

//# sourceMappingURL=new-saving-account-milestone.js.map

/***/ }),

/***/ 558:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComplaintDetailsSubjectProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComplaintDetailsSubjectProvider = (function () {
    function ComplaintDetailsSubjectProvider(http) {
        this.http = http;
        console.log('Hello ComplaintDetailsSubjectProvider Provider');
    }
    return ComplaintDetailsSubjectProvider;
}());
ComplaintDetailsSubjectProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
], ComplaintDetailsSubjectProvider);

//# sourceMappingURL=complaint-details-subject.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProductsProvider = (function () {
    function ProductsProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
    }
    ProductsProvider.prototype.getProducts = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getProducts()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    ProductsProvider.prototype.getproduct = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getproduct()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    ProductsProvider.prototype.getaccounttypes = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getaccounttypes()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    ProductsProvider.prototype.addProduct = function (product) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.addProduct(product)
                .then(function () {
                resolve();
            });
        });
    };
    ProductsProvider.prototype.updateProduct = function (product) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateProduct(product)
                .then(function () {
                resolve();
            });
        });
    };
    ProductsProvider.prototype.activateProduct = function (product) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.activateProduct(product)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    ProductsProvider.prototype.deleteProduct = function (product) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteProduct(product)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    ProductsProvider.prototype.getcustomerbyproductid = function (productID) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getcustomerbyproductid(productID)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    ProductsProvider.prototype.upload_Image = function (image) {
        var self = this;
        var url = "/accounts/uploadtempfile";
        return new Promise(function (resolve, reject) {
            self.gateway.upload_image(url, image)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    ProductsProvider.prototype.updateProduct_TargetCustomer = function (formData) {
        var self = this;
        var url = "/accounts/updateproductcustomercsv";
        return new Promise(function (resolve, reject) {
            self.gateway.updateProduct_TargetCustomer(url, formData)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    ProductsProvider.prototype.push_Product = function (product) {
        var self = this;
        var url = "/accounts/pushproduct";
        return new Promise(function (resolve, reject) {
            self.gateway.push_Product(url, product)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    ProductsProvider.prototype.push_Message = function (product) {
        var self = this;
        var url = "/accounts/sendmessagepush";
        return new Promise(function (resolve, reject) {
            self.gateway.push_Product(url, product)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    return ProductsProvider;
}());
ProductsProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_0__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], ProductsProvider);

//# sourceMappingURL=products.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dashboard_dashboard_module__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login__ = __webpack_require__(325);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var LoginPageModule = (function () {
    function LoginPageModule() {
    }
    return LoginPageModule;
}());
LoginPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__login__["a" /* LoginPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__login__["a" /* LoginPage */]),
            __WEBPACK_IMPORTED_MODULE_0__dashboard_dashboard_module__["DashboardPageModule"]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_3__login__["a" /* LoginPage */]
        ]
    })
], LoginPageModule);

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UtilsProvider = (function () {
    function UtilsProvider(http, loadingCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.loading = null;
    }
    UtilsProvider.prototype.show = function (msg) {
        if (msg === void 0) { msg = ""; }
        if (this.loading === null) {
            this.loading = this.loadingCtrl.create({
                spinner: 'crescent',
                content: msg,
                dismissOnPageChange: true,
                duration: 5000
            });
            // console.log("show loading");
            this.loading.present();
        }
        else {
            // console.log("show loading");
            this.loading.present();
        }
    };
    UtilsProvider.prototype.hide = function () {
        if (this.loading !== null) {
            this.loading.dismiss();
            this.loading = null;
        }
        // console.log("hide loading");
    };
    return UtilsProvider;
}());
UtilsProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["h" /* LoadingController */]])
], UtilsProvider);

//# sourceMappingURL=utils.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadButtonComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__upload_button__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UploadButtonComponentModule = (function () {
    function UploadButtonComponentModule() {
    }
    return UploadButtonComponentModule;
}());
UploadButtonComponentModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_0__upload_button__["a" /* UploadButtonComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */],
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_0__upload_button__["a" /* UploadButtonComponent */]
        ]
    })
], UploadButtonComponentModule);

//# sourceMappingURL=upload-button.module.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BarChartComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__bar_chart__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BarChartComponentModule = (function () {
    function BarChartComponentModule() {
    }
    return BarChartComponentModule;
}());
BarChartComponentModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_0__bar_chart__["a" /* BarChartComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */],
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_0__bar_chart__["a" /* BarChartComponent */]
        ]
    })
], BarChartComponentModule);

//# sourceMappingURL=bar-chart.module.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MemberProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MemberProvider = (function () {
    function MemberProvider(http, gateway) {
        this.http = http;
        this.gateway = gateway;
        console.log('Hello MemberProvider Provider');
    }
    MemberProvider.prototype.getMembers = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getMembers()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    MemberProvider.prototype.getmember = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getmember()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    MemberProvider.prototype.getcustomerlistbymemberid = function (Member) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getcustomerlistbymemberid(Member)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    MemberProvider.prototype.createMember = function (member) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.createMember(member)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    MemberProvider.prototype.updateMember = function (member) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.updateMember(member)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    MemberProvider.prototype.deleteMembers = function (member) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.deleteMembers(member)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    MemberProvider.prototype.activateMembers = function (membersId) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.activateMembers(membersId)
                .then(function (data) {
                resolve(data);
            });
        });
    };
    MemberProvider.prototype.upload_Image = function (image) {
        var self = this;
        var url = "/accounts/uploadtempfile";
        return new Promise(function (resolve, reject) {
            self.gateway.upload_image(url, image)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    return MemberProvider;
}());
MemberProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__gateway_client_gateway_client__["a" /* GatewayClientProvider */]])
], MemberProvider);

//# sourceMappingURL=member.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__customers_customers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__gateway_client_gateway_client__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DashboardProvider = (function () {
    function DashboardProvider(gateway, customersProvider, http) {
        this.gateway = gateway;
        this.customersProvider = customersProvider;
        this.http = http;
    }
    DashboardProvider.prototype.getOnboardingData = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.customersProvider.getDashbaordData()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    DashboardProvider.prototype.getDashboardCount = function (count) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getDashboardCount(count)
                .then(function (data) {
                resolve(data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    DashboardProvider.prototype.getPushMessagingData = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.customersProvider.getDashbaordData()
                .then(function (data) {
                resolve(data);
            });
        });
    };
    DashboardProvider.prototype.checkDateisMonth = function (date) {
        var date1 = new Date(date);
        var date2 = new Date(); //less than 1
        var start = Math.floor(date1.getTime() / (3600 * 24 * 1000)); //days as integer from..
        var end = Math.floor(date2.getTime() / (3600 * 24 * 1000)); //days as integer from..
        var daysDiff = end - start; // exact dates
        if (daysDiff <= 30) {
            return true;
        }
        else {
            return false;
        }
    };
    DashboardProvider.prototype.checkDateisYear = function (date) {
        var date1 = new Date(date);
        var date2 = new Date(); //less than 1
        var start = Math.floor(date1.getTime() / (3600 * 24 * 1000)); //days as integer from..
        var end = Math.floor(date2.getTime() / (3600 * 24 * 1000)); //days as integer from..
        var daysDiff = end - start; // exact dates
        if (daysDiff <= 365) {
            return true;
        }
        else {
            return false;
        }
    };
    DashboardProvider.prototype.checkDateisFiveDays = function (date) {
        var date1 = new Date(date);
        var date2 = new Date(); //less than 1
        var start = Math.floor(date1.getTime() / (3600 * 24 * 1000)); //days as integer from..
        var end = Math.floor(date2.getTime() / (3600 * 24 * 1000)); //days as integer from..
        var daysDiff = end - start; // exact dates
        if (daysDiff <= 5) {
            return true;
        }
        else {
            return false;
        }
    };
    DashboardProvider.prototype.getDashboardFilterCustomers = function (status, period, channel) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.customersProvider.getCustomers()
                .then(function (resultset) {
                if (channel == "all") {
                    if (status == "active") {
                        if (period == "five") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == true && self.checkDateisFiveDays(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "month") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == true && self.checkDateisMonth(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "year") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == true && self.checkDateisYear(obj.createdate) == true; });
                            resolve(alldata);
                        }
                    }
                    else if (status == "inactive") {
                        if (period == "five") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == false && self.checkDateisFiveDays(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "month") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == false && self.checkDateisMonth(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "year") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == false && self.checkDateisYear(obj.createdate) == true; });
                            resolve(alldata);
                        }
                    }
                    else if (status == "onboard") {
                        if (period == "five") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == null && self.checkDateisFiveDays(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "month") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == null && self.checkDateisMonth(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "year") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == null && self.checkDateisYear(obj.createdate) == true; });
                            resolve(alldata);
                        }
                    }
                }
                else if (channel == "mobileApp") {
                    if (status == "active") {
                        if (period == "five") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == 1 && obj.platform == "Android" && self.checkDateisFiveDays(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "month") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == 1 && obj.platform == "Android" && self.checkDateisMonth(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "year") {
                            console.log(resultset.data, "resultset.data");
                            var alldata = resultset.data.filter(function (obj) { return obj.active == 1 && obj.platform == "Android" && self.checkDateisYear(obj.createdate) == true; });
                            resolve(alldata);
                        }
                    }
                    else if (status == "inactive") {
                        if (period == "five") {
                            console.log(resultset.data, "resultset.data");
                            var alldata = resultset.data.filter(function (obj) { return obj.active == 0 && obj.platform == "Android" && self.checkDateisFiveDays(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "month") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == 0 && obj.platform == "Android" && self.checkDateisMonth(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "year") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == 0 && obj.platform == "Android" && self.checkDateisYear(obj.createdate) == true; });
                            resolve(alldata);
                        }
                    }
                    else if (status == "onboard") {
                        if (period == "five") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == null && obj.platform == "mobile" && self.checkDateisFiveDays(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "month") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == null && obj.platform == "mobile" && self.checkDateisMonth(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "year") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == null && obj.platform == "mobile" && self.checkDateisYear(obj.createdate) == true; });
                            resolve(alldata);
                        }
                    }
                }
                else if (channel == "onlineApp") {
                    if (status == "active") {
                        if (period == "five") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == true && obj.platform == "online" && self.checkDateisFiveDays(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "month") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == true && obj.platform == "online" && self.checkDateisMonth(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "year") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == true && obj.platform == "online" && self.checkDateisYear(obj.createdate) == true; });
                            resolve(alldata);
                        }
                    }
                    else if (status == "inactive") {
                        if (period == "five") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == false && obj.platform == "online" && self.checkDateisFiveDays(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "month") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == false && obj.platform == "online" && self.checkDateisMonth(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "year") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == false && obj.platform == "online" && self.checkDateisYear(obj.createdate) == true; });
                            resolve(alldata);
                        }
                    }
                    else if (status == "onboard") {
                        if (period == "five") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == null && obj.platform == "online" && self.checkDateisFiveDays(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "month") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == null && obj.platform == "online" && self.checkDateisMonth(obj.createdate) == true; });
                            resolve(alldata);
                        }
                        else if (period == "year") {
                            var alldata = resultset.data.filter(function (obj) { return obj.active == null && obj.platform == "online" && self.checkDateisYear(obj.createdate) == true; });
                            resolve(alldata);
                        }
                    }
                }
            });
        });
    };
    DashboardProvider.prototype.getPlatformGraph = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.customersProvider.getCustomers()
                .then(function (result) {
                var activecount = result.data.filter(function (obj) { return obj.active == true; });
                var allcount = {
                    "activecount": result.data.filter(function (obj) { return obj.active == true; }),
                    "inactivecount": result.data.filter(function (obj) { return obj.active == false; }),
                    "onboardingcount": result.data.filter(function (obj) { return obj.active == null; }),
                    "five": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.active == true; }),
                    "month": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.active == true; }),
                    "year": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.active == true; }),
                    "fiveinactive": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.active == false; }),
                    "monthinactive": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.active == false; }),
                    "yearinactive": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.active == false; }),
                    "fiveonboarding": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.active == null; }),
                    "monthonboarding": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.active == null; }),
                    "yearonboarding": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.active == null; })
                };
                console.log(allcount.onboardingcount, "allcount");
                var mobilecount = {
                    "activecount": result.data.filter(function (obj) { return obj.active == true && obj.platform == "mobile"; }),
                    "inactivecount": result.data.filter(function (obj) { return obj.active == false && obj.platform == "mobile"; }),
                    "onboardingcount": result.data.filter(function (obj) { return obj.active == null && obj.platform == "mobile"; }),
                    "five": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.platform == "mobile"; }),
                    "month": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.platform == "mobile"; }),
                    "year": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.platform == "mobile"; }),
                    "fiveinactive": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.active == false && obj.platform == "mobile"; }),
                    "monthinactive": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.active == false && obj.platform == "mobile"; }),
                    "yearinactive": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.active == false && obj.platform == "mobile"; }),
                    "fiveonboarding": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.active == null && obj.platform == "mobile"; }),
                    "monthonboarding": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.active == null && obj.platform == "mobile"; }),
                    "yearonboarding": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.active == null && obj.platform == "mobile"; })
                };
                var onlinecount = {
                    "activecount": result.data.filter(function (obj) { return obj.active == true && obj.platform == "online"; }),
                    "inactivecount": result.data.filter(function (obj) { return obj.active == false && obj.platform == "online"; }),
                    "onboardingcount": result.data.filter(function (obj) { return obj.active == null && obj.platform == "online"; }),
                    "five": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.platform == "online"; }),
                    "month": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.platform == "mobile"; }),
                    "year": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.platform == "online"; }),
                    "fiveinactive": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.active == false && obj.platform == "online"; }),
                    "monthinactive": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.active == false && obj.platform == "online"; }),
                    "yearinactive": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.active == false && obj.platform == "online"; }),
                    "fiveonboarding": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.active == null && obj.platform == "online"; }),
                    "monthonboarding": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.active == null && obj.platform == "online"; }),
                    "yearonboarding": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.active == null && obj.platform == "online"; })
                };
                var telephonecount = {
                    "activecount": result.data.filter(function (obj) { return obj.active == true && obj.platform == "telephone"; }),
                    "inactivecount": result.data.filter(function (obj) { return obj.active == false && obj.platform == "telephone"; }),
                    "onboardingcount": result.data.filter(function (obj) { return obj.active == null && obj.platform == "telephone"; }),
                    "five": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.platform == "telephone"; }),
                    "month": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.platform == "mobile"; }),
                    "year": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.platform == "telephone"; }),
                    "fiveinactive": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.active == false && obj.platform == "telephone"; }),
                    "monthinactive": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.active == false && obj.platform == "telephone"; }),
                    "yearinactive": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.active == false && obj.platform == "telephone"; }),
                    "fiveonboarding": result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.createdate) == true && obj.active == null && obj.platform == "telephone"; }),
                    "monthonboarding": result.data.filter(function (obj) { return self.checkDateisMonth(obj.createdate) == true && obj.active == null && obj.platform == "telephone"; }),
                    "yearonboarding": result.data.filter(function (obj) { return self.checkDateisYear(obj.createdate) == true && obj.active == null && obj.platform == "telephone"; })
                };
                var returnval = {
                    "allcount": allcount,
                    "mobile": mobilecount,
                    "online": onlinecount,
                    "telephone": telephonecount
                };
                resolve(returnval);
            });
        });
    };
    DashboardProvider.prototype.getPerformanceData = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            var date = new Date();
            var data = { deposits: {}, withdrawals: {} };
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            for (var i = 0; i < 6; i++) {
                data.deposits[monthNames[date.getMonth()] + ' ' + date.getFullYear()] = Math.round(Math.random() * 10000000) + 1000000;
                data.withdrawals[monthNames[date.getMonth()] + ' ' + date.getFullYear()] = Math.round(Math.random() * 10000000) + 1000000;
                date.setMonth(date.getMonth() - 1);
            }
            resolve(data);
        });
    };
    DashboardProvider.prototype.getTransaction = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getyearlytransactions().then(function (result) {
                var returnVal = {
                    total: 0,
                    five: 0,
                    fiveamount: 0,
                    month: 0,
                    monthamount: 0,
                    yearamount: 0,
                    year: 0
                };
                result.data.forEach(function (obj) {
                    if (self.checkDateisFiveDays(obj.create_date) == true) {
                        returnVal.fiveamount += obj["amount"];
                    }
                    if (self.checkDateisMonth(obj.create_date) == true) {
                        returnVal.monthamount += obj["amount"];
                    }
                    if (self.checkDateisYear(obj.create_date) == true) {
                        returnVal.yearamount += obj["amount"];
                    }
                });
                returnVal.total = result.data.length;
                returnVal.five = result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.create_date) == true; }).length;
                returnVal.month = result.data.filter(function (obj) { return self.checkDateisMonth(obj.create_date) == true; }).length;
                returnVal.year = result.data.filter(function (obj) { return self.checkDateisYear(obj.create_date) == true; }).length;
                resolve(returnVal);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    DashboardProvider.prototype.getDashboardFilterTransaction = function (status, period, channel) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getyearlytransactions()
                .then(function (results) {
                var resultset = results.data;
                if (channel == "all") {
                    if (period == "five") {
                        var alldata = resultset.filter(function (obj) { return self.checkDateisFiveDays(obj.create_date) == true; });
                        resolve(alldata);
                    }
                    else if (period == "month") {
                        var alldata = resultset.filter(function (obj) { return self.checkDateisMonth(obj.create_date) == true; });
                        resolve(alldata);
                    }
                    else if (period == "year") {
                        var alldata = resultset.filter(function (obj) { return self.checkDateisYear(obj.create_date) == true; });
                        resolve(alldata);
                    }
                }
                else if (channel == "mobileApp") {
                    if (period == "five") {
                        var alldata = resultset.filter(function (obj) { return obj.platform == "mobile" && self.checkDateisFiveDays(obj.create_date) == true; });
                        resolve(alldata);
                    }
                    else if (period == "month") {
                        var alldata = resultset.filter(function (obj) { return obj.platform == "mobile" && self.checkDateisMonth(obj.create_date) == true; });
                        resolve(alldata);
                    }
                    else if (period == "year") {
                        var alldata = resultset.filter(function (obj) { return obj.platform == "mobile" && self.checkDateisYear(obj.create_date) == true; });
                        resolve(alldata);
                    }
                }
                else if (channel == "onlineApp") {
                    if (period == "five") {
                        var alldata = resultset.filter(function (obj) { return obj.platform == "online" && self.checkDateisFiveDays(obj.create_date) == true; });
                        resolve(alldata);
                    }
                    else if (period == "month") {
                        var alldata = resultset.filter(function (obj) { return obj.platform == "online" && self.checkDateisMonth(obj.create_date) == true; });
                        resolve(alldata);
                    }
                    else if (period == "year") {
                        var alldata = resultset.filter(function (obj) { return obj.platform == "online" && self.checkDateisYear(obj.create_date) == true; });
                        resolve(alldata);
                    }
                }
            });
        });
    };
    DashboardProvider.prototype.getCalls = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getCalls().then(function (result) {
                console.log(result.data, "calls");
                var returnVal = {
                    total: 0,
                    five: 0,
                    month: 0,
                    year: 0
                };
                returnVal.total = result.data.length;
                returnVal.five = result.data.filter(function (obj) { return self.checkDateisFiveDays(obj.create_date) == true; }).length;
                returnVal.month = result.data.filter(function (obj) { return self.checkDateisMonth(obj.create_date) == true; }).length;
                returnVal.year = result.data.filter(function (obj) { return self.checkDateisYear(obj.create_date) == true; }).length;
                resolve(returnVal);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    DashboardProvider.prototype.getDashboardFilterCalls = function (period) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getCalls()
                .then(function (results) {
                var resultset = results.data;
                if (period == "five") {
                    var alldata = resultset.filter(function (obj) { return self.checkDateisFiveDays(obj.create_date) == true; });
                    resolve(alldata);
                }
                else if (period == "month") {
                    var alldata = resultset.filter(function (obj) { return self.checkDateisMonth(obj.create_date) == true; });
                    resolve(alldata);
                }
                else if (period == "year") {
                    var alldata = resultset.filter(function (obj) { return self.checkDateisYear(obj.create_date) == true; });
                    resolve(alldata);
                }
            });
        });
    };
    DashboardProvider.prototype.getProductCount = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getproduct().then(function (data) {
                resolve(data.data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    DashboardProvider.prototype.getMessagesCount = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.gateway.getmessages().then(function (data) {
                resolve(data.data);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    return DashboardProvider;
}());
DashboardProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__gateway_client_gateway_client__["a" /* GatewayClientProvider */],
        __WEBPACK_IMPORTED_MODULE_0__customers_customers__["a" /* CustomersProvider */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */]])
], DashboardProvider);

//# sourceMappingURL=dashboard.js.map

/***/ })

},[393]);
//# sourceMappingURL=main.js.map