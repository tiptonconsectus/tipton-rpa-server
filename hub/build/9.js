webpackJsonp([9],{

/***/ 588:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SavingAccountRulesMilestonePageModule", function() { return SavingAccountRulesMilestonePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__saving_account_rules_milestone__ = __webpack_require__(625);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SavingAccountRulesMilestonePageModule = (function () {
    function SavingAccountRulesMilestonePageModule() {
    }
    return SavingAccountRulesMilestonePageModule;
}());
SavingAccountRulesMilestonePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__saving_account_rules_milestone__["a" /* SavingAccountRulesMilestonePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__saving_account_rules_milestone__["a" /* SavingAccountRulesMilestonePage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], SavingAccountRulesMilestonePageModule);

//# sourceMappingURL=saving-account-rules-milestone.module.js.map

/***/ }),

/***/ 625:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavingAccountRulesMilestonePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_saving_account_rules_milestone_saving_account_rules_milestone__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SavingAccountRulesMilestonePage = (function () {
    function SavingAccountRulesMilestonePage(navCtrl, navParams, oauthProvider, languageProvider, savingAccountRulesMilestoneProvider, toastCtrl, globalProvider, settingsProvider, viewCtrl, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.savingAccountRulesMilestoneProvider = savingAccountRulesMilestoneProvider;
        this.toastCtrl = toastCtrl;
        this.globalProvider = globalProvider;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.milestoneOptions = {
            data: [],
            filteredData: [],
            columns: {
                "milestonename": { title: "Milestone Name", search: true },
                "goaltarget": { title: "Goal Target Achieved %", search: true }
            },
            pagination: true,
            filter: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.delete.bind(this),
                    color: 'danger'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Milestone",
                addAction: this.addMilestone.bind(this),
            },
            tapRow: this.editMilestone.bind(this),
            uploadOptions: {},
            resetFilter: null,
        };
        this.milestone1Options = {
            data: [],
            filteredData: [],
            columns: {
                "milestonename": { title: "Milestone Name", search: true },
                "goaltarget": { title: "Goal Target Achieved %", search: true }
            },
            pagination: true,
            filter: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Activate",
                    action: this.activate.bind(this),
                    color: 'primary'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Milestone",
                addAction: this.addMilestone.bind(this),
            },
            tapRow: this.editMilestone.bind(this),
            uploadOptions: {},
            resetFilter: null,
        };
        this.languageString = "en";
        this.resultdata = null;
        this.currentTab = "activeMilestone";
    }
    SavingAccountRulesMilestonePage.prototype.addMilestone = function () {
        this.navCtrl.push("NewSavingsMilestonePage");
    };
    SavingAccountRulesMilestonePage.prototype.editMilestone = function (milestoneData) {
        this.navCtrl.push("NewSavingsMilestonePage", { savingData: milestoneData });
    };
    SavingAccountRulesMilestonePage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("rulesengine")) {
            return true;
        }
        else {
            return false;
        }
    };
    SavingAccountRulesMilestonePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SavingAccountRulesMilestonePage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    SavingAccountRulesMilestonePage.prototype.segmentChanged = function (event) {
        console.log("eventValue", event._value);
        switch (event._value) {
            case 'activeMilestone':
                this.getgoalmilestones();
                break;
            case 'archiveMilestone':
                this.getArchivegoalmilestones();
                break;
            default:
                break;
        }
    };
    SavingAccountRulesMilestonePage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        this.getArchivegoalmilestones();
        var self = this;
        if (self.oauthProvider.user_role.goal_milestone) {
            self.getgoalmilestones();
        }
        else {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.setRoot("DashboardPage");
        }
    };
    SavingAccountRulesMilestonePage.prototype.getArchivegoalmilestones = function () {
        var _this = this;
        var self = this;
        this.savingAccountRulesMilestoneProvider.getgoalmilestones()
            .then(function (result) {
            self.milestone1Options.data = _this.archive(result.data, 0);
            self.milestone1Options = Object.assign({}, self.milestone1Options);
        }).catch(function (error) {
        });
    };
    SavingAccountRulesMilestonePage.prototype.archive = function (archiveData, activeData) {
        var archive = [];
        for (var i in archiveData) {
            if (archiveData[i].isactive == activeData) {
                archive.push(archiveData[i]);
            }
        }
        return archive;
    };
    SavingAccountRulesMilestonePage.prototype.getgoalmilestones = function () {
        var _this = this;
        var self = this;
        this.savingAccountRulesMilestoneProvider.getgoalmilestones()
            .then(function (result) {
            self.milestoneOptions.data = _this.archive(result.data, 1);
            self.milestoneOptions = Object.assign({}, self.milestoneOptions);
        }).catch(function (error) {
        });
    };
    SavingAccountRulesMilestonePage.prototype.delete = function (items) {
        console.log("items", items);
        var d = [];
        var self = this;
        console.log("delete product");
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            d.push(item.saving_goal_milestones_id);
        }
        var milestoneId = d.join(',');
        self.savingAccountRulesMilestoneProvider.deleteMilestone(milestoneId).then(function (result) {
            self.getgoalmilestones();
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
        });
    };
    SavingAccountRulesMilestonePage.prototype.activate = function (productsId) {
        var d = [];
        var self = this;
        console.log("delete product");
        for (var _i = 0, productsId_1 = productsId; _i < productsId_1.length; _i++) {
            var item = productsId_1[_i];
            d.push(item.saving_goal_milestones_id);
        }
        var prodcutIds = d.join(',');
        self.savingAccountRulesMilestoneProvider.activateMilestone(prodcutIds).then(function (result) {
            self.getArchivegoalmilestones();
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    return SavingAccountRulesMilestonePage;
}());
SavingAccountRulesMilestonePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-saving-account-rules-milestone',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\saving-account-rules-milestone\saving-account-rules-milestone.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel("Saving Goal Milestones | Consectus")}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n    <ion-segment-button value="activeMilestone">\n\n      {{settingsProvider.getLabel(\'Active\')}}\n\n    </ion-segment-button>\n\n    <ion-segment-button value="archiveMilestone">\n\n      {{settingsProvider.getLabel(\'Archive\')}}\n\n    </ion-segment-button>\n\n  </ion-segment>\n\n\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'activeMilestone\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <table-component [options]="milestoneOptions"></table-component>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid class="table-cell" *ngIf="currentTab == \'archiveMilestone\'">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n            <ion-col>\n\n              <table-component [options]="milestone1Options"></table-component>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\saving-account-rules-milestone\saving-account-rules-milestone.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_saving_account_rules_milestone_saving_account_rules_milestone__["a" /* SavingAccountRulesMilestoneProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], SavingAccountRulesMilestonePage);

//# sourceMappingURL=saving-account-rules-milestone.js.map

/***/ })

});
//# sourceMappingURL=9.js.map