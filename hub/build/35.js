webpackJsonp([35],{

/***/ 561:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppversionPageModule", function() { return AppversionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__appversion__ = __webpack_require__(598);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppversionPageModule = (function () {
    function AppversionPageModule() {
    }
    return AppversionPageModule;
}());
AppversionPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__appversion__["a" /* AppversionPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__appversion__["a" /* AppversionPage */]),
        ],
    })
], AppversionPageModule);

//# sourceMappingURL=appversion.module.js.map

/***/ }),

/***/ 598:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppversionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_appversion_appversion__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_app_setting_app_setting__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var AppversionPage = (function () {
    function AppversionPage(navCtrl, navParams, languageProvider, settingsProvider, alertCtrl, toastCtrl, viewCtrl, appversionProvider, appSettingProvider, cdr, oauthProvider, loadauditProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.languageProvider = languageProvider;
        this.settingsProvider = settingsProvider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.appversionProvider = appversionProvider;
        this.appSettingProvider = appSettingProvider;
        this.cdr = cdr;
        this.oauthProvider = oauthProvider;
        this.loadauditProvider = loadauditProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.verData = {
            devicetype: "",
            version: "",
            appname: "",
            appurl: ""
        };
        this.resultdata = null;
        this.canUpdate = false;
        this.new = false;
        console.log("appdata", navParams.get('appData'));
        this.versionlist = navParams.get('appData');
        var self = this;
        if (!navParams.get('appData')) {
            //add adata
            self.appData = {
                "id": new Date().toISOString,
                "devicetype": null,
                "version": null,
                "appname": null,
                "appurl": null
            };
            self.title = "Create New App Version";
            self.new = true;
        }
        else {
            //View Data
            self.appData = JSON.parse(JSON.stringify(navParams.get('appData')));
            self.appVersionData = self.appData;
            console.log("appdata", this.appData);
            self.title = "View App Version";
        }
    }
    AppversionPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("admin")) {
            return true;
        }
        else {
            return false;
        }
    };
    AppversionPage.prototype.saveVersion = function () {
        var _this = this;
        var self = this;
        if (self.appData.devicetype == "" || self.appData.devicetype == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter device type"));
        }
        else if (self.appData.version == "" || self.appData.version == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter app version"));
        }
        else if (self.appData.appname == "" || self.appData.appname == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter app name"));
        }
        else if (self.appData.appurl == "" || self.appData.appurl == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter app url"));
        }
        else {
            //result used for the response
            var checkVersion = this.versionList(self.appData, self.appVersionData);
            console.log("check version", checkVersion);
            if (!checkVersion) {
                this.errorAlert(self.appData.devicetype.toLowerCase() + "version should be greater than previous version");
            }
            else {
                self.appData = {
                    "id": new Date().getTime().toString(),
                    "devicetype": self.appData.devicetype.toLowerCase(),
                    "version": self.appData.version,
                    "appname": self.appData.appname.toLowerCase(),
                    "appurl": self.appData.appurl.toLowerCase(),
                    "download_customer": self.appData = 0
                };
                this.appSettingProvider.addappversion(JSON.parse(JSON.stringify(self.appData)))
                    .then(function (result) {
                    result.devicetype = "";
                    result.version = "";
                    result.appname = "";
                    result.appurl = "";
                    var toast = self.toastCtrl.create({
                        message: self.settingsProvider.getLabel('Appversion  added successfully'),
                        duration: 3000,
                        position: 'right',
                        cssClass: "success"
                    });
                    toast.present();
                    self.viewCtrl.dismiss();
                });
            }
        }
        var message = "Added " + this.appData.version + "in App Version";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "add",
            "screen": "app-version",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Add Audit Done"),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    //end of tab section 
    AppversionPage.prototype.versionList = function (appObj, verisonData) {
        var i;
        for (i = 0; i < verisonData.length; i++) {
            if (verisonData[i].devicetype == appObj.devicetype && verisonData[i].version == appObj.version) {
                return false;
            }
            else if (verisonData[i].devicetype == appObj.devicetype && verisonData[i].version > appObj.version) {
                return false;
            }
        }
        return true;
    };
    AppversionPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad AppversionPage');
        this.appSettingProvider.getAppData()
            .then(function (result) {
            _this.appVersionData = result;
        });
        var self = this;
        var message = "added " + this.appData.version + " in App Version ";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "add",
            "screen": "app-version",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
        });
    };
    AppversionPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    AppversionPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    return AppversionPage;
}());
AppversionPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-appversion',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\appversion\appversion.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n      <ion-row *ngIf="new">\n\n          <ion-col>\n\n              <ion-toolbar>\n\n                  <ion-buttons end>\n\n                      <button ion-button small round icon-left color="primary" [disabled]="!canUpdate && !new" (click)="saveVersion()">\n\n                          <ion-icon name="checkmark-circle"></ion-icon>\n\n                          {{settingsProvider.getLabel(\'Save Changes\')}}\n\n                      </button>\n\n                  </ion-buttons>\n\n              </ion-toolbar>\n\n          </ion-col>\n\n      </ion-row>\n\n      \n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-card padding>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Device Type\')}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="appData.devicetype" placeholder="{{settingsProvider.getLabel(\'Device Type\')}}" [disabled]="!canUpdate && !new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'App Version\')}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="appData.version" placeholder="{{settingsProvider.getLabel(\'App Version\')}}" [disabled]="!canUpdate && !new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'App Name\')}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="appData.appname" placeholder="{{settingsProvider.getLabel(\'App Name\')}}" [disabled]="!canUpdate && !new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'App Url\')}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="appData.appurl" placeholder="{{settingsProvider.getLabel(\'App Url\')}}" [disabled]="!canUpdate && !new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\appversion\appversion.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_appversion_appversion__["a" /* AppversionProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_app_setting_app_setting__["a" /* AppSettingProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_1__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], AppversionPage);

//# sourceMappingURL=appversion.js.map

/***/ })

});
//# sourceMappingURL=35.js.map