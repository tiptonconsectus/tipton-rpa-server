webpackJsonp([6],{

/***/ 590:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SavingAccoutRulesListPageModule", function() { return SavingAccoutRulesListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__saving_accout_rules_list__ = __webpack_require__(627);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SavingAccoutRulesListPageModule = (function () {
    function SavingAccoutRulesListPageModule() {
    }
    return SavingAccoutRulesListPageModule;
}());
SavingAccoutRulesListPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__saving_accout_rules_list__["a" /* SavingAccoutRulesListPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__saving_accout_rules_list__["a" /* SavingAccoutRulesListPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], SavingAccoutRulesListPageModule);

//# sourceMappingURL=saving-accout-rules-list.module.js.map

/***/ }),

/***/ 627:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavingAccoutRulesListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_saving_account_rules_list_saving_account_rules_list__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SavingAccoutRulesListPage = (function () {
    function SavingAccoutRulesListPage(navCtrl, navParams, languageProvider, savingAccountRulesListProvider, toastCtrl, oauthProvider, settingsProvider, globalProvider, viewCtrl, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.languageProvider = languageProvider;
        this.savingAccountRulesListProvider = savingAccountRulesListProvider;
        this.toastCtrl = toastCtrl;
        this.oauthProvider = oauthProvider;
        this.settingsProvider = settingsProvider;
        this.globalProvider = globalProvider;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.labelName = "";
        this.savingOptions = {
            data: [],
            filteredData: [],
            columns: {
                "accountname": { title: "Account Type", search: true },
                "daymaxtransferinamount": { title: "Daily Transfer In", format: 'function', fn: this.commonFunction.bind(this), search: true },
                "monthmaxtransferinamount": { title: "Monthly Transfer In", format: 'function', fn: this.commonFunction.bind(this), search: true },
                "yearmaxtransferinamount": { title: "Yearly Transfer In", format: 'function', fn: this.commonFunction.bind(this), search: true },
                "daymaxtransferoutamount": { title: "Daily Transfer Out", format: 'function', fn: this.commonFunction.bind(this), search: true },
                "monthmaxtransferoutamount": { title: "Monthly Transfer Out ", format: 'function', fn: this.commonFunction.bind(this), search: true },
                "yearmaxtransferoutamount": { title: "Yearly Transfer Out ", format: 'function', fn: this.commonFunction.bind(this), search: true },
                "istransferin": { title: "Transfer In ", search: true, format: 'function', fn: this.allowFunction.bind(this), col: 1 },
                "istransferout": { title: "Transfer Out", search: true, format: 'function', fn: this.allowFunction.bind(this), col: 1 }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Account Rules",
                addAction: this.create.bind(this),
            },
            tapRow: this.edit.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
        this.savingAccountsOption = {
            data: [],
            filteredData: [],
            columns: {
                "accountname": { title: "Account Type", search: true, format: 'function', fn: this.commonBlankFunction.bind(this) },
                "accountcode": { title: "Account Code", search: true, format: 'function', fn: this.commonBlankFunction.bind(this) },
                "interestrate": { title: "Interest Rate", search: true, format: 'function', fn: this.commonBlankFunction.bind(this) },
                "create_date": { title: "Created Date", format: 'function', fn: this.formatDateTime.bind(this), search: true }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Account Type",
                addAction: this.createAccountType.bind(this),
            },
            tapRow: this.editAccountType.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
        this.resultdata = null;
        this.languageString = "en";
        this.currentTab = "accountRules";
    }
    SavingAccoutRulesListPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("rulesengine")) {
            return true;
        }
        else {
            return false;
        }
    };
    SavingAccoutRulesListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SavingAccoutRulesListPage');
    };
    SavingAccoutRulesListPage.prototype.create = function () {
        this.navCtrl.push("SavingAccountRulesPage");
    };
    SavingAccoutRulesListPage.prototype.edit = function (SavingRulesData) {
        this.navCtrl.push("SavingAccountRulesPage", { accountType: SavingRulesData });
    };
    SavingAccoutRulesListPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        var self = this;
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        // else {
        // }
        if (self.oauthProvider.user_role.rulesengine) {
            self.getAccountTypesName();
            self.savingAccountRulesListProvider.getaccountrules()
                .then(function (result) {
                self.savingOptions.data = result.data;
                self.savingOptions = Object.assign({}, self.savingOptions);
            }).catch(function (error) {
            });
            this.getAccountTypesList();
        }
        else {
            var toast = self.toastCtrl.create({
                message: 'You are not authorised to access this page',
                duration: 4000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.setRoot("DashboardPage");
        }
    };
    SavingAccoutRulesListPage.prototype.formatDateTimeDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            var hh = newdate.getHours();
            var min = newdate.getMinutes().toString().length == 1 ? "0" + newdate.getMinutes().toString() : newdate.getMinutes();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            // newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    SavingAccoutRulesListPage.prototype.formatDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    SavingAccoutRulesListPage.prototype.getAccountTypesList = function () {
        var self = this;
        self.savingAccountRulesListProvider.getAccountTypes().then(function (result) {
            console.log("result accounttype", result);
            self.savingAccountsOption.data = result.data;
            self.savingAccountsOption = Object.assign({}, self.savingAccountsOption);
        }).catch(function (error) {
            console.log("error", error);
        });
    };
    SavingAccoutRulesListPage.prototype.segmentChanged = function (event) {
        console.log("event", event);
    };
    SavingAccoutRulesListPage.prototype.getAccoountType = function () {
    };
    SavingAccoutRulesListPage.prototype.commonFunction = function (data) {
        var str = "";
        if (data == 0 || data == null) {
            str = "No Limit";
        }
        else {
            str = "£ " + data;
        }
        return (str);
    };
    SavingAccoutRulesListPage.prototype.allowFunction = function (data) {
        var str = "";
        if (data == 0) {
            str = '<img class="imgUpload"  style="width:50%; height:50%" src ="assets/member_images/close.jpg" alt="">';
        }
        else {
            str = '<img class="imgUpload"  style="width:50%; height:50%" src ="assets/member_images/1398911.png" alt="">';
        }
        return (str);
    };
    SavingAccoutRulesListPage.prototype.formatDateTime = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            // var hh = newdate.getHours();
            // var min = newdate.getMinutes().toString().length == 1 ? "0" + newdate.getMinutes().toString() : newdate.getMinutes();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            // newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    SavingAccoutRulesListPage.prototype.commonBlankFunction = function (nullData) {
        var str = "";
        if (nullData == 0) {
            str = '--';
        }
        else if (nullData == null) {
            str = '--';
        }
        else if (nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    SavingAccoutRulesListPage.prototype.getAccountTypes = function (data) {
        var ret = "";
        if (data == 1) {
            ret = "Savings Bond";
        }
        else if (data == 2) {
            ret = "Savings JT";
        }
        else {
            ret = "Mortgage";
        }
        return (ret);
    };
    SavingAccoutRulesListPage.prototype.getAccountTypesName = function () {
        var _this = this;
        var self = this;
        self.savingAccountRulesListProvider.getAccountTypes().then(function (result) {
            console.log("result accounttype", result);
            if (result.data !== null) {
                _this.accountTypes = result.data;
            }
        });
    };
    SavingAccoutRulesListPage.prototype.createAccountType = function () {
        this.navCtrl.push("SavingAccountTypePage");
    };
    SavingAccoutRulesListPage.prototype.editAccountType = function (SavingRulesData) {
        this.navCtrl.push("SavingAccountTypePage", { accountType: SavingRulesData });
    };
    return SavingAccoutRulesListPage;
}());
SavingAccoutRulesListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-saving-accout-rules-list',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\saving-accout-rules-list\saving-accout-rules-list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(\'Account Types & Rules | Consectus\')}} </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <ion-row>\n\n    <ion-col>\n\n      <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n        <ion-segment-button value="accountRules">\n\n          {{settingsProvider.getLabel(\'Account Rules\')}}\n\n        </ion-segment-button>\n\n        <ion-segment-button value="accountTypes">\n\n          {{settingsProvider.getLabel(\'Account Type\')}}\n\n        </ion-segment-button>\n\n      </ion-segment>\n\n    </ion-col>\n\n  </ion-row>\n\n  <ion-row *ngIf="currentTab == \'accountRules\'">\n\n    <ion-col>\n\n      <table-component [options]="savingOptions"></table-component>\n\n    </ion-col>\n\n  </ion-row>\n\n  <ion-row *ngIf="currentTab == \'accountTypes\'">\n\n    <ion-col>\n\n      <table-component [options]="savingAccountsOption"></table-component>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\saving-accout-rules-list\saving-accout-rules-list.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_saving_account_rules_list_saving_account_rules_list__["a" /* SavingAccountRulesListProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], SavingAccoutRulesListPage);

//# sourceMappingURL=saving-accout-rules-list.js.map

/***/ })

});
//# sourceMappingURL=6.js.map