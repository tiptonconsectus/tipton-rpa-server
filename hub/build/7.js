webpackJsonp([7],{

/***/ 589:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SavingAccountTypePageModule", function() { return SavingAccountTypePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__saving_account_type__ = __webpack_require__(626);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SavingAccountTypePageModule = (function () {
    function SavingAccountTypePageModule() {
    }
    return SavingAccountTypePageModule;
}());
SavingAccountTypePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__saving_account_type__["a" /* SavingAccountTypePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__saving_account_type__["a" /* SavingAccountTypePage */]),
        ],
    })
], SavingAccountTypePageModule);

//# sourceMappingURL=saving-account-type.module.js.map

/***/ }),

/***/ 626:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavingAccountTypePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_saving_account_rules_list_saving_account_rules_list__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SavingAccountTypePage = (function () {
    function SavingAccountTypePage(navCtrl, languageProvider, navParams, savingAccountRulesListProvider, oauthProvider, alertCtrl, toastCtrl, viewCtrl, loadauditProvider, settingsProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.languageProvider = languageProvider;
        this.navParams = navParams;
        this.savingAccountRulesListProvider = savingAccountRulesListProvider;
        this.oauthProvider = oauthProvider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.loadauditProvider = loadauditProvider;
        this.settingsProvider = settingsProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.resultdata = null;
        this.languageString = "en";
        this.accountType = {
            accountname: "",
            accountcode: "",
            interestrate: 0,
            createdby: ""
        };
        this.canUpdate = false;
        this.new = false;
        this.accountList = this.navParams.get("accountType");
        console.log(this.accountList, "accountList");
        var self = this;
        if (!navParams.get('accountType')) {
            //add adata
            self.accountType = {
                "accountname": null,
                "accountcode": null,
                "interestrate": null
            };
            this.title = "Add Saving Account Type";
            self.new = true;
        }
        else {
            //View Data
            this.title = "Update Saving Account Type";
            self.accountType = JSON.parse(JSON.stringify(navParams.get('accountType')));
            console.log("accountType", this.accountType);
            self.canUpdate = true;
        }
    }
    SavingAccountTypePage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("rulesengine")) {
            return true;
        }
        else {
            return false;
        }
    };
    SavingAccountTypePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SavingAccountTypePage');
        var self = this;
        var message = "Viewed " + this.accountType.accountname + " in  ACcount Type Rules";
        var auditUser = {
            "users_id": 2,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "view",
            "screen": "saving-account-type",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
        });
    };
    SavingAccountTypePage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    SavingAccountTypePage.prototype.addAccount = function () {
        var _this = this;
        var self = this;
        if (self.accountType.accountname == "" || self.accountType.accountname == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter account name"));
        }
        else if (self.accountType.accountcode == "" || self.accountType.accountcode == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter milestone goal target"));
        }
        else {
            self.accountType = {
                "accountname": self.accountType.accountname.toLowerCase(),
                "accountcode": self.accountType.accountcode.toLowerCase()
            };
            this.savingAccountRulesListProvider.addaccounttypes(JSON.parse(JSON.stringify(self.accountType)))
                .then(function (result) {
                result.accountname = "";
                result.accountcode = "";
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel("Account Type added successfully"),
                    duration: 3000,
                    position: 'right',
                    cssClass: "success"
                });
                toast.present();
                self.viewCtrl.dismiss();
            });
        }
    };
    SavingAccountTypePage.prototype.updateAccount = function () {
        var _this = this;
        var self = this;
        if (self.accountType.accountname == "" || self.accountType.accountname == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter Account name"));
        }
        else if (self.accountType.accountcode == "" || self.accountType.accountcode == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter Account code"));
        }
        else {
            console.log(this.accountType, "accountType");
            this.savingAccountRulesListProvider.updateaccounttypes(this.accountType)
                .then(function (data) {
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel("Account Type updated successfully"),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                self.navCtrl.pop();
            });
        }
    };
    SavingAccountTypePage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: this.settingsProvider.getLabel('Ok'),
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    return SavingAccountTypePage;
}());
SavingAccountTypePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-saving-account-type',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\saving-account-type\saving-account-type.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid class="table-cell">\n\n    <ion-row>\n\n      <ion-col col-6>\n\n        <ion-grid class="table-simple">\n\n          <ion-row>\n\n\n\n            <ion-col col-8>\n\n              <ion-label fixed>{{settingsProvider.getLabel("Account Name")}}</ion-label>\n\n              <ion-input [(ngModel)]="accountType.accountname"></ion-input>\n\n            </ion-col>\n\n            <ion-col col-8>\n\n              <ion-label fixed>{{settingsProvider.getLabel("Account Code")}}</ion-label>\n\n              <ion-input [(ngModel)]="accountType.accountcode"></ion-input>\n\n            </ion-col>\n\n            <ion-col col-8>\n\n              <ion-label fixed>{{settingsProvider.getLabel("Interest Rate")}}</ion-label>\n\n              <ion-input [(ngModel)]="accountType.interestrate"></ion-input>\n\n            </ion-col>\n\n            <ion-col col-12>\n\n              <button ion-button small default *ngIf="new" (click)="addAccount()">{{settingsProvider.getLabel("Add")}}</button>\n\n              <button ion-button small default *ngIf="canUpdate" (click)="updateAccount()">{{settingsProvider.getLabel("Update")}}</button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\saving-account-type\saving-account-type.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__providers_saving_account_rules_list_saving_account_rules_list__["a" /* SavingAccountRulesListProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], SavingAccountTypePage);

//# sourceMappingURL=saving-account-type.js.map

/***/ })

});
//# sourceMappingURL=7.js.map