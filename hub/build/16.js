webpackJsonp([16],{

/***/ 580:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewSavingsMilestonePageModule", function() { return NewSavingsMilestonePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__new_savings_milestone__ = __webpack_require__(617);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NewSavingsMilestonePageModule = (function () {
    function NewSavingsMilestonePageModule() {
    }
    return NewSavingsMilestonePageModule;
}());
NewSavingsMilestonePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__new_savings_milestone__["a" /* NewSavingsMilestonePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__new_savings_milestone__["a" /* NewSavingsMilestonePage */]),
        ],
    })
], NewSavingsMilestonePageModule);

//# sourceMappingURL=new-savings-milestone.module.js.map

/***/ }),

/***/ 617:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewSavingsMilestonePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_saving_account_rules_milestone_saving_account_rules_milestone__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NewSavingsMilestonePage = (function () {
    function NewSavingsMilestonePage(navCtrl, navParams, oauthProvider, languageProvider, alertCtrl, toastCtrl, viewCtrl, savingAccountRulesMilestoneProvider, loadauditProvider, settingsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.savingAccountRulesMilestoneProvider = savingAccountRulesMilestoneProvider;
        this.loadauditProvider = loadauditProvider;
        this.settingsProvider = settingsProvider;
        this.new = false;
        this.canUpdate = false;
        this.resultdata = null;
        this.languageString = "en";
        this.savingData = {
            saving_goal_milestones_id: "",
            milestonename: "",
            goaltarget: "",
            modified_by: "",
            isactive: ""
        };
        this.milestoneList = navParams.get("savingData");
        var self = this;
        if (!navParams.get('savingData')) {
            //add adata
            self.savingData = {
                "milestonename": null,
                "goaltarget": null,
            };
            this.title = "Add New Milestone";
            self.new = true;
        }
        else {
            //View Data
            this.title = "Update New Milestone";
            self.savingData = JSON.parse(JSON.stringify(navParams.get('savingData')));
            self.milestoneData = self.savingData;
            console.log("savingData", this.savingData);
            self.canUpdate = true;
        }
    }
    NewSavingsMilestonePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewSavingsMilestonePage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        var self = this;
        var message = "Viewed " + this.savingData.milestonename + " in  New Savings Milestone";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "view",
            "screen": "new-savings-milestone",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
        });
    };
    NewSavingsMilestonePage.prototype.moveSlideBar = function () {
    };
    NewSavingsMilestonePage.prototype.SaveMilestone = function () {
        var _this = this;
        var self = this;
        if (self.savingData.milestonename == "" || self.savingData.milestonename == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter milestone name"));
        }
        else if (self.savingData.goaltarget == "" || self.savingData.goaltarget == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter milestone goal target"));
        }
        else {
            self.savingData = {
                "milestonename": self.savingData.milestonename.toLowerCase(),
                "goaltarget": self.savingData.goaltarget,
            };
            this.savingAccountRulesMilestoneProvider.addgoalmilestone(JSON.parse(JSON.stringify(self.savingData)))
                .then(function (result) {
                result.milestonename = "";
                result.goaltarget = "";
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel("MileStone added successfully"),
                    duration: 3000,
                    position: 'right',
                    cssClass: "success"
                });
                toast.present();
                self.viewCtrl.dismiss();
            });
        }
        var message = "added " + this.savingData.milestonename + "in New Savings Milestone";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "add",
            "screen": "new-savings-milestone",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Add Audit Done"),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    NewSavingsMilestonePage.prototype.UpdateMilestone = function () {
        var _this = this;
        var self = this;
        if (self.savingData.milestonename == "" || self.savingData.milestonename == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter milestone name"));
        }
        else if (self.savingData.goaltarget == "" || self.savingData.goaltarget == null) {
            this.errorAlert(this.settingsProvider.getLabel("Please enter milestone goal target"));
        }
        else {
            console.log(this.savingData, "thissavingData");
            this.savingAccountRulesMilestoneProvider.updategoalmilestone(this.savingData)
                .then(function (data) {
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel("MileStone updated successfully"),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                self.navCtrl.pop();
            });
        }
        var message = "Updated " + this.savingData.milestonename + " in New Savings Milestone";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "update",
            "screen": "new-savings-milestone",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Updated Audit Done"),
                duration: 10000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    NewSavingsMilestonePage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: this.settingsProvider.getLabel('Ok'),
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    return NewSavingsMilestonePage;
}());
NewSavingsMilestonePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-new-savings-milestone',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\new-savings-milestone\new-savings-milestone.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <ion-grid>\n\n      <ion-row  *ngIf="new">\n\n          <ion-col>\n\n              <ion-toolbar>\n\n                  <ion-buttons end>\n\n                      <button ion-button small round icon-left color="primary"  (click)="SaveMilestone()">\n\n                          <ion-icon name="checkmark-circle"></ion-icon>\n\n                          {{settingsProvider.getLabel(\'Save Changes\')}}\n\n                      </button>\n\n                  </ion-buttons>\n\n              </ion-toolbar>\n\n          </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row  *ngIf="canUpdate">\n\n          <ion-col>\n\n              <ion-toolbar>\n\n                  <ion-buttons end>\n\n                      <button ion-button small round icon-left color="primary" (click)="UpdateMilestone()">\n\n                          <ion-icon name="checkmark-circle"></ion-icon>\n\n                          {{settingsProvider.getLabel(\'Save Changes\')}}\n\n                      </button>\n\n                  </ion-buttons>\n\n              </ion-toolbar>\n\n          </ion-col>\n\n      </ion-row>\n\n\n\n\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-card padding>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Milestone Name\')}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="savingData.milestonename" placeholder="{{settingsProvider.getLabel(\'Milestone Name\')}}"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel(\'Percentage of Milestone Compeleted\')}}</ion-label>\n\n              <ion-range min="0" max="100" pin="true" snaps="true" step="5" [(ngModel)]="savingData.goaltarget" (ionChange)="moveSlideBar()">\n\n                <ion-label range-left>0</ion-label>\n\n                <ion-label range-right>100</ion-label>\n\n              </ion-range>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\new-savings-milestone\new-savings-milestone.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_saving_account_rules_milestone_saving_account_rules_milestone__["a" /* SavingAccountRulesMilestoneProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */]])
], NewSavingsMilestonePage);

//# sourceMappingURL=new-savings-milestone.js.map

/***/ })

});
//# sourceMappingURL=16.js.map