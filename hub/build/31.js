webpackJsonp([31],{

/***/ 564:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComplaintDetailsPageModule", function() { return ComplaintDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__complaint_details__ = __webpack_require__(601);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ComplaintDetailsPageModule = (function () {
    function ComplaintDetailsPageModule() {
    }
    return ComplaintDetailsPageModule;
}());
ComplaintDetailsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__complaint_details__["a" /* ComplaintDetailsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__complaint_details__["a" /* ComplaintDetailsPage */]),
        ],
    })
], ComplaintDetailsPageModule);

//# sourceMappingURL=complaint-details.module.js.map

/***/ }),

/***/ 601:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComplaintDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_customers_customers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ComplaintDetailsPage = (function () {
    function ComplaintDetailsPage(navCtrl, navParams, customersProvider, toastCtrl, languageProvider, settingsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.customersProvider = customersProvider;
        this.toastCtrl = toastCtrl;
        this.languageProvider = languageProvider;
        this.settingsProvider = settingsProvider;
        this.complaintData = {
            customer_complaint_id: "",
            subject: "",
            description: "",
            resolvedfeedback: "",
            create_date: "",
        };
        this.languageString = "en";
        this.resultdata = null;
        this.new = false;
        this.canUpdate = false;
        var self = this;
        if (!navParams.get('complaint')) {
            //add adata
            self.complaintData = {
                "customer_complaint_id": null,
                "subject": null,
                "description": null,
                "resolvedfeedback": null,
                "create_date": null,
            };
            self.title = "Update Transaction";
            self.new = true;
        }
        else {
            //View Data
            self.complaintData = JSON.parse(JSON.stringify(navParams.get('complaint')));
            self.complaintData = self.complaintData;
            console.log("complaintData", this.complaintData);
            self.canUpdate = true;
            self.title = "View Transaction";
        }
    }
    ComplaintDetailsPage.prototype.complaintResolved = function () {
        var _this = this;
        var self = this;
        var resolvedcomplaint = {
            "customer_complaint_id": self.complaintData.customer_complaint_id,
            "resolvedfeedback": self.complaintData.resolvedfeedback,
            "isresolved": 1
        };
        this.customersProvider.complaintResolved(resolvedcomplaint)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Complaint resolved Successfully"),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.pop();
        });
    };
    ComplaintDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ComplaintDetailsPage');
    };
    return ComplaintDetailsPage;
}());
ComplaintDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-complaint-details',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\complaint-details\complaint-details.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel(\'Complaint Details\')}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-list>\n\n          <ion-item>\n\n            <ion-label stacked>{{settingsProvider.getLabel(\'Subject\')}}</ion-label>\n\n            <ion-input type="text" [(ngModel)]="complaintData.subject" placeholder="{{settingsProvider.getLabel(\'Subject\')}}"></ion-input>\n\n          </ion-item>\n\n\n\n          <ion-item>\n\n            <ion-label stacked>{{settingsProvider.getLabel(\'Description\')}}</ion-label>\n\n            <ion-input type="text" [(ngModel)]="complaintData.description" placeholder="{{settingsProvider.getLabel(\'Description\')}}"></ion-input>\n\n          </ion-item>\n\n\n\n          <ion-item>\n\n            <ion-label stacked>{{settingsProvider.getLabel(\'Feedback\')}}</ion-label>\n\n            <ion-input type="text" [(ngModel)]="complaintData.resolvedfeedback" placeholder="{{settingsProvider.getLabel(\'Feedback\')}}"></ion-input>\n\n          </ion-item>\n\n\n\n          <ion-item *ngIf="complaintData.isresolved == 0">\n\n            <button small ion-button (click)="complaintResolved()">{{settingsProvider.getLabel(\'Resolved\')}}</button>\n\n          </ion-item>\n\n        </ion-list>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\complaint-details\complaint-details.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_customers_customers__["a" /* CustomersProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__["a" /* SettingsProvider */]])
], ComplaintDetailsPage);

//# sourceMappingURL=complaint-details.js.map

/***/ })

});
//# sourceMappingURL=31.js.map