webpackJsonp([19],{

/***/ 577:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessagesPageModule", function() { return MessagesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_table_table_module__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__messages__ = __webpack_require__(614);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MessagesPageModule = (function () {
    function MessagesPageModule() {
    }
    return MessagesPageModule;
}());
MessagesPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__messages__["a" /* MessagesPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__messages__["a" /* MessagesPage */]),
            __WEBPACK_IMPORTED_MODULE_2__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], MessagesPageModule);

//# sourceMappingURL=messages.module.js.map

/***/ }),

/***/ 614:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_table_table__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_oauth_oauth__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MessagesPage = (function () {
    function MessagesPage(navCtrl, languageProvider, settingsProvider, toastCtrl, navParams, globalProvider, oauthProvider, viewCtrl) {
        this.navCtrl = navCtrl;
        this.languageProvider = languageProvider;
        this.settingsProvider = settingsProvider;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.globalProvider = globalProvider;
        this.oauthProvider = oauthProvider;
        this.viewCtrl = viewCtrl;
        this.resultdata = null;
        this.languageString = "en";
        this.contactSubjectOptions = {
            data: [],
            filteredData: [],
            columns: {
                "customersid": { title: "Consectus ID", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "memberid": { title: "Member ID", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "subject": { title: "Subject", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "description": { title: "Message", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "emailid": { title: "Email Id", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "create_date": { title: "Date", search: true, format: 'function', fn: this.formatDateTime.bind(this) },
                "complaint_status": { title: "Status", format: 'function', search: true, fn: this.commonFunction.bind(this) },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.deleteContact.bind(this),
                    color: 'danger'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {},
            tapRow: this.editContactSubject.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
    }
    MessagesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MessagesPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    MessagesPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        this.loadContactsData();
    };
    MessagesPage.prototype.editContactSubject = function (contactMessageData) {
        console.log("contactMessageData", contactMessageData);
        this.navCtrl.push("ContactUsMessagePage", { contactUsMessage: contactMessageData });
    };
    // loadContactsData() {
    //   let self = this;
    //   this.settingsProvider.getcustomercomplaint()
    //     .then((result: any) => {
    //       console.log(result, "result");
    //       self.contactSubjectOptions.data = result.data;
    //       if (this.navParams.get("msg") !== undefined) {
    //         self.contactSubjectOptions = Object.assign({}, self.contactSubjectOptions);
    //       } else if (this.navParams.get("issend") !== undefined) {
    //         self.contactSubjectOptions = Object.assign({}, self.contactSubjectOptions);
    //       } else {
    //         self.contactSubjectOptions = Object.assign({}, self.contactSubjectOptions);
    //       }
    //       self.nullData = result.data;
    //       console.log(this.nullData, "nullDataData");
    //     }).catch(error => {
    //     })
    // }
    MessagesPage.prototype.loadContactsData = function () {
        var _this = this;
        var self = this;
        this.settingsProvider.getcustomercomplaint()
            .then(function (result) {
            console.log(result, "result");
            if (_this.navParams.get("msg") !== undefined) {
                self.contactSubjectOptions.data = result.data;
                self.contactSubjectOptions = Object.assign({}, self.contactSubjectOptions);
            }
            else if (_this.navParams.get("issend") !== undefined) {
                self.contactSubjectOptions = Object.assign({}, self.contactSubjectOptions);
            }
            else {
                self.contactSubjectOptions.data = result.data;
                self.contactSubjectOptions = Object.assign({}, self.contactSubjectOptions);
            }
        }).catch(function (error) {
        });
    };
    MessagesPage.prototype.formatDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    MessagesPage.prototype.deleteContact = function (contactusdata) {
        var _this = this;
        console.log(contactusdata);
        var d = [];
        var self = this;
        for (var _i = 0, contactusdata_1 = contactusdata; _i < contactusdata_1.length; _i++) {
            var contact = contactusdata_1[_i];
            d.push(this.settingsProvider.deleteContact(contact));
        }
        Promise.all(d)
            .then(function () {
            var toast = self.toastCtrl.create({
                message: contactusdata.length == 1 ? _this.settingsProvider.getLabel('Subject was Deleted successfully') : contactusdata.length + _this.settingsProvider.getLabel(' Subjects were Deleted successfully'),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
            self.loadContactsData();
        });
    };
    MessagesPage.prototype.commonFunctionisresolved = function (data) {
        var str = "";
        if (data == 0 || data == null) {
            str = 'Not Resolved';
        }
        else {
            str = 'Resolved';
        }
        return (str);
    };
    MessagesPage.prototype.commonFunction = function (nullData) {
        // console.log(this.nullData, "nullDataData");
        var str = "";
        if (nullData == "" || nullData == null || nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
    };
    MessagesPage.prototype.formatDateTime = function (date) {
        var str = "";
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            // newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
        else {
            str = '--';
            return (str);
        }
    };
    return MessagesPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_5__components_table_table__["a" /* TableComponent */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__components_table_table__["a" /* TableComponent */])
], MessagesPage.prototype, "table", void 0);
MessagesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])({ name: "Messages" }),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-messages',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\messages\messages.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel("Contact Us Messages | Consectus")}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-row>\n\n    <ion-col>\n\n      <ion-grid class="table-simple">\n\n        <ion-row class="header">\n\n          <ion-col>\n\n            {{settingsProvider.getLabel(\'Messages\')}}\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <table-component [options]="contactSubjectOptions"></table-component>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\messages\messages.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
], MessagesPage);

//# sourceMappingURL=messages.js.map

/***/ })

});
//# sourceMappingURL=19.js.map